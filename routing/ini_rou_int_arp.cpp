#include "../config/mnode.h"
#include "../config/node.h"
#include <boost/regex.hpp>
#include "interfaces.h"
#include "../arp/arp.h"
#include "route.h"
#include <rte_byteorder.h>

#include "../system_call/cli.h"//for system call 



using namespace boost;
using namespace std;



extern uint8_t G_first_config;






////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

//LOG Interfaces; ARP; Phy_interfaces


/////////////////////////////////////////////////////////////////


int init_log_phy_int_arp (node *config_tree,
					 ARP_TABLE &arp_table,
					vector<LogInt> &log_int_table,
					vector<PhyInt>  &phy_int_table,
					list<LoRo> &lo_ro_list
						)

	{

// meanse that size of phy_int_table is fixed. Zaranee!!!
//means that config_tree is filed before

int ret; 
node* phy_int_config=find("interface",config_tree);

if (phy_int_config==NULL) 
	{
	config_tree->err_gen("No configuration of Interfaces!!!");return 1;
}

if (phy_int_config->child_node==NULL) 
	{
	phy_int_config->err_gen("No configuration of Interfaces!!!");return 1;
}


arp_table.arp_arr.resize(255*1000); //1000 subunits with /24 mask

uint16_t arp_table_size=0;

phy_int_table.resize(Phy_Int_Count+1);
log_int_table.resize(0);

uint32_t log_int_counter=log_int_table.size();
					// should be zero
				// if would be else log_intrefaces all things will be fine too.
				 //counter of logical interfaces
				// how mactch logical interfaces are?

for (int i = 0; i< phy_int_table.size(); i++)
{
       PhyInt* phy_int = &(phy_int_table[i]);
        phy_int->phy_int=i;
	phy_int->erase_vlans();
        memcpy(phy_int->mac_addr,G_mac_ints+6*i,6); //set mac address
        if (i < Phy_Int_Count)
            phy_int->name = std::string("xe-") + std::to_string(i);
        else
            phy_int->name = std::string("kni-0");
}

for ( node *phy_int_c=phy_int_config->child_node; phy_int_c != NULL; phy_int_c=phy_int_c->next_node)
//for each phy_int
	{
	//get number from his its name
	smatch m;

	uint8_t is_xe=1;//
	
	string shad_phy_int;
	
	uint8_t curr_int_type=0;

	regex_search(phy_int_c->name,m,regex("\\d+"));
	if (*m.begin()=="")
		{ 
		phy_int_c->err_gen("No number part of interface!!!");return 1;
	}
	uint32_t n_phy_int=stoi(*m.begin());

	if (!(n_phy_int<Phy_Int_Count)) continue;

	regex_search(phy_int_c->name,m,regex("\\w+"));
	string s_type_of_phy_int=*m.begin();

	
	uint8_t is_correct_intf_caption=0;
	if (s_type_of_phy_int=="kni" )
		{
		if (n_phy_int!=0)
			{
			phy_int_c->err_gen("We only supporting kni-0 interface!!!");return 1;
		}
		n_phy_int=Phy_Int_Count;
		is_correct_intf_caption=1;
		is_xe=0;
	}
	
	

	if (s_type_of_phy_int=="xe")
		{
		is_correct_intf_caption=1;

	}
	if (is_correct_intf_caption==0)
		{
		phy_int_c->err_gen("Interfaces type"+s_type_of_phy_int+"is not valid!!!");return 1;

	}

	//check status of interface is it clients port or not
	node* client_node=find("QinQ_IPoE_clients",phy_int_c);





	curr_int_type=0;
	if ((client_node!=NULL)&&(client_node->value=="enable")) curr_int_type=1;

	

		//clients port could not changing during one launch time
	if (curr_int_type!=int_types[n_phy_int])
		{
		phy_int_c->err_gen("Ability to Process QinQ_IPoE_clients by port couldn't change dynamicly ");\
		return 1;
	}
	

	PhyInt* phy_int=&(phy_int_table[n_phy_int]);
	phy_int->phy_int=n_phy_int;
	phy_int->erase_vlans();

	memcpy(phy_int->mac_addr,G_mac_ints+6*n_phy_int,6); //set mac address
	phy_int->name=phy_int_c->name;



	node* unit_node=find("unit",phy_int_c);

	node *log_int_c=(unit_node!=NULL)?unit_node->child_node:NULL;

	node *desc_n;
	if ((desc_n=find("description",phy_int_c))!=NULL) phy_int->desc="\""+desc_n->value+"\"";
	else phy_int->desc="\"\"";

	

	for ( ; log_int_c != NULL; log_int_c=log_int_c->next_node)
		{//for each log_int
		LogInt log_int;

		log_int.name=phy_int->name+"."+log_int_c->name;
		
		if (s2numb(log_int_c->name)<0) 
			{
			log_int_c->err_gen("name should be value from 0 �� 65535");
			return 1;
		}

		log_int.phy_int=n_phy_int;
		string vlan_c;
	
		node* vlan_n=find("vlan",log_int_c);
		vlan_c=(vlan_n==NULL) ? "" : vlan_n->value;

		if ((desc_n=find("description",log_int_c))!=NULL) log_int.desc="\""+desc_n->value+"\"";
		else log_int.desc="\"\"";

		
		
		
		if (vlan_c=="")
			{
			log_int.vlan=4096;//case without vlan
		}
		else {

			smatch m;
			regex_search(vlan_c,m,boost::regex("^\\d+$"));
			vlan_c=*m.begin();
			if (vlan_c=="")
				{
				vlan_n->err_gen("That vlan value" + vlan_c + "is invalid!");return 1;
			}
			log_int.vlan=stoi(vlan_c);
			if (log_int.vlan>4095)
				{
				vlan_n->err_gen("That vlan value" + vlan_c + "is invalid!");return 1;
			}
			



		}
		if (phy_int->Vlan_log_int_table[log_int.vlan]!=0xFFFF)
			{
			log_int_c->err_gen("vlan of that unit has alreay defined for current physical interface");return 1;
		}
		phy_int->Vlan_log_int_table[log_int.vlan]=log_int_counter;

		node* ipv4_n=find("ipv4",log_int_c);
		if (ipv4_n==NULL) 
			{
			log_int_c->err_gen("ipv4 address should be!!!");return 1;

		}
		string net_s=ipv4_n->value;
		
		if (s2Netw(net_s,log_int.net)>0)
			{
			log_int_c->err_gen("ipv4 address " + net_s + "is wrong");return 1;
		}
		
		if (log_int.net.mask<20) 
			{

			log_int_c->err_gen("the mask should be longer than /19");return 1;

		}
			
		// write MASK as bit pattern for simplisity

		log_int.mask=~((1<<(32-log_int.net.mask))-1); // needed for check

		//config shadow loginterface ip address

		//--------------------------------------load system_ip_address------------>>>
		node* sys_ip_n=find("system_ip_address",log_int_c);

		if ((is_xe==1)&&(sys_ip_n!=NULL))
			{
			log_int_c->err_gen("system_ip_address is only valid for kni-0 interface");
			return 1;
		}

		uint32_t sys_ip;

		log_int.sys_ip_add=0;

		if (sys_ip_n!=NULL)//should configure ip address of system part
			{
			if (s2ip(sys_ip_n->value,sys_ip)>0)
				{
				sys_ip_n->err_gen("ipv4 address " + sys_ip_n->value + "has wrong format");
				return 1;
			}
			//check ip address of kni unit and system ip is in one metwork		
			if ((log_int.net.ip&log_int.mask)!=(sys_ip&log_int.mask))
				{
				sys_ip_n->err_gen("should be in the same subnet as unit of kni-0 interface");
				return 1;
			}	
			if (log_int.net.ip==sys_ip)
				{
				sys_ip_n->err_gen("should be differ from ip address of it's unit's ip address");
				return 1;
			}
		
			log_int.sys_ip_add=sys_ip;
			
		}
		//--------------------------------------load system_ip_address-------------|||


		if (is_xe==1) //only for xe interface
			{
			


		}
			
		///////////
		//needed to find size of arp block for that interface 

		// let's do it very simple not accurate
		// as size_arp_block =2^(32-mask); include broadcast
		// it will be wery simple for /31 mask
		log_int.arp_index=arp_table_size;
		arp_table_size+=1<<(32-log_int.net.mask);
		uint32_t index_our_mac=(log_int.net.ip-(log_int.net.ip&log_int.mask));//needed for check
		for (int i=log_int.arp_index;i<arp_table_size;i++)
			{
			arp_table.arp_arr[i].vlan=rte_bswap16(log_int.vlan);
			arp_table.arp_arr[i].physical_int_index=n_phy_int;
			arp_table.arp_arr[i].timer_n=-1;//means no timer set
			arp_table.arp_arr[i].log_int_index=log_int_counter;
			arp_table.arp_arr[i].is_ours=0;//this is not our mac address
			arp_table.arp_arr[i].state=0;
			arp_table.arp_arr[i].is_ok=0;
			arp_table.arp_arr[i].is_static_route=0;
		}
		ARPe* curr_arp=&(arp_table.arp_arr[index_our_mac+log_int.arp_index]);
		curr_arp->is_ours=1;//this is our mac address
		curr_arp->state=2;
		curr_arp->is_ok=1;

		memcpy(curr_arp->mac_addr,phy_int->mac_addr,6);		

		log_int_counter++;
		log_int_table.push_back(log_int);
//////////////////////////////////////////////////////////////////
		
/// LOCAL ROUTE

//////////////////////////////////
	//	Local Route from that local interface
		LoRo lo_ro;

		lo_ro.net=log_int.net;
		lo_ro.type=0;//local route

		lo_ro.local_next.push_back(ARPm(log_int.arp_index,10,1,0));
		lo_ro_list.push_back(lo_ro);
	}
 //   phy_int_table[n_phy_int]=phy_int;
}

arp_table.arp_arr.resize(arp_table_size);

return 0;
}



////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
//STATIC ROUTES
//
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

int init_st_routes (node *config_tree,
					list<StRo> &st_ro_list
						)

	{


node* st_config=find("routing-options",config_tree);

if (st_config==NULL)
	{
	return 0; // means no routing options
}


st_config=find("static",st_config);

if (st_config==NULL)
	{
	return 0; // means no static routes
}


for ( node *st_ro_c=st_config->child_node; st_ro_c != NULL; st_ro_c=st_ro_c->next_node)
//for each phy_int
	{
	//get number from his its name
		StRo st_ro;

		if (s2Netw(st_ro_c->name,st_ro.net)>0)
			{
			st_ro_c->err_gen("Static route value" + st_ro_c->name + "is invalid!");return 1;
		}

		st_ro.type=1; /// not local

	node* next_hops_n=find("next-hop",st_ro_c);
		if (next_hops_n==NULL)
			{
			st_ro_c->err_gen("No next-hop!!!");return 1;

		}
		if (next_hops_n->child_node==NULL)
			{
			st_ro_c->err_gen("No next-hop!!!");return 1;
		}
	for ( node *nh_c=next_hops_n->child_node; nh_c != NULL; nh_c=nh_c->next_node)
		{//for each next_hop
		NeHo nh; // next hop
		if (s2ip(nh_c->name,nh.ip)>0)
			{
			nh_c->err_gen("IP address"+nh_c->name+"is wrong!!!");return 1;
		}

		nh.m=5;//default value
//========================================================
		node * metric_c=find("metric",nh_c);
		if (metric_c!=NULL) 
			{
			int nh_m=s2numb(metric_c->value);
			if (nh_m<0)	{
				nh_c->err_gen("Metric value"+metric_c->value+"is wrong!!!");return 1;
			}
			nh.m=nh_m;
		}
//======================================================
		node * width_c=find("width",nh_c);
		
		nh.width=1;//default walue
		if (width_c!=NULL) 
			{
			int nh_width=s2numb(width_c->value);
			if ((nh_width<0)||(nh_width>0xFF))
				{
				nh_c->err_gen("Width values should be from 1 to 255");
				return 1;
			}
			nh.width=nh_width;
		}



		st_ro.next_hop_list.push_back(nh);

	}
st_ro.next_hop_list.sort();

st_ro_list.push_back(st_ro);
}

return 0;
}


/////////////////////////////////////////////////////////////////

int set_client_type_of_ports (node *config_tree)
	{
smatch m;
int ret; 
node* phy_int_config=find("interface",config_tree);

if (phy_int_config==NULL) 
	{
	config_tree->err_gen("No configuration of Interfaces!!!");return 1;
}

if (phy_int_config->child_node==NULL) 
	{
	phy_int_config->err_gen("No configuration of Interfaces!!!");return 1;
}

for ( node *phy_int_c=phy_int_config->child_node; phy_int_c != NULL; phy_int_c=phy_int_c->next_node)
//for each phy_int
	{


	//check status of interface is it clients port or not
	node* client_node=find("QinQ_IPoE_clients",phy_int_c);

	regex_search(phy_int_c->name,m,regex("\\d+"));
	if (*m.begin()=="")
		{ 
		phy_int_c->err_gen("No number part of interface!!!");return 1;
	}
	uint32_t n_phy_int=stoi(*m.begin());

	regex_search(phy_int_c->name,m,regex("\\w+"));
	string s_type_of_phy_int=*m.begin();

	
	uint8_t is_correct_intf_caption=0;
	if (s_type_of_phy_int=="kni" )
		{
		if (n_phy_int!=0)
			{
			phy_int_c->err_gen("We only supporting kni-0 interface!!!");return 1;
		}
		n_phy_int=Phy_Int_Count;
		is_correct_intf_caption=1;
	}
	
	if (!(n_phy_int<Phy_Int_Count+1)) continue;


	if (s_type_of_phy_int=="xe")
		{
		is_correct_intf_caption=1;

	}
	if (is_correct_intf_caption==0)
		{
		phy_int_c->err_gen("Interfaces type"+s_type_of_phy_int+"is not valid!!!");return 1;

	}




	uint8_t curr_int_type=0;
	if ((client_node!=NULL)&&(client_node->value=="enable")) curr_int_type=1;

	
	int_types[n_phy_int]=curr_int_type;

}

return 0;
}

///-------------------------------------------------------------------------------------------------------

int close_shad_int()
 
{
        for (int i = 0; i < Phy_Int_Count + 1; i++)
        {

            //-----------------------configure shadow interface switch on----->>>>>

            string phy_int_name = (i == Phy_Int_Count) ? ("kni-0") : ("xe-" + std::to_string(i)); //kni interface

            string shad_phy_int_name = "br_" + phy_int_name;


            //sleep(5);	

            run_system_call("ifconfig " + shad_phy_int_name + " down &");
        }

}


//delete not used shadows of subinterface


int refresh_shad_units(vector<PhyInt>  &pint_t, //physicla interface table
		 	vector<PhyInt>  &old_pint_t,
			vector<LogInt>  &lint_t//logical interface table current
			)
	{
for (int i=0;i<Phy_Int_Count+1;i++)
	{

	//-----------------------configure shadow interface switch on----->>>>>
	
	string phy_int_name= (i==Phy_Int_Count)?("kni-0"):("xe-"+std::to_string(i));//kni interface

	string shad_phy_int_name="br_"+phy_int_name;


	//sleep(5);	

	run_system_call("ifconfig " +shad_phy_int_name+" up &");
	
	char mac_s[6*2+1];//string of mac address
	
	for (int i1=0;i1<6;i1++)	
		{
		sprintf(mac_s+2*i1,"%02x",pint_t[i].mac_addr[i1]);				
	}
		
	//set mac address
	if (i<Phy_Int_Count) run_system_call("ifconfig "+shad_phy_int_name+" hw ether "+std::string(mac_s));
	
	//-----------------------configure shadow interface switch on-----|||||||||


	for (int vl=0;vl<4096+1;vl++) // vlan intex
		{
		string shad_log_int_name=shad_phy_int_name;

		if (pint_t[i].Vlan_log_int_table[vl]!=0xFFFF)
			{
			//--------------------------------------------------------------switch on log shad_interface--->>		
			if (vl<4096)
				{ //unit with vlan
				//-------config shadow subinterface-------------->>	
				shad_log_int_name+="."+std::to_string(vl);
				run_system_call("vconfig add "+shad_phy_int_name+" "+std::to_string(vl));
				run_system_call("ifconfig "+shad_log_int_name+" up");
				//-------config shadow subinterface--------------||
			}
			//--------------------------------------------------------------switch on log shad_interface---||
		
			LogInt log_int=lint_t[pint_t[i].Vlan_log_int_table[vl]];
		
			//--------------------------------------------------------------configure ip on log shad_interface or KNI--->>
			

			if ((i==Phy_Int_Count)&&(log_int.sys_ip_add!=0))
				{
				run_system_call("ifconfig "+shad_log_int_name+" "+ip2s(log_int.sys_ip_add)+"/"+std::to_string(log_int.net.mask));	
			}	
			else
				{
				run_system_call("ifconfig "+shad_log_int_name+" "+Netw2s(log_int.net));
			}		
			//--------------------------------------------------------------configure ip on log shad_interface or KNI ---||
		}

		//---delete old shadow interfaces---------------------------->>
		if ((old_pint_t[i].Vlan_log_int_table[vl]!=0xFFFF)&&(pint_t[i].Vlan_log_int_table[vl]==0xFFFF))
			{
			//should delete vlan subinterface
			string pint_s=(i!=Phy_Int_Count)?("br_xe-"+std::to_string(i)):"br_kni-0";//physocal interface string(name)

			int a=0;
			if (vl<4096)
				{
				run_system_call("vconfig rem "+pint_s+"."+std::to_string(vl));
			}
			else
				{		
				run_system_call("ifconfig "+pint_s+" 0.0.0.0");

			}

		}
		//---delete old shadow interfaces----------------------------||

	}
}


return 0;
}


