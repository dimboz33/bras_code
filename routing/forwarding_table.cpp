#include <cstddef>
#include <list>
#include "forwarding_table.h"
#include "../arp/arp.h"
#include "route.h"

void  FoRo::Add_FoPo(list<FoPo> &fp_list)
	{
	FoPo fp_str;//start
	FoPo fp_end;//end
	fp_str.ip= this->net.ip; // ip should be the smalest address in the net
	uint32_t MASK=0;
	if (this->net.mask>0)

		{
		 MASK=1<<(32-this->net.mask);
	}
	fp_str.ip=(fp_str.ip)&(~(MASK-1));

	fp_end.ip=fp_str.ip+MASK;//the next point after that net

//in the case 0/0 all points will be the same 0.

	fp_str.act_up=&this->my_act;
	fp_end.act_dn=&this->my_act;

	fp_str.act_dn=NULL;
	fp_end.act_up=NULL;

	fp_str.fo_ro=this;
	fp_end.fo_ro=this;
	
	fp_list.push_back(fp_str);
	this->po_str=--fp_list.end();
		
	

	if (fp_end.ip>0 ) // if fp_end is not greate 0xFF.FF.FF.FF. 0/0 or 1/1
		{
		fp_list.push_back(fp_end);
		
		this->po_end=--fp_list.end();
		
	}
	else {
		this->po_end=fp_list.end();///??????
	}
	
	return;
}

void ForTabTre::Refresh_routes(int flag)
	{
	for (list<FoRo>::iterator fr=this->froutes.begin();fr!=this->froutes.end();fr++)
		{
int a=1;
		fr->Refresh(flag);
a++;
	}
}

void FoRo::Refresh(int flag)
	{
	uint8_t am_nh=0;
                for (int i=0;(i<8)&&(am_nh==0);i++)
                        {
                        if (this->ro_prot[i]==NULL) continue;
                        am_nh=this->ro_prot[i]->Refresh(this,flag);
                
                }
		if (am_nh==0) {	
		 	if (this->par_act!=NULL) {
				this->my_act=*this->par_act;
			}
			else {

				/*
				this->my_act.arp_ind[0]=0xFFFFFFFF;

				for(int k=0;k<8;k++)
				this->my_act.arp_f_t[k]=0xFFFFFFFF;
				*/
			}
		}

		else this->my_act.ip_base=this->ip_base;

return;
}

//----------------------------------------------------------------------------------

void ForTabTre::free_routes()
{
	this->froutes.clear();
	return;
}

//----------------------------------------------------------------------------------

bool	Comp_by_mask_FoRo(FoRo &a,FoRo &b) //
//returns true if the first argument goes before the second argument
// list sort from smaller to bigger???

{

	if (a.net.mask==b.net.mask)
		{
	return a.net.ip<b.net.ip; 
	}
return a.net.mask<b.net.mask; // there bigger mask is more important
}

//----------------------------------------------------------------------------------

bool Comp_FoPo (FoPo &a, FoPo &b)
{

if (a.ip==b.ip) return a.fo_ro->net.mask<b.fo_ro->net.mask;

return a.ip<b.ip;
}
//----------------------------------------------------------------------------------

void ForTabTre::Generate_Tree(int flag)
	{
	
	this->fo_po_list.clear();

	this->froutes.sort(Comp_by_mask_FoRo);

	//---------------------------------------------------------------------------merding different protocol--->>
	for (list<FoRo>::iterator fr=this->froutes.begin();fr!= this->froutes.end();)
		{
		list<FoRo>::iterator fr_next=fr; fr_next++;
		if (fr_next==this->froutes.end()) break;

		if (fr_next->net==fr->net)
			{

			//should merg this route

			for (int i=0;i<8;i++)
				{
				if ((fr->ro_prot[i]==NULL)&&(fr_next->ro_prot[i]))
					{
					fr->ro_prot[i]=fr_next->ro_prot[i];
				}

			}
				
			this->froutes.erase(fr_next);


		}
		else
			{
			++fr;

		}
		
		
	}
	//---------------------------------------------------------------------------merding different protocol==||


	//------------------------------------------------ADD dummy route in empty route case
	if (this->froutes.begin()==this->froutes.end()) 
		{
		//should add 0/0->null route
		this->froutes.push_back(FoRo()); //one default forwarding route

	}

	

	for (list<FoRo>::iterator fr=this->froutes.begin();fr!= this->froutes.end(); ++fr)
		{
		fr->Add_FoPo(this->fo_po_list);
	}
	

this->fo_po_list.sort(Comp_FoPo);

	for (list<FoRo>::iterator fr=this->froutes.begin();fr!= this->froutes.end(); ++fr)
		{
		
		list<FoPo>::iterator fp=fr->po_str;
		list<FoPo>::iterator fp_be=fp; fp_be--;//fp_before;
		if (fp==this->fo_po_list.begin())
			{
			 fp->act_up=&fr->my_act;
			fp++;
		
		}
		else
			 {
	
			if (fp->ip==fp_be->ip)  // parent start interval is the sane as us.
				{
				fr->par_act=fp_be->act_up;
				fp_be->act_up=&fr->my_act;
	
				fr->po_str=fp_be;

				fp=fo_po_list.erase(fp_be);//the next forwarding point after start
				}
			else
				{
				fr->par_act=fp_be->act_up;
				fp->act_dn=fp_be->act_up;
				fp->act_up=&fr->my_act;
				fp++; // the next forwarding point after start

			}
		}
		for(;(fp->ip!=fr->po_end->ip)&&(fp!=this->fo_po_list.end());++fp)
			{// .ip is prinsiple becase there may be to points end of interval 
				//and the start of another. with smaler mask;
				// we shouldn't change act.up for start of bigger interval 
			
			//in the middle of oure net

			fp->act_up=&fr->my_act;
			fp->act_dn=&fr->my_act;
		}

		if (fp->ip==fr->po_end->ip)
			{
			if (fp!=fr->po_end) // 
				//WE FIND bound of net with smaller mask.	
				{
				fp->act_dn=&fr->my_act;
				fr->po_end=fp;
				this->fo_po_list.erase(++fp);// delete end of out range becase of coinsidense	
			}
			else 
				{
			// this is out end of interval
				fp->act_dn=&fr->my_act;
			}
		}
				 				

	}

	//all routes generate they points. Delete equal points. Fill correct actions. And 
	//fr.par_nh too.

//now we should count all FoPo 

this->fo_po_am=0;
this->Refresh_routes(flag);

for (list<FoPo>::iterator fp=this->fo_po_list.begin();fp!= this->fo_po_list.end(); ++fp)
	{
	if (fp->ip>0 )	this->fo_po_am++; // to exckude 0 bound
}

// 

if (this->fo_po_arr!=NULL) delete(this->fo_po_arr);

this->fo_po_arr= new FoPo [this->fo_po_am]; 

//copying
uint32_t i=0;
for (list<FoPo>::iterator fp=this->fo_po_list.begin();fp!= this->fo_po_list.end(); ++fp)
	{
	if (fp->ip>0 )	
		{
		this->fo_po_arr[i++]=*fp; // to exckude 0 bound	
	}

}


}

ForTabTre::~ForTabTre() 
{

delete(this->fo_po_arr);

}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/////////////////ACTUAL FORWARDING
///////////////////////////////////////////////


ACT* ForTabTre::get_act_for_ip (uint32_t ip) 
{
	
	
	if (ip<this->fo_po_arr[0].ip) 	return fo_po_arr[0].act_dn;
	
	if (!(ip<this->fo_po_arr[this->fo_po_am-1].ip)) return fo_po_arr[this->fo_po_am-1].act_up;
	
//now dihotomia is get started
int a=0;
int b=this->fo_po_am-1;
int c=(a+b)>>1;

	for (;a<b-1;c=(a+b)>>1)
		{
		if (this->fo_po_arr[c].ip>ip)
			{
			b=c;
			
		}
		else
			{
			a=c;
		}
	
	}
	
	if (this->fo_po_arr[c].ip>ip) 
		{
		return this->fo_po_arr[c].act_dn;
	}
	else 
		{
		return this->fo_po_arr[c].act_up;
	}

}

uint32_t ForTabTre::get_arp_index_for_act_ip (ACT* act, uint32_t ip,uint8_t hash) // get index of arp entry of that ip and next_hop
	{
	if (act==NULL) 
		{
		return 0xFFFFFFFF;
	}

	if (act->type>0) 
		{
			if (act->am_nh==0) return 0xFFFFFFFF;

			uint8_t first=0;
			uint8_t last=act->am_nh;
			uint8_t mid=first;
			
			while(last>first+1)
				{

				mid=(last+first)>>1;
				if (hash<act->arp_w[mid-1])	last=mid;
				else				first=mid;
			}

			return act->arp_ind[first]; // this is not local route -> arp is written in next-hop 63=64-1

	}
	else
		{ // this is local route

		        return act->arp_ind[0]+(ip-act->ip_base);// 
			// arp entries of local network lies one-by-one.


	} 	

}


uint32_t ForTabTre::get_arp_index_for_ip (uint32_t ip,uint8_t hash)
	{
ACT* act;

//if (this->fo_po_am==0) return 0xFFFFFFFF;
act=this->get_act_for_ip (ip);

return this->get_arp_index_for_act_ip(act,ip,hash);

}


