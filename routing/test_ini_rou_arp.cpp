#include "../config/mnode.h"
#include "../config/node.h"

#include "interfaces.h"
#include "arp.h"
#include "route.h"
#include "ini_rou_int_arp.h"
#include "routes_resolving.h"

uint8_t int_states[2]; // states of interfaces
vector<ARPe> arp_table;// arp table

vector<LogInt> log_int_table;
vector<PhyInt>  phy_int_table;
list <LoRo> lo_ro_list;


int main ()
 	{
string s;

Mnode model_tree;
node config_tree;

ifstream file_model;
ifstream file_config;

for (int i=0;i<2;i++)
	{
int_states[i]=1;
}

file_model.open("model_tree_config", ifstream::in);
s=next_good_str(file_model);
model_tree=Mnode(s,file_model);
file_model.close();



file_config.open("config",ifstream::in);
s=next_good_str(file_config);
config_tree=node(s,file_config,&model_tree,1);
file_config.close();

int a;


phy_int_table.resize(2); // only two interfaces 
//xe-0 and xe-1


a= init_log_phy_int_arp (&config_tree,
			arp_table,
			log_int_table,
			phy_int_table,
			lo_ro_list);


list<StRo> st_ro_list;
list<StRo> active_st_ro_list;

a= init_st_routes (&config_tree,
				 st_ro_list);

for (int i=0; i<log_int_table.size();i++)
	{
LogInt LOG_INT_TEST;
	LOG_INT_TEST=log_int_table[i];
}
for (list <LoRo>::iterator ro=lo_ro_list.begin(); ro!=lo_ro_list.end();ro++)
	{
LoRo Ro_TEST;
	Ro_TEST=*ro;
	for (list<ARPm>::iterator nh=ro->local_next.begin();
		nh!=ro->local_next.end();nh++)
		{
		ARPm nh1;
		nh1=*nh;	
	}
}
a= activate_static_routes(st_ro_list,lo_ro_list,active_st_ro_list);

for (list<StRo>::iterator ro=active_st_ro_list.begin(); ro!=active_st_ro_list.end();ro++)
        {
	StRo Ro_TEST;
        Ro_TEST=*ro;
        for (list<ARPm>::iterator nh=ro->local_next.begin();
                nh!=ro->local_next.end();nh++)
                {
                ARPm nh1;
                nh1=*nh;
        }
}
//--------------------

ForTabTre forw_table;

forw_table.free_routes();

forw_table.add_routes(lo_ro_list);
forw_table.add_routes(active_st_ro_list);

forw_table.Generate_Tree ();


a=2;
return 0;
}
