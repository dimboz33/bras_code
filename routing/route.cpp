#include "route.h"
#include "forwarding_table.h"
#include "interfaces.h"

extern uint8_t G_config_bit;

extern uint8_t  Phy_Int_Count;

uint8_t ROUTE::Refresh(FoRo* fr,int flag) //return number of nex-hops  //flag=1 we should see at interface state
	{ // FoRo originate Route:Refresh. This is very important
	// arpm should be oredered before from big metric to small;

	 ARPe *arp_table=&G_arp_table->arp_arr[0];// arp table
	uint8_t nh_index=0;// nh counter

	uint16_t curr_m=0; // current metric

	uint32_t total_width=0;
	uint16_t arp_width[16];

	for ( list<ARPm>::iterator a=this->local_next.begin();(a!=this->local_next.end())&&(nh_index<16);++a)
		{

		if ((flag>0)&&(int_states[arp_table[a->arpe].physical_int_index]==0)) continue;
/*		
		if (this->type==1)	arp_table[a->arpe].is_static_route==1;
		else 			arp_table[a->arpe].is_static_route==0;	
*/		
		if (flag>=0)
			{
			if ((this->type==1)&&(arp_table[a->arpe].is_ok!=1)) continue;//arp is not actual
		}
		if ((nh_index==0)||((curr_m==a->m)&&(nh_index>0)))
			{
			curr_m=a->m;
			fr->my_act.arp_ind[nh_index]=a->arpe;

			arp_width[nh_index]=total_width+a->width; //upper bound
			fr->my_act.arp_real_w[nh_index]=arp_width[nh_index];//for show route command
			total_width+=a->width;

			nh_index++;
		}
		//now config is not correctly made for unequal load balancing
			


	}

	//ip_base added by forwarding route

//---should generate arp_f_t forwarding table

int k=0;
for (int i=0;i<nh_index;i++)
	{
	fr->my_act.arp_w[i]=(uint32_t(arp_width[i])<<8)/total_width;
}
	fr->my_act.am_nh=nh_index;
	fr->my_act.type=this->type;

return nh_index;
}
//-----------------------------------

bool	Comp_by_mask_StRo(StRo &a,StRo &b) //
{

	if (a.net.mask==b.net.mask)
		{
		return a.net.ip<b.net.ip; 
	}
return a.net.mask<b.net.mask; // there bigger mask is more important
}
//------------------------------------------------------------------------------------

//----------------------------------------------------------------

uint8_t s2ip (string &ip_s1, uint32_t &ip) //string to Netw
{
	
		boost::smatch m;
		string ip_s;		
		boost::regex_search(ip_s1,m,boost::regex("^(\\d+)(\\.\\d+){0,3}"));
		ip_s=*m.begin();
		if (ip_s=="") return 1;

		boost::sregex_iterator xIt(ip_s.begin(),ip_s.end(),boost::regex("\\d+"));
		boost::sregex_iterator xInvalidIt;
		ip=0;
		int i=0;
		for (;i<4;i++)
			{
			ip=ip<<8;
			if (xIt!=xInvalidIt)
				{
				//string sss=(*(xIt)).str();
				uint16_t oct=stoi((*(xIt++)).str());
				if (!(oct<0x100)) return 1;
				ip+=oct;
			}
		}
		
return 0;
}

//------------------------------------------------------------------

uint8_t s2Netw (string &net_s, Netw &net) //string to Netw
{

		boost::smatch m;
		string ip_s;
		string mask_s;

		boost::regex_search(net_s,m,boost::regex("^(\\d+)(\\.\\d+){0,3}/[1-3]?[0-9]"));
		ip_s=*m.begin();
		if (ip_s=="") return 1;
		
		if (s2ip(net_s,net.ip)!=0)
			{
			return 1;//means error
		}
		
		//mask

		boost::regex_search(net_s,m,boost::regex("/\\d+$"));

		mask_s=*m.begin();

		mask_s=mask_s.substr(1,mask_s.size()); // exclude / element
		
		net.mask=stoi(mask_s);
		if (net.mask>32) return 1;	

return 0;
}

//--------------------------------------------------------------------------

string ip2s(uint32_t &ip)
	{
	string s;

	uint8_t* b=(uint8_t*)(&ip);
	
	s=	std::to_string(b[3])+"."+std::to_string(b[2])+"."+std::to_string(b[1])+"."+std::to_string(b[0]);
	return s;
}
//-------------------------------------------------------
string Netw2s(Netw &netw)
	{
	string s;

	s=ip2s(netw.ip)+"/"+to_string(netw.mask);
	return s;
}


//--------------------------------------------------
int s2numb(string &num_s) //string to Numb
	{

	boost::smatch m;
		
	if (!boost::regex_search(num_s,m,boost::regex("^\\d+$")))  return -1; //error

	num_s=*m.begin();
	return stoi(num_s);
return 0;
}

//--------------------------------------------------------
int s2phy_int(string &s)
	{

	if (s=="kni-0") return Phy_Int_Count;

	for (int i=0;i<Phy_Int_Count;i++)
		{
		if (s=="xe-"+std::to_string(i)) return i;
	}
	
	return -1;
}
//-------------------------------------------------------------
LogInt* s2log_int(std::string &s)
	{
	int curr_cb=G_config_bit;

	for (int i1=0;i1<log_int_table[curr_cb].size();i1++)
		{
		LogInt* log_int=&(log_int_table[curr_cb][0]) + i1;

		if (s==log_int->name) return log_int;
	}

	return NULL;

}
int s2log_int(std::string &s,uint8_t curr_cb)
	{
	for (int i1=0;i1<log_int_table[curr_cb].size();i1++)
		{
		LogInt* log_int=&(log_int_table[curr_cb][0]) + i1;

		if (s==log_int->name)
			{
			return i1;
		}
	}
	


	return -1;
}


//-------------------------------------------------------------

string mac2s(uint8_t* mac)
	{
	char mac_s[6*2+1+6];//string of mac address

	for (int i1=0;i1<6;i1++)	
		{
		sprintf(mac_s+3*i1,"%02x",mac[i1]);				
		sprintf(mac_s+3*i1+2,":");				
	}
	mac_s[6*2+1+6-2]=' ';

	return std::string(mac_s);

}

