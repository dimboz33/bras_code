#include <list>
#include <inttypes.h> // for uint32_t
#include "../arp/arp.h"
#include <boost/regex.hpp>
#include <cstddef> //fore NULL

#ifndef ROUTE_TYPES


#define ROUTE_TYPES


using namespace std;



//ip - ip address
// m = metric



///////////////////////////////////
//-----------------------------NEXT-HOP
///////////////////////////////////

struct NeHo {
	uint32_t ip; //network address
	uint32_t m;// information about best prefic match is written in another part of uint32_t метрик

	uint8_t width;//width of next-hop among other nex-hops in uneqial load balancing

bool operator < ( const NeHo& r) 
	{
	if (this->ip==r.ip) return this->m<r.m;
	return this->ip<r.ip;
}
bool operator > ( const NeHo& r) 
	{
	if (this->ip==r.ip) return this->m>r.m;
	return this->ip>r.ip;
}
bool operator == ( const NeHo& r) 
	{
	return (this->ip==r.ip)&&(this->m==r.m);
}


};





///////////////////////////////////
//---------------------------NETWORK
///////////////////////////////////

struct Netw {
	uint32_t ip; //network address
	uint8_t  mask; //network mask 0 - 32 


	bool operator==(const Netw &A)
		{

		return (A.ip==this->ip)&&(A.mask==this->mask);
	}
	bool operator<(const Netw &A)
		{
		if (A.mask==this->mask) return A.ip<this->ip;
		return (A.mask<this->mask);
	}
	bool operator>(const Netw &A)
		{
		if (A.mask==this->mask) return A.ip>this->ip;
		return (A.mask>this->mask);
	}
		
};



/// This is not actually ARP structure, just pointer to ARP entry with metric and routing protocol 
//index
struct ARPm {
	uint32_t arpe; // index of arp entry
	uint32_t m; // metric value

	uint8_t width;

	uint8_t prot; // as distanse in cisco 
			// and is used to arp match correct
			// onered by type from ROUTE object
	ARPm(uint32_t arp_index,uint32_t metric)
		{
		this->arpe=arp_index;
		this->m=metric;
		this->prot=-1;
		this->width=1;
	}
	ARPm(uint32_t arp_index,uint32_t metric,uint8_t width1,uint8_t protocol)
		{
		this->arpe=arp_index;
		this->m=metric;
		this->width=width1;
		this->prot=protocol;
		
	}

	ARPm()
		{
		this->arpe=-1;
		this->m=-1 ;
		this->prot=-1;
	}

};

////////////////////



class FoRo; // Will be declared in another file forwarding_table.h

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////// ROUTE general pparent class
class ROUTE {
public: 
	Netw net;
	ROUTE ** ref2Fr_ref; // reference to reference to himself as 
			//Forwarding Route 
			//IF we want to delete route we must delete 
			// that value by this referense to null.

	list<ARPm> local_next; // local_next_hops arp entries
// in case of local route only one element of list of arp entries with metric

	
	uint8_t type ; // type of route  0 - local route 1 static
	// it is important for calculation arp for next hop

uint8_t Refresh(FoRo* fr,int flag);


bool operator< (const ROUTE &A)	
	{
	return this->net<A.net;
}
bool operator> (const ROUTE &A)	
	{
	return this->net>A.net;
}
};





//////////////////////////////////////
//----------------------------LOCAL ROUTE
/////////////////////////////////////
class LoRo : public ROUTE { // local route type 

};

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\





	//		STATIC ROUTES


////////////////
/////////////////////////////////////////////////////////



//-----------------------------------------------static_route_T



class StRo : public ROUTE { // static route type 
	
public:	
	list <NeHo> next_hop_list; // ip addresses of next hops readed from config with metric 

};



bool	Comp_by_mask_StRo(StRo &a,StRo &b); //
//returns true if the first argument goes before the second argument
// list sort from smaller to bigger???



//--------------------------------------------------------------------


uint8_t s2Netw (string &net_s, Netw &netw);
uint8_t s2ip (string &ip_s1,uint32_t &ip);
string Netw2s (Netw &netw);
string ip2s (uint32_t &ip);
int s2numb(string &num_s);
class LogInt;

LogInt* s2log_int(string &s);
int s2log_int(string &s,uint8_t curr_cb); // current config bit
int s2phy_int(string &s);
string mac2s(uint8_t* mac);

#ifndef MAIN_FILE

extern list <LoRo> lo_ro_list;
extern list<StRo> st_ro_list,old_st_ro_list;
extern list<StRo> active_st_ro_list;

#else

list <LoRo> lo_ro_list;
list<StRo> st_ro_list,old_st_ro_list;
list<StRo> active_st_ro_list;


#endif

#endif

