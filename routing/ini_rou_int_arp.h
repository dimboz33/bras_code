#include "../config/mnode.h"
#include "../config/node.h"

#include "interfaces.h"
#include "../arp/arp.h"
#include "route.h"


using namespace boost;
using namespace std;



int init_log_phy_int_arp (node *config_tree,
					ARP_TABLE &arp_table,
					vector<LogInt> &log_int_table,
					vector<PhyInt>  &phy_int_table,
					list<LoRo> &lo_ro_list
						);


int init_st_routes (node *config_tree,
					list<StRo> &st_ro_list
	
					);

int set_client_type_of_ports (node *config_tree);


int refresh_shad_units(vector<PhyInt>  &pint_t, //physicla interface table
		 	vector<PhyInt>  &old_pint_t,
			vector<LogInt>  &lint_t//logical interface table current
			);

int close_shad_int();






