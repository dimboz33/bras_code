#include <list>
#include "route.h"
#include "../config/node.h"
#include "../config/mnode.h"
#include "interfaces.h"
#include "forwarding_table.h"

#include "../system_call/cli.h"//for system call


int is_addr_match_route (uint32_t net_add,Netw  network)
{
// >> is work very stange. 
	if (network.mask>0)
		{
		if  ((network.ip>>(32-network.mask))==(net_add>>(32-network.mask)))
			{
			return 0;
		}
		else
			{
			return 1;

		}
	}
	else 
		{
		return 0;
	}
	
}



bool comp_arpm(ARPm &a1,ARPm &a2)

        {
        if ( a1.m==a2.m)
                {
                return a1.arpe<a2.arpe;
        }

                return (a1.m<a2.m);
}



int activate_static_routes (list<StRo> &static_routes,
			list<LoRo> &local_routes,
			list<StRo> &st_ref_local
			 ) // make next-hops to be local_next-hops poined to the local interfaces
 	{
	
	
/////////----------------------

	//list<StRo*> st_ref_local ;// static routes that refere to local routes from they config
ForTabTre local_forw_table; // forwarding table that consist of local routes only


local_forw_table.free_routes();

local_forw_table.add_routes(local_routes);

local_forw_table.Generate_Tree (0); //don't look at interface current state



for (list<StRo>::iterator st_r=static_routes.begin(); st_r != static_routes.end(); ++st_r)  
	{	

	StRo ac_st=*st_r; // active static route
///matcihng of local routes !!!!!!!!!!!!!!!!

	uint8_t is_ref_to_local=0;
/// for seaching ARP entries of local_next_hops ip, we need to generate arp table with 
//only local routes. And then match static local_next_hops among local routes



		//check if next hop of route matches local routes
		for (list<NeHo>::iterator nt_hop=st_r->next_hop_list.begin(); nt_hop != st_r->next_hop_list.end(); ++nt_hop)
		   		
			{
	
			int arp_index;
                
	                arp_index=local_forw_table.get_arp_index_for_ip(nt_hop->ip,0);

			if (arp_index==0xFFFFFFFF) continue;	// mean matching is ok
				
			ARPe* curr_arp=&(G_arp_table->arp_arr[arp_index]);

			if (curr_arp->is_ours==1) continue;

			curr_arp->is_static_route=1;


			Generate_ARP_Request(curr_arp);
			ac_st.local_next.push_back(ARPm(arp_index,nt_hop->m,nt_hop->width,1));//fill local_next_hop
										// 1 means static protocol
			is_ref_to_local=1;
				
		}
	if (is_ref_to_local==1)
		{
		st_ref_local.push_back(ac_st);
	}		

//matching of static routes!!!!!!!!!!!!!!!!!!

	
}


//now we need to sort local next-hops for each static route 

for (list<StRo>::iterator st_r=st_ref_local.begin(); st_r !=st_ref_local.end();st_r++)
	{
	
	st_r->local_next.sort(comp_arpm);//sorting of local_next;

}


return 0;

}

//-------------------------------------------------------------------------------

int refresh_system_static_routes(list<StRo> 	&st_ro_list,
				 list<StRo> &old_st_ro_list)
	{
	//should add new routes and delete old one's
	//----------------------------------------------
	st_ro_list.sort();


		//---add_new_routes------
	
	for (list<StRo>::iterator b=st_ro_list.begin();b!=st_ro_list.end();b++)
		{
		for (list<NeHo>::iterator b_nh=b->next_hop_list.begin();b_nh!=b->next_hop_list.end();b_nh++)
			{
			
			run_system_call("ip route add " + Netw2s(b->net)+"  via "+
					ip2s(b_nh->ip)+ "  metric "+ 
					std::to_string(b_nh->m+100));
			

		}
	}	

		//----------------------------------------------


	//---delete_old_routes------
	


	list<StRo>::iterator b=st_ro_list.begin();

	
	for (list<StRo>::iterator a=old_st_ro_list.begin();(a!=old_st_ro_list.end());)
		{

		if ((b==st_ro_list.end())||(a->net<b->net))
			{

			//that old route is not prolongated
			//should delete it next hops
			for (list<NeHo>::iterator nh=a->next_hop_list.begin();nh!=a->next_hop_list.end();nh++)
				{
			
				run_system_call("ip route del " +Netw2s(a->net)+"  via "+
					ip2s(nh->ip)+ "  metric "+ 
					std::to_string(nh->m+100));

			

			}
			a++;continue;
		}
		

		if (a->net>b->net)
			{
			b++;
			continue;
			
		}
		if (a->net==b->net)
			{
			//route a still exists in new config 

			//should check next-hops 

               	    	list<NeHo>::iterator b_nh=b->next_hop_list.begin();
		
			for (list<NeHo>::iterator a_nh=a->next_hop_list.begin();a_nh!=a->next_hop_list.end();)
				{
				if (b_nh==b->next_hop_list.end()||*a_nh<*b_nh)
					{
					
			//should delete that next hop
					run_system_call("ip route del " +Netw2s(a->net)+" via "+
					ip2s(a_nh->ip)+ "  metric "+ 
					std::to_string(a_nh->m+100));
			
					a_nh++;continue;

				}

				if (*a_nh>*b_nh)
					{
					b_nh++; continue;
				}
				if (*a_nh==*b_nh)
					{
					//next_hop is already exist
					a_nh++;b_nh++; continue;
				}
				
	
			}			

			b++;a++;
			continue;
		}
	
	}
	
	old_st_ro_list=st_ro_list;

	return 0;
}