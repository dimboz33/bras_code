
#include <list>
#include "route.h"
#include "../config/node.h"
#include "../config/mnode.h"
#include "interfaces.h"
#include "forwarding_table.h"

int activate_static_routes (list<StRo> &static_routes,
			list<LoRo> &local_routes,
			list<StRo> &active_static_routes
			 ); // make next-hops to be local_next-hops poined to the local interfaces
 	
int refresh_system_static_routes(list<StRo> 	&st_ro_list,
				 list<StRo> &old_st_ro_list);