
#ifndef INTERFACES
#define INTERFACES


#include "route.h"
#include <string>///???

struct Counters {
	::uint64_t byte;
	::uint64_t pack;
};

struct PhyInt;





class LogInt 
 ///--logical interface
{
public:
	uint8_t  phy_int;//PhyInt number
	uint16_t vlan;//0xffff without vlan case
	/// needed to include arp class
	Netw net; //network of that logical interface
	uint32_t mask;	
	uint16_t arp_index;	//first element in arp block for that log_int
	uint32_t sys_ip_add;	//system ip address
				//arp string in arp table wich elements belongs to that interface
	//Counters counters;
	uint16_t name;

};



class PhyInt  ///--logical interface
{
public:
	uint16_t Vlan_log_int_table[4097];//to determinte log_int_by vlan
	uint8_t  phy_int;//PhyInt number
	
	//firewall will be later

	uint8_t mac_addr[6];
	//Counters counters; 

	PhyInt()
		{
		erase_vlans();
	}

void erase_vlans()
		{
		for (int i=0;i<4097;i++)
			{
			this->Vlan_log_int_table[i]=0xFFFF; // means nothing value set
		}
	}

};

#ifndef MAIN_FILE

extern struct PhyInt* G_phy_ints;
extern struct LogInt* G_log_ints;

extern LogInt* G_log_ints_arr[2];
extern PhyInt* G_phy_ints_arr[2];

extern  uint8_t const PPhy_Int_Count;

extern vector<LogInt> log_int_table[2];  
extern vector<PhyInt>  phy_int_table[2];

extern Counters G_log_int_counters_in  [][4098];
extern Counters G_log_int_counters_out [][4098];


#endif



#endif




