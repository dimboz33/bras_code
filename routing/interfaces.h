
#ifndef INTERFACES
#define INTERFACES


#include "route.h"
#include <string>///???

struct Counters {
	::uint64_t byte;
	::uint64_t pack;
};

struct PhyInt;





class LogInt 
 ///--logical interface
{
public:
	uint8_t  phy_int;//PhyInt number
	uint16_t vlan;//0xffff without vlan case
	/// needed to include arp class
	Netw net; //network of that logical interface
	uint32_t mask;	
	uint16_t arp_index;	//first element in arp block for that log_int
	uint32_t sys_ip_add;	//system ip address
				//arp string in arp table wich elements belongs to that interface
	//Counters counters;
	std::string name;
	std::string desc;//description
	

};



class PhyInt  ///--logical interface
{
public:
	uint16_t Vlan_log_int_table[4097];//to determinte log_int_by vlan
	uint8_t  phy_int;//PhyInt number
	
	//firewall will be later

	uint8_t mac_addr[6];
	//Counters counters; 

	std::string desc;//description
	std::string name;
	PhyInt()
		{
		erase_vlans();
	}

void erase_vlans()
		{
		for (int i=0;i<4097;i++)
			{
			this->Vlan_log_int_table[i]=0xFFFF; // means nothing value set
		}
	}

};

#ifndef MAIN_FILE

extern struct PhyInt* G_phy_ints;
extern struct LogInt* G_log_ints;

extern LogInt* G_log_ints_arr[2];
extern PhyInt* G_phy_ints_arr[2];

extern  uint8_t const PPhy_Int_Count;

extern vector<LogInt> log_int_table[2];  
extern vector<PhyInt>  phy_int_table[2];

extern Counters G_log_int_counters_in  [][4098];
extern Counters G_log_int_counters_out [][4098];

extern uint8_t  Phy_Int_Count;


extern uint8_t int_states[];
extern uint8_t int_types[];

extern uint8_t G_mac_ints[]; 

#else

uint8_t const PPhy_Int_Count=16; // maximum number of (phy + 1 kni)interfaces
//numeration in config should be lineary xe-0 xe-1 xe-2..xe-15; kni-0

Counters G_log_int_counters_in[PPhy_Int_Count][4098]={0};
Counters G_log_int_counters_out[PPhy_Int_Count][4098]={0};

uint8_t  Phy_Int_Count=PPhy_Int_Count;

uint8_t int_states[PPhy_Int_Count+1]={0}; // states of interfaces
uint8_t int_types[PPhy_Int_Count+1]={0}; // 0-simple ; 1 -clients


uint8_t G_mac_ints[6*(PPhy_Int_Count+1)]; 

//-----------------------------------------REAL DATA very easy to change size---->>>
vector<LogInt> log_int_table[2];  
vector<PhyInt>  phy_int_table[2]; 
//-------------------------------------------------------------------------------|||


LogInt *G_log_ints_arr[2];    //--------------------for PFE module
PhyInt *G_phy_ints_arr[2];    //--------------------for PFE module

LogInt *G_log_ints;	    //----------------------for RE module
PhyInt *G_phy_ints;         //----------------------for RE module


#endif


#endif




