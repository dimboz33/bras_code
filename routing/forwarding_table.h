
#include "route.h"
#include <list>
#include <cstddef>//for NULL


#ifndef FORWARDING_TABLE
#define FORWARDING_TABLE


struct FoPo;
////////////////////////////////////////////////////
/////////////////////// forwarding Next HOP
///////////////////////////////////////////////////
///////////////////////////////////////////////////
/// one element was originated by only one protocol
/// type of wich is written in filed "type" 



// в первом приближении делаем один Local Next hop на маршрут



struct ACT {

//DON"T shange fileds//PRODUCTIVITY depends on int's length and sequence/ That is my guesswork

uint8_t  am_nh; //total amount of next-hop

uint32_t arp_ind[16];// indexes in ARP table 
			// each index is next hop

uint8_t arp_w[16];// arp weigth or width //THAT VALUE 64 is VERY GOOD FOR PRODUCTIVITY!!!!!!!!!!!!///change from 16_t to 8_t
			

uint8_t arp_real_w[16];//!!!!!!!!!!!!!!!!!!!!!!!!!may degrade productivity for show route command

uint8_t type; //0 local; 1 -static// needed to fill correcly

uint32_t ip_base;	// ip address of start of ip range  of net, that originate that FoPo;
			// that information is needed for local routes



ACT()
	{
	this->am_nh=0;
	this->arp_ind[0]=0xFFFFFFFF;
}


};




struct FoRo //Forwarding Route //only number of action ; Anothe table of actions should be

 {
	Netw net;
	uint32_t ip_base;
	ACT my_act; // my next-hop
	ACT *par_act; // parents route next-hop/.,motb bbv  eok 

	uint8_t is_active_parent;//may changing speed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!new element

	ROUTE* ro_prot[8];// each protocol can generate that route
	// and in case of interface down we need to 
	// run through all protocols
	// 0 - local route
	// 1 - static route		
				 		

	list<FoPo>::iterator po_str; // start of intervaln 
	list<FoPo>::iterator po_end; // end of interval 


	void Add_FoPo (list<FoPo> &fp_list); // to the list of forwarding points
	void Refresh(int flag=1);//1- we look at interface states

FoRo()
	{
	ip_base=0;
	
	net.ip=1; //because 0 point is not included in forwarding database
	net.mask=32;
	par_act=NULL;

	for (int i=0;i<8;i++)
		{
		ro_prot[i]=NULL;
	}
}

 };	



struct FoPo //Forwrading Point
{
	uint32_t ip;

	ACT* act_up;// > or = upper

	ACT* act_dn;// < lower

	FoRo *fo_ro; // that borne that for point 

	FoPo()
		{
		act_up=NULL;
		act_dn=NULL;
	}

	

};



class ForTabTre // routing table in fast algorithm form
{
public:

	list<FoRo> froutes;
	
	FoPo* fo_po_arr; // forwarding points array
	uint16_t fo_po_am; // forwarding point amount
	
	list<FoPo> fo_po_list;
	

	template < typename RoT_list>
	void	add_routes(RoT_list &Routes1);


	void 	free_routes();
	void	Generate_Tree (int flag=1);
	void 	Refresh_routes(int flag=1);
	ForTabTre()
		{
	fo_po_arr=NULL;
	}
	~ForTabTre();

//------------------------------------------------------------------------+
									//|
ACT* get_act_for_ip (uint32_t ip) ;  				//|
									//|
uint32_t get_arp_index_for_act_ip (ACT* act, uint32_t ip,uint8_t hash);//|
 // get index of arp entry of that ip and next_hop			//|
      									//|
//------------------------------------------------------------------------+
      // ||||||||
uint32_t get_arp_index_for_ip (uint32_t ip,uint8_t hash);



};

//template//

template < typename RoT_list>
void ForTabTre::add_routes (RoT_list &Routes1)
	{
	FoRo fr;
	for (typename RoT_list::iterator ro=Routes1.begin();ro != Routes1.end(); ++ro)
		{
		//fr.net=ro->net;
		//for (int i=0;i<8;i++) -->>by constructor initialization
		//	{
		// fr.ro_prot[i]=NULL;
		//}
		
		fr.ro_prot[ro->type]=&(*ro);
		fr.ip_base= (ro->net.mask>0)? (ro->net.ip)&(~((1<<(32-ro->net.mask))-1)):0;

		fr.net.ip=fr.ip_base;
		fr.net.mask=ro->net.mask;

		this->froutes.push_back(fr);
	}
return;
}
#ifndef MAIN_FILE

extern ForTabTre* G_for_table;
extern ForTabTre* G_for_table_arr[2] ; //---------------------------------------------------CHANGED

extern ForTabTre* G_for_int_table; 		//forwarding table of interface routes
extern ForTabTre* G_for_int_table_arr[2] ; 	//forwarding table of interface routes
#else
ForTabTre* G_for_table_arr[2]={NULL};//--------------------for PFE module
ForTabTre* G_for_int_table_arr[2]={NULL};

ForTabTre* G_for_table;
ForTabTre* G_for_int_table; //forwarding table of interface routes


#endif

#endif
