
#define CLIENTS_CPP
//#include <clients.h>
#include <arpa/inet.h> // for change bit order
#include <inttypes.h> // for uint32_t
#include <rte_fbk_hash.h> // experiment with hash include
#include "../clients/clients.h"
#include "../filter/policer.h"
#include "../filter/service.h"
#include "../radius/rad_client.h"

#include "../timers/timers_array.h"

#include "clients_const.h"
#include <boost/regex.hpp>

#include "dhcp.h"
#include "../bgp/bgp.h"



extern std::ofstream file_log;


//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
CL_FORW_DATA::CL_FORW_DATA()
	{
	this->cl_num=1;
	return;
}

void CL_FORW_DATA::init(uint32_t cl_num1)
	{
	this->cl_num=cl_num1;
	this->cl_arr=new CL_INT_DAEL[this->cl_num];
	free_cells=cl_arr;
	
	for (int i=0; i<this->cl_num-1;i++)
		{
		(this->cl_arr+i)->next_cl=this->cl_arr+i+1;
		(this->cl_arr+i)->state=0;
	}
	(this->cl_arr+cl_num-1)->next_cl=NULL;//end of clients;
	(this->cl_arr+cl_num-1)->state=0;
}

CL_FORW_DATA::~CL_FORW_DATA()
	{
	delete[] cl_arr;
}
uint32_t CL_FORW_DATA::new_cl () // get new client;
	{
	uint32_t ind_new_cl;

	if (this->free_cells==NULL)
		{
		return 1; // no free cells;
	}

	ind_new_cl=this->free_cells-this->cl_arr;
	this->free_cells->state=1;// client initiated
	this->free_cells=this->free_cells->next_cl;
	
	return ind_new_cl;
}

void CL_FORW_DATA::delete_cl (uint32_t cl_index) // free memory with client data\

{
	if ((this->cl_arr+cl_index)->state>0) // this is not free space
		{
		(this->cl_arr+cl_index)->state=0;

		(this->cl_arr+cl_index)->service_bits=0;


		for (int i=0;i<C_serv_count;i++)
			{
			(this->cl_arr+cl_index)->tr_co[i].pack_counter=0;
			(this->cl_arr+cl_index)->tr_co[i].byte_counter=0;
			(this->cl_arr+cl_index)->tr_co[i].bucket_level=0;
			(this->cl_arr+cl_index)->tr_co[i].last_time_slot=0;
			(this->cl_arr+cl_index)->tr_co[i].policer=0;

		}
		(this->cl_arr+cl_index)->next_cl=this->free_cells;
		this->free_cells=this->cl_arr+cl_index;
	}
	
return;

}
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

void CL_INT_DAEL::clear()
	{
	for (int i=0;i<C_serv_count;i++)
		{
		tr_co[i].policer=-1;
	}
}

CL_INT_DAEL::CL_INT_DAEL ()
	{
	clear();
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
int set_def_service_and_policer_to_user(uint32_t cl_numb_ip)
	{
CL_IP_DAEL *cl_ip=cl_ip_dt+cl_numb_ip;

CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_numb_ip;

if (cl_sess->state!=1) return -1;// meance client is deleted

CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
CL_INT_DAEL *cl_forw_out=cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;

cl_sess ->is_default_service=true;


uint8_t def_serv_ind=-1;//G_rad_auth.conf.p0.default_ind;//no activated that function yet


//initial value of policer is not important we will know that at initial state we only need to send accounting
//start message -> we only need to know default policer == old_service==0;

if (!(def_serv_ind<C_serv_count))
	{
	return -1;
}
 cl_forw_in->service_bits=1<<(def_serv_ind);
cl_forw_out->service_bits=1<<(def_serv_ind);

uint32_t pol_index_in =G_serv_agr->arr[def_serv_ind].pol_index_in;
uint32_t pol_index_out=G_serv_agr->arr[def_serv_ind].pol_index_out;

G_pol_agr->set_pol_to_user(cl_numb_ip,0,def_serv_ind,pol_index_in);	
G_pol_agr->set_pol_to_user(cl_numb_ip,1,def_serv_ind,pol_index_out);	

 cl_forw_in->tr_co[def_serv_ind].old_policer= cl_forw_in->tr_co[def_serv_ind].policer;
cl_forw_out->tr_co[def_serv_ind].old_policer=cl_forw_out->tr_co[def_serv_ind].policer;


//don't send accounting information before RAD authenticated 

//G_rad_acc.request(cl_numb_ip_dt,1,def_serv_ind);//0-auth;1-start;2=stop;3 update


//we should clear servic history only from radius accounting function


return 0;
}



//-------------------------------------------------------------------------------------

int add_service_and_policer_to_user(	std::string service_name,
					std::string input_policer_name,
					std::string output_policer_name,	
					uint32_t cl_numb_ip_dt)

	{

//find service index

uint8_t service_id=0xFF; //0xFF fail 
	try 	{ service_id=G_serv_agr->h_t.at(service_name); }catch(...){}

if (service_id==0xFF) return -1; // no such service;


////-----------find policer-------->>


int16_t input_policer_index=0xFFFF;
int16_t output_policer_index=0xFFFF;

	try 	{  input_policer_index=G_pol_agr->h_t.at( input_policer_name); }catch(...){}
	try 	{ output_policer_index=G_pol_agr->h_t.at(output_policer_name); }catch(...){}

if (input_policer_index==0xFFFF)	input_policer_index=G_serv_agr->arr[service_id].pol_index_in;
if (output_policer_index==0xFFFF)	output_policer_index=G_serv_agr->arr[service_id].pol_index_out;


////-----------find policer--------||
int res=add_service_and_policer_to_user(service_id,input_policer_index,output_policer_index,cl_numb_ip_dt);
return res;

}

//---------------------------------------------

int add_service_and_policer_to_user(	uint8_t service_id,
					int16_t input_policer_index,
					int16_t output_policer_index,	
					uint32_t cl_numb_ip_dt)
	{	

if (!(service_id<C_pol_count)) return -1; //error in service index

if  (input_policer_index<0)  input_policer_index=G_serv_agr->arr[service_id].pol_index_in;   
if (output_policer_index<0) output_policer_index=G_serv_agr->arr[service_id].pol_index_out;  

CL_IP_DAEL *cl_ip=cl_ip_dt+cl_numb_ip_dt;

CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_numb_ip_dt;

if (cl_sess->state!=1) return -1;// meance client is deleted

CL_INT_DAEL *cl_forw_in =  cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;
CL_INT_DAEL *cl_forw_out=cl_forw_dt[1+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_out;

if ( cl_sess->is_default_service) //only default service configuration is present
	{
	cl_forw_in->service_bits =0;
	cl_forw_out->service_bits=0;

}
uint8_t is_new_service=0; //that service is already configured

//uint32_t new_serv_bit= (service_id==0)?1:(1<<service_id);

if (((cl_forw_in->service_bits)&(1<<service_id))==0) //if no such service 
	{
	cl_forw_in->service_bits+=(1<<service_id);
	cl_forw_out->service_bits+=(1<<service_id);
	is_new_service=1;//that service configured first time

	//need to add reference to client in service list 
	//serv_users[service_id].push_front(cl_numb_ip_dt);
	//cl_sess->serv_iter[service_id]=serv_users[service_id].begin();
}

G_pol_agr->set_pol_to_user(cl_numb_ip_dt,0,service_id,input_policer_index);	
G_pol_agr->set_pol_to_user(cl_numb_ip_dt,1,service_id,output_policer_index);	

if (is_new_service)  
	{

	cl_forw_in->tr_co[service_id].pack_counter=0;
	cl_forw_in->tr_co[service_id].byte_counter=0;

	cl_forw_out->tr_co[service_id].pack_counter=0;
	cl_forw_out->tr_co[service_id].byte_counter=0;

	cl_sess->new_sess(service_id); //set new session id for that service 
	G_rad_acc.request(cl_numb_ip_dt,1,service_id);//------------------RADIUS------ACCOUNTING--START

	G_rad_acc.request(cl_numb_ip_dt,3,C_serv_count); //accounting update all session
}

	cl_sess ->is_default_service=false;

return 0;

}

//----------------------------------------------



//------------------------------------------------------------------------------------------------------


int Send_RAD_Acc_Update(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		)
	{
	if (!(d1[0]<size_cl_ip_ht)) return -1; // error 

	CL_IP_DAEL *cl_ip=cl_ip_dt+d1[0];

	if (cl_ip->phy_int==0xFF) return -1;// meance client is deleted
	
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+d1[0];

	if (cl_sess->state!=1) return -1;


CL_INT_DAEL *cl_forw_in =  cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;

for (int i=0;i<C_serv_count;i++)
	{
	if ((cl_forw_in->service_bits&(1<<i))!=0)
		{
		G_rad_acc.request(d1[0],3,i);//------------------RADIUS------ACCOUNTING--UPDATE for that service
	}
}

G_rad_acc.request(d1[0],3,C_serv_count); //accounting update all session


cl_sess->rad_acc_timer=G_timers_arr->new_timer(G_rad_acc.conf.p1.update_timer,Send_RAD_Acc_Update,d1,p1);

return 0;
}




//------------------------------------------------------------------------------------------------------

std::string cl_ids(uint32_t ip_hash)
	{
CL_IP_DAEL *cl_ip=cl_ip_dt+ip_hash;
auto cl_sess=cl_ip_sess_dt+ip_hash;
	if (cl_sess->state!=1) return "";

CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
CL_INT_DAEL *cl_forw_out=cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;

uint16_t vlan_in=cl_forw_in->vlan_in;
uint16_t vlan_out=cl_forw_in->vlan_out;

string s_vlan_in=std::to_string(htons(vlan_in));
string s_vlan_out=std::to_string(htons(vlan_out));
string s_ip=ip2s(cl_sess->ip);
char mac_s[6*2+1+6];//string of mac address

for (int i1=0;i1<6;i1++)	
	{
	sprintf(mac_s+3*i1,"%02x",cl_forw_in->mac[i1]);				
	sprintf(mac_s+3*i1+2,":");				
}

mac_s[6*2+1+6-2]=' ';

string s;

s=std::string("clients_mac: ")+	mac_s+		"; "+\
		"vlan_out: "+	s_vlan_out+	"; "+\
		"vlan_in: "+	s_vlan_in+	"; "+\
		"IP: "+		s_ip+		";";

return  s;

}


//------------------------------------------------------------------------------------------------------


int Send_ARP_keepalive(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		)
	{
	if (!(d1[0]<size_cl_ip_ht)) return -1; // error 

	CL_IP_DAEL *cl_ip=cl_ip_dt+d1[0];

	if (cl_ip->phy_int==0xFF) return -1;// meance client is deleted

	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;

	CL_IP_SESS *cl_sess=cl_ip_sess_dt+d1[0];

	if (cl_sess->state!=1) return -1;

//printf("arp_timer %d is finished, rest is %d\n",cl_sess->arp_k_timer,cl_sess->arp_k_counter);

if (cl_sess->arp_k_counter==0)
	{
	//should delete session

	std::string outp=std::string("arp timer is over_")+cl_ids(d1[0])+"\n";
	
	//printf("%s",outp.c_str());
	//file_log<<outp;
	
	del_cl(d1[0],2); //start process of deleting client //Lost Carrier

	return 0;
}

cl_sess->arp_k_counter--;

cl_sess->arp_k_timer=G_timers_arr->new_timer(G_rad_auth.conf.p0.arp_k_time,Send_ARP_keepalive,d1,p1);
//printf("arp_timer %d is created\n",cl_sess->arp_k_timer);


//send arp request
cl_Generate_ARP_Request(d1[0]);

return 0;
}


//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------


int rebind_dhcp_timer_del_cl(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		)
	{
	if (!(d1[0]<size_cl_ip_ht)) return -1; // error 

	CL_IP_DAEL *cl_ip=cl_ip_dt+d1[0];

	if (cl_ip->phy_int==0xFF) return -1;// meance client is deleted

	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;

	CL_IP_SESS *cl_sess=cl_ip_sess_dt+d1[0];

	if (cl_sess->state!=1) return -1;

	std::string s_cl_id=cl_ids(d1[0]);

	//printf("dhcp timer is over %s\n",s_cl_id.c_str());

	//file_log<<"dhcp timer is over "<<s_cl_id<<"\n";
	
	cl_sess->dhcp_rebind_timer=-1;

	del_cl(d1[0],5);// start process of deleting client

return 0;
}

//-----------------------------------------------------------------------------------------------------

int del_service_from_user(	uint32_t cl_numb_ip_dt,
				uint8_t service_id)
	{

	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_numb_ip_dt;

	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_numb_ip_dt;

	if (cl_sess->state!=1) return -1;// meance client is deleted

	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;
	CL_INT_DAEL *cl_forw_out=cl_forw_dt[1+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_out;

	if (!(service_id<16)) return -1; // no such serv;

	if (((cl_forw_in->service_bits)&(1<<service_id))==0) return -2;//no such service


	cl_sess->terminate_cause[service_id]=10;//Lost Service
	G_rad_acc.request(cl_numb_ip_dt,2,service_id); //accounting stop

	cl_forw_in->service_bits-=(1<<service_id);
	cl_forw_out->service_bits-=(1<<service_id);
	
	//-------------------delete indexes from policers list------>>
	
	uint16_t pol_in =  cl_forw_in->tr_co[service_id].policer;
	uint16_t pol_out= cl_forw_out->tr_co[service_id].policer;


	//us_pol[pol_in].erase(cl_sess->pol_iter[0][service_id]);
	//us_pol[pol_out].erase(cl_sess->pol_iter[1][service_id]);

	

	//-------------------delete indexes from policers list------||

	//-------------------delete users from services list of users
	//serv_users[service_id].erase(cl_sess->serv_iter[service_id]);

	G_rad_acc.request(cl_numb_ip_dt,3,C_serv_count); //accounting update all session


	 cl_forw_in->tr_co[service_id].policer=~(0);//invalid
	cl_forw_out->tr_co[service_id].policer=~(0);//invalid

	
	cl_forw_in->tr_co[service_id].pack_counter=0;
	cl_forw_in->tr_co[service_id].byte_counter=0;

	cl_forw_out->tr_co[service_id].pack_counter=0;
	cl_forw_out->tr_co[service_id].byte_counter=0;

	cl_sess->sess[service_id]=~(0);//all 111; invalid;
	
	return 0;	

}

//-------------------------------------------------------------------------------------------

int del_cl(uint32_t cl_hash,uint8_t term_cause) // first step
	{
	//printf("client deleted!!\n");
	std::lock_guard<std::mutex> lock(cl_ht.mut);
	
	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;

	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;

	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
	CL_INT_DAEL *cl_forw_out=cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;


	if (cl_sess->state==0) return 1;//means client is deleted

	if (cl_sess->dhcp_release==1)  DHCP_release_gen(cl_hash);

	G_bgp_conf->del_client(cl_hash);

	//----------------------------------delete entry from hash tables----->>
	cl_ht.M_sess.erase(cl_sess->M_sess);

	if (cl_sess->username!="") cl_ht.username.erase(cl_sess->username);//trick for collizion avoidance
	::uint64_t mac_64;
	memcpy(&(mac_64),cl_forw_in->mac,6);
	cl_ht.mac.erase(mac_64);
	//cl_ht.heap.remove(cl_sess);
	//----------------------------------delete entry from hash tables-----||
	for (int i=0;i<C_serv_count;i++)
		{
		if ((cl_forw_in->service_bits&(1<<i))!=0)
			{
			cl_sess->terminate_cause[i]=term_cause;

			uint16_t pol_in =  cl_forw_in->tr_co[i].policer;
			uint16_t pol_out= cl_forw_out->tr_co[i].policer;

			//us_pol[pol_in].erase(cl_sess->pol_iter[0][i]);
			//us_pol[pol_out].erase(cl_sess->pol_iter[1][i]);

			G_rad_acc.request(cl_hash,2,i); //accounting stop

			//serv_users[i].erase(cl_sess->serv_iter[i]);


			
			cl_forw_in->tr_co[i].pack_counter=0;
			cl_forw_in->tr_co[i].byte_counter=0;

			cl_forw_out->tr_co[i].pack_counter=0;
			cl_forw_out->tr_co[i].byte_counter=0;
			
		}	
	}

	cl_forw_in->service_bits=0;

	//for session
	cl_sess->terminate_cause[C_serv_count]=term_cause;
	G_rad_acc.request(cl_hash,2,C_serv_count); //accounting stop




	G_timers_arr->delete_timer(cl_sess->rad_acc_timer);cl_sess->rad_acc_timer=-1;
	G_timers_arr->delete_timer(cl_sess->arp_k_timer);cl_sess->arp_k_timer=-1;

	G_timers_arr->delete_timer(cl_sess->dhcp_rebind_timer);cl_sess->dhcp_rebind_timer=-1;


	//-------------------------L2-key---->>>>>
	uint8_t l2_key[11];

	memcpy(l2_key+0,&(cl_forw_in->mac),6);//source address

	memcpy(l2_key+6,&(cl_forw_in->vlan_out),2);//VLAN_OUT address
	memcpy(l2_key+8,&(cl_forw_in->vlan_in),2);//VLAN_IN address
	l2_key[10]=cl_ip->phy_int;
	//-------------------------L2-key----|||||
		
	//----------------------------------------delete l2_key------------------->>
	rte_hash_del_key(cl_l2_ht,l2_key); 
	cl_l2_dt[cl_sess->l2_hash].clear();

	//---------------------------------------delete cl_sess and cl_ip--------->>
	int a =rte_hash_del_key(cl_ip_ht,&cl_sess->ip);

	//-----------------------------------------delete forw table entry-:98------->>

	cl_forw_in->clear();
	cl_forw_out->clear();
		
	cl_forw_dt[0+2*cl_ip->phy_int].delete_cl(cl_ip->cl_index_in);
	cl_forw_dt[1+2*cl_ip->phy_int].delete_cl(cl_ip->cl_index_out);


	cl_sess->clear();
	cl_ip->clear();

return 0;

}


//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------


CL_IP_SESS::CL_IP_SESS()
	{
	this->clear();
	
	for (int i=0;i<C_serv_count;i++)
		{
		this->sess[i]=0;
		this->terminate_cause[i]=0;
	}


}

void CL_IP_SESS::new_M_sess()
	{
	this->M_sess=++this->curr_M_sess;
	return; 
}

void CL_IP_SESS::new_sess(uint8_t service_index)	
	{
	if (!(service_index<C_serv_count)) return;
	this->serv_id++;

	this->sess[service_index]=this->serv_id;

	return;
}


::uint64_t CL_IP_SESS::curr_M_sess=0;
std::string CL_IP_SESS::username_template="%vlan_ext%,%vlan_int%";
std::string CL_IP_SESS::password_template="%mac%";



std::string CL_IP_SESS::generate_string(const std::string &s_tem)//generate string from template
	{

	string result=s_tem;
	
	if (this->state!=1) return std::string("");// meance client is deleted

	uint32_t cl_numb_ip_dt=this-cl_ip_sess_dt;

	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_numb_ip_dt;

	CL_INT_DAEL *cl_forw_in = cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;


	//-----------------------------------------MAC
	char mac_s[6*2];//string of mac addres

	for (int i1=0;i1<6;i1++)
		{
		sprintf(mac_s+2*i1,"%02x",cl_forw_in->mac[i1]);				
	}

	result=boost::regex_replace(result,boost::regex("%mac%"),string(mac_s));

	//-----------------------------------------vlan_ext

	result=boost::regex_replace(result,boost::regex("%vlan_ext%"),std::to_string(htons(cl_forw_in->vlan_out)));

	//-----------------------------------------vlan_int

	result=boost::regex_replace(result,boost::regex("%vlan_int%"),std::to_string(htons(cl_forw_in->vlan_in)));

	//-----------------------------------------circuit_id

	result=boost::regex_replace(result,boost::regex("%circuit_id%"),this->cir_id);

	//-----------------------------------------remote_id

	result=boost::regex_replace(result,boost::regex("%remote_id%"),this->rem_id);

	//-----------------------------------------phy_int

	result=boost::regex_replace(result,boost::regex("%phy_int%"),"xe-"+std::to_string(cl_ip->phy_int));

	return result;

}

