#include "../routing/route.h" // for s2ip
#include "../config/node.h"

extern uint32_t ip_dhcp_server;
extern uint32_t ip_agent;

extern std::string dhcp_server_run_string;

int load_dhcp_config(node* config_tree1)
	{

	node* dhcp_n=find("dhcp",config_tree1); 
	if (dhcp_n==NULL) 
		{
		config_tree1->err_gen("How will we assign subscribers they ip address? Where id dhcp server config?");	
		return -1;
	}
//-----------------------------------------------------------server ip---->>	
	node* serv_ip_n=find("server_ip",dhcp_n);

	if (serv_ip_n==NULL) 
		{
		dhcp_n->err_gen("Were is config of dhcp server ip ?");
		return -1;
	}

	
	uint32_t s_ip;//server ip
	if (s2ip(serv_ip_n->value,s_ip)!=0)
		{
		serv_ip_n->err_gen(serv_ip_n->value+"this is not ip address");
		return -1;
	}

	ip_dhcp_server=s_ip;

//-----------------------------------------------------------server ip----||

//----------------------------------------------------------- agent ip---->>

	node* ag_ip_n=find("our_agent_ip",dhcp_n);

	if (ag_ip_n==NULL) 
		{
		dhcp_n->err_gen("Were is config of our agent ip?");
		return -1;
	}

	uint32_t ag_ip;//agent ip

	if (s2ip(ag_ip_n->value,ag_ip)!=0)
		{
		ag_ip_n->err_gen(ag_ip_n->value+"this is not ip address");
		return -1;
	}

	ip_agent=ag_ip;

//-----------------------------------------------------------agent ip-----||

	node* start_dhcp_server_n=find("start_dhcp_server",dhcp_n);

	if (start_dhcp_server_n!=NULL)
		{
		dhcp_server_run_string=start_dhcp_server_n->value;
	}


}