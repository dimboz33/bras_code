#include <vector>
#include "stdio.h"

template<class T> 
struct HP_PO_EL //HEAP POINTER element
	{
T* po; //pointer to element of T class	
int val;//value
int max;// maximum among him and it's child
};

template<class T> //HEAP POINTER ELEMENT
struct HP_PO
	{
std::vector<HP_PO_EL<T> > arr;
int max;// maximum value

void remove(T* po);// delete element x 
void check(int x);//check consistency of x element

void check_deep(int x);

void push(T* po,int v);


HP_PO()
	{
	HP_PO_EL<T> a;
	a.po=NULL;
	a.val=-1;
	arr.push_back(a); //dummy
}
};

//----------------------------------------------------------
template<class T>
void HP_PO<T>::check(int x)
	{
	while ((arr[x/2].max<arr[x].max)&&(x>1))
		{
		arr[x/2].max=arr[x].max;
		x=x/2;
	}
}
//----------------------------------------------------------
template<class T>
void HP_PO<T>::push(T* po1,int val1)
	{
	HP_PO_EL<T> a;

	a.val=val1;
	a.po=po1;
	a.max=a.val;

	arr.push_back(a);
	
	a.po->heap_ind=(arr.size()-1);

	check(arr.size()-1);// check consistency of last elemen
	max=arr[1].max;

}
//----------------------------------------------------------
template<class T>
void HP_PO<T>::check_deep(int x)
	{
	int size=arr.size();

	if ((x<1)||(x>size-1)) return;
	int i=0;
	while (x>0)
		{
		arr[x].max=arr[x].val;
		if (((i==1)||(x*2<size))&&(arr[x*2].max>arr[x].max)) arr[x].max=arr[x*2].max;  
		if (((i==1)||(x*2+1<size))&&(arr[x*2+1].max>arr[x].max)) arr[x].max=arr[x*2+1].max;
		x=x/2;
		i=1;
	}

}
//-----------------------------------------------------------
template<class T>
void HP_PO<T>::remove(T* po1)
	{
	int x=po1->heap_ind;
	if ((x<1)||(x>arr.size()-1)) return;
	int last=arr.size()-1;

	if (x<last)
		{
		arr[x]=arr[last];
		arr[last].po->heap_ind=x;
	}
	
	arr.pop_back(); //delete last element

	check_deep(last/2);

	check_deep(x);

	max=arr[1].max;
	return;
}
//----------------------------------------------------------