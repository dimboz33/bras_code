#include <inttypes.h> // for uint32_t

#include "../config/node.h"
#ifndef DHCP

#define DHCP


//return number of output interface 
uint8_t DHCP_packet_from_server(uint8_t* m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff);

uint8_t DHCP_packet_from_client(uint8_t* m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff);

int DHCP_release_gen(uint32_t cl_hash);

int load_dhcp_config(node* config_tree1);

#ifndef MAIN_FILE
	extern uint32_t ip_agent;
	extern uint32_t ip_dhcp_server;
	extern std::string dhcp_server_run_string;
#else
	uint32_t ip_agent=0;//0x65000002;//0x0C0C0C0C; 0x65000002;0x0b0b0b02;
	uint32_t ip_dhcp_server=0;//0x65000001;//0x0C0C0C0D;0x65000001;0x0d0d0d02
	std::string dhcp_server_run_string="";

#endif

#endif