#include <inttypes.h> // for uint32_t
#include <arpa/inet.h> // for change bit order
#include <rte_fbk_hash.h> // experiment with hash include
#include "clients.h"
#include "../routing/interfaces.h"
#include "../PP/PP.h"

#include "../radius/rad_client.h"
#include "../timers/timers_array.h"
#include "../filter/service.h"
#include "../arp/arp.h"
#include "../routing/forwarding_table.h" // forwarding table
#include "../bgp/bgp.h"

#include "dhcp.h"




//-----------------------------------------------------------------




uint8_t find_option (uint8_t* st_op,uint8_t* end_op,uint8_t code,uint8_t* &op_field)//header of dhcp frame 
	{

// *end_p - pointer on byte after last byte of packet
uint8_t* op=st_op; // options start
//find code option

	for (;(*op!=code)&&(op<end_op);)
		{
		op=op+2+*(op+1);//2 byte for type and length field
	}
	if (*op==code)
		{
		op_field=op;
		return 0;
	}

return 0xFF;
}

//-------------------------------------------------------------------------------
int add_82_option_to_str(uint8_t* st_op,uint8_t* end_op,string &cir_id,string &rem_id)
	{
	cir_id="";
	rem_id="";
	uint8_t* op_field;
	char st_byte[2];

	if (!find_option(st_op,end_op,82,op_field))
		{
		uint8_t* Circ_id_add;
		uint8_t* Remt_id_add;
					
		if (!find_option(op_field,op_field+op_field[1],1,Circ_id_add))
			{
			uint8_t len_circ_id=Circ_id_add[1];
			for (int i=0;i<len_circ_id;i++)
				{
				sprintf(st_byte,"%02x",Circ_id_add[2+i]);
				cir_id+=string(st_byte);
			}
			
		}	


		if (!find_option(op_field,op_field+op_field[1],2,Remt_id_add))
			{
			uint8_t len_remt_id=Remt_id_add[1];
			for (int i=0;i<len_remt_id;i++)
				{
				sprintf(st_byte,"%02x",Remt_id_add[2+i]);
				rem_id+=string(st_byte);
			}

		}
	}
return 0;
}




//-----------------------------------------------------------------------

int add_82_option_to_key(uint8_t* st_op,uint8_t* end_op,uint8_t* key)
	{
	uint8_t* op_field;
	if (!find_option(st_op,end_op,82,op_field))
		{
		uint8_t* Circ_id_add;
		uint8_t* Remt_id_add;
		
			

		if (!find_option(op_field,op_field+op_field[1],1,Circ_id_add))
			{
			uint8_t len_circ_id=(Circ_id_add[1]<4)?Circ_id_add[1]:4;
			memcpy(key+6,Circ_id_add+2,len_circ_id);
		}	


		if (!find_option(op_field,op_field+op_field[1],2,Remt_id_add))
			{
			uint8_t len_remt_id=(Remt_id_add[1]<6)?Remt_id_add[1]:6;
			memcpy(key+10,Remt_id_add+2,len_remt_id);
		}

	}
return 0;
}

//-----------------------------------------------------------


//return number of output interface 
uint8_t DHCP_packet_from_server(uint8_t* m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff)
	{
	//packet started from ip header;
	//needed to determine index of client in CL_VLAN_DATAbase.
	int32_t dhcp_hash;
	uint8_t* mac_pozition;

	uint8_t dhcp_key[16]={0xFF};

	uint8_t* L4_data=m+28; // udp + ip headers

	mac_pozition=(L4_data+28);// 28 shift to mac address 


	uint8_t* st_op=L4_data+240;     //start option
	uint8_t* end_op=m+pk_len; // end option pointers
	
	uint8_t* op_field;	

	memcpy(dhcp_key+0,mac_pozition,6);//source address
	add_82_option_to_key(st_op,end_op,dhcp_key);

	dhcp_hash=rte_hash_lookup(cl_dhcp_ht, dhcp_key);
	if (dhcp_hash<0) //no match
		{
	return 0xFF;
	}
//debug 
	if (!(dhcp_hash<size_cl_dhcp_ht))
		{
		return 0xFF;
	}

//debug

	rte_hash_del_key(cl_dhcp_ht,dhcp_key);// delete entry in dhcp hash table

	CL_DHCP_DAEL* curr_cl=cl_dhcp_dt+dhcp_hash;

	*((uint16_t*)(m+20)+1)=68<<8; //68 destination port
	

	uint32_t cl_ip_add=ntohl(*((uint32_t*)(L4_data+16)));

	//----------------------------WE NEED TO cHECK is THAT IP MATCH ANY OUR IP ADDRESS---->>

	uint32_t check_arp_index=G_for_int_table->get_arp_index_for_ip(cl_ip_add,0);


	if (check_arp_index!=0xFFFFFFFF)
		{
		ARPe* check_curr_arp=&G_arp_table->arp_arr[check_arp_index];

		if (check_curr_arp->is_ours==1) // that is our ip!!!
			{ // should delete that offer
		 	return 0xFF; 		
		}

	}


	//----------------------------WE NEED TO cHECK is THAT IP MATCH ANY OUR IP ADDRESS----||

	*((uint32_t*)m+3)=htonl(ip_dhcp_server);//source ip //ip_agent
	*((uint32_t*)m+4)=htonl(cl_ip_add);//destination ip //AS ixia ip offered ip address 0xFFFFFFFF;

	//set ip header chechsum correct
	
	ip_check_sum((uint16_t*)m);

	L4_data[-1]=0;L4_data[-2]=0;	//udp checksum==0

	uint8_t len_L2;//kength L2 header

	len_L2=(curr_cl->vlan_in==0x10)?18:22;


	uint8_t* m2=m-len_L2;//m2 - L2 pointer may be a problem with zeroing of data before prepend packet

	*pk_diff+=len_L2;
	for (int i=0;i<6;i++) //set src and dst mac address
		{
		m2[i]=mac_pozition[i];
		m2[i+6]=G_phy_ints[curr_cl->phy_int].mac_addr[i];
	}

	//outer VLAN TAG 0x8100
	m2[12]=0x81; m2[13]=0x00;
	*((uint16_t*)(m2+14))=curr_cl->vlan_out;

	//inner VLAN TAG 0x8100//88a8

	if (len_L2==22)
		{
		m2[16]=0x81; m2[17]=0x00;
		*((uint16_t*)(m2+18))=curr_cl->vlan_in;	
	}

	// add  hops 
	L4_data[3]=0;//L4_data[3]=++; //for IXIA
	
	//set agent ip address ( our address)
	*((uint32_t*)(L4_data+4*6))=0;//ntohl(ip_agent); //from dhcp config	 for IXIA

	uint8_t opt53;

	if (find_option(st_op,end_op,53,op_field)!=0) //no such option
		{
		return 0xFF;
	}	
	opt53=op_field[2];

	if (opt53==5) // means ack should try to create client entry 
		{

		std::lock_guard<std::mutex> lock(cl_ht.mut);
		//that was DHCPACK and we should start generate client forwarding data
		
		uint32_t rebind_timer=0;
		
		//----------------------------------------rebind timer-->>
		if (find_option(st_op,end_op,51,op_field)==0)
			{
			rebind_timer=htonl(*((uint32_t*)(op_field+2)));

		}

		if (find_option(st_op,end_op,59,op_field)==0)
			{
			rebind_timer=htonl(*((uint32_t*)(op_field+2)));

		}
		if (rebind_timer==0) return 0xFF;
		rebind_timer+=20;//givs client addition 5min
		//----------------------------------------rebind timer--||



		//---------------------------------------default gatyeway ip------>>
		uint32_t cl_def_gw_ip=0;
		if (find_option(st_op,end_op,3,op_field)==0)
			{
			cl_def_gw_ip=*((uint32_t*)(op_field+2));
		}
		//---------------------------------------default gatyeway ip------|||
		
		//---------------------------------------subnet mask ip------>>
		uint32_t cl_mask=0xFFFFFFFF;
		if (find_option(st_op,end_op,1,op_field)==0)
			{
			cl_mask=*((uint32_t*)(op_field+2));
		}
		//---------------------------------------subnet mask ip------|||
		

		//rte_hash_del_key(cl_dhcp_ht,dhcp_key);// delete entry in dhcp hash table

		//-------------------------------------------------------------------------

		int32_t ip_hash=rte_hash_lookup(cl_ip_ht,&cl_ip_add);

		//if (!(ip_hash<0)) return 0xFF; //no any dublication of mac address should be
	
	

		if (ip_hash<0)
			{
			ip_hash=rte_hash_add_key(cl_ip_ht,&cl_ip_add);	
		}
		else
			{
			if (cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash!=ip_hash) return 0xff; //

			// policer bag; old_policer value isn't -1;
			//it means thath ip already exist in cl_ip_ht and don't equals old value of that client
		}

		//--variables for timers------->>>
		uint32_t d1[4];	//d1[0] - ip_hash of user
		void* p1[4];   	//	
		d1[0]=ip_hash;	
		//--variables for timers-------|||

		CL_IP_SESS *cl_sess=cl_ip_sess_dt+ip_hash;

		if (!(cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash<0))
			{
			//it means that L2_client already have ip address and now we need to change that ip address

			if (cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash!=ip_hash)
				{
				//we need to delete old session 
				//delete_cl(cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash);?????????????????

				int32_t old_ip_hash=cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash;

				CL_IP_DAEL *cl_ip=cl_ip_dt+ip_hash;
				cl_ip->phy_int=curr_cl->phy_int;

				cl_ip->cl_index_in=cl_ip_dt[old_ip_hash].cl_index_in;

				cl_ip->cl_index_out=cl_ip_dt[old_ip_hash].cl_index_out;

				//------------------------


				cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash=ip_hash;


				CL_IP_SESS *cl_old_sess=cl_ip_sess_dt+old_ip_hash;

				G_bgp_conf->del_client(old_ip_hash);

				DHCP_release_gen(old_ip_hash);
	
				rte_hash_del_key(cl_ip_ht,&cl_old_sess->ip);

				*cl_sess=*cl_old_sess; // copy values

				//-------------------------------------
				//cl_ht.heap.remove(cl_old_sess);
				//cl_ht.heap.push(cl_sess,ip_hash);

				
				G_timers_arr->delete_timer(cl_sess->rad_acc_timer);cl_old_sess->rad_acc_timer=-1;

				G_timers_arr->delete_timer(cl_sess->arp_k_timer);cl_old_sess->arp_k_timer=-1;
				//printf("arp_timer %d is deleted\n",cl_sess->arp_k_timer);

				G_timers_arr->delete_timer(cl_sess->dhcp_rebind_timer);cl_old_sess->dhcp_rebind_timer=-1;
				//printf("dhcp_timer %d is deleted\n",cl_sess->dhcp_rebind_timer);
				cl_old_sess->clear();
				cl_ip_dt[old_ip_hash].clear();

				

				//---radius acc
				cl_sess->rad_acc_timer=G_timers_arr->new_timer(G_rad_acc.conf.p1.update_timer,Send_RAD_Acc_Update,d1,p1);
				//-------arp
				cl_sess->arp_k_max_counter=G_rad_auth.conf.p0.arp_k_counter; //no any +1;; number of arp request after that without respon 

				cl_sess->arp_k_counter=cl_sess->arp_k_max_counter;
				//session deleted	
				cl_sess->arp_k_timer=G_timers_arr->new_timer(G_rad_auth.conf.p0.arp_k_time,Send_ARP_keepalive,d1,p1);
				//printf("arp_timer %d is created\n",cl_sess->arp_k_timer);			

				cl_sess->dhcp_rebind_timer=G_timers_arr->new_timer(rebind_timer,rebind_dhcp_timer_del_cl,d1,p1);
				//printf("dhcp_timer %d is created\n",cl_sess->dhcp_rebind_timer);

				cl_sess->ip=cl_ip_add;
				cl_sess->def_gw_ip=cl_def_gw_ip;
				cl_sess->mask = cl_mask;
				//cl_sess->l2_hash should be reassigmented from cl_sess_old
	

				//------------------reset clients iterators--------->>
                                /*
                                    CL_INT_DAEL *cl_forw_in =  cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+ip_hash;
                                    for (int i=0;i<C_serv_count;i++)
                                    {
                                            if (cl_forw_in->service_bits&(1<<i)==1)
                                            {			
                                                    //serv_users[i].erase(cl_sess->serv_iter[i]);
                                                    //serv_users[i].push_front(ip_hash);
                                                    //cl_sess->serv_iter[i]=serv_users[i].begin();
                                            }
                                    }
                                */
				//--------------------reset clients iterators---------||

				G_bgp_conf->add_client(ip_hash);
				ci[0]=0;// from RE
				return curr_cl->phy_int;

			}
			//if it equel that mean that client have same ip , we only need to change dhcp_lease_timer_client_erase
			else
				{
				G_timers_arr->delete_timer(cl_sess->dhcp_rebind_timer);
				//printf("dhcp_timer %d is deleted\n",cl_sess->dhcp_rebind_timer);
				cl_sess->dhcp_rebind_timer=-1;
				

				cl_sess->dhcp_rebind_timer=G_timers_arr->new_timer(rebind_timer,rebind_dhcp_timer_del_cl,d1,p1);
				//printf("dhcp_timer %d is created\n",cl_sess->dhcp_rebind_timer);

				cl_sess->def_gw_ip=cl_def_gw_ip;
				ci[0]=0;// from RE
				return curr_cl->phy_int;
				
			}

		}

			
		cl_l2_dt[curr_cl->cl_L2_hash].cl_ip_hash=ip_hash;
		cl_ip_dt[ip_hash].phy_int=curr_cl->phy_int;
		cl_ip_dt[ip_hash].vlan_in=curr_cl->vlan_in;
		cl_ip_dt[ip_hash].vlan_out=curr_cl->vlan_out;

		//--------------generate data for input core
		int32_t cl_forw_index_input=cl_forw_dt[0+2*curr_cl->phy_int].new_cl();
		cl_ip_dt[ip_hash].cl_index_in=cl_forw_index_input;
		

		//--------------generate data for output core
		int32_t cl_forw_index_output=cl_forw_dt[1+2*curr_cl->phy_int].new_cl();
		cl_ip_dt[ip_hash].cl_index_out=cl_forw_index_output;
	

		cl_forw_dt[0+2*curr_cl->phy_int].cl_arr[cl_forw_index_input].state=1;
		cl_forw_dt[1+2*curr_cl->phy_int].cl_arr[cl_forw_index_output].state=1;
		cl_forw_dt[0+2*curr_cl->phy_int].cl_arr[cl_forw_index_input].clear();
		cl_forw_dt[1+2*curr_cl->phy_int].cl_arr[cl_forw_index_output].clear();

		for (int i=0;i<6;i++)
			{
			cl_forw_dt[0+2*curr_cl->phy_int].cl_arr[cl_forw_index_input].mac[i]=mac_pozition[i];
			cl_forw_dt[1+2*curr_cl->phy_int].cl_arr[cl_forw_index_output].mac[i]=mac_pozition[i];
		}	
		cl_forw_dt[0+2*curr_cl->phy_int].cl_arr[cl_forw_index_input].vlan_in=curr_cl->vlan_in;
		cl_forw_dt[1+2*curr_cl->phy_int].cl_arr[cl_forw_index_output].vlan_in=curr_cl->vlan_in;

		cl_forw_dt[0+2*curr_cl->phy_int].cl_arr[cl_forw_index_input].vlan_out=curr_cl->vlan_out;
		cl_forw_dt[1+2*curr_cl->phy_int].cl_arr[cl_forw_index_output].vlan_out=curr_cl->vlan_out;
	
	


		add_82_option_to_str(st_op,end_op,cl_sess->cir_id,cl_sess->rem_id);

		
		

		//hear we should initiate radius request

		cl_sess->state=1;



		cl_sess->new_M_sess();//set new multisession;
		
		cl_sess->def_gw_ip=cl_def_gw_ip;
		cl_sess->ip=cl_ip_add;
		cl_sess->l2_hash=curr_cl->cl_L2_hash;
		cl_sess->dhcp_serv=ip_dhcp_server;
		cl_sess->dhcp_release=1;
		
		//generate username and password;
		cl_sess->username=cl_sess->generate_string(CL_IP_SESS::username_template);

		cl_sess->password=cl_sess->generate_string(CL_IP_SESS::password_template);
		
		//--------------------------------------------------------------------check username database-->>
		
		
		if (cl_ht.username.find(cl_sess->username)!=cl_ht.username.end())
			{
			//if client has not unique username it willn't get ack packet  
			//shohuld delete client's entry	
			cl_sess->username="";	
			del_cl(ip_hash,18);

			//entry at cl_dhcp_ht already deleted
			//--------------------------
			return 0xFF;
		}
		//--------------------------------------------------------------------check username database--||
		//----------------------------------------------fill hash tables------->>
		cl_ht.username[cl_sess->username]=ip_hash;
		cl_ht.M_sess.insert({cl_sess->M_sess,ip_hash});

		::uint64_t mac_64=0;
		memcpy(&mac_64,mac_pozition,6);
		cl_ht.mac.insert({mac_64,ip_hash});

		//cl_ht.heap.push(cl_sess,ip_hash);			
		//----------------------------------------------fill hash tables-------||	



		
		set_def_service_and_policer_to_user(ip_hash);//set only default bit not any service
	
		//send RAD auth request
		G_rad_auth.request(ip_hash,0,0); //hash_vlan type=0- auth; 0 service means nothing 

		//-------------------------------------------------------set RAD account request timer-->	
		cl_sess->rad_acc_timer=G_timers_arr->new_timer(G_rad_acc.conf.p1.update_timer,Send_RAD_Acc_Update,d1,p1);

		//-------------------------------------------------------set arp timer-->>
		cl_sess->arp_k_max_counter=G_rad_auth.conf.p0.arp_k_counter; //no any +1;; number of arp request after that without respon 
		cl_sess->arp_k_counter=cl_sess->arp_k_max_counter;
		//session deleted	
		cl_sess->arp_k_timer=G_timers_arr->new_timer(G_rad_auth.conf.p0.arp_k_time,Send_ARP_keepalive,d1,p1);
		//printf("arp_timer %d is created\n",cl_sess->arp_k_timer);

		//-------------------------------------------------------set arp timer--||
		
		//rebind timer	
		cl_sess->dhcp_rebind_timer=G_timers_arr->new_timer(rebind_timer,rebind_dhcp_timer_del_cl,d1,p1);
		//printf("dhcp_timer %d is created\n",cl_sess->dhcp_rebind_timer);

		//set ip of client and it's default gateway
		
		G_rad_acc.request(ip_hash,1,C_serv_count);

		G_bgp_conf->add_client(ip_hash);
	}


ci[0]=0;// from RE
return curr_cl->phy_int;
}

//return number of output interface //packet with L2 header

//---------------------------------------------------------------------------------------------------------------------

uint8_t DHCP_packet_from_client(uint8_t* m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff)
	{//we know that ther are two vlans and dhcp protocol at L4



	uint8_t* L4_data=m+ci[8]+20+8;//l2+ip+udp

	uint8_t* st_op=L4_data+240;     //start option
	uint8_t* end_op=m+pk_len; // end option pointers

	uint8_t* op_field;

	uint8_t phy_int=ci[9];

	uint8_t l2_key[11]={0};//very important because of 1 vlan client hash L2 no vlan_in
	uint8_t dhcp_key[16]={0xFF};

	

	memcpy(l2_key+0,m+6,6);	//source address
	memcpy(dhcp_key+0,m+6,6);//source address

	memcpy(l2_key+6,m+14,2);//VLAN_OUT address

	if (ci[8]==22) memcpy(l2_key+8,m+18,2);//VLAN_IN address
	
	l2_key[10]=phy_int;
		
	int32_t l2_hash=rte_hash_lookup(cl_l2_ht, l2_key);
	if (l2_hash<0) 
		{	// should create entry 
			
		l2_hash=rte_hash_add_key(cl_l2_ht, l2_key);// 
		//debug
		if (!(l2_hash<size_cl_l2_ht))
			{
			return 0xFF;
		}
		
	}
	//--------------------------------------------------get 82 option for dhcp field

	add_82_option_to_key(st_op,end_op,dhcp_key);



	//should check is present entry in CL_DHCP_MAP table

	int32_t dhcp_hash=rte_hash_lookup(cl_dhcp_ht,dhcp_key);

	if (dhcp_hash<0)
		{
		//should create entry
		dhcp_hash=rte_hash_add_key(cl_dhcp_ht,dhcp_key);
		//debug

	
		if (!(dhcp_hash<size_cl_dhcp_ht))
			{
			return 0xFF;
		}
		cl_dhcp_dt[dhcp_hash].cl_L2_hash=l2_hash;
		cl_dhcp_dt[dhcp_hash].phy_int=phy_int;

		cl_dhcp_dt[dhcp_hash].vlan_out=(*((uint16_t*)(m+14)))&0xFF0F;

		cl_dhcp_dt[dhcp_hash].vlan_in=(ci[8]!=22)?(0x10):(*((uint16_t*)(m+18)))&0xFF0F;
		

		cl_dhcp_dt[dhcp_hash].tsc=rte_rdtsc();	
	
	
		
	cl_l2_dt[l2_hash].cl_dhcp_hash=dhcp_hash;
	
	}

	cl_l2_dt[l2_hash].tsc=rte_rdtsc();


	//-----change packet	
	// add  hops 

	
	L4_data[3]++;
	//set agent ip address ( our address)
	*((uint32_t*)(L4_data+4*6))=ntohl(ip_agent); //from dhcp config


	m[ci[8]+20+1]=67;//set source port as 67

	((uint16_t*)(m+ci[8]+20))[3]=0;	//set udp chechsum to 0

	//now should transmite packet to dhcp server; 
	*pk_diff=*pk_diff-ci[8]; // delete  vlan tags
	m=m+ci[8];


        //set ip address correct
        *((uint32_t*)m+3)=htonl(ip_agent);
	*((uint32_t*)m+4)=htonl(ip_dhcp_server);

	
	uint8_t opt53;

	if (find_option(st_op,end_op,53,op_field)!=0) //no such option
		{
		return 0xFF;
	}	
	opt53=op_field[2];

	int ip_hash=cl_l2_dt[l2_hash].cl_ip_hash;

	

	if (opt53==7) //DHCPRELEASE
		{
		if (ip_hash<0) return 0xFF;
		if (cl_ip_sess_dt[ip_hash].state!=1) return 0xFF;
		cl_ip_sess_dt[ip_hash].dhcp_release=0;
		del_cl(cl_l2_dt[l2_hash].cl_ip_hash,18);

		//delete cl_dhcp_ht
		//--------------------------
		rte_hash_del_key(cl_dhcp_ht,dhcp_key);// delete entry in dhcp hash table
	}
	
	uint8_t out_phy_int = Forwarding_table_act(ip_dhcp_server,m,ci,pk_diff);


return out_phy_int;

}
