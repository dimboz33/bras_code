

#ifndef CLIENTS

#define CLIENTS
#include <string>
#include <list>

#include <inttypes.h> // for uint32_t
#include <rte_hash.h> // experiment with hash include

#include <rte_cycles.h> // rte_rdtsc
#include <unordered_map>
#include "clients_const.h"

//#include "heap.h"
#include <mutex>

#include "../bgp/bgp.h"

//data structures
//--------------------DAEL DATA ELEMENT---------->



//typedef int16_t POLICER_TYPE;

//#define  P_POLICER_TYPE *int16_t;

//////////////////////////
struct CL_L2_DAEL //key=mac+vlans_out(2byte)+vlan_in(2byte)+phy_int(1byte)
	{ 
	int32_t cl_ip_hash;  //-1 - null
	int32_t cl_dhcp_hash;//-1 is null value
	::uint64_t tsc;//timer stamp clock // last packet arrived

void clear()
	{
	cl_ip_hash=-1;
	cl_dhcp_hash=-1;
	tsc=0;
}

CL_L2_DAEL()
	{
	this->clear();
}



};

//------------------------------------------------------------------------

struct CL_DHCP_DAEL //key=mac+4_byte(circuit ID)+6_byte(Agent_id)
	{
	uint16_t vlan_out;
	uint16_t vlan_in;
	uint8_t phy_int;
	int32_t cl_L2_hash;
	::uint64_t tsc;//timer stamp clock // last packet arrived

void clear ()
	{
	cl_L2_hash=-1;
	phy_int=-1;
	vlan_out=0;
	vlan_in=0;
	tsc=0;

	
}


CL_DHCP_DAEL()
	{
	this->clear();
}


};


//////////////////////////////////
struct CL_IP_DAEL //key=32;for ip/32 routing  find indexes for entries in forwarding tables
	{
	uint32_t cl_index_in;// client index in cl_data array of interface // from client traffic
	uint32_t cl_index_out;//to client traffic

	//uint32_t ip_gw;

	uint16_t vlan_in;
	uint16_t vlan_out;

	uint8_t phy_int;//physical interface

void clear()
	{
	phy_int=0;//need to write real values because of cli server doesn't sinchronizined with RE thread
	cl_index_in =0;
	cl_index_out=0;
}

CL_IP_DAEL()
	{
	clear();
}


};

//-----------------------------------------------------------

class CL_IP_SESS;

struct CL_HT //client hash tables
	{
std::mutex	mut;//mutex of all hash tables

std::unordered_map <std::string, uint32_t>	username;
std::unordered_map <::uint64_t, uint32_t>		M_sess; 
std::unordered_multimap <::uint64_t,uint32_t>	mac;

//HP_PO<CL_IP_SESS>  heap;
//should enable only in case what configured limit on number of client for given vlan tags.
//static std::multimap <uint16_t,uint32_t> vlan_in_ht;
//static std::multimap <uint16_t,uint32_t> vlan_out_ht;

};


class CL_IP_SESS //key =32; ip hash
	{
public:

int32_t l2_hash;

uint32_t ip;
uint32_t def_gw_ip; //default gateway address
uint32_t mask;

uint32_t dhcp_serv;

uint8_t dhcp_release;//should be 

int bgp_route;// 0 -> no /32 (may be agregate); 1 ->/32;

bool is_default_service;
//--------------------------------for heap-->>
int heap_ind;

//--------------------------------for heap--||

uint8_t state;// 0 - no client; 1 - ok ; 2 - deleting

int32_t rad_acc_timer; //that is not TIME, BUT inex of timer!!

int32_t dhcp_rebind_timer;//if client not prolongate that time - client is deleted

int32_t arp_k_timer; //that is not TIME, BUT inex of timer!!
int32_t arp_k_counter; //k - keepalive
int32_t arp_k_max_counter;

static uint64_t curr_M_sess;	

//----------------82_option--->>>
std::string cir_id;
std::string rem_id;
//----------------82_option---|||

//------------------interamation and searching users-------------->>


std::string password;
std::string username;

static std::string username_template;
static std::string password_template;
							//COA
::uint64_t M_sess;//multisession;				//COA
uint16_t serv_id;//it used for construct sessin id for new service of subscriber

uint16_t sess[C_serv_count];//session number for each service





//------------------interamation and searching users--------------||


//std::list<int16_t*>::iterator pol_iter[2][C_serv_count]; //iterator in policers list of users policer indexes

//std::list<uint32_t>::iterator serv_iter[C_serv_count]; //iterator in services list of users


uint32_t terminate_cause[C_serv_count+1];// for session account to




void clear()
	{
	state=0;//means client is state deleting; Only acc stop can run with that client
	def_gw_ip=0;
	ip=0;
	l2_hash=-1;

	rad_acc_timer=-1; //that is not TIME, BUT inex of timer!!

	dhcp_rebind_timer=-1;//if client not prolongate that time - client is deleted

	arp_k_timer=-1;

	serv_id=0;

	
	

}
	
CL_IP_SESS();


void new_M_sess();

void new_sess(uint8_t service_index);

std::string generate_string(const std::string &s_tem);//generate string from template
 
};

//-----------------------------------------------------------

////////////////////////
struct CL_DHCP_MAC_DAEL // key= mac address; find interface and vlans by mac address in responce by DHCP server
	{
	uint8_t phy_int;
	uint16_t vlan_in;
	uint16_t vlan_out;

};

//////////////////////////
struct CL_TRAFFIC_COUNTERS //client traffic counters
	{
	uint64_t pack_counter;//packet counter
	uint64_t byte_counter;//byte counter

	uint32_t bucket_level;//bits

	uint64_t last_time_slot;//timeslot of processor 

	int16_t policer;
	int16_t old_policer;
};



struct CL_INT_DAEL // Client interface core -counters, filters, mac, vlan tags
	{
//one data for input traffic and one for output

	CL_INT_DAEL *next_cl; // for list of free cells_in CL_FORW_DATA erray
	uint8_t state; // 0 -cell is free;
	uint8_t mac[6];

	uint16_t vlan_in;
	uint16_t vlan_out;

	uint16_t service_bits;

CL_TRAFFIC_COUNTERS tr_co[C_serv_count+1];//traffic counters 5 class of traffic +1 for total counters

	//------------------------------------
void clear();


CL_INT_DAEL();


};

//---------------------------------------------------------------------------


class CL_FORW_DATA //Client forwarding array with functions
	{
public:
CL_INT_DAEL *cl_arr;
uint32_t cl_num;//maximum numbers of clients in interface 
CL_INT_DAEL *free_cells;//free cells of client;
CL_FORW_DATA();
void init(uint32_t cli_num1);
~CL_FORW_DATA();

uint32_t new_cl (); // get new client;
void delete_cl (uint32_t cl_index);
}; 


int set_def_service_and_policer_to_user(uint32_t cl_numb_ip_dt);

int add_service_and_policer_to_user(std::string service_name,
						std::string input_policer_name,
						std::string output_policer_name,	
						uint32_t cl_numb_ip_dt);
int add_service_and_policer_to_user(	uint8_t service_id,
					int16_t input_policer_index,
					int16_t output_policer_index,	
					uint32_t cl_numb_ip_dt);

int del_service_from_user(	uint32_t cl_numb_ip_dt,
				uint8_t service_id);

int del_cl(uint32_t cl_index,uint8_t term_cause); // free memory with client data

int Send_ARP_keepalive(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		);

int Send_RAD_Acc_Update(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		);


int rebind_dhcp_timer_del_cl(int time,
		uint32_t d1[4],	//d1[0] - ip_hash of user
		void* p1[4]   	//
		);
//------------------------------------------------extern 
#ifndef CLIENTS_CPP

extern CL_L2_DAEL	*cl_l2_dt;
extern CL_IP_DAEL	*cl_ip_dt;
extern CL_IP_SESS	*cl_ip_sess_dt;
extern CL_DHCP_DAEL	*cl_dhcp_dt;
extern CL_FORW_DATA	*cl_forw_dt;//@@@for one interface each array for each direction input and output


extern rte_hash *cl_l2_ht;
extern rte_hash *cl_ip_ht;
extern rte_hash *cl_dhcp_ht;

extern uint32_t size_cl_ip_ht;
extern uint32_t size_cl_dhcp_ht;
extern uint32_t size_cl_l2_ht;


extern CL_HT cl_ht;

#else


/// --- DATA TABLES --->>>

CL_L2_DAEL 	*cl_l2_dt=NULL; //data table 
CL_IP_DAEL	*cl_ip_dt=NULL;
CL_IP_SESS	*cl_ip_sess_dt;
CL_DHCP_DAEL	*cl_dhcp_dt = NULL;
CL_FORW_DATA    *cl_forw_dt = NULL;//for one interface each array for each direction input and output

/// --- DATA TABLES ---|||

//////---------HASH TABLES------->>

rte_hash *cl_l2_ht=NULL;
rte_hash *cl_ip_ht=NULL;
rte_hash *cl_dhcp_ht=NULL;

uint32_t size_cl_dhcp_ht=1<<18;
uint32_t size_cl_l2_ht=1<<18;
uint32_t size_cl_ip_ht=1<<18;

//////---------HASH TABLES-------||

CL_HT cl_ht;



#endif




#endif
