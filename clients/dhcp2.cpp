#include <inttypes.h> // for uint32_t
#include <arpa/inet.h> // for change bit order

#include "clients.h"
#include "../routing/interfaces.h"
#include "../PP/PP.h"
#include "../routing/forwarding_table.h"

#include "../chassis/bras_chassis.h"//send packet
#include "dhcp.h"


/// from dhcp config--->

extern uint32_t ip_agent;
extern uint32_t ip_dhcp_server;

// from dhcp config----|

int DHCP_release_gen(uint32_t cl_hash)
	{

CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;

CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;

CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
CL_INT_DAEL *cl_forw_out=cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;


if (cl_sess->state==0) return 1;//means client is deleted


	
u_char  data[512]={0}; // data is where we will form packet dhcp release
uint16_t dt_p; //data pointer in dhcp  packet

uint8_t boot_p=46;
dt_p=boot_p;//18[ethernet]+20[ip]+8[udp]

//--------------start filling field-------------------------->>>
data[dt_p++]=1; //message_type Boost request
data[dt_p++]=1; //hardware type ethernet
data[dt_p++]=6; //hardware address length
data[dt_p++]=1; //
dt_p+=4; //transaction id 
dt_p+=2; //second elapsed
dt_p+=2; //boot flag
*(uint32_t*)(data+dt_p)=htonl(cl_sess->ip);dt_p+=4;// Client IP address
dt_p+=4;  // your ip address
dt_p+=4;  //next server ip address
*(uint32_t*)(data+dt_p)=htonl(ip_agent);dt_p+=4;

memcpy(data+dt_p,cl_forw_in->mac,6);

//server host name (unused)
//boot file (unused)

dt_p=boot_p+240-4; *(uint32_t*)(data+dt_p)=0x63538263; dt_p+=4; //magic cookie

//options
data[dt_p++]=53; data[dt_p++]=1;data[dt_p++]=7; // 53 option
data[dt_p++]=54; data[dt_p++]=4;*(uint32_t*)(data+dt_p)=htonl(cl_sess->dhcp_serv);dt_p+=4; // 54 option

if ((cl_sess->cir_id!="")||(cl_sess->rem_id!="")) //option 82
	{
	data[dt_p++]=82;
	int len_p=dt_p++;
	data[dt_p++]=1;//subO  cir_id
	data[dt_p]=sprintf((char*)(data+dt_p),"%s",cl_sess->cir_id);dt_p+=data[dt_p];
	data[dt_p++]=2;//rem_id
	data[dt_p]=sprintf((char*)(data+dt_p),"%s",cl_sess->rem_id);dt_p+=data[dt_p];
	data[len_p]=dt_p-len_p;
}

data[dt_p++]=0xFF;// option 255 End

//--------------start filling field--------------------------|||

//----------------------------------UDP header------------------------------------->>>

uint8_t udp_p=18+20;//udp pointer; ethernet-18 + ip - 20
uint16_t length=dt_p-boot_p;

*(uint16_t*)(data+udp_p)=0x4300; //source port
*(uint16_t*)(data+udp_p+2)=0x4300; //dest port
*(uint16_t*)(data+udp_p+4)=ntohs(length+8);

//----------------------------------UDP header-------------------------------------|||

// ip header


//----------------------------------IP header------------------------------------->>>

uint8_t ip_p=18;// ip pointer

data[ip_p]=0x45;
///ip length
*(uint16_t*)(data+ip_p+2)=ntohs(length+8+20);//8 udp header
// TTL
data[ip_p+8]=0xFF;//
// PROTOCOL UDP 
data[ip_p+9]=0x11;// 

//----------------------------------IP header------------------------------------||||

//get output interface and write L2 header
int16_t pk_diff=0;
uint8_t ci[8];
uint8_t out_phy_int =Forwarding_table_act(cl_sess->dhcp_serv,data+ip_p,(uint8_t*)&ci,&pk_diff);


if (out_phy_int!=0xFF)
	{
 	send_packet(data+ip_p-pk_diff,dt_p+ip_p-pk_diff,out_phy_int,ci);

}
//if only exist mac address

return 0;

}