#include <cstddef>//for NULL
#include <inttypes.h> // for uint32_t



#include <mutex>

#ifndef TIMERS_ARRAY

#define TIMERS_ARRAY

class timers_array;

struct timers_struct { // struct of data of single timers in array
// data part
uint32_t d[4];
void* p[4]; //pointer to something

int (*call_back_function)(int time,      // time at witch timer is done   
			uint32_t d[4],void* p[4]
			);



timers_struct *next; // pointer to the next timer in that timeslot
timers_struct *prior; // pointer to the priori timer in that timeslot

int timeslot;//timeslot at wich timer shuold finishes.
};



class timers_array {

 std::recursive_mutex	mut;

public:

int Max_num_timers;
int Time_slot_value;//in miliseconds
int Max_time_slot; // size in timeslots in cycle array of timeslots;
int curr_time_slot;//current time_slot

timers_struct ** ring_t_s; // ting_t_s it should be array of timeslots with references to
//timers 

timers_struct * free_t; // pointer to free timers 

timers_struct *t_data; // timers array

timers_array(int MAX_NUM_TIMERS,
		 int TIME_SLOT_VALUE,
		int MAX_TIME_SLOT);

~timers_array()
        {
        delete(this->t_data);
        delete(this->ring_t_s);
}



int new_timer(int TIME_MS,
			int (*CALLBACK_FUNCTION)(int time,      // time at witch timer is done
                                uint32_t d[4],void* p[4]),
	 		uint32_t d1[4],void* p1[4]);

int reset_timer (int NEW_TIME_MS,int NUM_OF_TIMER);

int delete_timer (int NUM_OF_TIMER);

int time_slot_change ();

int timer_distance(int NUM_OF_TIMER);


};



#ifndef MAIN_FILE

extern timers_array* G_timers_arr;

#else

timers_array* G_timers_arr=NULL;

#endif

#endif



