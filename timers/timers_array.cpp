

#include "timers_array.h"


timers_array::timers_array(int MAX_NUM_TIMERS,
		 int TIME_SLOT_VALUE,
		int MAX_TIME_SLOT)
{
	this->Max_num_timers=MAX_NUM_TIMERS;
	this->Time_slot_value=TIME_SLOT_VALUE;
	this->Max_time_slot=MAX_TIME_SLOT;
	this->t_data=new timers_struct[MAX_NUM_TIMERS];
	this->ring_t_s=new timers_struct*[Max_time_slot];
	
	for (int i=0;i<Max_time_slot;i++)
		{	
		ring_t_s[i]=NULL;	
	}
	
	this->free_t=t_data;

	// set appropriate pointers for timers data.
	//for free element list prior pointers is not needed
	
	for (int i=0;i<MAX_NUM_TIMERS-1;i++)///-2
		{
		t_data[i].next=t_data+i+1;
		t_data[i].timeslot=-1;
	}
	t_data[MAX_NUM_TIMERS-1].next=NULL; //mean last elemen
	t_data[MAX_NUM_TIMERS-1].timeslot=-1;

	this->curr_time_slot=0;

}

int timers_array::new_timer(int TIME_MS,
			int (*CALLBACK_FUNCTION)(int time,      // time at witch timer is done  
                         			uint32_t d[4],void* p[4]),
	  		uint32_t d1[4],void* p1[4])
	{

	std::lock_guard<std::recursive_mutex> lock(mut);

	//TIME_MS- time when timer is finished in miliseconds
	//return number of timer//for you to change them;
	
	if ((TIME_MS<1)||(TIME_MS>Max_time_slot)) 
		{
		int a;
		a++;
		return -1;
	}

	timers_struct * p_to_free_timer;
	
	p_to_free_timer=this->free_t; // pointer to free timers element;
	
	//change pointer to free element
	if (this->free_t==NULL)
		{
		int b;
		b++;
		return -1;
	} // means no valiable timers data

	
	
	this->free_t=this->free_t->next;//second element becomse first;
	
	if (this->free_t!=NULL) this->free_t->prior=NULL;

	///now we must write data to free timer;
	for (int i=0;i<4;i++)
		{
		p_to_free_timer->d[i]=d1[i];	
		p_to_free_timer->p[i]=p1[i];
	}

	p_to_free_timer->call_back_function=CALLBACK_FUNCTION;
	
	if (p_to_free_timer->call_back_function==NULL)
		{
		return -2;
	}

	//now we need to set that timer in appropriate timeslot
	
	int TSL; 
	
	TSL=(TIME_MS/Time_slot_value+this->curr_time_slot)%Max_time_slot;
	
	p_to_free_timer->next=ring_t_s[TSL];
	
	if (ring_t_s[TSL]!=NULL)
		{
		ring_t_s[TSL]->prior=p_to_free_timer;
	}
	
	p_to_free_timer->prior=NULL;
	ring_t_s[TSL]=p_to_free_timer;
	

	p_to_free_timer->timeslot=TSL;


	return p_to_free_timer-this->t_data;

}


int timers_array::reset_timer (int NEW_TIME_MS,int NUM_OF_TIMER)

{

	std::lock_guard<std::recursive_mutex> lock(mut);
	int old_TSL;
	timers_struct *our_timer;
	
	if (NUM_OF_TIMER<0)
		{
		return -3;
	}
 
	
	if (NUM_OF_TIMER > Max_num_timers-1 )
		{
		return -1; // means number of timer is too large
	}
	
	
	our_timer=this->t_data+NUM_OF_TIMER;
	
	if  (our_timer->timeslot<0)
		{
	return -2;// means timer is not active;
	}

	old_TSL=our_timer->timeslot;
	
	int new_TSL;
	
	new_TSL=(NEW_TIME_MS/Time_slot_value+this->curr_time_slot)%Max_time_slot;
	
	
	/// delete timer from old_TSL
	
	if (our_timer->prior==NULL) // if this timer is the first.
		{
		this->ring_t_s[old_TSL]=our_timer->next;
	
		if (our_timer->next!=NULL) // if there is second timer;
			{
			our_timer->next->prior=NULL;//second element becams first
		}
	}
	else  // timer in the middle of list of old_TSL timer;
		{
		our_timer->prior->next=our_timer->next;
		
		if (our_timer->next!=NULL) // this time is not the last.
			{
			our_timer->next->prior=our_timer->prior;
		}
		
	}
	
	
	///set timer is another timeslot data
	
	our_timer->prior=NULL;
	our_timer->next=ring_t_s[new_TSL];
	our_timer->timeslot=new_TSL;


	if (ring_t_s[new_TSL]!=NULL)	ring_t_s[new_TSL]->prior=our_timer;	

	ring_t_s[new_TSL]=our_timer;
	
	return NUM_OF_TIMER;
	
}

int timers_array::delete_timer (int NUM_OF_TIMER)	
	{

	std::lock_guard<std::recursive_mutex> lock(mut);

	timers_struct *our_timer;
	if ((NUM_OF_TIMER > Max_num_timers-1 )||(NUM_OF_TIMER<0))
		{
		return -1; // means number of timer is too large
	}

	our_timer=this->t_data+NUM_OF_TIMER;

	if (our_timer->timeslot<0)
		{
		return -2;// means timer is not active;
	}

	int TSL=our_timer->timeslot;


	if (our_timer->prior==NULL) // if this timer is the first.
		{
		this->ring_t_s[TSL]=our_timer->next;
	
		if (our_timer->next!=NULL) // if there is second timer;
		{
			our_timer->next->prior=NULL;//second element becames first
		}
	}
	else  // timer in the middle of list of old_TSL timer;
		{
		our_timer->prior->next=our_timer->next;
		
		if (our_timer->next!=NULL) // this time is not the last.
			{
			our_timer->next->prior=our_timer->prior;
		}
		
	}

	our_timer->next=this->free_t;
	if (this->free_t!=NULL) this->free_t->prior=our_timer;
	our_timer->prior=NULL;
	our_timer->timeslot=-2;
	this->free_t=our_timer;

return 0;
}


int timers_array::time_slot_change ()

	{	

	std::lock_guard<std::recursive_mutex> lock(mut);

	//we needed to go throught all timers at current timeslot and call callback functions
	//of all timers at that timeslot
	int TSL;
	this->curr_time_slot=(this->curr_time_slot+1)%Max_time_slot;
	
	TSL=this->curr_time_slot;
	
	timers_struct *c_t;
	
	timers_struct *last_t=NULL;//current timer;
	int i;
	
	
	for (c_t=this->ring_t_s[TSL];c_t!=NULL;c_t=c_t->next)
		{
		c_t->timeslot=-1;//not valid timer //needed befor call_back function for protect deletion that timer from callback function
		i=c_t->call_back_function(TSL,
						c_t->d,c_t->p);
		
		last_t=c_t;
		
	}
	//now we need to set that timer to the free timers list
	
	if (last_t!=NULL)
		{
		last_t->next=this->free_t;
		if (this->free_t!=NULL) this->free_t->prior=last_t;
		this->free_t=this->ring_t_s[TSL];
		if (this->free_t->prior!=NULL)
			{
			int c=0;
			c++;
		}

	}
	this->ring_t_s[TSL]=NULL;//new modification

	return 0;
}

int timers_array::timer_distance(int NUM_OF_TIMER)
	{
	timers_struct *our_timer;
	if ((NUM_OF_TIMER > Max_num_timers-1 )||(NUM_OF_TIMER<0))
		{
		return -1; // means number of timer is too large
	}

	our_timer=this->t_data+NUM_OF_TIMER;

	if (our_timer->timeslot<0)
		{
		return -2;// means timer is not active;
	}

	return (our_timer->timeslot-this->curr_time_slot+Max_time_slot)%Max_time_slot;


}
