#include "clients/clients.h"
#include  "chassis/bras_chassis.h"

#include "timers/timers_array.h"
#include "arp/arp.h"
#include "routing/interfaces.h"
#include "routing/route.h"
#include "routing/ini_rou_int_arp.h"
#include "routing/routes_resolving.h"
#include "config/mnode.h"
#include "config/node.h"
#include "filter/service.h"
#include "filter/policer.h"

#include "radius/rad_client.h"
#include "radius/rad_coa.h"

#include "filter/filter_extern.h"
#include "clients/dhcp.h"
#include "clients/clients.h"
#include "bgp/bgp.h"

#include <unistd.h> //for exec

#include <csignal>



//---------------------------for turning optimization-->>
extern uint32_t NB_MBUF;
extern uint32_t MEMPOOL_CACHE_SIZE;
extern uint32_t RING_SIZE;
extern uint32_t prefetch_numb;

extern uint32_t RING_P_B_RECV;
extern uint32_t	INT_P_B_RECV;
extern uint32_t	INT_P_B_SEND;
extern uint32_t	ADD_CLIENT_BY_PACKET;


//---------------------------for turning optimization--||

extern node* config_tree;
extern Mnode model_tree;




extern uint8_t G_config_bit;//0 or 1 to change configuration

//==============================================================


extern vector<LogInt> log_int_table[2]; //---------------------------------------------------CHANGED

extern vector<PhyInt>  phy_int_table[2];//---------------------------------------------------CHANGED 


//=============================================================
 




extern char config_filename[];

///-----------------------------------------------------RADIUS------->>



///-----------------------------------------------------RADIUS-------||

extern uint8_t G_first_config;




extern list <LoRo> lo_ro_list;
extern list<StRo> st_ro_list, old_st_ro_list;
extern list<StRo> active_st_ro_list;

void generate_new_mac_table(int config_bit);

int counter_of_reload=0;


int set_int_from_node(node* par_node,string name,uint32_t &value);

extern uint32_t ip_agent;
extern uint32_t ip_dhcp_server;
extern std::string dhcp_server_run_string;




extern std::ofstream file_log;

 uint8_t reload_config ()
	{

	int a;
	
	string s;	
	

	uint8_t config_bit=1-G_config_bit;

	counter_of_reload++;
	
	bool ini_conf_b=counter_of_reload<3;

	if (counter_of_reload == 1) 
        {
            cl_forw_dt = new CL_FORW_DATA[Phy_Int_Count*2];
            for ( int i=0; i < Phy_Int_Count ; i++)
                if (int_types[i] == 1)
                {
                    cl_forw_dt[2*i].init(1<<18);
                    cl_forw_dt[2*i+1].init(1<<18);
                }
        
        }

	if (counter_of_reload>0)	{
	

if (false)//counter_of_reload==3)//counter_of_reload==3)
{


	//run exabgp
		//printf("nohup exabgp bgp_config 0<&- &>/dev/null &\n");
		//int a=std::system("nohup exabgp bgp_config 0<&- &>/dev/null &");
		//printf("return %d", a);
	int bgp_pid=fork();


	//system("./system_call/bras_system_call");



	if (bgp_pid==0)
	{ //env exabgp.daemon.user=root exabgp.daemon.daemonize=false
		int a=execl("/usr/local/bin/exabgp","exabgp","bgp_config",NULL);
		printf("Can't run exabgp process!!!!!!");
		exit(-1);
	}


}


}



	ifstream file_config;

	file_config.open(config_filename,ifstream::in);

	if (!file_config.good())
		{
		printf("no config file\n");
		return -1;//bad new config
	}

	s=next_good_str(file_config,config_tree);


	if (config_tree!=NULL)	
		{
		//config_tree->free_memory();
		delete config_tree;
	}

	node::line_n=0;
	node::error_n=-1;
	node::err_s="";

	config_tree=new node(s,file_config,&model_tree,1,1);

	file_config.close();

	if (config_tree->error_n>-1) 
		{
		printf ("Error in Config2 file at line %u\n",config_tree->error_n);
		return -1;
	}

	config_tree->collaps();

	if (config_tree->error_n>-1)//(config_tree->err_s!="") 
		{

		printf("config error!!! \n  %s \n",config_tree->err_s.c_str());
		
		return 1;

	}

///-------------------------------

//---------------------------------------------prog_compile_options--->
node* comp_opt_n =find("prog_opt",config_tree);
if (comp_opt_n!=NULL)	set_int_from_node(comp_opt_n,"pref",prefetch_numb);

printf("prefatch is %u \n",prefetch_numb);

//---------------------------------------------prog_compile_options-----||

//service reload first (with policers)


node* firewall_conf;
node* pol_all_n;
node* serv_all_n;
//------------------------------------------RESERVED_COPIES_OF_RE_STRUCTURES-------->>
RAD_CL_conf G_rad_auth_old_conf=G_rad_auth.conf;
RAD_CL_conf G_rad_acc_old_conf=G_rad_acc.conf;

RAD_SERVERS  G_rad_servers_old=G_rad_servers;


RAD_COA    G_rad_coa_old=G_rad_coa;

uint32_t ip_agent_old=ip_agent;
uint32_t ip_dhcp_server_old=ip_dhcp_server;
//------------------------------------------RESERVED_COPIES_OF_RE_STRUCTURES--------||

list <LoRo> lo_ro_list_old=lo_ro_list;			
list<StRo> st_ro_list_old=st_ro_list;
list<StRo> active_st_ro_list_old=active_st_ro_list;

lo_ro_list.clear();	
st_ro_list.clear();	
active_st_ro_list.clear();

//--------------------------------------------------------------
//-----------------------bgp------------->>

if (G_first_config!=1)
	{
	
	node* bgp_n =find("bgp_announce",config_tree);
	if ( (G_bgp_conf_arr[config_bit].Load_bgp_conf(bgp_n))!=0) 
		{
		goto ROLLBACK;
	}
}
//-----------------------bgp-------------||



//--------------------------------------------------DEF POLICERS--------------------->>

	if (1)	{//(ini_conf_b)	{ //###

	int k;

	firewall_conf=find("Firewall",config_tree); //-------------------FIREWALL-CONFIG
	if (firewall_conf==NULL)
		{
		config_tree->err_gen("No section FIREWALL is configured");
		goto ROLLBACK;
	}

	pol_all_n=find("policer",firewall_conf);

	k=G_pol_agr_arr[config_bit].Load_all_pol_conf(pol_all_n,G_pol_agr);

	if (k!=0)
		 {
		//printf("Load_all_pol_conf error %d\n",k);
		goto ROLLBACK;//
	}

	G_pol_agr=G_pol_agr_arr+config_bit;
//--------------------------------------------------DEF POLICERS----------------------||


	
//--------------------------------------------------LOAD SERVICES CONFIG-------------->>
	serv_all_n=find("Services",config_tree);
	
	k=G_serv_agr_arr[config_bit].Load_all_serv_conf(serv_all_n,G_serv_agr);
	
	if (k!=0)
		{
		// printf("Load_all_serv_conf error %d\n",k);
		 goto ROLLBACK; //load and sort services

	}
	G_serv_agr=G_serv_agr_arr+config_bit;
//--------------------------------------------------LOAD SERVICES CONFIG--------------||
	k= apply_acl_config_cpp(config_bit);
	if (k!=0)
		{
		//  printf("apply_acl_config_cpp error %d\n",k);
		  goto ROLLBACK;//activate new ACL)
		}

	}///####



//---------------------RADIUS_CONFIG----------------------------->>

if (G_rad_servers.load_config(config_tree)!=0) goto ROLLBACK;

if (G_rad_auth.load_config(config_tree)!=0) goto ROLLBACK;
if (G_rad_acc.load_config(config_tree)!=0) goto ROLLBACK;
if (G_rad_coa.load_config(config_tree)!=0) goto ROLLBACK;
//---------------------RADIUS_CONFIG-----------------------------||


load_dhcp_config(config_tree);



a= init_log_phy_int_arp (config_tree,
			G_arp_table_arr[config_bit],
			log_int_table[config_bit],
			phy_int_table[config_bit],
			lo_ro_list);
if (a!=0) goto ROLLBACK;


a= init_st_routes (config_tree,	 st_ro_list);

if (a!=0) goto ROLLBACK;

if  (config_tree->error_n>-1) 
	{
	ROLLBACK:

	printf("config arror!!! \n  %s \n",config_tree->err_s.c_str());

	//file_log<<"config arror!!! \n"<<config_tree->err_s<<'\n' ;

	//restore valid configuration
	
	G_rad_servers=G_rad_servers_old;
	G_rad_auth.conf=G_rad_auth_old_conf;	//radius authentication client
	G_rad_acc.conf=G_rad_acc_old_conf;	// radius accounting client

	ip_agent=	ip_agent_old;
	ip_dhcp_server=	ip_dhcp_server_old;

	lo_ro_list=lo_ro_list_old;
	st_ro_list=st_ro_list_old;
	active_st_ro_list=active_st_ro_list_old;
	

	config_bit=1-config_bit;
	
	G_serv_agr=G_serv_agr_arr+config_bit;
	G_pol_agr=G_pol_agr_arr+config_bit;

	return -1;
}
//------------------------CONFIG IS GOOD and WE CAN CHANGE GLOBAL STRUCTERS


file_log<<"config_ok\n";

if (1)	{//(ini_conf_b)	{ //###


G_bgp_conf_arr[config_bit].apply_bgp_conf(G_bgp_conf);

G_bgp_conf=G_bgp_conf_arr+config_bit;


//--refresh system interfaces and routes------------------------->>


refresh_shad_units(	phy_int_table[config_bit],
		 	phy_int_table[1-config_bit],
			log_int_table[config_bit]);

} //###

refresh_system_static_routes(st_ro_list,old_st_ro_list);



//--refresh system interfaces and routes-------------------------||


G_log_ints_arr[config_bit]=&(log_int_table[config_bit][0]);
G_phy_ints_arr[config_bit]=&(phy_int_table[config_bit][0]);

G_log_ints=G_log_ints_arr[config_bit];
G_phy_ints=G_phy_ints_arr[config_bit];

//we need to check states of interfaces after construction of routing table

generate_new_mac_table(config_bit);

G_arp_table=G_arp_table_arr+config_bit;

a= activate_static_routes(st_ro_list,lo_ro_list,active_st_ro_list);




//---------we need to get interface states up or down-WE NEED TO CHANGE THAT!!!!

for (int i=0;i<Phy_Int_Count+1;i++)
	{
	int_states[i]=1;

}
//------------------------------------------------WE NEED TO CHANGE THAT!!!!
G_for_table_arr[config_bit]->free_routes();
G_for_int_table_arr[config_bit]->free_routes();


G_for_table_arr[config_bit]->add_routes(lo_ro_list);
G_for_int_table_arr[config_bit]->add_routes(lo_ro_list);

G_for_table_arr[config_bit]->add_routes(active_st_ro_list);




G_for_table_arr[config_bit]->Generate_Tree(1);
G_for_int_table_arr[config_bit]->Generate_Tree(1);


G_for_int_table=G_for_int_table_arr[config_bit];//forwarding table
G_for_table=G_for_table_arr[config_bit];//forwarding table
//-------------------------------------------------





//-----------------------------------------------GENErate ARP requests for static routes---->>>

for (int i=0;i<G_arp_table_arr[config_bit].arp_arr.size();i++)
	{
	ARPe* curr_arp=&(G_arp_table->arp_arr[i]);
	if (curr_arp->is_static_route==0) continue;
	curr_arp->get_arp();
}

//-----------------------------------------------GENErate ARP requests for static routes----|||

G_arp_table =G_arp_table_arr+config_bit;//for activization of static route


//###########################3

G_config_bit=config_bit;



G_first_config=0;

fflush(stdout);

return 0;

}

extern "C" int C_reload_config()
	{
return reload_config();
}

///----------------------------------------------------------------------------------

void generate_new_mac_table(int config_bit)
	{

	//write old arp entries to new table and change parameters in timers

	for (int i1=0;i1<log_int_table[1-config_bit].size();i1++)
		{

		LogInt *A1=&(log_int_table[1-config_bit][i1]);
		LogInt *A2;
		//for each logical interface
		for (int i2=0;i2<log_int_table[config_bit].size();i2++)
			{
			A2=&(log_int_table[config_bit][i2]);		
			if  ((A1->phy_int==A2->phy_int)&&(A1->vlan==A2->vlan)&&(A1->net==A2->net))
				{
				//should change timers pointers
				for (int i=0;i<(1<<(32-A1->net.mask));i++)
					{
					ARPe* MAC_old=&(G_arp_table_arr[1-config_bit].arp_arr[A1->arp_index+i]);

					if ((MAC_old->is_ok==1)&&(MAC_old->is_ours!=1))
						{
						//arp is valid
						

						ARPe* MAC_new=&(G_arp_table_arr[config_bit].arp_arr[A2->arp_index+i]);
						

						//no copy of is_static_route
						memcpy(MAC_new->mac_addr,MAC_old->mac_addr,6);

						MAC_new->state=MAC_old->state;
						MAC_new->is_ok=MAC_old->is_ok;
						MAC_new->counter=MAC_old->counter;
						MAC_new->timer_n=MAC_old->timer_n;
						G_timers_arr->t_data[MAC_old->timer_n].p[1]=(void*)MAC_new;

						MAC_old->timer_n=-1;//isn't any timer referred to that arp entry

					
					}
				
				}

			}
		
		}
		//A1 - old log interface
		//delete timers assosiated only with old mac table for that interface
		for (int i=0;i<(1<<(32-A1->net.mask));i++)
			{
			ARPe* MAC_old=&(G_arp_table_arr[1-config_bit].arp_arr[A1->arp_index+i]);
	
			if (!(MAC_old->timer_n<0))
				{
				G_timers_arr->delete_timer(MAC_old->timer_n);
			}

		}


	}




return ;
}
//---------------------




///----------------------------------------------------------------------------------
///
int pre_load_config()
	{

string s;	

	ifstream file_config;

	file_config.open(config_filename,ifstream::in);

	if (!file_config.good())
		{
		printf("no config file\n");
		return -1;//bad new config
	}

	s=next_good_str(file_config,config_tree);


	if (config_tree!=NULL)	
		{
		config_tree->free_memory();
	}

	node::line_n=0;
	node::error_n=-1;
	node::err_s="";

	config_tree=new node(s,file_config,&model_tree,1,1);

	file_config.close();

	if (config_tree->error_n>-1) 
		{
		printf ("Error in Config2 file at line %u\n",config_tree->error_n);
		return -1;
	}

	config_tree->collaps();

	node* comp_opt_n =find("prog_opt",config_tree);
	if (comp_opt_n!=NULL)
		{

		set_int_from_node(comp_opt_n,"NB_MBUF",NB_MBUF);
		set_int_from_node(comp_opt_n,"MEMPOOL_CACHE_SIZE",MEMPOOL_CACHE_SIZE);
		set_int_from_node(comp_opt_n,"RING_SIZE",RING_SIZE);

		set_int_from_node(comp_opt_n,"RING_P_B_RECV",RING_P_B_RECV);
		set_int_from_node(comp_opt_n,"INT_P_B_RECV",INT_P_B_RECV);
		set_int_from_node(comp_opt_n,"INT_P_B_SEND",INT_P_B_SEND);
		set_int_from_node(comp_opt_n,"ADD_CLIENT_BY_PACKET",ADD_CLIENT_BY_PACKET);

	}


	if  (set_client_type_of_ports (config_tree)!=0) 
		{
		printf("config arror!!! \n  %s \n",config_tree->err_s.c_str());
		return -1;
	}



	// tries to run dhcp server




return 0;
}

int set_int_from_node(node* par_node,string name,uint32_t &val)
	{
	node* CC=find(name,par_node);
	if (CC==NULL) return -1;
	int32_t a=s2numb(CC->value);
	if (a<0) return -1;
		
	val=a;

	return 0;
}



