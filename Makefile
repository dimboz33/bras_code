
ifeq ($(RTE_SDK),)
$(error "Please define RTE_SDK environment variable")
endif

CC=g++

# Default target, can be overriden by command line or environment
RTE_TARGET ?= x86_64-native-linuxapp-gcc

include $(RTE_SDK)/mk/rte.vars.mk

# binary name
APP = bras_dim

CUR=$(shell pwd)/..
# all source are stored in SRCS-y
SRCS-y := $(CUR)/main.cpp 
#----------------------------
D1=chassis
D2=PP


######################## BGP
SRCS-y +=$(CUR)/bgp/bgp.cpp 

######################## filters
SRCS-y +=$(CUR)/filter/filter.cpp 
SRCS-y +=$(CUR)/filter/read_fields.cpp 
SRCS-y +=$(CUR)/filter/policer.cpp $(CUR)/filter/policer2.cpp $(CUR)/filter/policer3.cpp
SRCS-y +=$(CUR)/filter/service.cpp $(CUR)/filter/service2.cpp
#########################


######################## bras chassis

SRCS-y += chassis/bras_chassis.c 
SRCS-y += chassis/CORE_ETH.c
SRCS-y += chassis/CORE_FLT.c
SRCS-y += chassis/CORE_RE_KNI.c
#SRCS-y += chassis/CORE_KNI.c
#SRCS-y += chassis/CORE_RE.c
SRCS-y += chassis/display.c

######################## Packet processing

SRCS-y += $(CUR)/PP/Input_Core1.cpp $(CUR)/PP/Input_Core2.cpp $(CUR)/PP/Input_Core3.cpp

SRCS-y += $(CUR)/PP/RE.cpp  $(CUR)/PP/Output_Core.cpp $(CUR)/PP/FILTER.cpp

######################## arp

SRCS-y += $(CUR)/arp/arp.cpp

######################## timer

SRCS-y += $(CUR)/timers/timers_array.cpp
SRCS-y += $(CUR)/timers/time_slot_change.cpp

######################## system_call

SRCS-y += $(CUR)/system_call/cli.cpp

######################## cli_server

SRCS-y += $(CUR)/comm/func.cpp
SRCS-y += $(CUR)/comm/m_tree.cpp
SRCS-y += $(CUR)/comm/proc.cpp
SRCS-y += $(CUR)/comm/server.cpp
SRCS-y += $(CUR)/comm/tree.cpp

SRCS-y += $(CUR)/comm/sub_param.cpp
SRCS-y += $(CUR)/comm/show_sub.cpp
SRCS-y += $(CUR)/comm/mon_sub.cpp
SRCS-y += $(CUR)/comm/mon_int.cpp
SRCS-y += $(CUR)/comm/show_arp.cpp
SRCS-y += $(CUR)/comm/show_route.cpp
SRCS-y += $(CUR)/comm/clear_sub.cpp
SRCS-y += $(CUR)/comm/show_bgp.cpp

######################## CONFIG

SRCS-y += $(CUR)/config/node.cpp $(CUR)/config/mnode.cpp

######################## ROUTING

SRCS-y +=$(CUR)/routing/routes_resolving.cpp
SRCS-y +=$(CUR)/routing/route.cpp
SRCS-y +=$(CUR)/routing/ini_rou_int_arp.cpp
SRCS-y +=$(CUR)/routing/forwarding_table.cpp
######################## Clients

SRCS-y +=$(CUR)/clients/clients.cpp  $(CUR)/clients/dhcp.cpp $(CUR)/clients/dhcp_config.cpp $(CUR)/clients/dhcp2.cpp

######################### RELOAD CONFIG

SRCS-y +=$(CUR)/reload_config.cpp  

######################### RADIUS 
SRCS-y +=$(CUR)/radius/rad_global.cpp
SRCS-y +=$(CUR)/radius/rad_w_s_request_auth.cpp
SRCS-y +=$(CUR)/radius/rad_w_s_request_acc.cpp  
SRCS-y +=$(CUR)/radius/rad_servers.cpp  
SRCS-y +=$(CUR)/radius/rad_read_responce.cpp
SRCS-y +=$(CUR)/radius/rad_oven.cpp 
SRCS-y +=$(CUR)/radius/rad_conf.cpp 
SRCS-y +=$(CUR)/radius/rad_coa.cpp 
SRCS-y +=$(CUR)/radius/rad_coa_conf.cpp 
SRCS-y +=$(CUR)/radius/rad_coa_tlv.cpp 
######################### LIRAY

#SRCS-y +=$(CUR)/liray/liray.cpp

######################### FLAGS


CFLAGS+= -O3 
CFLAGS+= -w 
CFLAGS+= -g 
#CFLAGS+= -ggdb

#CFLAGS+=-Wno-unused-variable -Wno-unused-parameter -Wno-unused-but-set-variable

#CFLAGS+=-std=gnu++11
CFLAGS+=-std=c++11 -I/usr/include/crypto++/
#CFLAGS+=-DSO

#CFLAGS+=-D_GLIBCXX_USE_CXX11_ABI=0

#libraries
LDFLAGS += -lstdc++ -lboost_regex -lcrypto


include $(RTE_SDK)/mk/rte.extapp.mk
