

#ifndef BGP

#define BGP
#include <fstream>
using namespace std;
#include "../routing/route.h"
#include "../routing/forwarding_table.h"


#include "../config/node.h"

#include <mutex>

class BGP_CONF;

class AGR_ROUTE : public ROUTE
	{
public:
int count;// count if such clients
int  state;// 1 - new; 0 -not new; -1 no more exists

int add();
int remove();

};

class BGP_CONF {
public:

vector<string> conf_str;

static std::mutex mut;

static fstream bgp_input; //exaBgp pipe

static ostream bgp_pipe; //exaBgp pipe

vector<AGR_ROUTE> agr_arr;//agregate route array



ForTabTre s_table; //search table give index agr_route in vector ag_r

int gen_agr_route();

int Load_bgp_conf (node* bgp_n);

int apply_bgp_conf(BGP_CONF* old_bgp);

int add_client (uint32_t ip_hash);
int del_client (uint32_t ip_hash);

void shutdown();


int get_bgp_responce(vector<string>& s_arr, int time=200); //time in milisecond
int get_bgp_responce(string& s, int time=200);


};






#ifndef MAIN_FILE

extern BGP_CONF G_bgp_conf_arr[];	
extern BGP_CONF *G_bgp_conf;

#else

BGP_CONF G_bgp_conf_arr[2];	
BGP_CONF *G_bgp_conf=G_bgp_conf_arr+G_config_bit;

#endif

#endif
