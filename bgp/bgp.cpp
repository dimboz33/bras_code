#include "bgp.h"

#include "../routing/route.h"

#include "../algo/update_coll.h"
#include "../clients/clients.h"
#include <sys/stat.h>   //mkfifo
#include "../log/log.h"
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <ext/stdio_filebuf.h>

ostream BGP_CONF::bgp_pipe; //exaBgp pipe
fstream BGP_CONF::bgp_input; //exaBgp pipe

std::mutex BGP_CONF::mut;



int getline_asi(istream& is,string& s)
	{
	char res_ch;
	if (is.readsome(&res_ch,1)>0)
		{
		is.putback(res_ch);

		std::getline(is,s); 

		return 1;
		
	}
	else return 0;// no avaliable data

}

int get_arr_str_asi(istream& is,vector<string>& s_arr, int add_str=0)
	{
	if (add_str==0)	s_arr.clear();
	string s;
	int res=0;
	while (getline_asi(is,s)>0)
		{
		s_arr.push_back(s);
		res=1;
	}

	return res;
}

int BGP_CONF::get_bgp_responce(vector<string>& s_arr ,int time) //time in milisecond
	{

	usleep(time);
	get_arr_str_asi(this->bgp_input,s_arr);
	if (s_arr.size()==0) return -1;// no responce
	
	
	if (s_arr.back()!="done")
		{
		return 2;//error command not completed
	}

	s_arr.pop_back();

	int n_to_del=0;//need to delete responce from previous command
	
	int i;
	int prev_found=0;//previous command
	//find start of previous command
	for (i=s_arr.size()-1;i>=0;i--)
		{
		if (s_arr[i]=="done")
			{
			prev_found=1;
			break;
		}
	}

	//clear result from previous command
	if (prev_found==1)
		{
		s_arr.erase(s_arr.begin(),s_arr.begin()+i+1);
	}
	return 0;

}

int BGP_CONF::get_bgp_responce(string& s,int time)
	{
	vector<string> s_arr;
	int ret=this->get_bgp_responce(s_arr,time);
	if (ret!=0) return ret;

	if (s_arr.size()!=1) return 3;// to big size
	
	s=s_arr[0];

	return 0;

}


//-------------------
int AGR_ROUTE::add()
	{
	state=1;
}
//-------------------
int AGR_ROUTE::remove()
	{
	state=-1;
}

//---------------------------
int BGP_CONF::gen_agr_route()
	{
	for (int i=0;i<agr_arr.size();i++)
		{
		agr_arr[i].state=0;
		agr_arr[i].count=0;
		agr_arr[i].type=1;
		agr_arr[i].local_next.push_back(ARPm(i,1));
	}
}

//---------------------

int BGP_CONF::Load_bgp_conf (node* bgp_n )

	{
	//return 0 ;//##!!!!!!!!!!!!!!!!!
	this->agr_arr.clear();


	//bgp_pipe.close();
	//bgp_input.close();
	if (!bgp_pipe.is_open())
		{
		ASSERT_LOG(mkfifo("/tmp/bras_exabgp_fifo",0666) == 0 ); 

		ASSERT_LOG(chmod("/tmp/bras_exabgp_fifo", 0666) == 0);

                int posix_handle = open("/tmp/bras_exabgp_fifo", O_WRONLY | O_NONBLOCK);

                __gnu_cxx::stdio_filebuf<char> filebuf(posix_handle, std::ios::out); // 1
                bgp_pipe(&filebuf);
                
		//printf("waiting for exabgp to start\n");

		//bgp_pipe.open("/tmp/bras_exabgp_fifo",ifstream::out|ifstream::in); //ifstream::in
		//printf ("ok\n");
	}

	if (!bgp_pipe.is_open()) 
		{
		printf("Can't open pipe for exabgp process");
		return -1;
	}

	if (!bgp_input.is_open())
		{

		mkfifo("/tmp/bras_exabgp_out",0666);

		chmod("/tmp/bras_exabgp_out", 0666);

		
		//printf("waiting for exabgp to start\n");

		bgp_input.open("/tmp/bras_exabgp_out",ifstream::out|ifstream::in); //ifstream::out|
		
		//printf ("ok\n");
	}

	if (!bgp_input.is_open()) 
		{
		printf("Can't open pipe for reading exabgp process");
		return -1;
	}
	

	if (bgp_n==NULL) return 0;
	node* ag_r_n=find("aggregate_route",bgp_n);
	if (ag_r_n==NULL) return 0;

	int i=0;
	for (node* c_r=ag_r_n->child_node;c_r!=NULL;c_r=c_r->next_node,i++)
		{
		AGR_ROUTE ag_r;
		if (s2Netw(c_r->name,ag_r.net)!=0)
			{
			c_r->err_gen(" is not network");
			return -1;
		}
		agr_arr.push_back(ag_r);
	}

	this->conf_str.clear();

	//-------------------------------cheack config of bgp peers-------------------->>>


	node* nbrs_n=find("neighbor",bgp_n);	

	if (nbrs_n==NULL) return 0;// no neighbors

	//---------------------------------------------------------------CHECK local-as---->>>

	node* loc_as_n=find("local-as",bgp_n);


	if (nbrs_n->child_node!=NULL) 
		{ // check presents local address
		
		if (loc_as_n==NULL)	
			{
			bgp_n->err_gen("config of local-as should be");
			return -1;	
		}
		int loc_as_v=s2numb(loc_as_n->value);

		if ((loc_as_v<1)||(loc_as_v>65535))
			{
			loc_as_n->err_gen("value should be from 1 to 65535");
			return -1;
		}

	}
	//-----------------------------------------------------------------CHECK local-as----|||


	for (node* nbr_n=nbrs_n->child_node;nbr_n!=NULL;nbr_n=nbr_n->next_node)
		{
		//-----------------------------------ip address_of peer-->>
		uint32_t ip;
		if (s2ip(nbr_n->name,ip)!=0)
			{
			nbr_n->err_gen("that is not ip");
			return -1;
		}
		//-----------------------------------ip address_of peer--|||

		conf_str.push_back( nbrs_n->name+" "+nbr_n->name+" {");

		//-----------------------------------peer-as------------->>>
		node* peer_as_n=find("peer-as",nbr_n);
		if (peer_as_n==NULL)
			{
			nbr_n->err_gen("peer-as should be configured");
			return -1;
		}
		int peer_as_v=s2numb(peer_as_n->value);

		if ((peer_as_v<1)||(peer_as_v>65535))
			{
			peer_as_n->err_gen("value should be from 1 to 65535");
			return -1;
		}

		//-----------------------------------peer-as-------------|||
		
		conf_str.push_back(peer_as_n->name+" "+peer_as_n->value+";");

		//-----------------------------------local-address------->>>
		node* loc_add_n=find("local-address",nbr_n);
		if (loc_add_n==NULL)
			{
			nbr_n->err_gen("local-address should be configured");
			return -1;
		}
		
		if (s2ip(loc_add_n->value,ip)!=0)
			{
			loc_add_n->err_gen("that is not ip");
			return -1;
		}
		//-----------------------------------local-address-------|||		

		conf_str.push_back(loc_add_n->name+" "+loc_add_n->value+";");
		conf_str.push_back(loc_as_n->name+" "+loc_as_n->value+";");


		conf_str.push_back("\tfamily {");
		conf_str.push_back("\t\tipv4 unicast;");
		conf_str.push_back("\t}");
		
		conf_str.push_back("}");

	}

	//-------------------------------check config of bgp peers--------------------|||

	

	return 0;

}

//-----------------------------------

int BGP_CONF::apply_bgp_conf (BGP_CONF* old_bgp) // now could be rollback
	{
	//return 0 ;//##!!!!!!!!!!!!!!!!!

	std::sort(agr_arr.begin(),agr_arr.end());
	gen_agr_route();
	s_table.free_routes();
	s_table.add_routes(agr_arr);
	s_table.Generate_Tree(-1);
	uint32_t ag_r_ind;

//--------------------------------------------synchronize agr_routes with clients---->>
	for (int i=0;i<size_cl_ip_ht;i++)
		{
		if (cl_ip_sess_dt[i].state!=1) continue;
	
		ag_r_ind=s_table.get_arp_index_for_ip(cl_ip_sess_dt[i].ip,0);

		if ((ag_r_ind==0xFFFFFFFF)&&(cl_ip_sess_dt[i].bgp_route==0))
				{
				//need to anonce /32 route 
				bgp_pipe<<std::string("announce route "+ip2s(cl_ip_sess_dt[i].ip)+"/32"+" next-hop self")<<endl;
				cl_ip_sess_dt[i].bgp_route=1;
		}
		if (ag_r_ind!=0xFFFFFFFF)
			{

				if (cl_ip_sess_dt[i].bgp_route!=0)
					{
					bgp_pipe<<std::string("withdraw route "+ip2s(cl_ip_sess_dt[i].ip)+"/32")<<endl;
					cl_ip_sess_dt[i].bgp_route=0;
				}
			agr_arr[ag_r_ind].count++;
		}
	}
//--------------------------------------------synchronize agr_routes with clients----||

	update_coll(agr_arr,old_bgp->agr_arr);
	//--delete unused
	for (auto ag_r= old_bgp->agr_arr.begin();ag_r!=old_bgp->agr_arr.end();ag_r++)
		{
		
		if ((ag_r->state==-1)&&(ag_r->count!=0))
			{
			bgp_pipe<<std::string("withdraw route "+Netw2s(ag_r->net))<<endl;
		}
		ag_r->state=0;
	}
	//-- add new one
	for (auto ag_r= agr_arr.begin();ag_r!=agr_arr.end();ag_r++)
		{	
		
		if ((ag_r->state==1)&&(ag_r->count!=0))
			{
			bgp_pipe<<std::string("announce route "+Netw2s(ag_r->net)+" next-hop self")<<endl;
		}
		ag_r->state=0;
	}

//======================

	//-----------------------------------------------------CHANGE BGP CONFIG FILE----------------->>>

	ofstream bgp_conf_f;
	bgp_conf_f.open("bgp_config",ifstream::out); //ifstream::in
	if (!bgp_conf_f.is_open()) 
		{
		printf("Can't open bgp_config file");
		return 0;
	} 

	conf_str.push_back("");
	conf_str.push_back("\tprocess announce-routes {");
	conf_str.push_back("\t\trun bgp/br_ex_scr.sh;");
	conf_str.push_back("\t\tencoder text;");
	conf_str.push_back("\t}");

	for (int i=0;i<conf_str.size();i++)
		{
		bgp_conf_f<<conf_str[i]<<endl;

	}
		
	bgp_conf_f.close();
	//-----------------------------------------------------CHANGE BGP CONFIG FILE-----------------||


	std::lock_guard<std::mutex> lock(mut);//for interaction with exabgp

	bgp_pipe<<std::string("reload")<<endl;

	string s_res;

	if (get_bgp_responce(s_res,200)!=0)
		{
		printf("Exabgp don't respond\n");
		return -1;
	}

	if (s_res!="reload in progress")
		{
		printf("Exabgp couldn't reload config\n");
		return 0;
	}
	
}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------


int BGP_CONF::add_client (uint32_t ip_hash)
	{
		//return 0 ;//##!!!!!!!!!!!!!!!!!

		CL_IP_SESS* cl=cl=cl_ip_sess_dt+ip_hash;
		uint32_t ag_r_ind;
		ag_r_ind=s_table.get_arp_index_for_ip(cl->ip,0);

		if ((ag_r_ind==0xFFFFFFFF)&&(cl->bgp_route==0))
				{
				//need to anonce /32 route 
				bgp_pipe<<std::string("announce route "+ip2s(cl->ip)+"/32"+" next-hop self")<<endl;
				cl->bgp_route=1;
		}
		else	{
			if (agr_arr[ag_r_ind].count==0)
				{
				bgp_pipe<<std::string("announce route "+Netw2s(agr_arr[ag_r_ind].net)+" next-hop self")<<endl;
			}
			agr_arr[ag_r_ind].count++;
		}


}
//------------------
int BGP_CONF::del_client (uint32_t ip_hash)
	{
		//return 0 ;//##!!!!!!!!!!!!!!!!!

		CL_IP_SESS* cl=cl=cl_ip_sess_dt+ip_hash;
		int32_t ag_r_ind;
		ag_r_ind=s_table.get_arp_index_for_ip(cl->ip,0);

		if ((ag_r_ind==0xFFFFFFFF)&&(cl->bgp_route==1))
				{
				//need to anonce /32 route 
				bgp_pipe<<std::string("withdraw route "+ip2s(cl->ip)+"/32")<<endl;
				cl->bgp_route=0;
		}
		else	{
			if (agr_arr[ag_r_ind].count>0)
				{
				agr_arr[ag_r_ind].count--;
				if (agr_arr[ag_r_ind].count==0)
					{
					bgp_pipe<<std::string("withdraw route "+Netw2s(agr_arr[ag_r_ind].net))<<endl;
				}
			}
		}

}
//--------------------
void BGP_CONF::shutdown ()
	{
		//return 0 ;//##!!!!!!!!!!!!!!!!!

		bgp_pipe<<std::string("shutdown")<<endl;

		bgp_pipe.close();

	return;
}

