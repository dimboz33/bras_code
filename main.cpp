
#define MAIN_FILE

#include <inttypes.h> 
uint8_t G_config_bit=1;// volatile  0 or 1 to change configuration
uint8_t force_quit = 0;

#include "clients/clients.h"
#include "clients/dhcp.h"
#include  "chassis/bras_chassis.h"
#include "timers/timers_array.h"
#include "arp/arp.h"
#include "routing/interfaces.h"
#include "routing/route.h"
#include "routing/ini_rou_int_arp.h"
#include "routing/routes_resolving.h"
#include "config/mnode.h"
#include "config/node.h"
#include "radius/rad_servers.h"
#include "radius/rad_client.h"
#include "radius/rad_coa.h"
#include "filter/filter_extern.h"
#include "filter/service.h"
#include "filter/policer.h"
#include "system_call/cli.h"
//--------------------for thread generation-->>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
//--------------------for thread generation--||
#include <fcntl.h>//for O_RDONLY constant
#include <csignal>///for SIG_TERM handling
#include <thread>

//-------------------
//-------------------
//-------------------
//-------------------

//----------------------------------------------------part of global variables--------------------->>>

volatile uint32_t  G_first_config=1; //volatile



char config_filename[]="config2";

int syscall_pid=0;

int RANDOM_DATA_FILE;

std::ofstream file_log;
//---------------------------------------------------part of global variables----------------------|||


//-------------------
//-------------------
uint8_t ADD_CLIENT_BY_PACKET=0;
//-------------------
//-------------------

//-------------------------------------------------------FUNCTIONS--------------------------------->>>>

int server_cli_main();

uint8_t reload_config ();

int pre_load_config();

//----------------------------
void free_NO_dpdk_data ()
	{

	printf("Releasing C++ data structures ...\n");		

	//---free data tables-->

	if (cl_l2_dt!=NULL)	delete cl_l2_dt;
	if (cl_ip_dt!=NULL)	delete cl_ip_dt;	
	if (cl_dhcp_dt!=NULL)	delete cl_dhcp_dt;	

	///---free data tables--|

	//--delete date structers--->>

	for (int i=0;i<2;i++)
		{
		if  (G_log_ints_arr[i]!=NULL)	delete G_log_ints_arr[i];
		if  (G_phy_ints_arr[i]!=NULL)	delete G_phy_ints_arr[i];
		if  (G_for_table_arr[i]!=NULL)	delete G_for_table_arr[i];
	}

	if (G_timers_arr!=NULL) delete G_timers_arr;

	//--delete c++ date structers---||

	printf("Done\n");


}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

void signalHandler( int signum )
	{
	G_bgp_conf->shutdown();
	close_shad_int();
	force_quit=1;

//	free_dpdk_data();//it should stop other CORES first of all 
//	free_NO_dpdk_data();
/*
	delete_cli_queue();

   	printf("RECEIVE SIGNAL number %d",signum);
   	// cleanup and close up stuff here  
   	// terminate program  

	

	//file_log.close();


	signal(signum, SIG_DFL);

	if (syscall_pid!=0)   kill(syscall_pid, signum);
		
	raise(SIGTERM); 
*/ 

}



//-------------------------------------------------------------------------------FUNCTIONS----------------|||

//--------------------------------
//--------------------------------
//--------------------------------
//--------------------------------
//--------------------------------
//--------------------------------

//===================================================================================MAIN==function=====>>>

int main(int argc, char **argv)
{

//register exit functions
//atexit(free_NO_dpdk_data);
//atexit(free_dpdk_data);


//--allocate data for routing structers----------------------------------------->>>

G_log_ints_arr[0]=new LogInt;G_log_ints_arr[1]=new LogInt;    //--------------------for PFE module
G_phy_ints_arr[0]=new PhyInt;G_phy_ints_arr[1]=new PhyInt;    //--------------------for PFE module

G_for_table_arr[0]=new ForTabTre;G_for_table_arr[1]=new ForTabTre;//--------------------for PFE module

G_for_int_table_arr[0]=new ForTabTre;G_for_int_table_arr[1]=new ForTabTre;//--------------------for PFE module


G_timers_arr=new timers_array(size_cl_ip_ht*5,1,3600*24);//arp;dhcp_rebind;rad_acc;+anything

//--allocate data for routing structers-----------------------------------------|||



RANDOM_DATA_FILE=open("/dev/urandom",O_RDONLY);// presodorandom// for truly random we must use random


//------------------------------------------------------------------------------------open restart counter-->>
std::string restart_count_s;//restart counter string 

fstream start_count_file("restart_counter.txt", fstream::in);

if (start_count_file.good())
	{
	if (!(getline(start_count_file,restart_count_s)))
		{
		restart_count_s="0";
	}
	start_count_file.close();
}

file_log.open("bras_log",fstream::out);
if (!file_log.good())
	{
	printf("could not open log file for writing\n");
	return -1;//bad new config
}


::uint64_t rest_counter=0;

try {rest_counter=stoi(restart_count_s);}catch(...){}

rest_counter=(rest_counter+1)&(0xFF);

fstream start_count_file1("restart_counter.txt", fstream::out|  std::fstream::trunc);
start_count_file1<<rest_counter;
start_count_file1.close();

CL_IP_SESS::curr_M_sess=rest_counter<<(64-8);

//----------------------------------------------------------------------------------------open restart counter--||




string s;

//------------------------------load config_tree--->>

ifstream file_model;
file_model.open("model_tree_config1", ifstream::in);
s=Mnext_good_str(file_model);
model_tree=Mnode(s,file_model);
file_model.close();

//------------------------------load config_tree---||



//--------------------------------find filename in arguments--------->>


int i_file=-1;

for (int i=1;i<argc;i++)
	{
	if (strcmp(argv[i],"-f")==0) i_file=i; //-f -- file
		
}
if (i_file>0)
	{
	strcpy(config_filename,argv[i_file+1]);

//----------------------------------delete file from arguments-------->>>

	for (int i=i_file+2;i<argc;i++)
		{
		argv[i-2]=argv[i];
	}
	argc=argc-2;

}

//---------------------------------------------------------------------|||





phy_int_table[0].resize(Phy_Int_Count+1); // only two interfaces 
//xe-0 and xe-1
phy_int_table[1].resize(Phy_Int_Count+1); // only two interfaces 
//xe-0 and xe-1



//////////////////////////////////////////////

///----------initializing DATA TABLES------>

cl_l2_dt=	new CL_L2_DAEL[size_cl_l2_ht]();
cl_ip_dt=		new CL_IP_DAEL[size_cl_ip_ht]();
cl_dhcp_dt=	new CL_DHCP_DAEL[size_cl_dhcp_ht]();

cl_ip_sess_dt= 	new CL_IP_SESS[size_cl_ip_ht]();



///----------initializing DATA TABLES-----|

if ( pre_load_config ()!=0) return -1;
///configuration should be to read configu of each port type
//set pointer of mac for chassis to fill them



//-------------------------------------------------generate system command thread---->>

pthread_t tid;

int key=0x11041988;//getpid();

int G_qid=msgget(key,IPC_CREAT| 0666);

if (G_qid==-1)	return -1;

syscall_pid=fork();


//system("./system_call/bras_system_call");



if (syscall_pid==0)
	{
	int a=execl("./system_call/bras_system_call","bras_system_call",NULL);
	printf("Can't run syscall process!!!!!!");
	exit(-1);
}



signal(SIGTERM, signalHandler);
signal(SIGINT, signalHandler);// --- haning at ctrl+c
signal(SIGQUIT,signalHandler);//-----haning at ctrl+c


//-------------------------------------------------generate system command thread------||


//---------------------------------------generate cli server thread------------------------->>>

	std::thread(server_cli_main).detach();

//---------------------------------------generate cli server thread-------------------------|||

int a= main_bras_chassis(  argc,argv,PPhy_Int_Count);


return 0;
} 

