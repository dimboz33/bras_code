#include <string.h>
#include <stddef.h>
#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <cstddef>//for NULL


#include "server.h"

#ifndef C_NODE_CLASS


#define C_NODE_CLASS

/*
f_n -funtion name
exc_t (+/-); default is + that meance exc_t =0; childrent could supplement each other

// 1 - node with necessarily value;//0-node without values; // 2 with values or with childs
type (t0,t1,t2);	// if category has NO child - default type 1;
			// if categoty has    child - default type 0;
			

//@invisible@ -> state=1;

//if child needed for function set "@child@" word

level (l0,l1,l2);


rule is : category_name { (exc_t)(type)f_n==
		or
	    categoty_name (exc_t)(type)f_n==

//----------------------
AAA	{ - 0 

	BBB	{ + 0 @child@ f_n=="AAA_BBB_function"

		ccc t1

		ddd t1

		eee t1

		eeee {	@invisible@




		}
	}
}	
//---------------------

*/

struct C_NODE //c - meance command node
	{
	std::string name;
	std::string value;

	int exc_t;//1-childrens are mutial exclusive; 0-childrens could supplement each other
	

	std::string func_name; //  fuction 
	int func_ind;//number of function that process such request in array of command functions
				//-1 nothing no function
	//int func_type;// 0 - no childred is requered; 1 - any children is requered;

	int type;// 1 - node with necessarily value;//0-node without values;

	int state ;//1 -node is precent; 0 - is absent;

	int func_child;//1- child needed for function 0 - default value

	C_NODE* next;
	C_NODE* child;
	C_NODE* parent;

	C_NODE* f_child;//filled child;

	int level;// default 1 ; 2 - detail/terse/count/; 3 no-more

C_NODE* get_child_node();//for construction of tree

C_NODE* get_next_node();//for construction of tree

C_NODE* find_child(std::string name); // find, child

//C_NODE();

~C_NODE();

};
//-------------------------------------

C_NODE* copy_node(const C_NODE* node);

int find_new_node(std::string s, C_NODE* comm_tree,std::vector<C_NODE*> &match_node,int lev);
C_NODE* load_node(std::ifstream& file);

int hint_node(C_NODE* node,std::vector<std::string> &res,int lev);






//------------------------------------

#endif