
#ifndef CLI_SESS_CLASS


#define CLI_SESS_CLASS


#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sys/stat.h>   //mkfifo
#include <string>
#include <sys/stat.h> // for checking file exist

#include <unistd.h>   // for mutex
#include <pthread.h>  // for threading
#include <unordered_map>
#include <signal.h>   //for SIGPIPE handler
//---------------------------------------------

#include "tree.h"

int const max_line_n=20;


struct CLI_SESS
	{
int func_ind;
C_NODE* arg_node; 
int more;//0 no-more //1 - --more--

std::string path;//key path to cli_pipe

std::fstream* cl_p;
std::ifstream* s_p;


int line_n;// line number

CLI_SESS ()
	{
cl_p=NULL;
s_p=NULL;
arg_node=NULL;
line_n=0;
}
~CLI_SESS ()
	{
//if (cl_p) delete cl_p;
//if (s_p) delete s_p;
//if (arg_node) delete arg_node;
line_n=0;
}


//----------------------------------------
void send_line (const std::string &s);

void read_line ( std::string &s);

void send_end ();

};

#endif