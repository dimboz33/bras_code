
#include <vector>

#include "../filter/service.h"

//------------------------
struct SUB_PARAM //CLI parameter
	{
int fast_find;// 1 - find small group

std::string name;
std::string value;


//--processed values--->>

std::string str;
uint16_t _2b;
uint32_t _4b;
::uint64_t _8b;

//--processed values---||


int (SUB_PARAM::* p_d)();				//process data //SUB_PARAM::
int (SUB_PARAM::* get_cl)(std::vector<int>& );	//find appropriate array of clients
int (SUB_PARAM::* check)(int32_t cl_id);

//------------------------------functions of each parameter--->>>-
int p_username();
int p_ip();
int p_sess_id();
int p_mac();

int p_vlan();
int p_service();

int get_cl_username(std::vector<int>& cl_arr);
int get_cl_ip(std::vector<int>& cl_arr);
int get_cl_sess_id(std::vector<int>& cl_arr);
int get_cl_mac(std::vector<int>& cl_arr);

int (check_username)(int32_t cl_hash);
int (check_ip)(int32_t cl_hash);
int (check_sess_id)(int32_t cl_hash);
int (check_mac)(int32_t cl_hash);
int (check_vlan_int)(int32_t cl_hash);
int (check_vlan_ext)(int32_t cl_hash);
int (check_service)(int32_t cl_hash);

//------------------------------functions of each parameter---|||


};

//-----------------------------
