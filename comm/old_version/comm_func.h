

#ifndef COMM_FUNC_CLASS


#define COMM_FUNC_CLASS

//----------------------------------

struct COMM_PROC_RESULT {

C_MODE* arg_node;

vector<std::string> s_arr;// string array

int func_ind;//-1 - no found

int fast; //slow output monitor or -more- 0 ; 1 - fast output

int more;// 0 - meance no-more; 1 meance more

}

struct COMM_FUNC {

int (*func)(C_NODE* arp_node, CLI_SESS cli_sess);

std::string name;

///int more_sw; // 0 - no more ; 1 more 

int fast; // 0 meance no amy fast output? like monitor
	  // 1 meance always output fast
	  // -1 depend on more_sw spesification 

}
//--------------------

int show_interface 	(C_NODE* arg_node,CLI_SESS c_s);
int show_arp 		(C_NODE* arg_node,CLI_SESS c_s);
int show_route 		(C_NODE* arg_node,CLI_SESS c_s);
int show_subscribers 	(C_NODE* arg_node,CLI_SESS c_s);
int monitor_interface 	(C_NODE* arg_node,CLI_SESS c_s);
int monitor_subscriber 	(C_NODE* arg_node,CLI_SESS c_s);

//--------------------

#ifndef COMM_FILE_CPP

extern vector<COMM_FUNC> G_comm_func_arr;

#else

vector<COMM_FUNC> G_comm_func_arr ={
{
.name=show_interface,
.func=&show_interface,
.fast=1,
},
{
.name=show_arp,
.func=&show_arp,
.fast=1,
}};

#endif

#endif