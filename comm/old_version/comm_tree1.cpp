#include <string.h>
#include "comm_tree.h"

C_NODE* C_NODE::get_child_node()
	{
	if (this->child!=NULL) return this->child;
	else	{
		return this->get_next_node();

	}

}

//--------------------------------------

C_NODE* C_NODE::get_next_node()
	{

	if ((this->next!=NULL)&&( (this->parent->exc_t==0)||(this->parent->f_child==0) )) return this->next;
	else	

		if (this->parent==NULL) return NULL;
		else {
			return (this->parent->get_next_node());
		}
	}
}

//------------------------------------------------------------------------------

C_NODE* find_new_node(std::string s, C_NODE* comm_tree)
	{

	for (C_NODE* curr_node=comm_tree;curr_node!=NULL;) // curr_node==NULL means we have run  all command tree
		{

		if (curr_node->state==0)
			{
			if (curr_node->name==s) return curr_node; //node is NOT exist

			else
				{
				curr_node=curr_node->get_next_node();
				continue;
			}
		}
		else	{
						//node is     exist
			if (curr_node->exc_t==1) //child is mutial exclusive				
				{
				if (curr_node->f_child!=NULL) {
					curr_node=curr_node->f_child; //one child is present
					continue;
				}
				else	{ //no any child is present
							//should tries to find node among childs
							curr_node=curr_node->get_child_node();
							continue;
					
				}
			}
			else
				{//could be many childs
				curr_node=curr_node->get_child_node();
			}

		}

	}
}

//------------------------------------------------------------------------------

C_NODE* copy_node(C_NODE* node)
	{
	C_NODE* node1=new C_NODE;
		
	*node1=*node;

	C_NODE* ch_pre=NULL;		
	for (C_NODE* ch_n=node1->child;ch_n!=NULL;ch_n=ch_n->next)
		{
		C_NODE* ch_n1=copy_node(ch_n);

		if (ch_pre!=NULL)	ch_pre->next=ch_n1;
		else			node1->child=ch_n1;

		ch_n1->parent=node1;
		if (ch_n==node1->f_child) node1->f_child=ch_n1;
		ch_pre=ch_n1;
	}
	return node1;

}

//------------------------------------------------------------------------------

int  hint_node(C_NODE* node,std::vector<std::string> &res)
	{

	if (node->state==0) {
		res.push_back(node->name);
		return 0;
	}
	if ((node->exc_t==1)&&(node->f_child!=NULL)) return hint_node(node->f_child,res);

	for (C_NODE* ch_n=node->child;ch_n!=NULL;ch_n=ch_n->next)
		{
		hint_node(ch_n,res);
	}
return 1;
}




//------------------------------------------------------------------------------


/*

int load_comm_tree (std::string input, std::string &result, c_node* comm_tree )
	{
	//comm_tree - command tree should be copied from model for many threads couldn't 

	curr_node=comm_tree;

	while (input!="")
		{
	}
}

//----------------hint case---->>
std::string hint_function (std::string input)  //return hint of word interleved by spaces
	{

}
//----------------hint case----||

*/