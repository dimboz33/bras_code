
#ifndef CLI_SESS_CLASS


#define CLI_SESS_CLASS


#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sys/stat.h>   //mkfifo
#include <string>
#include <sys/stat.h> // for checking file exist

#include <unistd.h>   // for mutex
#include <pthread.h>  // for threading
#include <unordered_map>
#include <signal.h>   //for SIGPIPE handler
//---------------------------------------------





struct CLI_SESS
	{

std::string comm; //command
std::string path;//key path to cli_pipe
std::fstream cl_p;
std::ifstream* s_p;

int line_n;// line number

CLI_SESS ()
	{
line_n=0;
}

//----------------------------------------
void send_line (std::string &s)
	{

	(cl_p) << s << std::endl; line_n++;	
	if (line_n<max_line_n) return;
	
	//need to send more an wait recponce from client
	(cl_p) << "@@@more" << std::endl;

	std::string res;
	int state=0;

	while (state==0) 
		{
		std::string path_c;
		std::getline(*s_p, res );
		std::string comm_c;

		if (parce_request( res, path_c,comm_c)!=0) continue;

		if (comm_c=="more") state=1;
		if (comm_c=="buy")  state=-1;
	}
	if (state==-1) throw -1;//end of pipe
	line_n=0; //continue print result
}

//---------------------------

void send_end ()
	{
	cl_p << "@@@end"  << std::endl;

	std::string res;

}


};

#endif