#include "comm_tree.h"

using namespace std;

int main(int argc, char **argv)	
	{

ifstream f_model_comm_tree;

f_model_comm_tree.open("model_comm_tree",ifstream::in);

if (!f_model_comm_tree.is_open()) 
	{
	printf("can't open file model_comm_tree");
}

C_NODE* m_c_tree=load_node(f_model_comm_tree);


C_NODE* m_c_tree1=copy_node(m_c_tree);


delete m_c_tree;


std::string s="monitor subscriber sess_id username show";


boost::regex reg_w("\\S+");

boost::sregex_token_iterator iter_word(s.begin(), s.end(), reg_w, 0);
boost::sregex_token_iterator iter_end;

std::string pro_str;

int is_text=0;

int is_error=0;

std:string f_name="";

//for(;iter_word!=iter_end;++iter_word)

std::vector<C_NODE*> match_node;

C_NODE* n_node;

std::string word;

for (int i=1;i<argc;i++)
		{

		word=std::string(argv[i]);//(*iter_word).str();
		
		if (is_text==0)
			{
			find_new_node(word,m_c_tree1,match_node);
			if (match_node.size()!=1)
				{
				is_error=1;
				break;
			}
			n_node=match_node[0];

			n_node->state=1;
		
			n_node->parent->f_child=n_node;


			if (n_node->type==1) is_text=1;

			pro_str+=n_node->name+" ";
		}
		else
			{
			n_node->value=word;
			is_text=0;

			pro_str+=word+" ";

		}
		if (n_node->func_name!="") f_name=n_node->func_name;

}
if (f_name=="") is_error=1;



printf("%s\n", pro_str.c_str());




	printf ("hints:\n");
	
	if (is_text==1) printf("<text>\n");
	else
		{

		if (match_node.size()<2) // error in text or full command
			{
			//should generate array of hints
			std::vector<std::string> hint_s;

			hint_node(m_c_tree1,hint_s);

			for (int i=0;i<hint_s.size();i++)
				{
				printf("%s\n",hint_s[i].c_str());

			}
		}
		else
			{
			for (int i=0;i<match_node.size();i++)
				{

				printf("%s\n",match_node[i]->name.c_str());

			}
						

		}

	}



printf("function is : %s\n",f_name.c_str());



return 0;

}


//-------------------------------------------------------------------------

int comm_process(	COMM_PROC_RESULT &res,
			const C_NODE* m_node,
			 	// model witch should be copied before searching
				// and after searching the copy should be deleted

			std::string in_str,//input string 
			int type_req,//0 - completion ; 1 - function 
			)

	{

	C_NODE* cp_node=copy_node(m_node);

	
	boost::regex reg_w("\\S+");

	boost::sregex_token_iterator iter_word(s.begin(), s.end(), reg_w, 0);
	boost::sregex_token_iterator iter_end;

	std::string pro_str;

	int is_text=0;

	int is_error=0;

	std:string f_name="";

	std::vector<C_NODE*> match_node;

	res.func_ind=-1;

	for(;iter_word!=iter_end;++iter_word)
		{
		word=(*iter_word).str();

		if (is_text==0)
			{
			find_new_node(word,m_c_tree1,match_node);
			if (match_node.size()!=1)
				{
				is_error=1; //wrong node or many variants
				break;
			}
			cp_node=match_node[0];

			cp_node->state=1;
		
			cp_node->parent->f_child=cp_node;


			if (cp_node->type==1) is_text=1;

			pro_str+=cp_node->name+" ";
		}
		else
			{
			cp_node->value=word;
			is_text=0;

			pro_str+=word+" ";

		}
		if (!(cp_node->func_ind<0))
			{
			f_name=cp_node->func_name;
			res.func_ind=cp_node->func_ind; //set function index

		}

	}


	//------------------proceess bad cases or complete cases-->>>

	if (is_text==1) 
		{
		res.s_arr.push_back("<text>");
		return -1;//meance no active function

	}



	if (is_error==1)
		{
		if (match_node.size()==0) //wrong node value
			{
			//should print good part and variants

			res.s_arr.push_back(pro_str+":");
			hint_node(cp_node,res.s_arr);


		}
		else		//many variants
			{

			for (int i=0;i<match_node.size();i++)
				{
				res.s_arr.push_back(match_node[i]->name);
			}

		}

		return -1;//meance no active function

	}
	else
		{ //tree loaded without problems

		if ((f_name="")||(type_req==0)) // completion or function is not found yet
			{
			if (f_name!="") res.s_arr.push_back("<Enter>");

			hint_node(cp_node,res.s_arr);

			return -1;//meance no active function
		}

	}
	//------------------proceess bad cases command or complete cases--||

	//---good command

	//res.func_ind is ok
	res.more=1; //no any command no-more by default

	res.fast=(G_comm_func_arr[i].fast<0)?(1-res.more):G_comm_func_arr[i].fast;
	return 0;

}
