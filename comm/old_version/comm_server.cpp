#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sys/stat.h>   //mkfifo
#include <string>
#include <sys/stat.h> // for checking file exist

#include <unistd.h>   // for mutex
#include <pthread.h>  // for threading
#include <unordered_map>
#include <signal.h>   //for SIGPIPE handler
//---------------------------------------------

#include "comm/cli_sess.h"

int max_line_n=50;

int parce_request(      std::string s,
                        std::string &path,
                        std::string &command);


struct CLI_SESS
	{

std::string comm; //command
std::string path;//key path to cli_pipe
std::fstream cl_p;
std::ifstream* s_p;

int line_n;// line number

CLI_SESS ()
	{
line_n=0;
}


void send_line (std::string &s)
	{

	(cl_p) << s << std::endl; line_n++;	
	if (line_n<max_line_n) return;
	
	//need to send more an wait recponce from client
	(cl_p) << "@@@more" << std::endl;

	std::string res;
	int state=0;

	while (state==0) 
		{
		std::string path_c;
		std::getline(*s_p, res );
		std::string comm_c;

		if (parce_request( res, path_c,comm_c)!=0) continue;

		if (comm_c=="more") state=1;
		if (comm_c=="buy")  state=-1;
	}
	if (state==-1) throw -1;//end of pipe
	line_n=0; //continue print result
}
void send_end ()
	{
	cl_p << "@@@end"  << std::endl;

	std::string res;



}

};

//------------------------------------------

void command_processing(CLI_SESS *c_s)
	{
for (int i=0;i<101;i++)
	{
std::string aa=c_s->comm+std::to_string(i);
c_s->send_line(aa);

//sleep(1);
}

c_s->send_end();

return;
}

//------------------------------------------
void sig_pipe_handler (int signal)
	{

throw -1;
return;
}

//------------------------------------------
void* cli_sess_process (void * arg)
	{
std::string path,command, input_new_pipe;
std::string s;
s=*((std::string*)arg);
if (parce_request(s,path,command)!=0) return NULL;

CLI_SESS c_s; //current session

input_new_pipe=path+"_server_input";

mkfifo(input_new_pipe.c_str(),0666);

chmod(input_new_pipe.c_str(), 0666);

signal(SIGPIPE, sig_pipe_handler);

c_s.cl_p.open(path.c_str(), std::ifstream::out);

(c_s.cl_p) << "@@@server_pipe " << input_new_pipe << std::endl;

c_s.s_p=new std::ifstream; //input stream for requests

c_s.s_p->open((input_new_pipe.c_str()),std::ifstream::in | std::ifstream::out);

//try to open cl pipe for output 
//c_s.cl_p=new std::ofstream;



c_s.comm=command;

//send redirect to client


//function of command
try {

command_processing(&c_s);


}
catch (...) 	{
//something goes wrong
}

c_s.cl_p.close();
c_s.s_p->close();
delete c_s.s_p;

remove(input_new_pipe.c_str());
remove(path.c_str());
return NULL;
}
//-------------------------------------------------------------

//-------------------------------------------------
int parce_request(	std::string s,
			std::string &path,
			std::string &command)
	{
int n=0;
if ((n=s.find("@@@"))==std::string::npos) return -1;

path=s.substr(0,n);
command=s.substr(n+3,s.length());
return 0;
}
//-------------------------------------------------

int main(int argc, char **argv)
{
std::string p_s_name="/tmp/p_serv";

while (true)
	{
mkfifo(p_s_name.c_str(),0666);

chmod(p_s_name.c_str(), 0666);

std::ifstream pipe_serv; //input stream for requests

pipe_serv.open(p_s_name.c_str(),std::ifstream::in);

if (!pipe_serv.good())
	{
	printf("coldn't open pipe_server\n");
	return 1;
}
std::string s;

while (std::getline(pipe_serv, s ))//(std::getline(pipe_serv, s))
	{
	//std::cout << s << std::endl;
	std::string path,command;
	std::string* s1;
	
	if (parce_request(s,path,command)!=0) continue;

	//std::cout << s+"\n\n";

	std::cout << path+"###"+command +"\n";
	if (command=="buy") continue;
	if (command=="more") continue;


	s1=new std::string;
	*s1=s;
	pthread_t th_id;

	//pthread_setname_np(th_id, "client_thread");

	int res=pthread_create(&th_id, NULL, cli_sess_process , s1);
	//should be mutex
	int ind;
	std::string thread_name="cli_s";
	if ((ind=path.find_last_of("_"))>0)
		{
		thread_name+=path.substr(ind,10);
	}
	
	pthread_setname_np(th_id,thread_name.c_str());


	if (res != 0)
	{
		printf("pthread_create\n");
		return -1;
	}


}



pipe_serv.close();

}

return 0;
}