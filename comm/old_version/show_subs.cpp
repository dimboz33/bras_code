#include <boost/algorithm/string.hpp> 
#include <boost/regex.hpp>
#include <string>

#include "../filter/service.h"
#include "../routing/route.h"
#include "../clients/clients.h"

//------------------------
struct C_PARAM //CLI parameter
	{
int fast_find;// 1 - find small group

std::string name;
std::string value;

//--processed values--->>
std::string str;
uint16_t _2b;
uint32_t _4b;
::uint64_t _8b;

//--processed values---||

int (C_PARAM::* p_d)();				//process data //C_PARAM::
int (C_PARAM::* get_cl)(vector<int>& );	//find appropriate array of clients
int (C_PARAM::* check)(int32_t cl_id);

//------------------------------functions of each parameter--->>>
int C_PARAM::p_username();
int C_PARAM::p_ip();
int C_PARAM::p_sess_id();
int C_PARAM::p_mac();

int C_PARAM::p_vlan();
int C_PARAM::p_service();

int get_cl_username(vector<int>& cl_arr);
int get_cl_ip(vector<int>& cl_arr);
int get_cl_sess_id(vector<int>& cl_arr);
int get_cl_mac(vector<int>& cl_arr);

//------------------------------functions of each parameter---|||


}

//----------------------------------------------process data---------------------->>>>
int C_PARAM::p_username()
	{
	str=value;
	return 0;
}
//-----------------------------
int C_PARAM::p_ip()
	{
	if (s2ip(value,_4b)!=0) return -1;
	return 0;
}
//-----------------------------
int C_PARAM::p_sess_id()
	{
	if (sscanf(value.c_str(),"%016llX_%x",&_8b,&_2b)!=2)
		{
		if (sscanf(value.c_str(),"%016llX",&_8b)!=1)
			{
			return -2;/// couln't find such M_session_id
		}
		_2b=-1;
	}
	return 0;
}
//-----------------------------
int C_PARAM::p_vlan()
	{int i;
	if ((i=s2numb(value))<0) return -1;
	if (i>4095) return -1;
	_2b=i;
	return 0;
}
//-----------------------------
int C_PARAM::p_mac()
	{

	boost::smatch m;
	
	boost::regex mac_reg("([0-9,a-f,A-F]{2}.*){6}");
	boost::regex_search(s,m,w_reg);

	std::locale loc;
	value=boost::algorithm::to_upper_copy(value);

	if (!(boost::regex_match(value,mac_reg))) return -1;
	
	boost::sregex_token_iterator iter_mac(value.begin(), value.end(), ("[0-9,A-F]{2}"), 0);
	boost::sregex_token_iterator iter_end;
	int i=0;
	uint8_t* mac_add=(uint8_t*)(&_8b);
	for(;iter_mac!=iter_end;++iter_mac,i++)
		{
		if (sscanf((*iter_mac.str(),"%02X",mac_add[i]==1) continue;
		return -3;// error in programm
	}
	return 0;
}

//-----------------------------

int C_PARAM::p_service()
	{
	auto serv_id_f=G_serv_agr->h_t.find(value); // _f -> found
	if (serv_id_f==G_serv_agr->h_t.end()) return -2;//couldn't find service
	_2b=serv_id_f->second;

	return 0;
}


//----------------------------------------------process data----------------------|||

//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------

//----------------------------------------------GET_CLIENT-------------------------->>>>
int C_PARAM::get_cl_ip(vector<int>& cl_arr)
	{
	int32_t ip_hash=rte_hash_lookup(cl_ip_ht,&_4b);
	if (!(ip_hash<0)) cl_arr.push_back(ip_hash);
	return 0;
}
//------------------------------------------------------
int C_PARAM::get_cl_username(vector<int>& cl_arr)
	{
	std::lock_guard<std::mutex> lock(cl_ht.mut);
	auto find_value=cl_ht.username.find(str);
	if (find_value==cl_ht.username.end()) return -2;
	cl_arr.push_back(find_value->second);

}
//---------------------------------------------------
int C_PARAM::get_cl_mac(vector<int>& cl_arr)
	{
	std::lock_guard<std::mutex> lock(cl_ht.mut);

	if (cl_ht.mac.count(_8b)>(1<<14)) return -1; // to many clients
	auto find_value=cl_ht.mac.find(_8b);
	if (find_value==cl_ht.mac.end()) return -2;//no match
	for (auto find_value=cl_ht.mac.find(_8b);find_value!=cl_ht.mac.end();++find_value)
		{
		cl_arr.push_back(find_value->second);//print each user with that mac address
	}
}
//--------------------------------------------------------
int C_PARAM::get_cl_sess_id(vector<int>& cl_arr)
	{
	std::lock_guard<std::mutex> lock(cl_ht.mut);
	auto find_value=cl_ht.M_sess.find(_8b);
	if (find_value==cl_ht.M_sess.end()) return -2;
	cl_arr.push_back(find_value->second);
}
//----------------------------------------------GET_CLIENT--------------------------||||

//----------------------------------------------CHECK_CLIENTS------------------------------->>>>>
int (C_PARAM::check_username)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	if (cl_sess->username==str) return 0;
	return -1; //not match
}
//-------------------------------------------------------------------------
int (C_PARAM::check_ip)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	if (cl_sess->ip==_4b) return 0;
	return -1; //not match
}
//--------------------------------------------------------------------------
int (C_PARAM::check_sess_id)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	if (cl_sess->M_sess==_8b) return 0;
	return -1; //not match
}
//--------------------------------------------------------------------------
int (C_PARAM::check_mac)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;
	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;

	if (memcmp(_8b,cl_forw_in->mac,6)==0) return 0;
	return -1;
}


int (C_PARAM::check_vlan_int)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;
	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;

	if (cl_forw_in->vlan_in==_2b) return 0;
	return -1;
}
int (C_PARAM::check_vlan_ext)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;
	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;

	if (cl_forw_in->vlan_out==_2b) return 0;
	return -1;
}
int (C_PARAM::check_service)(int32_t cl_hash)
	{
	CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_hash;
	if (cl_sess->state==0) return -2;//means client is deleted
	CL_IP_DAEL *cl_ip=cl_ip_dt+cl_hash;
	CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;

	if (((cl_forw_in->service_bits)&(1<<_2b))!=0) return 0;
	return -1;

}

//----------------------------------------------CHECK_CLIENTS-------------------------------||||||