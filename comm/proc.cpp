#include "tree.h"
#include "func.h"



//-------------------------------------------------------------------------

int comm_process(	COMM_PROC_RESULT &res,
			const C_NODE* m_node,
			 			// model witch should be copied before searching
						// and after searching the copy should be deleted
			std::string in_str,	//input string 
			int type_req		//0 - completion ; 1 - function 
			)
	{

	C_NODE* cp_node=copy_node(m_node);

	
	boost::regex reg_w("\\S+");

	

	in_str=boost::regex_replace(in_str,boost::regex("\\t"),std::string(" "));


	boost::sregex_token_iterator iter_word(in_str.begin(), in_str.end(), reg_w, 0);
	boost::sregex_token_iterator iter_end;

	std::string pro_str="";

	int is_text=0;

	int is_error=0;

	std::string f_name="";

	std::vector<C_NODE*> match_node;

	C_NODE* fun_node;
	C_NODE* cur_node;//current node

	res.func_ind=-1;

	std::string word;

	int lev=0;

	int word_is_full=0;

	is_error=1;

	int is_last_space=0;
	
	char last_simb=in_str[in_str.length()-1];
	
	if (last_simb==' ') is_last_space=1;

	for(;iter_word!=iter_end;++iter_word)
		{

		is_error=0;
		word=(*iter_word).str();

		if (is_text!=1)
			{
			find_new_node(word,cp_node,match_node,lev);
			if ( (match_node.size()>1) || ((match_node.size()==0)&&(is_text==0)) )
				{
				is_error=1; //wrong node or many variants
				break;
			}

			if (match_node.size()==1)
				{

			cur_node=match_node[0];

			cur_node->state=1;
		
			cur_node->parent->f_child=cur_node;

			if (lev>0) lev=cur_node->level;

			//if (cur_node->type==1) is_text=1;
		
			is_text=cur_node->type;

			pro_str+=cur_node->name+" ";

			word_is_full=(cur_node->name==word)?1:0;
			}
			else
				{
				//case when value for type 2 
				cur_node->value=word;
				is_text=0;
				pro_str+=word+" ";
			}
		}
		else
			{


			cur_node->value=word;
			is_text=0;
			pro_str+=word+" ";

		}
		if (!(cur_node->func_ind<0))
			{
			lev=1;
			f_name=cur_node->func_name;
			res.func_ind=cur_node->func_ind; //set function index
			fun_node=cur_node;
		}

	}

	bool fun_not_valid=((f_name=="")||((fun_node->func_child==1)&&(fun_node->f_child==NULL)));


	//------------------proceess bad cases or complete cases-->>>

	





	if (is_error==1)
		{
		if (match_node.size()==0) //wrong node value
			{
			//should print good part and variants

			res.s_arr.push_back(pro_str+":");
			hint_node(cp_node,res.s_arr,lev);


		}
		else		//many variants
			{

			for (int i=0;i<match_node.size();i++)
				{
				res.s_arr.push_back(match_node[i]->name);
			}
			if (is_text==2) res.s_arr.push_back("<value>");


		}

		return -1;//meance no active function

	}
	else
		{ //tree loaded without problems

		if (fun_not_valid&&(type_req==1)) //  function is not found yet
			{
			res.s_arr.push_back(pro_str+":");
			hint_node(cp_node,res.s_arr,lev);

			return -1;//meance no active function
		}

		if (type_req==0) //completion 
			{
			if ((word_is_full==1)||(is_last_space==1))
				{

				if (is_text>0) res.s_arr.push_back("<text>");
				if (is_text!=1)
					{
					if (!fun_not_valid) res.s_arr.push_back("<Enter>");
					hint_node(cp_node,res.s_arr,lev);
				}
			}	
			else  //should complete word
				{
				res.s_arr.push_back(cur_node->name);

			}
			return -1;

		}


	}
	//------------------proceess bad cases command or complete cases--||


		


	//---good command
	res.arg_node=copy_node(fun_node);
	delete cp_node;
	//res.func_ind is ok


	COMM_FUNC*	curr_func=&G_comm_func_arr[0]+res.func_ind;


	if (curr_func->fast<0)
		{
		res.more=1;

		if (fun_node->find_child("no-more")!=NULL) res.more=0;

		 //no any command no-more by default
		res.fast=(1-res.more);
	}
	else	//in that case res.fast is determed by curr_func->fast value
		{
		res.fast=curr_func->fast;
		res.more=0;
	}
	

	res.fast=(curr_func->fast<0)?(1-res.more):curr_func->fast;
	return 0;

}
