#include "func.h"
#include "tree.h"
#include "server.h"
#include <string>

#include <arpa/inet.h> // for change bit order

#include "sub_param.h"

#include "../filter/service.h"
#include "../filter/policer.h"
#include "../clients/clients.h"

#include <unistd.h> // for usleep microsecond 

int is_client_match(int i,std::vector<SUB_PARAM> &sp_v);//from show_sub.cpp

int clear_subscribers	(CLI_SESS* c_s)
	{
std::vector<SUB_PARAM> sp_v;
SUB_PARAM sp;

//-------------------------------------------------LOAD_PARAMETERS--------->>>


sp.name="username";	sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_username;	sp.get_cl=&SUB_PARAM::get_cl_username;	sp.check=&SUB_PARAM::check_username;	
sp_v.push_back(sp);

sp.name="ip";		sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_ip;		sp.get_cl=&SUB_PARAM::get_cl_ip;		sp.check=&SUB_PARAM::check_ip;
sp_v.push_back(sp);

sp.name="sess_id";	sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_sess_id;	sp.get_cl=&SUB_PARAM::get_cl_sess_id;		sp.check=&SUB_PARAM::check_sess_id;
sp_v.push_back(sp);

sp.name="mac";		sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_mac;		sp.get_cl=&SUB_PARAM::get_cl_mac;		sp.check=&SUB_PARAM::check_mac;
sp_v.push_back(sp);

sp.name="vlan_int";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_vlan;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_vlan_int;
sp_v.push_back(sp);

sp.name="vlan_ext";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_vlan;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_vlan_ext;
sp_v.push_back(sp);

sp.name="service";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_service;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_service;
sp_v.push_back(sp);

//-------------------------------------------------LOAD_PARAMETERS---------|||


//-----------------------------------load param node and clear all not configured parameters from list
int param_command=0; //1 standart ; count -1 ;0 terse ; 2 detail; 

C_NODE* c_n;



for (auto c_p=sp_v.begin();c_p!=sp_v.end();	)
	{
	int is_match=0;
	for (C_NODE* c_n=c_s->arg_node->child;c_n!=NULL;c_n=c_n->next)
		{

		if ((c_p->name==c_n->name)&&(c_n->state==1)) 
			{
			is_match=1;
			c_p->value=c_n->value;
			break;
		}
	}
	if (is_match==0) c_p=sp_v.erase(c_p);
	else ++c_p;
}
//check data 

//----------------------------------process of data check consisency-------------->>>
for (int i=0;i<sp_v.size();i++)
	{
	int res=(sp_v[i].*(sp_v[i].p_d))(); //process of values
	if (res!=0)
		{
		c_s->send_line("The value "+ sp_v[i].value+ " of parameter " + sp_v[i].name+ " is wrong!!!");
		return -1;
	}
}
//----------------------------------process of data check consisency--------------|||


int total_db=1;//searching in all data base 


//-------------------------------------------select group of clients--------------->>
std::vector<int32_t> cl_arr;
if ((sp_v.size()>0)&&(sp_v[0].fast_find==1))
	{
	int ret=(sp_v[0].*(sp_v[0].get_cl))(cl_arr);	//(a.*(a.p_d))();
	if ((cl_arr.size()==0)&&(ret!=-1))		// -1 case when to many clients(mac case)
		{
		c_s->send_line("No clients found with parameter: "+ sp_v[0].name+ " = "+sp_v[0].value);
		return -1;
	}
	if (ret!=-1)
		{
		total_db=0;
	}
}
//-------------------------------------------select group of clients---------------||


int count_cl=0;


list<int> sel_subs;//selected subscribers

if (total_db==0)
	{
	for (int i1=0;i1<cl_arr.size();i1++)
		{
		int i=cl_arr[i1];
		if (is_client_match(i,sp_v))
			{
			count_cl++;
			sel_subs.push_back(i);
		}
		
	}
}
else
	{
	for (int i=0;i<size_cl_ip_ht;i++)
		{
		if (is_client_match(i,sp_v))
			{
			count_cl++;
			sel_subs.push_back(i);
		}
	}
}
std::string ans;


int q_n=3;//questinon number
int ans_flag=0;

while ((ans_flag==0)&&(q_n>0)) 
	{
	if (q_n==3) c_s->send_line("I found  "+std::to_string(count_cl)+" clients with such parameters. Do you really what to clear all of them? Y/N");
	if (q_n==2) c_s->send_line("Really??? You what to delete "+std::to_string(count_cl)+" subscribers??? Y/N");
	if (q_n==1) c_s->send_line("Ask you last time.Really? Y/N");

	c_s->read_line(ans);
	
	if ((ans=="Y")||(ans=="y")) q_n--;
	if ((ans=="N")||(ans=="n")) ans_flag=-1;
}

if (ans_flag!=0) return -1;

c_s->send_line("Start clearing of "+std::to_string(count_cl)+" clients");

int del_c=0;

int k=0;
int p=0;

for (list<int>::iterator c_cl=sel_subs.begin();c_cl!=sel_subs.end();c_cl++,del_c++,k++,p++)
	{
	del_cl(*c_cl,10);
	
	if (k==100)
		{
		c_s->send_line("Deleted "+std::to_string(del_c)+" clients");
		k=0;
		
	}
	if (p==3)
		{
		usleep(10000);//miscosecond
		p=0;
	}



}

/*
for (int i =0;i<30000;i++,k++,p++)
	{
	if (k==100)
		{
		c_s->send_line("Deleted "+std::to_string(i)+" clients");
		k=0;
		
	}
	if (p==1)
		{
		usleep(10000);//miscosecond
		p=0;
	}

}
*/
c_s->send_line("Deleted all "+std::to_string(del_c)+" clients");



return 0;
}