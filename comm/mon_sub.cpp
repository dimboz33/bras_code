#include "func.h"
#include "tree.h"
#include "server.h"
#include <string>

#include <unistd.h>//for sleep


#include "sub_param.h"


#include "../filter/service.h"

#include "../clients/clients.h"
#include "../routing/route.h"
#include <arpa/inet.h> // for change bit order
#include "../timers/timers_array.h"

#include "../filter/service.h"
#include "../filter/policer.h"

int is_client_match(int i,std::vector<SUB_PARAM> &sp_v);//from show_sub.cpp file

int monitor_one_client(int ip_hash,CLI_SESS* c_s)
	{

	auto cl_sess=cl_ip_sess_dt+ip_hash;

	if (cl_sess->state!=1) 		{
		c_s->send_line("no active client");
		return -1;
	}

	CL_IP_DAEL *cl_ip=cl_ip_dt+ip_hash;

	CL_INT_DAEL *cl_forw_in=cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
	CL_INT_DAEL *cl_forw_out=cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;


	CL_TRAFFIC_COUNTERS tr_co_out_prev[C_serv_count+1];
	CL_TRAFFIC_COUNTERS tr_co_inp_prev[C_serv_count+1];
	CL_TRAFFIC_COUNTERS tr_co_out[C_serv_count+1];
	CL_TRAFFIC_COUNTERS tr_co_inp[C_serv_count+1];

	CL_TRAFFIC_COUNTERS tr_co_out_diff[C_serv_count+1];
	CL_TRAFFIC_COUNTERS tr_co_inp_diff[C_serv_count+1];

	for (int i=0;i<C_serv_count+1;i++)
		{
		tr_co_inp_prev[i]=cl_forw_in->tr_co[i];
		tr_co_out_prev[i]=cl_forw_out->tr_co[i];
	}

	uint16_t vlan_in=cl_forw_in->vlan_in;
	uint16_t vlan_out=cl_forw_in->vlan_out;
	vlan_in=htons(vlan_in);
	vlan_out=htons(vlan_out);

	std::string interface_string=("interface:\t\txe-"+std::to_string(cl_ip->phy_int)+"."+std::to_string(vlan_out)+"."+std::to_string(vlan_in)); 


	char mac_s[6*2+1+6];//string of mac address

	for (int i1=0;i1<6;i1++)	
		{
		sprintf(mac_s+3*i1,"%02x",cl_forw_in->mac[i1]);				
		sprintf(mac_s+3*i1+2,":");				
	}
	mac_s[6*2+1+6-2]=' ';

	std::string mac_string=("mac:\t\t\t"+std::string(mac_s));


	sleep(1);

	while (1)
		{
		if (cl_sess->state!=1) 		
			{
			c_s->send_line("no active client");
			return -1;
		}	

		c_s->send_line("\f");
		c_s->send_line("");
		c_s->send_line("clients username: \t"+cl_sess->username); 
		c_s->send_line("");
		c_s->send_line(interface_string);
		c_s->send_line(mac_string);

		c_s->send_line("arp next request in \t\t"+std::to_string(G_timers_arr->timer_distance(cl_sess->arp_k_timer))+" s");
		c_s->send_line("arp failed requests \t\t"+std::to_string(cl_sess->arp_k_max_counter-cl_sess->arp_k_counter));
		c_s->send_line("maximum failed requests \t"+std::to_string(cl_sess->arp_k_max_counter));

		c_s->send_line("IP address:\t\t" + ip2s (cl_sess->ip));
		c_s->send_line("valid  for "+std::to_string(G_timers_arr->timer_distance(cl_sess->dhcp_rebind_timer))+" s");
		c_s->send_line("");
		c_s->send_line("");
		//---------

		string inp_b="\t";
		string inp_p="\t";
		string inp_bs="\t";
		string inp_ps="\t";

		string inp_pol="\t";


		string out_b="\t";
		string out_p="\t";
		string out_bs="\t";
		string out_ps="\t";

		string out_pol="\t";

		string serv_n="Service name\t|";


		//--------------------------------------------------------------------PRINT COUNTER------>>

		for (int i=0;i<C_serv_count+1;i++)
			{
			if (((cl_forw_in->service_bits&(1<<i))==0)&&(i<C_serv_count)) continue;

			tr_co_inp[i]=cl_forw_in->tr_co[i];
			tr_co_out[i]=cl_forw_out->tr_co[i];

			tr_co_inp_diff[i].pack_counter=tr_co_inp[i].pack_counter-tr_co_inp_prev[i].pack_counter;
			tr_co_out_diff[i].pack_counter=tr_co_out[i].pack_counter-tr_co_out_prev[i].pack_counter;


			tr_co_inp_diff[i].byte_counter=(tr_co_inp[i].byte_counter-tr_co_inp_prev[i].byte_counter)<<3;
			tr_co_out_diff[i].byte_counter=(tr_co_out[i].byte_counter-tr_co_out_prev[i].byte_counter)<<3;

			tr_co_inp_prev[i]=tr_co_inp[i];
			tr_co_out_prev[i]=tr_co_out[i];			


			if (i<C_serv_count)
				{
				uint16_t filter_inp = cl_forw_in->tr_co[i].policer;
				uint16_t filter_out=cl_forw_out->tr_co[i].policer;
				inp_pol+="|"+G_pol_agr->arr[filter_inp].name+"\t";//"speed "+std::to_string(G_pol_agr->arr[filter_inp].speed)+"kb/s\t";
				out_pol+="|"+G_pol_agr->arr[filter_out].name+"\t";//"speed "+std::to_string(G_pol_agr->arr[filter_out].speed)+"kb/s\t";
				serv_n+=" "+G_serv_agr->arr[i].name+"\t|";

			}
			else
				{
				serv_n+=" ALL Traffic\t|";
			}
	
			inp_p+="| "+std::to_string(tr_co_inp[i].pack_counter)+"\t";
			inp_b+="| "+std::to_string(tr_co_inp[i].byte_counter)+"\t";
			inp_ps+="| "+std::to_string(tr_co_inp_diff[i].pack_counter)+"p/s\t";
			inp_bs+="| "+std::to_string(tr_co_inp_diff[i].byte_counter)+"b/s\t";
			
			out_p+="| "+std::to_string(tr_co_out[i].pack_counter)+"\t";
			out_b+="| "+std::to_string(tr_co_out[i].byte_counter)+"\t";
			out_ps+="| "+std::to_string(tr_co_out_diff[i].pack_counter)+"p/s\t";
			out_bs+="| "+std::to_string(tr_co_out_diff[i].byte_counter)+"b/s\t";


		}
		//==============================
		c_s->send_line("\t\t\t"+serv_n);
		c_s->send_line("COUNTERS");
		c_s->send_line("==================================================");
		c_s->send_line("INPUT\tpolicer"+inp_pol);
		c_s->send_line("\t\tbytes\t"+inp_b);
		c_s->send_line("\t\tpackets\t"+inp_p);
		c_s->send_line("-------------------------------------------------------------------------------");
		c_s->send_line("OUTPUT\tpolicer"+out_pol);
		c_s->send_line("\t\tbytes\t"+out_b);
		c_s->send_line("\t\tpackets\t"+out_p);
		c_s->send_line("==================================================");
		c_s->send_line("");
		c_s->send_line("SPEEDS");
		c_s->send_line("==================================================");
		c_s->send_line("INPUT");
		c_s->send_line("\t\tbits/s\t"+inp_bs);
		c_s->send_line("\t\tpackets/s\t"+inp_ps);
		c_s->send_line("-------------------------------------------------------------------------------");
		c_s->send_line("OUTPUT");
		c_s->send_line("\t\tbits/s\t"+out_bs);
		c_s->send_line("\t\tpackets/s\t"+out_ps);
		c_s->send_line("==================================================");

		//-------------------------------------------------------------------------PRINT COUNTER-------||


		sleep(1);

	}
	
	return 0;

}




int monitor_subscriber (CLI_SESS* c_s)
	{

std::vector<SUB_PARAM> sp_v;
SUB_PARAM sp;


//-------------------------------------------------LOAD_PARAMETERS--------->>>


sp.name="username";	sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_username;	sp.get_cl=&SUB_PARAM::get_cl_username;	sp.check=&SUB_PARAM::check_username;	
sp_v.push_back(sp);

sp.name="ip";		sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_ip;		sp.get_cl=&SUB_PARAM::get_cl_ip;		sp.check=&SUB_PARAM::check_ip;
sp_v.push_back(sp);

sp.name="sess_id";	sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_sess_id;	sp.get_cl=&SUB_PARAM::get_cl_sess_id;		sp.check=&SUB_PARAM::check_sess_id;
sp_v.push_back(sp);

sp.name="mac";		sp.fast_find=1;
sp.p_d=&SUB_PARAM::p_mac;		sp.get_cl=&SUB_PARAM::get_cl_mac;		sp.check=&SUB_PARAM::check_mac;
sp_v.push_back(sp);

sp.name="vlan_int";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_vlan;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_vlan_int;
sp_v.push_back(sp);

sp.name="vlan_ext";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_vlan;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_vlan_ext;
sp_v.push_back(sp);

sp.name="service";	sp.fast_find=0;
sp.p_d=&SUB_PARAM::p_service;		sp.get_cl=NULL;					sp.check=&SUB_PARAM::check_service;
sp_v.push_back(sp);

//-------------------------------------------------LOAD_PARAMETERS---------|||



C_NODE* c_n;

for (auto c_p=sp_v.begin();c_p!=sp_v.end();	)
	{
	int is_match=0;
	for (C_NODE* c_n=c_s->arg_node->child;c_n!=NULL;c_n=c_n->next)
		{

		if ((c_p->name==c_n->name)&&(c_n->state==1)) 
			{
			is_match=1;
			c_p->value=c_n->value;
			break;
		}
	}
	if (is_match==0) c_p=sp_v.erase(c_p);
	else ++c_p;
}
//check data 

//===========================================================================================

//----------------------------------process of data check consisency-------------->>>
for (int i=0;i<sp_v.size();i++)
	{
	int res=(sp_v[i].*(sp_v[i].p_d))(); //process of values
	if (res!=0)
		{
		c_s->send_line("The value "+ sp_v[i].value+ " of parameter " + sp_v[i].name+ " is wrong!!!");
		return -1;
	}
}
//----------------------------------process of data check consisency--------------|||

int total_db=1;//searching in all data base 

//-------------------------------------------select group of clients--------------->>
std::vector<int32_t> cl_arr;
if ((sp_v.size()>0)&&(sp_v[0].fast_find==1))
	{
	int ret=(sp_v[0].*(sp_v[0].get_cl))(cl_arr);	//(a.*(a.p_d))();
	if ((cl_arr.size()==0)&&(ret!=-1))		// -1 case when to many clients(mac case)
		{
		c_s->send_line("No clients found with parameter: "+ sp_v[0].name+ " = "+sp_v[0].value);
		return -1;
	}
	if (ret!=-1)
		{
		total_db=0;
	}
}
//-------------------------------------------select group of clients---------------||


std::vector<int32_t> cl_arr2;

//---------------------find client that match all criterias

if (total_db==0)
	{
	for (int i1=0;i1<cl_arr.size();i1++)
		{
		int i=cl_arr[i1];
		if (is_client_match(i,sp_v))
			{
			cl_arr2.push_back(i);
			if (cl_arr2.size()>9) break;
		}
		
	}
}
else
	{
	for (int i=0;i<size_cl_dhcp_ht;i++)
		{
		if (is_client_match(i,sp_v))
			{
			cl_arr2.push_back(i);
			if (cl_arr2.size()>9) break;
		}
	}
}

if (cl_arr2.size()==0)
		{
		c_s->send_line("Couldn't find such client");
		return -1;
}
if (cl_arr2.size()>1)
	{
	c_s->send_line("I found many clients with such parameters");
	for (int i=0;(i<cl_arr2.size());i++ )
		{
		auto cl_sess=cl_ip_sess_dt+cl_arr2[i];
		if (cl_sess->state!=1) continue;
		c_s->send_line("username: \t"+cl_sess->username);
	}			
	c_s->send_line("I could monitor only one client for one cli session");
	return -1;					
}

monitor_one_client( cl_arr2[0],c_s);

return 0; 


}

