#include <string.h>
#include "tree.h"

C_NODE* C_NODE::get_child_node()
	{
	if (this->child!=NULL) return this->child;
	else	{
		return this->get_next_node();

	}

}

//--------------------------------------

C_NODE* C_NODE::get_next_node()
	{

	if ((this->next!=NULL)&&( (this->parent->exc_t==0)||(this->parent->f_child==0) )) return this->next;
	else	
		{
		if (this->parent==NULL) return NULL;
		else {
			return (this->parent->get_next_node());
		}
	}
}

//------------------------------------------------------------------------------
C_NODE* C_NODE::find_child(std::string name1) // find, child
	{ 

	for (C_NODE* ch_n=this->child;ch_n!=NULL;ch_n=ch_n->next)
		{
		C_NODE* x;
		if (ch_n->state==1)
			{
			if (ch_n->name==name1) return ch_n;
			if ((x=ch_n->find_child(name1))!=NULL) return x;
		}
	}
return NULL;

}



//--------------------------------------------------
bool level_pred(int level,int n_l)
	{
	if (level==0) return (n_l==1);		//no function spesified
	if (level==1) return 1;			//function spesified
	if (level>1)  return (n_l>level);	// l2 or l3 node is configured

}


//------------------------------------------------------------------------------

int find_new_node(std::string s, C_NODE* comm_tree,std::vector<C_NODE*> &match_node,int lev)
	{

	match_node.clear();

	for (C_NODE* curr_node=comm_tree;curr_node!=NULL;) // curr_node==NULL means we have run  all command tree
		{

		if (curr_node->state==0)
			{
			if (curr_node->name.find(s,0)==0) 
				{

				if  (level_pred(lev,curr_node->level)) match_node.push_back(curr_node);
				//node is NOT exist
				
			}
			
			curr_node=curr_node->get_next_node();
			continue;
			
		}
		else	{
						//node is     exist
			if (curr_node->exc_t==1) //child is mutial exclusive				
				{
				if (curr_node->f_child!=NULL) {
					curr_node=curr_node->f_child; //one child is present
					continue;
				}
				else	{ //no any child is present
							//should tries to find node among childs
							curr_node=curr_node->get_child_node();
							continue;
					
				}
			}
			else
				{//could be many childs
				curr_node=curr_node->get_child_node();
			}

		}

	}

	return 0;
}

//------------------------------------------------------------------------------

C_NODE* copy_node(const C_NODE* node)
	{
	C_NODE* node1=new C_NODE;
		
	*node1=*node;

	node1->next=NULL;

	C_NODE* ch_pre=NULL;		
	for (C_NODE* ch_n=node1->child;ch_n!=NULL;ch_n=ch_n->next)
		{
		C_NODE* ch_n1=copy_node(ch_n);

		if (ch_pre!=NULL)	ch_pre->next=ch_n1;
		else			node1->child=ch_n1;

		ch_n1->parent=node1;
		if (ch_n==node1->f_child) node1->f_child=ch_n1;
		ch_pre=ch_n1;
	}
	return node1;

}

//------------------------------------------------------------------------------

int  hint_node(C_NODE* node,std::vector<std::string> &res, int lev)
	{

	if (node->state==0) {

		if ( level_pred(lev,node->level)) res.push_back(node->name);
		return 0;
	}
	if ((node->exc_t==1)&&(node->f_child!=NULL)) return hint_node(node->f_child,res,lev);

	for (C_NODE* ch_n=node->child;ch_n!=NULL;ch_n=ch_n->next)
		{
		hint_node(ch_n,res,lev);
	}
return 1;
}





