#include <boost/algorithm/string.hpp> 
#include <boost/regex.hpp>
#include <string>
#include <arpa/inet.h> // for change bit order
#include "func.h"
#include "tree.h"
#include "server.h"


#include <stdio.h> // for memcmp

#include "../arp/arp.h"
#include "../routing/interfaces.h"
#include "../timers/timers_array.h"
#include "../routing/forwarding_table.h"


extern uint8_t G_config_bit;


//-------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------


int mac_load(std::string &value,uint8_t* mac)
	{

	boost::smatch m;
	
	boost::regex mac_reg("([0-9,a-f,A-F]{2}(\:){0,2}){6}");

	value=boost::algorithm::to_upper_copy(value);

	if (!(boost::regex_match(value,mac_reg))) return -1;
	
	boost::sregex_token_iterator iter_mac(value.begin(), value.end(), boost::regex("[0-9,A-F]{2}"), 0);
	boost::sregex_token_iterator iter_end;
	int i=0;
	for(;iter_mac!=iter_end;++iter_mac,i++)
		{
		if (sscanf(iter_mac->str().c_str(),"%02X",mac+i)==1) continue;
		return -3;// error in programm
	}
	return 0;
}

int interface_load(std::string &value,int& int_number,int curr_cb)
	{
		//_2b 0 phy_nt; 1 log_int

		int log_int_n;
		int phy_int_n;

		if ((phy_int_n=s2phy_int(value))>=0)
			{

			int_number=phy_int_n;

			return 0;
	
		}
		if ((log_int_n=s2log_int(value,curr_cb))>=0)
			{
			int_number=log_int_n;

			return 1;
		}
	return -1;
}

int ip_load(std::string &value, uint32_t& ip)
	{
	if (s2ip(value,ip)!=0) return -1;
	return 0;
}

int vlan_load(std::string &value, uint16_t &vlan)
	{
	int i;
	if ((value=="none")||(value=="untagged"))
		{
		vlan=4096;
		return 0;
	}

	if ((vlan=s2numb(value))<0) return -1;
	if (vlan>4095) return -1;
	//vlan=htons(vlan);
	return 0;
}



//----------------------------------process parameter value----||||

void print_arp_entry(ARPe* curr_arp, uint32_t ip, LogInt* log_int, CLI_SESS* c_s)
	{
	
	uint16_t vlan_n=htons(curr_arp->vlan);
	std::string vlan_str=(vlan_n==4096)?"----":(std::to_string(vlan_n));

	std::string state_str="empty";
	if (curr_arp->state==1) state_str="getting";
	if (curr_arp->state==2) state_str="valid";

	std::string valid_s=(curr_arp->is_ok==1)?"ok":"NOT";

	std::string timer_s=(curr_arp->is_ours!=1)?std::to_string(G_timers_arr->timer_distance(curr_arp->timer_n)):"--";

	std::string count_req_s=(curr_arp->is_ours!=1)?std::to_string(curr_arp->counter):"--";//count or rest requests


	std::string type_s="";
	if (curr_arp->is_ours==1) type_s="our mac";
	if (curr_arp->is_static_route==1) type_s="next hop";

	c_s->send_line(mac2s(curr_arp->mac_addr)+"\t"+vlan_str+"\t"+state_str+"\t"+ip2s(ip)+"\t"+log_int->name+"\t"+timer_s+"\t"+count_req_s+"\t"+type_s+"\t"+valid_s);

return ;
}


int print_arp_of_log_int(uint8_t* mac,int is_mac,LogInt* log_int,int curr_cb,CLI_SESS* c_s)
	{
	

	int min_value=log_int->arp_index;
	int max_value=log_int->arp_index+(1<<(32-log_int->net.mask));

	uint32_t ip_base=((log_int->net.ip)&(log_int->mask));

	
	int k=0;
	for (int i=min_value;i<max_value;i++,k++)
		{
		uint32_t curr_ip=ip_base+k;
		ARPe* curr_arp=&(G_arp_table_arr[curr_cb].arp_arr[i]);
		if ((is_mac)&&(memcmp(curr_arp->mac_addr,mac,6)!=0)) continue;
		if (curr_arp->state==0) continue;
		print_arp_entry(curr_arp,curr_ip,log_int,c_s);
	}

return 0;
}



//------------------------------------------------------------------------------

int show_arp (CLI_SESS* c_s) //responce
	{
	
	uint8_t mac[6];
	uint16_t vlan;
	uint32_t ip;
	int	int_n;
	int	int_type=-1;

	int	is_mac=0;
	int	is_vlan=0;
	int	is_ip=0;

	int curr_cb=G_config_bit;

	C_NODE* c_n;

	int good_data=1;

	for ( c_n=c_s->arg_node->child;c_n!=NULL;c_n=c_n->next)
		{
		int bad_data=0;
		if (c_n->state!=1) continue;
	//--------------------------------------------
		if (c_n->name=="mac")
			{
			is_mac=1;
			if (mac_load(c_n->value,mac)<0) bad_data=1;
		}
		if (c_n->name=="vlan")
			{
			is_vlan=1;
			if (vlan_load(c_n->value,vlan)<0) bad_data=1;

		}
		if (c_n->name=="ip")
			{
			is_ip=1;
			if (ip_load(c_n->value,ip)<0) bad_data=1;

		}
		if (c_n->name=="interface")
			{
			
			if ((int_type=interface_load(c_n->value,int_n,curr_cb))<0) bad_data=1;
		}	

	//--------------------------------------------

		if (bad_data==1)
			{
			c_s->send_line(c_n->name+": "+ c_n->value+" is wrong!!!");
			return -1;
		}

	}

	//-------------------------------------------------------------------------

	c_s->send_line("mac address\t\tvlan\tstate\tip address\tint\ttimer\tcounter\ttype\tvalid");


	if (is_ip==1)
		{
		uint32_t arp_ind=G_for_int_table->get_arp_index_for_ip(ip,0);

		ARPe* curr_arp=&G_arp_table->arp_arr[arp_ind];

		if (int_type>=0)
			{
			if ((int_type==0)&&(curr_arp->physical_int_index!=int_n))	return -2;
			if ((int_type==1)&&(curr_arp->log_int_index!=int_n))		return -2;
		}
		if (is_mac)
			{
			if (memcmp(curr_arp->mac_addr,mac,6)!=0) return -1;
		}

		if (is_vlan)
			{
			if (curr_arp->vlan!=vlan) return -1;
		}

		print_arp_entry(curr_arp,ip,&(G_log_ints_arr[curr_cb][curr_arp->log_int_index]),c_s);
		return 0;
	}

	
	//-------------------------------------------------------------------------

	if (int_type==1)
		{
		 //logical  interface
		LogInt* log_int=&(log_int_table[curr_cb][0]) + int_n;
		if ((is_vlan)&&(log_int->vlan!=vlan)) return -1;
		print_arp_of_log_int( mac,is_mac,log_int,curr_cb,c_s); //print all arps with such mac
		return 0;
	}

	// may be spesified phy_int, mac or vlan

	for (int i1=0;i1<log_int_table[curr_cb].size();i1++)
		{
		LogInt* log_int=&(log_int_table[curr_cb][0]) + i1;

		if ((int_type==0)&&( log_int->phy_int!=int_n)) continue;
		if ((is_vlan)&&( log_int->vlan!=vlan)) continue;
		print_arp_of_log_int(mac,is_mac,log_int,curr_cb,c_s); //print all arps with such mac
	}



	
return 0;

}