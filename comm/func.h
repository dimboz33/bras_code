#include "tree.h"
#include "server.h"
#include <vector>

#ifndef COMM_FUNC_CLASS


#define COMM_FUNC_CLASS

//----------------------------------

struct COMM_PROC_RESULT {

std::vector<std::string> s_arr;

C_NODE* arg_node;

int func_ind;//-1 - no found

int fast; //slow output monitor or -more- 0 ; 1 - fast output

int more;// 0 - meance no-more; 1 meance more

};

struct COMM_FUNC {

std::string name;

int (*func)(CLI_SESS* cli_sess);

///int more_sw; // 0 - no more ; 1 more 

int fast; // 0 meance no amy fast output? like monitor
	  // 1 meance always output fast
	  // -1 depend on more_sw spesification 

};
//--------------------

int show_interface 	(CLI_SESS* c_s);
int show_arp 		(CLI_SESS* c_s);
int show_route 		(CLI_SESS* c_s);
int show_subscribers 	(CLI_SESS* c_s);
int monitor_interface 	(CLI_SESS* c_s);
int monitor_subscriber 	(CLI_SESS* c_s);
int monitor_chassis 	(CLI_SESS* c_s);
int clear_subscribers	(CLI_SESS* c_s);
int show_bgp	(CLI_SESS* c_s);

//--------------------

#ifndef FUNC_CPP

extern std::vector<COMM_FUNC> G_comm_func_arr;

#else

std::vector<COMM_FUNC> G_comm_func_arr ={
{
.name="show_interface",
.func=&show_interface,
.fast=1,
},
{
.name="show_arp",
.func=&show_arp,
.fast=1,
},
{
.name="show_route",
.func=&show_route,
.fast=1,
},
{
.name="show_subscribers",
.func=&show_subscribers,
.fast=-1,
},
{
.name="monitor_interface",
.func=&monitor_interface,
.fast=0,
},
{
.name="monitor_subscriber",
.func=&monitor_subscriber,
.fast=0,
},
{
.name="monitor_chassis",
.func=&monitor_chassis,
.fast=0,
},
{
.name="clear_subscribers",
.func=&clear_subscribers,
.fast=0,
},
{
.name="show_bgp",
.func=&show_bgp,
.fast=1,
}
//clear_subscribers
};

#endif

#endif