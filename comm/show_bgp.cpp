#include "../bgp/bgp.h"


#include "func.h"
#include "tree.h"
#include "server.h"

#include <string>
#include <vector>


using namespace std;

int show_bgp (CLI_SESS* c_s) //responce
	{

std::lock_guard<std::mutex> lock(G_bgp_conf->mut);

G_bgp_conf->bgp_pipe<<std::string("show neighbor status")<<endl;

vector<string> resp;

if (G_bgp_conf->get_bgp_responce(resp,1000)!=0)
	{
	c_s->send_line("bgp module exabgp doesn't respond");
	return -1;
}

for (int i=0;i<resp.size();i++)
	{
	c_s->send_line(resp[i]);
}


return 0;

}