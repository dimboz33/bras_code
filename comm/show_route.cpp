#include <string>
#include <arpa/inet.h> // for change bit order
#include "func.h"
#include "tree.h"
#include "server.h"

#include <stdio.h> // for memcmp

#include "../arp/arp.h"
#include "../routing/interfaces.h"
#include "../routing/forwarding_table.h"

extern uint8_t G_config_bit;


int show_route (CLI_SESS* c_s) //responce
	{
int curr_cb=G_config_bit;

ForTabTre* for_table=G_for_table_arr[curr_cb];
ARP_TABLE* arp_table=G_arp_table_arr+curr_cb;
LogInt*    log_ints=G_log_ints_arr[curr_cb];

	c_s->send_line("");

	for (list<FoRo>::iterator fr=for_table->froutes.begin();fr!=for_table->froutes.end();fr++)
		{
		std::string route_str;
		if (fr->is_active_parent==1) continue;

		route_str=Netw2s(fr->net)+"\t\t";
		ACT* r_act=&(fr->my_act);//route action

		if (r_act->am_nh==0) route_str+="Discard";	
		
		if (r_act->type==0) route_str+="[ local ] ";
		if (r_act->type==1) route_str+="[ static ] ";
		
		if (r_act->type==0)
			{
			//print local interface
			int arp_ind=r_act->arp_ind[0];
			ARPe* curr_arp=&(arp_table->arp_arr[0])+arp_ind;
			
			route_str+="--> "+log_ints[curr_arp->log_int_index].name;
			c_s->send_line(route_str);
			c_s->send_line("");
		}
		else
			{

			if (fr->my_act.am_nh>1)	c_s->send_line(route_str);

			for (int i=0;i<fr->my_act.am_nh;i++)
				{
				int arp_ind=r_act->arp_ind[i];
				ARPe* curr_arp=&(arp_table->arp_arr[0])+arp_ind;
			
				LogInt* log_int=log_ints+curr_arp->log_int_index;
				uint32_t ip=(log_int->net.ip)&(log_int->mask);

				ip+=arp_ind-log_int->arp_index;

				if (fr->my_act.am_nh==1) c_s->send_line(route_str+"--> "+log_int->name+" ip "+ip2s(ip));
				else			 c_s->send_line("_\t\t\t\t\t --> "+log_int->name+" ip "+ip2s(ip));
				 c_s->send_line("");
			}


		}

	}


return 0;

}