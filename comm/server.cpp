#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sys/stat.h>   //mkfifo
#include <string>
#include <sys/stat.h> // for checking file exist

#include <unistd.h>   // for mutex
#include <pthread.h>  // for threading
#include <unordered_map>
#include <signal.h>   //for SIGPIPE handler
//---------------------------------------------

#include "server.h"
#include "tree.h"
#include "func.h"

//#include <sys/types.h>


//------  for gettid
/*
#include <unistd.h>
#include <sys/syscall.h>
#define gettid() int(syscall(SYS_gettid))
*/
//------



int parce_request(      std::string s,
                        std::string &path,
                        std::string &command);


//---------------------------------------------------------
void CLI_SESS::send_line (const std::string &s)
	{

	(*cl_p) << s << std::endl; line_n++;
	
	if (line_n<max_line_n) return;

	if (this->more==1) //if --more-- is enable
		{
	//need to send more an wait recponce from client
	(*cl_p) << "@@@more" << std::endl;

	std::string res;
	int state=0;

	while (state==0) 
		{
		std::string path_c;
		std::getline(*s_p, res );
		std::string comm_c;

		if (parce_request(res,path_c,comm_c)<0) continue;

		if (comm_c=="more") state=1;
		if (comm_c=="buy")  state=-1;
	}
	if (state==-1) throw -1;//end of pipe
	}
	if (errno == EPIPE)
		{
		//errno=0;
		 throw -1;
	}
	line_n=0; //continue print result
}

//------------------------------------

void CLI_SESS::read_line ( std::string &s)
	{
	line_n=0; 

		//need to send  <enter text> client
	(*cl_p) << "@@@text" << std::endl;

	std::string res;
	int state=0;
	std::string comm_c;

	while (state==0) 
		{
		std::string path_c;
		std::getline(*s_p, res );
		

		if (parce_request(res,path_c,comm_c)<0) continue;

		state=1;

		if (comm_c=="buy")  state=-1;
	}
	if (state==-1) throw -1;//end of pipe
	
	if (errno == EPIPE)
		{
		//errno=0;
		 throw -1;
	}
	s=comm_c;
}

//---------------------------

void CLI_SESS::send_end ()
	{
	*cl_p << "@@@end"  << std::endl;

	std::string res;

}



//--------------------------------------------------------

//int max_line_n=50;



//-----------------------------------------------FROM PROC.CPP--->>
int comm_process(	COMM_PROC_RESULT &res,
			const C_NODE* m_node,
			 			// model witch should be copied before searching
						// and after searching the copy should be deleted
			std::string in_str,	//input string 
			int type_req		//0 - completion ; 1 - function
			);

//----------------------------------------------FROM PROC.CPP---||


//------------------------------------------
void sig_pipe_handler (int signal)
	{
//printf("catch signal %d,  process %d, thread_id %d \n",signal,getpid(),gettid());


//kill(pthread_self(), 15);
//throw -1;
return ;
}



//------------------------------------------
void* cli_sess_process (void * arg)
	{

std::string path,command, input_new_pipe;


CLI_SESS* c_s=(CLI_SESS*)arg; //current session =r_cli_sess
c_s->s_p=NULL;
c_s->cl_p=NULL;


input_new_pipe=c_s->path+"_server_input";

mkfifo(input_new_pipe.c_str(),0666);

chmod(input_new_pipe.c_str(), 0666);

signal(SIGPIPE, sig_pipe_handler);

c_s->cl_p=new std::fstream;
c_s->cl_p->open(c_s->path.c_str(), std::ifstream::out);

if (!c_s->cl_p->is_open())
	{
	delete c_s->cl_p;
	delete c_s->arg_node;
	delete c_s;
	return NULL;
}	



c_s->s_p=new std::ifstream; //input stream for requests

//try to open client pipe for output 
c_s->s_p->open((input_new_pipe.c_str()),std::ifstream::in | std::ifstream::out);

if (!c_s->s_p->is_open())
	{
	delete c_s->cl_p;
	delete c_s->s_p;
	delete c_s->arg_node;
	delete c_s;
	
	return NULL;
}

//send redirect to client
//sleep(1);

*(c_s->cl_p) << "@@@server_pipe " << input_new_pipe << std::endl;

//function of command
try {

G_comm_func_arr[c_s->func_ind].func(c_s);
c_s->send_end();
}
catch (...)	{
	printf("catch exeption in thread\n");

}

remove(input_new_pipe.c_str()); //delete server serponce pipe
remove(c_s->path.c_str());		//delete client request  pipe

c_s->cl_p->close();
c_s->s_p->close();

delete c_s->s_p;
delete c_s->cl_p;


delete c_s->arg_node;

delete c_s;


//printf("memory is released!!!");

return NULL;
}
//-------------------------------------------------------------

//-------------------------------------------------
int parce_request(	std::string s,    // 0 - simple hint;//1 - command // -1 error
			std::string &path,
			std::string &command
			)
	{
// format path@@@type@@@command

int n=0;

if ((n=s.find("@@@"))==std::string::npos) return -1;

path=s.substr(0,n);

command=s.substr(n+3,s.length());

if ((n=command.find("@@@"))==std::string::npos) return -1;

std::string s_type=command.substr(0,n);

command=command.substr(n+3,command.length());

if (s_type=="0") return 0;
if (s_type=="1") return 1;


return -1;
}
//-------------------------------------------------

int server_cli_main()
{

//load c_node model

//-----------------------LOAD C_NODE MODEL------------->>

std::ifstream f_model_comm_tree;

f_model_comm_tree.open("model_comm_tree",std::ifstream::in);

if (!f_model_comm_tree.is_open()) 
	{
	printf("can't open file model_comm_tree\n");
	return 0;
}

C_NODE* m_c_tree=load_node(f_model_comm_tree);

//------------------------LOAD C_NODE MODEL------------||


std::string p_s_name="/tmp/p_serv";

while (true)
	{
mkfifo(p_s_name.c_str(),0666);

chmod(p_s_name.c_str(), 0666);

std::ifstream pipe_serv; //input stream for requests

pipe_serv.open(p_s_name.c_str(),std::ifstream::in);

if (!pipe_serv.good())
	{
	printf("coldn't open pipe_server\n");
	return 1;
}
std::string s;

CLI_SESS cli_sess;

cli_sess.cl_p=new std::fstream;

//signal(SIGPIPE, sig_pipe_handler);


while (std::getline(pipe_serv, s ))//(std::getline(pipe_serv, s))
	{
	//std::cout << s << std::endl;
	std::string path,command;
	std::string* s1;
	int c_type=-1; //command type
	
	if ((c_type=parce_request(s,path,command))<0) continue;

	//std::cout << s+"\n\n";

	std::cout << path+"###"+command +"\n";
	if (command=="buy") continue;
	if (command=="more") continue;

//------processing of command------->>
	

	cli_sess.cl_p->open(path.c_str(), std::ios_base::out);

	if (!cli_sess.cl_p->is_open()) continue; // wrong path

	COMM_PROC_RESULT cp_res; //command process result

	int int_res=comm_process(cp_res,m_c_tree,command,c_type); 
	if (int_res<0) //no function process
		{

		if (c_type==0)	(*cli_sess.cl_p)<<cp_res.s_arr.size()<<"\n";

		for (int i=0;i<cp_res.s_arr.size();i++)
			{
			(*cli_sess.cl_p)<<cp_res.s_arr[i]+"\n";
		}

	
	if ((c_type==0)&&(cp_res.s_arr.size()!=1)) cli_sess.send_end();

	cli_sess.cl_p->close();
	continue;

	}
	//should process function

	///let's generate CLI_SESS

	if (cp_res.fast==1) //if function could be fast processed by main thread
		{
		cli_sess.arg_node=	cp_res.arg_node;
		cli_sess.func_ind=	cp_res.func_ind;
		cli_sess.path=		path;//client pipe for thread to set up no needed for main thread
		cli_sess.more=		cp_res.more;
		cli_sess.arg_node=	cp_res.arg_node;
		try	{
			G_comm_func_arr[cli_sess.func_ind].func(&cli_sess);
			cli_sess.send_end();
		}
		catch (...){
		//printf("catch exeption main process");
		
		}
		delete cli_sess.arg_node;
	}
//-----processiong of command-------||

	else
	
	{
	CLI_SESS* r_cli_sess = new CLI_SESS; // reference to cli sess

	r_cli_sess->arg_node=	cp_res.arg_node; //������ ����������� ������ ����
	r_cli_sess->func_ind=	cp_res.func_ind;
	r_cli_sess->path=	path;//client pipe for thread to set up no needed for main thread
	r_cli_sess->more=	cp_res.more;
	//r_cli_sess->arg_node=	cp_res.arg_node;

//--------------generate thread--->>>
	pthread_t th_id;

	int ind;
	int res=pthread_create(&th_id, NULL, cli_sess_process , r_cli_sess);
	
	if (res != 0)
	{
		printf("pthread_create\n");
		return -1;
	}	

	std::string thread_name="cli_s";
	if ((ind=path.find_last_of("_"))>0) thread_name+=path.substr(ind,10);
	pthread_setname_np(th_id,thread_name.c_str());
	pthread_detach(th_id);

//--------------generate thread---|||

	//delete r_cli_sess->arg_node;
	//delete r_cli_sess;
	}

	cli_sess.cl_p->close();

}

delete cli_sess.cl_p;

pipe_serv.close();

}

return 0;
}