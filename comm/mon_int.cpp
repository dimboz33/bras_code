
#include "func.h"
#include "tree.h"
#include "server.h"
#include <string>

#include <unistd.h>//for sleep
#include "../routing/route.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/interfaces.h"

extern uint8_t G_config_bit;

extern uint8_t  Phy_Int_Count;

extern "C" void sprint_stats(char*);

int print_possible_interfaces (CLI_SESS* c_s)
	{
	int curr_cb=G_config_bit;

	//c_s->send_line("There is no such interface");
	c_s->send_line("Possible interfaces: ");
	for(int i=0;i<Phy_Int_Count;i++)
		{
		c_s->send_line("xe-"+std::to_string(i));
	}
	c_s->send_line("kni-0");
	c_s->send_line("");
	c_s->send_line("");
	c_s->send_line("Possible subinterfaces: ");

	std::string phy_int_s;

	LogInt* log_int;

	for (int i1=0;i1<log_int_table[curr_cb].size();i1++)
		{
		log_int=&(log_int_table[curr_cb][0])+i1;
			
		c_s->send_line(log_int->name);
	}


return 0;
}


int monitor_interface (CLI_SESS* c_s) //responce
	{
while (true) // because of logical interface could change dinamicly
	{

std::string int_str=c_s->arg_node->value;

int curr_cb=G_config_bit;

int type_int=-1; // 0 phy_int ; 1 log_int


int phy_int_n;
LogInt* log_int;

int vlan_n=4097;


do {

	phy_int_n=s2phy_int(int_str);
	
	if (phy_int_n>=0)
		{
		type_int=0;
		break;
	}

	log_int=s2log_int(int_str);

	if (log_int!=NULL)
		{	
		type_int=1;
		phy_int_n=log_int->phy_int;
		vlan_n=log_int->vlan;
		break;
	}	

} while (false);


if (type_int<0)
	{
	c_s->send_line("Can't find interface "+int_str);

	print_possible_interfaces (c_s);
	return -1;
}


Counters curr_in, curr_out;
Counters prev_in, prev_out;
Counters diff_in, diff_out;
int k=0;

prev_in =G_log_int_counters_in[phy_int_n][vlan_n];
prev_out=G_log_int_counters_out[phy_int_n][vlan_n];

sleep(1);

std::string mac_s=mac2s( G_phy_ints[phy_int_n].mac_addr);

while (curr_cb==G_config_bit)
	{


	c_s->send_line("\f");

	curr_in=G_log_int_counters_in[phy_int_n][vlan_n];
	curr_out=G_log_int_counters_out[phy_int_n][vlan_n];

	diff_in.pack=curr_in.pack-prev_in.pack;
	diff_out.pack=curr_out.pack-prev_out.pack;

	diff_in.byte =(curr_in.byte- prev_in.byte )<<3;
	diff_out.byte=(curr_out.byte-prev_out.byte)<<3;

	prev_in =curr_in;
	prev_out=curr_out;
//----------------------------------------------------------------------
	if (type_int==0)
		{
		c_s->send_line("Physical interface "+int_str);
	}
	else	{
		c_s->send_line("Logical  interface "+int_str);
		c_s->send_line("ip address: "+Netw2s(log_int->net));
		if (vlan_n==4096)	c_s->send_line("no vlan configured");
		else			c_s->send_line("vlan: "+std::to_string(vlan_n));

	
	}

	c_s->send_line("mac: "+mac_s);
	c_s->send_line("ethernet mtu 1600");
//------------------------------------------------------------------------

		c_s->send_line("COUNTERS");
		c_s->send_line("==================================================");
		c_s->send_line("INPUT");
		c_s->send_line("\t\tbytes\t"	+std::to_string(curr_in.byte));
		c_s->send_line("\t\tpackets\t"	+std::to_string(curr_in.pack));
		c_s->send_line("-------------------------------------------------------------------------------");
		c_s->send_line("OUTPUT");
		c_s->send_line("\t\tbytes\t"	+std::to_string(curr_out.byte));
		c_s->send_line("\t\tpackets\t"	+std::to_string(curr_out.pack));
		c_s->send_line("==================================================");
		c_s->send_line("");
		c_s->send_line("SPEEDS");
		c_s->send_line("==================================================");
		c_s->send_line("INPUT");
		c_s->send_line("\t\tbits/s\t"	+std::to_string(diff_in.byte));
		c_s->send_line("\t\tpackets/s\t"+std::to_string(diff_in.pack));
		c_s->send_line("-------------------------------------------------------------------------------");
		c_s->send_line("OUTPUT");
		c_s->send_line("\t\tbits/s\t"	+std::to_string(diff_out.byte));
		c_s->send_line("\t\tpackets/s\t"+std::to_string(diff_out.pack));
		c_s->send_line("==================================================");

//---------------------------------------------------------------------------

		
	sleep(1);
	}
}
	
return 0;

}

//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

int monitor_chassis (CLI_SESS* c_s) //responce
	{

	while (true)
		{
		char buffer[20000]={0};

		sprint_stats(buffer);
		std::string s=std::string(buffer);

		c_s->send_line(s);

		sleep(1);
	}

return 0;
}

//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

int show_log_phy_interface (void* int_p,int const int_type,std::string param, CLI_SESS* c_s) //int_type=0 - phy int; 1 - log_int
	{

	int vlan_n;

	PhyInt* phy_int=NULL;
	LogInt* log_int=NULL;
	int phy_int_n;


	std::string name_d,desc_d,vlan_str;///name device // description device ///vlan string;
	if (int_type==0)
		{
		phy_int=(PhyInt*)int_p;
		name_d=phy_int->name;
		desc_d=phy_int->desc;
		vlan_n=4097;
		vlan_str="";
		phy_int_n=phy_int->phy_int;
	}
	else
		{
		log_int=(LogInt*)int_p;
		name_d=log_int->name;
		desc_d=log_int->desc;
		vlan_n=log_int->vlan;
		vlan_str=(vlan_n==4096)?"untagged":(std::to_string(vlan_n));
		phy_int_n=log_int->phy_int;

	}
	if (param=="description")
		{	

		c_s->send_line(name_d+"\t"+desc_d);
		return 0;
	}
	
	if (int_type==1)
		{

		vlan_n=log_int->vlan;
		phy_int=G_phy_ints+log_int->phy_int;
	}
	
	if (param=="terse")
		{
		std::string p_str; //printed string 
		
		p_str=name_d+"\t";
		if (int_type==1)
			{
			p_str+=Netw2s(log_int->net)+"\t"+vlan_str+"\t";
		}
		else
			{
			p_str+="\t\t\t\t";
		}

		p_str+=desc_d;
		c_s->send_line(p_str);

		return 0;

	}

	if (param!="") return -1;
	
	// is noting parameter
	c_s->send_line("========================================================");	
	c_s->send_line("");	
	c_s->send_line("interface \t"+name_d);
	c_s->send_line("");	

	c_s->send_line("\t description: \t"+desc_d);

	uint8_t curr_cb=G_config_bit;

	c_s->send_line("");


	if (int_type==0)
		{

		c_s->send_line("Subinterfaces: ");
		c_s->send_line("name\tip\tvlan\tdescription");
		c_s->send_line("------");
		
		//should print all subinterfaces terse

		for (int i=0;i<log_int_table[curr_cb].size();i++)
			{

			LogInt* c_log_int=&(log_int_table[curr_cb][0])+i;
			if (c_log_int->phy_int==phy_int->phy_int) show_log_phy_interface (c_log_int,1,"terse",  c_s); 
		}
		c_s->send_line("------");

	}
	else
		{
		c_s->send_line("ip address: "+Netw2s(log_int->net));
		c_s->send_line("");
		c_s->send_line("vlan: "+vlan_str);
	}
	c_s->send_line("");

	c_s->send_line("mac: "+mac2s( phy_int->mac_addr));
	c_s->send_line("");
	c_s->send_line("ethernet mtu 1600");
	
	Counters curr_in,curr_out;
	c_s->send_line("");
		
	curr_in =G_log_int_counters_in[phy_int_n][vlan_n];
	curr_out=G_log_int_counters_out[phy_int_n][vlan_n];


		c_s->send_line("COUNTERS");
		c_s->send_line("===========================");
		c_s->send_line("INPUT");
		c_s->send_line("\t\tbytes\t"	+std::to_string(curr_in.byte));
		c_s->send_line("\t\tpackets\t"	+std::to_string(curr_in.pack));
		c_s->send_line("------------");
		c_s->send_line("OUTPUT");
		c_s->send_line("\t\tbytes\t"	+std::to_string(curr_out.byte));
		c_s->send_line("\t\tpackets\t"	+std::to_string(curr_out.pack));
		c_s->send_line("=============================");
		c_s->send_line("");

return 0;


}
//--------------------------------------------------------------------------------

int show_interface (CLI_SESS* c_s) //responce
	{

int curr_cb=G_config_bit;

	std::string param;
	C_NODE* ch=c_s->arg_node->f_child;
	if (ch!=NULL) param=ch->name;
	std::string int_str=c_s->arg_node->value;

	
	if (param=="description")	c_s->send_line("Interface\tdescription");
	if (param=="terse")		c_s->send_line("Interface\tip\tvlan\tdescription");
	



	if (int_str!="")
		{
		PhyInt* phy_int;
		LogInt* log_int;
	
		int phy_int_n;

		if ((phy_int_n=s2phy_int(int_str))>=0)
			{
			phy_int=G_phy_ints+phy_int_n;
			
			show_log_phy_interface(phy_int,0,param,c_s);
			return 0;
	
		}
		if ((log_int=s2log_int(int_str))!=NULL)
			{
			show_log_phy_interface(log_int,1,param,c_s);
			return 0;
		}
	
		c_s->send_line("");
		c_s->send_line("");
		c_s->send_line("Can't find interface "+ int_str);	
				
		print_possible_interfaces (c_s);


	}
	else
		{ //print for all ports
			for (int i=0;i<Phy_Int_Count+1;i++)
			{
			PhyInt* phy_int=G_phy_ints+i;
			show_log_phy_interface(phy_int,0,param,c_s);

			for (int k=0;k<log_int_table[curr_cb].size();k++)
				{
				LogInt* c_log_int=&(log_int_table[curr_cb][0])+k;
				if (c_log_int->phy_int==phy_int->phy_int) show_log_phy_interface (c_log_int,1,param,c_s); 
			}


		}


	}




return 0;
}

