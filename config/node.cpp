#include "node.h"




int node::line_n=1;
int node::error_n=-1;
string node::err_s="";







string next_good_str ( ifstream &  file, node* NN)

	{
	string s;
        //find good text string   

	// getline if end of file, return false;
			
	
        for(;(getline(file,s,char(0x0a)));)
                {
			node::line_n++;//=node::line_n+1;
			
			s=boost::regex_replace(s,boost::regex("##.*"),string("")); // allow comments

			if (boost::regex_match(s,boost::regex(".*\\S+.*"))) break;	

		} //find not null string

		
	s=boost::regex_replace(s,boost::regex("\\t"),string(" "));
	s=boost::regex_replace(s,boost::regex(" +"),string(" ")); 

	s=boost::regex_replace(s,boost::regex("^ "),string("")); //cut space at the begining
	s=boost::regex_replace(s,boost::regex(" $"),string("")); //cut space at the end

	s=boost::regex_replace(s,boost::regex("\r"),string("")); //cut space at the end	 

        return s;
}




string get_next_word(string & s)
	{
	string s2;

	boost::smatch m;
	boost::regex w_reg("^(\".*\"|\\[.*\\]|[^ ;]*)");
	boost::regex_search(s,m,w_reg);

	s2=*m.begin();

	s=(s2.size()+1>s.size())?"":s.substr(s2.size()+1, s.size());
	if (s2[0]=='"') s2=s2.substr(1,s2.size()-2);

	return s2;	
}


 node::node ( string & s , ifstream & file,Mnode * Model_node,int type_node, int new_str1) //s-must be good string with good information
//type_node is 0,1,2 value/category/elements

	{
	// that function get caption of node all child and next classes of that node. 
	// So , if you use that function at you first child-> you collect all of you childs
	
boost::regex  open_bras("^( )?\\{( )?$");
boost::regex close_bras("^( )?\\}( )?$");

//.lineNumber()

	this->child_node=NULL;
	this->next_node=NULL;
	this->parent=NULL;
	this->prev_node=NULL;

	this->name=get_next_word(s);
	this->new_str=new_str1;


////---------------------------------------------------FIND TYPE OF THAT ELEMENT---------->>
	
	Mnode * c_Model_node; ///current Mnode;
	int  c_type_node; 
	
	c_Model_node=Model_node; // for the case of type 2 - objects
	c_type_node=1; //ogjects only type 1



	if ((type_node<2)&&(this->name!="root"))
		{

		c_Model_node=find(this->name,Model_node);
		if (c_Model_node==NULL)
			{
		this->name="";
		this->error_n=this->line_n; //error of syntax
		return;
		}
	c_type_node=c_Model_node->element_types;
	}
	
////---------------------------------------------------FIND TYPE OF THAT ELEMENT-----------||
	

	if (c_type_node==0) ///needed to get value   
		{ 
		this->value=get_next_word(s);

		if (s=="{")
			{
			this->error_n=this->line_n; //error of syntax
			return;
		}
	}

//------------- FOR ALL OTHER CASES ---------->

	this->node_line=this->line_n;

		if (s=="}")
			{
			this->error_n=this->line_n; // "}" symbol must be only on free string
			return;

		}
	
		if (s=="{")
			{
			s=next_good_str(file,this);	

			if (!boost::regex_match(s,close_bras))
				{
				this->child_node=new node(s,file,c_Model_node,c_type_node,1);
			}
			else
				{

				s=next_good_str(file,this);
			}

			for (node* aa=this->child_node;aa!=NULL;aa=aa->next_node)	
				{
				aa->parent=this;
			}


			if (error_n>0) return;
			// end of all childs - " }" and then go to another string
			// we need no write his neigbours
			

			if (this->new_str==0)
				{
				return;
			}

			// if next string is "}" not next node -> that is the end of prosedure
			
			if (boost::regex_match(s,close_bras))
				{
				s=next_good_str(file,this);
				return; //end of all child and all next classes
			}

			////////////////////////////////////////////-next-hode--->
			if (this->name=="root")	return;
			this->next_node=new node(s,file,Model_node,type_node,1);
			this->next_node->parent=this->parent;
			this->next_node->prev_node=this;
			if (error_n>0) return;
			if (this->next_node->name=="") 
					{
					error_n=line_n;
					return;
				} 
			////////////////////////////////////////////-next-hode---||
			
			//collect all next nodes - end of function
			return;

		}
		else // string is continued
			{

			
			string s2=s;

			if ((s=="")||(s==";")) //that node is the last
				{
				s=next_good_str(file,this);
				goto _END_OF_STRING;
			}
			// try to find child node
			this->child_node=new node(s,file,c_Model_node,c_type_node,0);
			for (node* aa=this->child_node;aa!=NULL;aa=aa->next_node)	
				{
				aa->parent=this;
			}

			if (error_n>0) return;

			if (this->child_node->name=="") 
					{
					//child node is not found
					delete this->child_node;
					s=s2;
			
					////////////////////////////////////////////-next-hode--->
					if (this->name=="root")	return;
					this->next_node=new node(s,file,Model_node,type_node,0);
					this->next_node->parent=this->parent;
					this->next_node->prev_node=this;

					if (error_n>0) return;
					if (this->next_node->name=="") 
						{
						error_n=line_n;
						return;
					} 
					////////////////////////////////////////////-next-hode---||
					//we collect all next nodes

					goto _END_OF_STRING; 
			}
		 
				//we collect all childs  and that is should be end of string such rull 
				// in case all nodes is written in string we cant go up from leaves and collect next node after child. That was done for simplicity

			_END_OF_STRING:
			if (this->new_str==1)

				{

				if (boost::regex_match(s,close_bras))
					{
					s=next_good_str(file,this);
					return; //end of all child and all next classes
				}

				////////////////////////////////////////////-next-hode--->
				if (this->name=="root")	return;
 				this->next_node=new node(s,file,Model_node,type_node,1);
				this->next_node->parent=this->parent;
				this->next_node->prev_node=this;
				if (error_n>0) return;
				if (this->next_node->name=="") 
					{
					error_n=line_n;
					return;
				} 
		
				////////////////////////////////////////////-next-hode---||
			}
			return;
		
			}



}


//////---------------------------tree collaps


void node::collaps () {

//	node *ch_n1;
	
	for (node* ch_n= this->child_node; ch_n!=NULL;ch_n=ch_n->next_node)
		{
	for (node* ch_n1= ch_n->next_node;ch_n1!=NULL;)
		{

		if (ch_n1==NULL)
			{
			ch_n->collaps();
			if (error_n>0) return;
			break;
		}		
		

		if (ch_n1->name==ch_n->name)
			{
			if ((ch_n1->value!="")||(ch_n->value!=""))
				{
				//that node contain value should have multiple declaration

				ch_n->err_gen(" - That node should contain value, but has multiple declarations in config file");
				error_n=node_line;			
				return;
			}
		
			//node ** new_ch_ref=&(ch_n->child_node);//reference to new child
			node* last_ch_node=ch_n->child_node;
			
			if (last_ch_node!=NULL)
				{
				for (;((last_ch_node->next_node)!=NULL);last_ch_node=last_ch_node->next_node);
			}

			if (ch_n1->child_node!=NULL) ch_n1->child_node->prev_node=last_ch_node;
			
			if (last_ch_node!=NULL)
				{
				last_ch_node->next_node=ch_n1->child_node;
			}
			else	{

				ch_n->child_node=ch_n1->child_node;
			}
			

			for (node* ff=ch_n1->child_node;ff!=NULL;ff=ff->next_node)	
				{
				ff->parent=ch_n;
			}
			
			if (ch_n1->prev_node!=NULL)	ch_n1->prev_node->next_node=ch_n1->next_node;
			if (ch_n1->next_node!=NULL)	ch_n1->next_node->prev_node=ch_n1->prev_node;
			
			//ch_n->next_node=ch_n1->next_node;
			node* next_ch_n1=ch_n1->next_node;

			ch_n1->child_node=NULL;//for childrent not to be deleted 

			delete ch_n1;

			ch_n1=next_ch_n1;
			

		}
		else
			{
			ch_n1=ch_n1->next_node;	
		}
	}

	ch_n->collaps();

	}

}

//-----------------------------------------------------------------------------------

void node::err_gen(string err_string)
	{

string A=err_string;

	for (node* curr_node=this;curr_node!=NULL;curr_node=curr_node->parent)
		{
		A=curr_node->name+" "+A; 
	}
this->err_s=this->err_s+"\n"+A+"\n";
return;
}

//----------------------------------------------------------------------------------

void node::free_memory()
	{

	for (node* ch_n=this->child_node;ch_n!=NULL;)
		{
		node* next_n=ch_n->next_node;
		ch_n->free_memory();
		
		ch_n=next_n;
	}

	delete this;

}

node::~node()
	{
	for (node* ch_n=this->child_node;ch_n!=NULL;)
		{
		node* next_n=ch_n->next_node;
		delete ch_n;
		
		ch_n=next_n;
	}

}




