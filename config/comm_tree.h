#include <string.h>
#include <stddef.h>
#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <cstddef>//for NULL

#ifndef C_NODE_CLASS


#define C_NODE_CLASS

/*
f_n -funtion name
exc_t (+/-); default is + that meance exc_t =0; childrent could supplement each other
type (0,1,2);	// if category has NO child - default type 1;
			// if categoty has    child - default type 0;

rule is : category_name { (exc_t)(type)f_n==
		or
	    categoty_name (exc_t)(type)f_n==

//----------------------
AAA	{ - 0 

	BBB	{ + 0 f_n=="AAA_BBB_function"

		ccc 1

		ddd 1

		eee 1
	}
}	
//---------------------

*/

struct C_NODE //c - meance command node
	{
	std::string name;
	std::string value;

	int exc_t;//1-childrens are mutial exclusive; 0-childrens could supplement each other
	

	std::string func_name; //  fuction 
	int func_index;//number of function that process such request in array of command functions
				//-1 nothing no function
	//int func_type;// 0 - no childred is requered; 1 - any children is requered;

	int type;// 1 - node with necessarily value;//0-node without values;// 2-that may include value

	int state ;//1 -node is precent; 0 - is absent;

	C_NODE* next;
	C_NODE* child;
	C_NODE* parent;

	C_NODE* f_child;//filled child;


C_NODE* get_child_node();

C_NODE* get_next_node();

//C_NODE();

~C_NODE();

};
//-------------------------------------


C_NODE* find_new_node(std::string s, C_NODE* comm_tree);
C_NODE*  load_node(std::ifstream& file);

#endif