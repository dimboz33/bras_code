#include <string.h>
#include <stddef.h>
#include <iostream>
#include <fstream>
#include <boost/regex.hpp>

#include <cstddef>//for NULL


using namespace std;


#include "mnode.h"

#ifndef NODE_CLASS


#define NODE_CLASS






 class node
{
        public:
	string  name;
	string value;

	int new_str;//for config reading

	node*  next_node;
	node* child_node;
	
	node* parent;

	node* prev_node;	

			//int type;
			//0 - value;
			//1- categories;
			//2 - objects;	
	
	bool is_last;

	static int line_n;//line number

	int node_line=0;

	static int error_n;

	static string err_s;

  node ()
	{
	this->child_node=NULL;this->next_node=NULL;this->parent=NULL;
	this->value="";this->name="";

	}


  node ( string & s1 , ifstream & file,Mnode * Model_node,int type_node,int new_str1); //s-must be good string with good information
//type_node is 0,1,2 value/category/elements

 void err_gen(string err_string);

 void collaps();

 void free_memory(void);

 ~node();

};

//class for simple_config

string next_good_str ( ifstream &  file, node* NN);

	#ifndef MAIN_FILE
			extern node* config_tree;
	#else
			node* config_tree=NULL;
	#endif

#endif
