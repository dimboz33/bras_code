#include "node.h"
#include "mnode.h"


string next_good_str ( ifstream &  file);



string get_next_word(string & s,string reg_str)
	{
		string s2;

		boost::smatch m;
		boost::regex w_reg(reg_str);
		boost::regex_search(s,m,("^[^ ,{,},;]*$"));

		s2==*m.begin();

		s=s.substr(s2.size()+1, s.size());
return s;
}


 node::node ( string & s , ifstream & file,Mnode * Model_node,int type_node,uint &is_new_str,int & str_error) //s-must be good string with good information
//type_node is 0,1,2 value/category/elements

	{
	is_new_str=0;
	
	
string open_bras("^( )?\\{( )?$"); 
string close_bras("^( )?\\}( )?$");


//.lineNumber()

	if (boost::regex_match(s,close_bras))
		{
		s=next_good_str(file);is_new_str=1;			
		return;
	}

	this->name=get_next_word(s);
	int written_type;	
	




////---------------------------------------------------FIND TYPE OF THAT ELEMENT---------->>
	
	Mnode * c_Model_node; ///current Mnode;
	int  c_type_node; 
	
	c_Model_node=Model_node; // for the case of type 2 - objects
	c_type_node=1; //ogjects only type 1

///////////////////////////////

	if ((type_node<2)&&(this->name!="root"))
		{
	// нужно проверить название подкласса на предмет соответствия модели
	c_Model_node=find(this->name,Model_node);
		if (c_Model_node==NULL)
			{
		this->value="wrong_name";
		return;
		}
	c_type_node=c_Model_node->element_types;
	}
	
////---------------------------------------------------FIND TYPE OF THAT ELEMENT-----------||
	

	if (c_type_node==0) ///needed to get value   
		{ 
		this->value=get_next_word(s);
		if ((s==";")||(s==""))
			{
			s=next_good_str(file);is_new_str=1;	
			return; //end of the string
		}
		if (s=="{")
			{
			str_error=file.lineNumber(); //error of syntax
			return;
		}
	}

		
		if (s=="}")
			{
			s=next_good_str(file);is_new_str=1;	
			return;

		}
	
		if (s=="{")
			{
			s=next_good_str(file);is_new_str=1;	
			
			this->child_node=new node(s,file,c_Model_node,c_type_node,is_new_str1,str_error);
			if str_error>0 return;

			return;

		}
		else // string is continued
			{
			string s2=s;
			// try to find child node
			this->child_node=new node(s,file,c_Model_node,c_type_node,is_new_str1);
			if str_error>0 return;
			if is_new_str
			//hear we also create all child_nodes

			if (this->child_node->value=="wrong_name") 
					{
					//child node is not found
					delete this->child_node;
					s=s2;
					this->next_node=new node(s,file,Model_node,type_node,is_new_str1);
					if str_error>0 return;
					if (this->next_node->value=="wrong_name") 
						{
						str_error=file.lineNumber();
						return;
					} 


					return; 
				}
		
//now we write all child elements and needed to write next element		
		if (this->name=="root")
			 {
			return;
		}		
	
		 s=next_good_str(file);
		is_new_str=1;
			
	}
		 

	
	
	
	// is node is simple value	
	
	if (boost::regex_match(s,boost::regex(".*\\}.*"))) // this is end of upper class and we fill all neighbours
			
			{
			this->next_node=NULL;
			
			return;
		}
		else 
			{
			this->next_node=new node(s,file,Model_node,type_node,is_new_str1);
		}
	
		return;

}

