#include "mnode.h"


/////////////////////////////////////////////////////



//class for config_tree_model

/////////////////////////////////////////////////////////////////

string Mnext_good_str ( ifstream &  file)
{string s;
        //find good text string         
                for( getline(file,s);!boost::regex_match(s,boost::regex(".*\\S.*")); getline(file,s))
                {
			int a;
			a=1;
		} //find not null string
        return s;
}



/////////////////////////////
 Mnode::Mnode ( string  s1 , ifstream & file) //s-must be good string with good information
	{
	string s;
	boost::smatch m;
	s=s1;
	s=boost::regex_replace(s,boost::regex("##.*"),string("")); //clear comments
	s=boost::regex_replace(s,boost::regex("\\t"),string(" "));
	s=boost::regex_replace(s,boost::regex(" +"),string(" ")); 

	s=boost::regex_replace(s,boost::regex("^ "),string("")); //cut space at the begining
	s=boost::regex_replace(s,boost::regex(" $"),string("")); //cut space at the end
	s=boost::regex_replace(s,boost::regex("\r"),string("")); //cut space at the end	 
	boost::regex with_objects("^[^ ]* _\\(s\\)_ \\{$");
	boost::regex with_categories ("^[^ ]* \\{$");
	boost::regex value("^[^ ]*$");
	boost::regex name_reg("^[^ ]*");
	boost::regex_search(s,m,name_reg);	
	
	this->name=*m.begin();
	this->child_node=NULL;
	this->next_node=NULL;
	bool aa;
	aa=boost::regex_match(s,with_objects);	
	if (boost::regex_match(s,with_objects)) 
		{
		this->element_types=2;goto good_recognition;
	}
	if (boost::regex_match(s,with_categories))
		{
		this->element_types=1;goto good_recognition;
	}
	if (boost::regex_match(s,value)) 
		{
		this->element_types=0;goto good_recognition;
	}
	value="error";
	return;
	good_recognition:
	
	s=Mnext_good_str(file);

		
	if (this->element_types>0)
		{
		if (!boost::regex_match(s,boost::regex(".*\\}.*")))  //there is some sub_elements
			{	
				
				
				this->child_node=new Mnode(s,file); //hear we also create all child_nodes
				if (this->child_node->value=="error")
					{
					this->value="error";return; 
				}
		
				//now we write all child elements and needed to write next element		
				if (this->name=="root")
					{
					return;
				}		
	
				s=Mnext_good_str(file);
		}
		else
			{
				s=Mnext_good_str(file); //goto next element
				//return; 

		}
	}
		 

	
	
	
	// next node	

	
	
	if (boost::regex_match(s,boost::regex(".*\\}.*"))) // this is end of upper class and we fill all neighbours
			
			{
			this->next_node=NULL;
			
			return;
		}
		else 
			{
			this->next_node=new Mnode(s,file);
		}
	
		return;

}

