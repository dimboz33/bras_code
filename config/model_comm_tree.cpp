
#include "comm_tree.h"

std::string C_next_good_str ( std::ifstream&  file)
	{
	std::string s;
        //find good text string         
        
	for( ;(getline(file,s));) //char(0x0a)
              	{
		if (boost::regex_match(s,boost::regex(".*\\S.*"))) break;
	} //find not null string

	s=boost::regex_replace(s,boost::regex("##.*"),std::string("")); //clear comments
	s=boost::regex_replace(s,boost::regex("\\t"),std::string(" "));
	s=boost::regex_replace(s,boost::regex(" +"),std::string(" ")); 

	s=boost::regex_replace(s,boost::regex("^ "),std::string("")); //cut space at the begining
	s=boost::regex_replace(s,boost::regex(" $"),std::string("")); //cut space at the end
	s=boost::regex_replace(s,boost::regex("\r"),std::string("")); //cut space at the end	
        return s;
}

C_NODE*  load_node(std::ifstream & file) //m_tree - model tree
	{
	std::string s1=C_next_good_str (file);

	std::string s;
	boost::smatch m;
	s=s1;
	if (s1==""){
		 return NULL;
	}
	if (!(int(s1.find_first_of('}',0))<0))
		{
		 return NULL;
	}
	boost::regex with_nodes("^[^ ]* \\{.*");
	boost::regex name_reg("^[^ ]*");
	//
	
	C_NODE* curr_node=new C_NODE;

	//-----------------------------------------loading parameters------->>
	boost::regex_search(s,m,name_reg);

	curr_node->name=*m.begin();

	curr_node->f_child=NULL;
	curr_node->child=NULL;
	curr_node->next=NULL;

	int node_type=0;

	int param_ind=s.find_first_of(' ',0);
	
	if (boost::regex_match(s,with_nodes))
		{
		node_type=1;// 
		param_ind=s.find_first_of('{',0);
	}
	
	std::string param=(param_ind<0)?"":s.substr(param_ind+1,s.size());
	
	curr_node->exc_t=1;				//default value
	curr_node->type=(node_type==1)?0:1;	//default value
	
	if (!(int(param.find_first_of('+',0))<0)) 	curr_node->exc_t=0;
	
	if (!(int(param.find_first_of('-',0))<0)) curr_node->exc_t=1;

	if (!(int(param.find_first_of('0',0))<0)) curr_node->type=0;
	if (!(int(param.find_first_of('1',0))<0)) curr_node->type=1;
	if (!(int(param.find_first_of('2',0))<0)) curr_node->type=2;

	std::string f_label="f_n==";

	int f_ind=param.find_first_of(f_label,0);
	std::string f_name=(f_ind<0)?"":param.substr(f_ind+f_label.size(),param.size());

	if (f_name!="") curr_node->func_name=f_name;


	//-----------------------------------------loading parameters---------||

	if (node_type==1)
		{
		curr_node->child=load_node(file); //hear we also create all child_nodes
			//now we write all child elements and needed to write next element		
			
		for (C_NODE* x=curr_node->child;x!=NULL;x=x->next)
			{
			x->parent=curr_node;	
		}

		if (curr_node->name=="root")	return curr_node;

	}
	// next node


	curr_node->next=load_node(file);
	
	return curr_node;
}

//-------------------------------------

C_NODE::~C_NODE()
	{
	if (this->child!=NULL) delete this->child;

	if (this->next!=NULL) delete this->next;

}

//----------------------------------