template <class list_T>
int update_coll(list_T &coll_new,list_T &coll_old) //collection
	//new collection should be sorted yet
	{
	//std::sort(coll_new.begin(),coll_new.end());// operator > is determened
	std::sort(coll_old.begin(),coll_old.end());

	typename list_T::iterator new_el=coll_new.begin();
	typename list_T::iterator old_el=coll_old.begin();
	
	for (;((new_el!=coll_new.end())&&(old_el!=coll_old.end()));)
		{
		if (*new_el>*old_el)
			{
			old_el->remove();
			old_el++;
			continue;
		}
		if (*new_el<*old_el)
			{
			new_el->add();
			new_el++;
			continue;
		}
		
		//*new_el=*old_el;

		new_el++;
		old_el++;

	}

	for (;new_el!=coll_new.end();new_el++)
		{
		new_el->add();
	}

	for (;old_el!=coll_old.end();old_el++)
		{
		old_el->remove();
	}

	

}