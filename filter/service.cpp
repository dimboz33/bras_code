

#include "service.h"
#include "policer.h"

#include "../routing/route.h"

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------



int SERV::Load_serv_conf(node* serv_n)
	{

	if (serv_n==NULL) 
		{
		serv_n->err_gen("No services configured");
		return -3; // no filters config;
	}

	//---------------------------------------------NAME OF SERVICE
	this->name=serv_n->name;

	//-------------------------------------------------priority--->>
	node* prior_conf=find("priority",serv_n);
	if (prior_conf==NULL)
		{
		serv_n->err_gen("No configured priority");	
		return -5;
	}
	string prior_st=prior_conf->value;
	int ii=s2numb(prior_st);
	if ((ii<1)||(ii>64))
		{
		prior_conf->err_gen("Priority "+ prior_conf->value + "is wrong!!!");
		return -5;
	}
	this->priority=ii;
	
	//-------------------------------------------------priority---||

//----------------------------------------------------------------------------INPUT------>>
	
	node* input_n=find("input",serv_n);
	if (input_n==NULL)
		{
		serv_n->err_gen("No configured input filter");	
		return -5;
		
	}

	//--------------------------------------------------FILTER NAME-->>

	node* fil_n=find("filter",input_n);

	if (fil_n==NULL)
		{
		serv_n->err_gen("No configured filter");	
		return -5;
	}

	if (fil_n->value=="")
		{
		serv_n->err_gen("No configured filter");	
		return -5;
	}
	
	
	this->filter_in=fil_n->value;

	//------------------------------------------------FILTER NAME-----||

	//--------------------------------------------------POLICER------->>

	node* pol_n=find("def_policer",input_n);

	if (pol_n==NULL)
		{
		serv_n->err_gen("No configured default policer");	
		return -5;
	}

	if (pol_n->value=="")
		{
		serv_n->err_gen("No configured default policer");	
		return -5;
	}
	
	this->pol_name_in=pol_n->value;

	try 
		{
		this->pol_index_in=G_pol_agr->h_t.at(this->pol_name_in);
	}
	catch(...) 
		{
		serv_n->err_gen("Default input policer"+this->pol_name_in+"is not configured");	
		return -5;
	}
	//------------------------------------------------POLICER----------||

//----------------------------------------------------------------------------INPUT------||



//---------------------------------------------------------------------------OUTPUT------>>
	
	node* output_n=find("output",serv_n);
	if (input_n==NULL)
		{
		serv_n->err_gen("No configured output filter");	
		return -5;
		
	}

	//--------------------------------------------------FILTER NAME-->>

	 fil_n=find("filter",output_n);

	if (fil_n==NULL)
		{
		serv_n->err_gen("No configured filter");	
		return -5;
	}

	if (fil_n->value=="")
		{
		serv_n->err_gen("No configured filter");	
		return -5;
	}
	
	
	this->filter_out=fil_n->value;

	//------------------------------------------------FILTER NAME-----||

	//--------------------------------------------------POLICER------->>

	 pol_n=find("def_policer",output_n);

	if (pol_n==NULL)
		{
		serv_n->err_gen("No configured default policer");	
		return -5;
	}

	if (pol_n->value=="")
		{
		serv_n->err_gen("No configured default policer");	
		return -5;
	}
	
	this->pol_name_out=pol_n->value;
	try
		{
		this->pol_index_out=G_pol_agr->h_t.at(this->pol_name_out);
	}
	catch(...)
		{
		serv_n->err_gen("Default output policer"+this->pol_name_out+"is not configured");	
		return -5;
	}
	//------------------------------------------------POLICER----------||

//---------------------------------------------------------------------------OUTPUT------||
	//--------------------------------------------------IS DEFAULT------->>

	node* def_n=find("default",serv_n);

	if (0)//def_n!=NULL)
		{
		
		this->is_default=true;
		
	}
	
	//-------------------------------------------------IS DEFAULT ----------||

return 0;


}

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

int SERV_AGR::Load_all_serv_conf(node* serv_all_n, SERV_AGR* old_serv_agr)
	{

	if (serv_all_n==NULL) 
		{
		return -3; // no filters config;
	}
	
	if (serv_all_n->child_node==NULL) 
		{
		serv_all_n->err_gen("No services configured");
		return -3; // no filters config;
	}

	this->h_t.clear();//clear hash table

	uint8_t is_configured[16]={0};//is configured old service

	///uint16_t f_ind=0;// index of free service consept was wrong
	
	vector<SERV> new_servs;

	this->serv_count=0;

	for (node* serv_n=serv_all_n->child_node;(serv_n!=NULL);serv_n=serv_n->next_node)
		{

		SERV serv;

		uint16_t serv_ind;//
		if (serv.Load_serv_conf(serv_n)!=0) return -1;//error in config 

		this->serv_count++;


		try
			{
			serv_ind=old_serv_agr->h_t.at(serv.name);
			this->arr[serv_ind]=serv;
			this->h_t[serv.name]=serv_ind;


			//SERV::swap_users(this->arr+serv_ind,old_serv_agr->arr+serv_ind);

			is_configured[serv_ind]=1;
		}	
		catch(...){
			new_servs.push_back(serv);
		}
	}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

//now we need to check is any old used service with clietns that no more in new config

for (int i=0;i<16;i++)
	{
	if ((!serv_users[i].empty())&&(is_configured[i]!=1)) // clietns is present and service was not configured again
		{
		serv_all_n->err_gen("service "+old_serv_agr->arr[i].name+"is used, but it is not present in new config file!!!");
		return -4;
	}

}

//---------------------------------------set new services----------->>

	for (int i=0;i<new_servs.size();i++)
		{
		if (!(i<C_serv_count)) 
			{
			serv_all_n->err_gen("Maximum number of services is 16!!!");
			return -3;
		}
		if (is_configured[i]==1) continue;

		this->arr[i]=new_servs[i];
		this->h_t[new_servs[i].name]=i;

	}

//---------------------------------------set new services-----------||


//get priotiorized que of indexes of serveses

	SERV_AGR* c_this=this;
 std::sort(this->p_que.begin(), this->p_que.end(), [&](int a, int b) {
        return this->arr[b].priority > this->arr[a].priority;   //the smaller - the more priority
    });




return 0;

}

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------


