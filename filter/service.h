

#ifndef SERV_TYPE

#define SERV_TYPE





#include "../config/node.h"


#include <mutex>

#include <unordered_map>

class SERV
	{
public:

uint16_t priority;

uint16_t filter_index_in;
uint16_t filter_index_out;

bool is_default;

std::string name;

std::string filter_in;
std::string filter_out;

uint16_t pol_index_in;//default shaped  
uint16_t pol_index_out;

std::string pol_name_in;// default policer name
std::string pol_name_out;// default policer name

int max_cl_count;

int Load_serv_conf(node* serv_n);



int set_serv_to_user(uint32_t cl_hash);



SERV()
	{

	this->priority=0xFFFF;// means service is empty

}

~SERV()
	{

	this->priority=0xFFFF;// means service is empty

}


};


bool comp_SERV_prior(SERV &a, SERV &b);
//----------------------------------------------------------

class SERV_AGR //Services agregat
	{
public:
SERV arr[16];
uint16_t serv_count;

vector<uint8_t> p_que;//priotiorized que;

std::unordered_map <string, int> h_t; //hash table

int Load_all_serv_conf(node* serv_all_n, SERV_AGR *old_serv_agr);

int find_service_id(std::string service_name);

static list<uint32_t> users[16];//users with such service

SERV_AGR()
	{
	this->p_que.resize(16);
	for (int i=0;i<16;i++)
		{
		this->p_que[i]=i;
	}
}


};


class SERV_USERS_LIST
	{

list<uint32_t> a;

std::mutex	mut;


public:

bool empty();//!!!!
list<uint32_t>::iterator erase (list<uint32_t>::iterator position);//!!!
void push_front(uint32_t val);//!!!

list<uint32_t>::iterator begin ();///!!
list<uint32_t>::iterator end ();//!!

};


#ifndef MAIN_FILE
extern SERV_AGR *G_serv_agr;
extern SERV_AGR G_serv_agr_arr[2];
extern SERV_USERS_LIST  serv_users[16];


#else
SERV_AGR G_serv_agr_arr[2];	//no more that 16//--------------------CHANGED
SERV_AGR *G_serv_agr=G_serv_agr_arr+G_config_bit;	//no more that 16//--------------------CHANGED
SERV_USERS_LIST  serv_users[16];

#endif



#endif
