#include "policer.h"


bool 			 POL_USERS_LIST::empty()	{ return a.empty(); }
std::list<int16_t*>::iterator POL_USERS_LIST::begin () {return a.begin();}
std::list<int16_t*>::iterator POL_USERS_LIST::end ()	{ return a.end();}


std::list<int16_t*>::iterator POL_USERS_LIST::erase (list<int16_t*>::iterator position)
	{
	std::lock_guard<std::mutex> lock(mut);
	return a.erase(position);
}

		    void POL_USERS_LIST::push_front(int16_t* val)
	{
	std::lock_guard<std::mutex> lock(mut);
	return a.push_front(val);
}


