#ifndef POLICER_TYPE

#define POLICER_TYPE

#include <mutex>

#include "../config/node.h"
#include "../routing/route.h"
#include <boost/regex.hpp>

#include <unordered_map>
#include <list>
#include "policer_const.h"
#include "../clients/clients.h"

using namespace boost;
using namespace std;

class POLICER
	{
public:

	std::string name;

	uint32_t speed;// kb/s
	uint32_t bucket_size;
	uint32_t bucket_size_in_ms;// 

int Load_pol_conf(node* pol_n); //load_policer_config

};

class POL_AGR //policer agregat
	{
public:

POLICER arr[C_pol_count];

std::unordered_map <string, int> h_t; //hash table

int Load_all_pol_conf(node* pol_all_n, POL_AGR *old_pol_agr);

int set_pol_to_user(uint32_t cl_hash,uint8_t is_output,uint8_t service,uint16_t pol_ind);

};

class POL_USERS_LIST
	{

std::mutex	mut;

list<int16_t*> a;
public:

bool empty();//!!!!
list<int16_t*>::iterator erase (list<int16_t*>::iterator position);//!!!
void push_front(int16_t* val);//!!!

list<int16_t*>::iterator begin ();///!!
list<int16_t*>::iterator end ();//!!

};



#ifndef MAIN_FILE
extern POL_AGR *G_pol_agr;
extern POL_AGR G_pol_agr_arr[2];

extern POL_USERS_LIST  us_pol[C_pol_count];
#else

POL_AGR G_pol_agr_arr[2]; //no more than 1000//------------------CHANGED

POL_AGR *G_pol_agr=G_pol_agr_arr+G_config_bit;  	//no more than 1000//------------------CHANGED

POL_USERS_LIST  us_pol[C_pol_count];


#endif

#endif
