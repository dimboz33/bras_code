//here is declaration C type of filter 
#define FILTER_CCP

#include <vector>
#include "../config/node.h"


#include <rte_acl.h>

#include "../routing/route.h" // network structure function for reading network 



#include "config_fields.h"
#include "read_fields.h"
#include "filter_const.h"

#include "service.h"
#include "policer.h"



extern node* config_tree;


rte_acl_ctx*  acl_arr[4];//acl_arr_amount]; input_0;output_0;input_1;output_1; for each conveer


uint32_t G_field_value[5][max_n_r]; 

uint32_t G_field_mask[5][max_n_r];

int const max_priority=0xFFFF;

uint32_t G_field_elem[5];//number of elements in each filed 

///READ_FIELD_DATA r_field_data[5];
///rte_acl_field_def ipv4_defs[5];



///////----------------------------------------------- config_one_acl

int config_one_filter(node* filter_conf,uint16_t acl_index,uint8_t acl_elem_in_arr) // fil_conf - filter config
	{ //acl_elem_in_arr=(is_output)+2*config_index;
string name_filter_s=filter_conf->name;//string 
//should find that filter   
char * name_filter_c=new char[name_filter_s.length()+1]; // char
std::strcpy (name_filter_c, name_filter_s.c_str());

//prm.name=name_filter_c; // config of ACL parameter variable

rte_acl_ctx* acl_p=acl_arr[acl_elem_in_arr];


int cur_priority=max_priority;

//should find all not null fields in ACL term

node* termS_n=find("term",filter_conf);
if (termS_n==NULL) return 0;


//------------------------------------START of PROCESSING OF ONE TERM------------------------------------------------------------>>>>

for (node* term_n=termS_n->child_node;term_n!=NULL;term_n=term_n->next_node)
	{
cur_priority--;
//Each value of that array is configured field in READ_FIELD_DATA r_field_data array

node* from_n=find("from",term_n);
node* then_n=find("then",term_n);

uint32_t then_act; //then action

if (then_n==NULL) 
	{
	term_n->err_gen("There isn't \"then\" section for that term");
	return -2; // ther is no then state
}

if (read_action_from_term(then_n,then_act)!=0) return 1;// bad then state;
// if FROM term will be NULL that fact willn't lead to problem.// need to check that hypothesis

if ((then_act&0xFF)==1) then_act+=acl_index;

uint8_t val_f[5]={0};// vields valid in config are collected one by one in queue.
//FOR example--->>
// 0->0
// 1->2
// 2->4
// 3->0
// 4->0
//FOR example---||
//(means fiels 0,2,4 are presents in term)





uint8_t f_count=0; //fields count
///-------------------------read data from field and write NOT NULL fields at val_f array------------------------------->>
for (int i=0;i<5;i++)
	{
	node * XX=find(r_field_data[i].name,from_n);
	if (XX==NULL)  continue;
	if (r_field_data[i].read_data(XX,i)>0) return 3;// there is error;
	if (G_field_elem[i]==0) continue;
	val_f[f_count++]=i;

}


///-------------------------read data from field and write NOT NULL fields at val_f array--------------------------------||




uint32_t cur_f_elem[5]={0};//currect number of element of each field in current combination;
uint32_t curr_comb[5]={0}; //current combination of fields
uint32_t total_combinations_f=1;
//--------------------------------------------------------write combinations number and count of interfalce of each field----->>
for (int i=0;i<f_count;i++)
	{
	cur_f_elem[i]=G_field_elem[val_f[i]];
	total_combinations_f*=cur_f_elem[i];
	curr_comb[i]=0;
}

//---------------------------------------------------------write combinations number and count of interfalce of each field-----||





acl_ipv4_rule* acl_rules=new acl_ipv4_rule [total_combinations_f]();//() - for zeroing array

//--------------------------------------------------Write each combination of rules to acl_rules data---------------------->>
int i1=0;
for (int a=f_count-1;(!(a<0));)
	{
	
//--------------------here we should set field combination in ACL--------->
	acl_rules[i1].data.userdata= then_act;
	acl_rules[i1].data.category_mask=1<<acl_index;
	acl_rules[i1].data.priority = cur_priority;
	
//init total match dst port
acl_rules[i1].field[3].value.u32=0;
acl_rules[i1].field[3].mask_range.u32=0xFFFF;
//init total match src port
acl_rules[i1].field[4].value.u32=0;
acl_rules[i1].field[4].mask_range.u32=0xFFFF;	


	for (int k=0;k<f_count;k++)
		{
		acl_rules[i1].field[val_f[k]].value.u32=G_field_value[val_f[k]][curr_comb[k]];
		acl_rules[i1].field[val_f[k]].mask_range.u32=G_field_mask[val_f[k]][curr_comb[k]];
	}

//--------------------here we should set field combination in ACL---------||
	
	
	for (;(curr_comb[a]==cur_f_elem[a]-1)&&(!(a<0));a--)
		{
	}
	if (a<0) continue;

	curr_comb[a]++;	i1++;
	
	for(int aa=a+1;aa<f_count;aa++) ///////////?????????????????
		{
		curr_comb[aa]=0;
	}
	a=f_count-1;

}
if (f_count==0) //if nothing is configured
	{

	acl_rules[i1].data.userdata= then_act;
	acl_rules[i1].data.category_mask=1<<acl_index;
	acl_rules[i1].data.priority = cur_priority;


	acl_rules[i1].field[3].value.u32=0;
	acl_rules[i1].field[3].mask_range.u32=0xFFFF;
	//init total match src port
	acl_rules[i1].field[4].value.u32=0;
	acl_rules[i1].field[4].mask_range.u32=0xFFFF;	

}


//if f_count = 0 then total_combinations_f=1 -> one rule will be NULL. We need to check that hypothesis


//--------------------------------------------------Write each combination of rules to acl_rules data-------------------------||

// if current data fields willn't match any ACL rules then result of function <<rte_acl_classify >>  will be ZERO. That means that ZERO is default behaviour.
// And we don't need to create default rule for any posible type of data filed with drop result.

 int ret=0;

if  (cur_priority==max_priority) return -4;// no terms 

ret = rte_acl_add_rules(acl_p,( rte_acl_rule*) acl_rules,total_combinations_f);

delete acl_rules; 


  if (ret != 0) {

	term_n->err_gen("rte Can't add rule");

	return -5 ;// cant add rules
}



int k=0;

//---------------------------------------END of PROCESSING ONE TERM----------------------------------------------------------------||

	

	}



return 0;
}



//--------------Initialization of ACL structers
extern "C"
	int ini_acl_structer()
	{
cfg.num_categories = 4;
cfg.num_fields = RTE_DIM(ipv4_defs);
memcpy(cfg.defs, ipv4_defs, sizeof (ipv4_defs));

rte_acl_param prm[4];


for (int i=0;i<4;i++)	
	{
	prm[i].socket_id=SOCKET_ID_ANY;
	prm[i].rule_size=RTE_ACL_RULE_SZ(RTE_DIM(ipv4_defs));// number of fields per rule.
	prm[i].max_rule_num=C_max_rule_num;// maximum number of rules in the AC context. 
}

	prm[0].name="clients_filter_In_0";
	prm[1].name="clients_filter_Out_0";
	prm[2].name="clients_filter_In_1";
	prm[3].name="clients_filter_Out_1";


for (int i=0;i<4;i++)
	{

	

	acl_arr[i] = rte_acl_create(&(prm[i]));
	if (acl_arr[i]==NULL) return 7;////means rte is not initiated
}

return 0;
}
//------------------------------------------------







//--------------------------------------------------------------APPLY CONFIGURATION ON FILTERS SERVICES POLICERS---------------------

extern "C"
 int apply_acl_config (uint8_t config_bit) 
	{
// return 0 if ok
// return -1 - not enough free acl pointer
// return -2 config is wrong  
 
if (config_bit>1) return -1;
 
//--------------------------------------------SOME ACTION WITH DPDK FILTER------------>>
 
if (acl_arr[0+2*config_bit]==NULL)  return 0;//no initialization stady
 
rte_acl_reset_rules(acl_arr[0+2*config_bit]);
rte_acl_reset_rules(acl_arr[1+2*config_bit]);
 
 

// c_ser_conf current serv
//--------------------------------------------SOME ACTION WITH DPDK FILTER------------||



node* firewall_conf=find("Firewall",config_tree); //-------------------FILTER-CONFIG
if (firewall_conf==NULL)
	{
	config_tree->err_gen("No section FIREWALL is configured");
	return 8;
}

// policers and filter have been loaded yet before
//--------------------------------------------------DEF POLICERS--------------------->>

	node* pol_all_n=find("policer",firewall_conf);

	//if (G_pol_agr->Load_all_pol_conf(pol_all_n)!=0) return -5;//

//--------------------------------------------------DEF POLICERS----------------------||

//--------------------------------------------------LOAD SERVICES CONFIG-------------->>
	node* serv_all_n=find("Services",config_tree);
	//if (G_serv_agr->Load_all_serv_conf(serv_all_n)!=0) return -5; //load and sort services

//--------------------------------------------------LOAD SERVICES CONFIG--------------||


//----------------------------------------------------------FIREWALL FILTER CONFIG-->>

node* filter_conf=find("filter",firewall_conf);
if (filter_conf==NULL) 
	{
	config_tree->err_gen("No filter is subsection FIREWALL is configured");
	return 9;
}

//----------------------------------------------------------FIREWALL FILTER CONFIG--||


//----------------------------------------------------------GET IDEX OF DEFAULT POLICERS FOR SERV AGR---->>

for (int i=0;i<16;i++)
	{
	if (G_serv_agr->arr[i].priority==0xFFFF)  continue;
	try
	{
		G_serv_agr->arr[i].pol_index_in=G_pol_agr->h_t.at(G_serv_agr->arr[i].pol_name_in);
	}
	catch(...)
		{
		serv_all_n->err_gen("No  policer "+G_serv_agr->arr[i].pol_name_in+" is configured in section firewal policer!");
		return -5;
	}

	try
	{
		G_serv_agr->arr[i].pol_index_out=G_pol_agr->h_t.at(G_serv_agr->arr[i].pol_name_out);
	}
	catch(...)
		{
		serv_all_n->err_gen("No  policer "+G_serv_agr->arr[i].pol_name_out+" is configured in section firewal policer!");
		return -5;
	}


}


//----------------------------------------------------------GET IDEX OF DEFAULT POLICERS FOR SERV AGR----||
int ret;

G_f_par=G_f_par_arr+config_bit;

G_f_par->nh_c=0;


//-----------------------------------------------------GENERATE ACL IN and OUT FROM FILTERS IN SEQUENCE OF SERVICES--------->>
for (int i=0;i<16;i++)
	{
	uint8_t serv_ind=G_serv_agr->p_que[i];
	
	if (G_serv_agr->arr[serv_ind].priority==0xFFFF)  continue;//it means that current service is empty
	
	node* cur_fil_conf_in= find(G_serv_agr->arr[serv_ind].filter_in,filter_conf);
	node* cur_fil_conf_out=find(G_serv_agr->arr[serv_ind].filter_out,filter_conf);

	if (cur_fil_conf_in==NULL)
		{
		filter_conf->err_gen("No filter "+G_serv_agr->arr[serv_ind].filter_in+ "is configured!!!");
		return -6; //no such filter configured in config
	}

	if (cur_fil_conf_out==NULL)
		{
		filter_conf->err_gen("No filter "+G_serv_agr->arr[serv_ind].filter_out+ "is configured!!!");
		return -6; //no such filter configured in config
	}

	ret=config_one_filter(cur_fil_conf_in,i,0+config_bit*2);	//current input filter

	if (ret !=0) return ret; //wrong in reading config of one filter

	ret=config_one_filter(cur_fil_conf_out,i,1+config_bit*2);	//current output filter

	if (ret !=0) return ret; //wrong in reading config of one filter


	//hear should be config of policer

}

//-----------------------------------------------------GENERATE ACL IN and OUT FROM FILTERS IN SEQUENCE OF SERVICES-----------||


 ret = rte_acl_build(acl_arr[0+2*config_bit], &cfg);

if (ret != 0) {
	filter_conf->err_gen("DPDK can't generate input filter structure!!!");
	return -3;/* handle error at build runtime structures for ACL context. */
}
 ret = rte_acl_build(acl_arr[1+2*config_bit], &cfg);

if (ret != 0) {
	filter_conf->err_gen("DPDK can't generate output filter structure!!!");
	return -3;/* handle error at build runtime structures for ACL context. */
}




return 0;
}


//-----------------------------------------

int apply_acl_config_cpp (uint8_t config_bit)
	{
	return apply_acl_config (config_bit); 

}

