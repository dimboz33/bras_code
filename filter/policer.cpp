#include "../config/node.h"
#include "../routing/route.h"
#include <boost/regex.hpp>

#include "policer.h"



using namespace std;



int POLICER::Load_pol_conf(node* pol_n)
	{
if (pol_n==NULL) return -1;


this->name=pol_n->name;

//----------------------------------------SPEED---------------------------->>

node* speed_n=find("speed",pol_n);



if (speed_n==NULL) 
	{
	pol_n->err_gen("No configured speed");	
	return -5;
}
string speed_s=speed_n->value;

smatch m;

if (!boost::regex_match(speed_s,m,regex("^(\\d+)(M|K|G)$")))
	{
	pol_n->err_gen("Configured speed"+ speed_n->value +"has wrong format; write is N(K|M|G); where N- digit");	
	return -5;
}

char type_s=speed_s[speed_s.size()-1]; //type of 
string digit_s=speed_s.substr(0,speed_s.size()-1);

this->speed=s2numb(digit_s);

int multipl=1;

if (type_s=='M')	multipl=1000;
if (type_s=='G')  multipl=1000000;

if (!(this->speed<(0x80000000/multipl)))
	{
	speed_n->err_gen("speed is too much max value is 2Gbit/s");	
	return -5;
}
this->speed=this->speed*multipl;


//----------------------------------------SPEED----------------------------||

//--------------------------------------------------------------------------------------------------------------

//----------------------------------------BUCKET---------------------------->>

node* bucket_n=find("bucket",pol_n);

if (bucket_n==NULL) 
	{
	pol_n->err_gen("No configured bucket");	
	return -5;
}
string bucket_s=bucket_n->value;



if (!boost::regex_match(bucket_s,m,regex("^(\\d+)(((M|K|G)(b|B))|(m?s))$")))
	{
	pol_n->err_gen("Configured bucket "+bucket_n->value+" has wrong format; write is N(K|M|G)(b|B) or N(ms|s); where N- digit");	
	return -5;
}


if (regex_match(bucket_s,m,regex("^(\\d+)(m?s)$")))
	{
	int mult=1000;
	int shift=1;
	if ((bucket_s[bucket_s.size()-2])=='m') 
		{
		mult=1;shift=2;
	}
	string digit_s=bucket_s.substr(0,bucket_s.size()-shift);

	int buck_digit=s2numb(digit_s);
	if (!(buck_digit<1000000/mult))
		{
		pol_n->err_gen("Configured bucket "+bucket_n->value+" is too large; maximum is 1000 seconds");	
		return -5;
	}

	this->bucket_size_in_ms=buck_digit*mult;

	if (!(this->bucket_size_in_ms<0x80000000/this->speed))
		{
		pol_n->err_gen("Configured bucket "+bucket_n->value+" is too large; maximum is 2Gb");	
		return -5;
	}

	this->bucket_size=this->bucket_size_in_ms*this->speed;//ms*kb/s=bits

	return 0;
}


type_s=bucket_s[bucket_s.size()-2]; //type M G K

char type_byte=bucket_s[bucket_s.size()-1]; // byte of bit

digit_s=bucket_s.substr(0,bucket_s.size()-2);

this->bucket_size=s2numb(digit_s);

multipl=1;

if (type_byte=='B') multipl=multipl*8;

if (type_s=='G') multipl=multipl*1000000;
if (type_s=='M') multipl=multipl*1000;

if (!(this->bucket_size<(0x80000000/multipl)))
	{
	speed_n->err_gen("bucket is too much max value is 2Gb");	
	return -5;
}

this->bucket_size=this->bucket_size*multipl;

if (!(this->bucket_size/this->speed<1000))
	{
	speed_n->err_gen("bucket is too much max value is 1000 seconds");	
	return -5;
}
this->bucket_size_in_ms=this->bucket_size*1000/this->speed;

//----------------------------------------BUCKET----------------------------||

return 0;
}


//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------



int POL_AGR::Load_all_pol_conf(node* pol_all_n, POL_AGR* old_pol_agr)
	{
	
if (pol_all_n==NULL)
	{
	pol_all_n->err_gen("No policer is subsection firewall is configured");
	return -1;
}
if (pol_all_n->child_node==NULL)
	{
	pol_all_n->err_gen("No policer is subsection firewall is configured");
	return -1;
}

this->h_t.clear();//clear hash table


uint8_t is_configured[C_pol_count]={0};//is configured old policers
	 
	
uint16_t f_ind=0;//index of free policer with no users

for (node* pol_n=pol_all_n->child_node;(pol_n!=NULL);pol_n=pol_n->next_node)
	{
	POLICER pol;//nw policer
	if (pol.Load_pol_conf(pol_n)!=0) return -2; //add policer to array of policers


	uint16_t pol_ind;//policer_index;

	try 	
		{
		pol_ind=old_pol_agr->h_t.at(pol.name);
		this->arr[pol_ind]=pol;

		//POLICER::swap_users(this->arr+pol_ind,old_pol_agr->arr+pol_ind);
		
		this->h_t[pol.name]=pol_ind;
		is_configured[pol_ind]=1;	 

	}
	catch(...)
		{
		for (;!us_pol[f_ind].empty();f_ind++)
			{
			if (!(f_ind<C_pol_count))
				{
				pol_all_n->err_gen("Maximum number of policers is"+std::to_string(C_pol_count)+"!!!");
				return -3;

			}
		}
		//get new index of policer with empty users list
		this->arr[f_ind]=pol;
		this->h_t[pol.name]=f_ind;
		f_ind++;

	}

}	

//now we need to check is any old used policers with clietns that no more in new config
for (int i=0;i<C_pol_count;i++)
	{
	if ((!us_pol[i].empty())&&(is_configured[i]!=1)) // clietns is present and policer was not configured again
		{
		pol_all_n->err_gen("policer"+old_pol_agr->arr[i].name+"is used, but it is not present in new config file!!!");
		return -4;
	}

}


return 0;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------



