#include "../config/node.h"
#include "../routing/route.h" // network structure // function for reading network 

#include "filter_const.h"
#include "filter.h"


extern uint32_t G_field_value[5][max_n_r]; 

extern uint32_t G_field_mask[5][max_n_r];

extern uint32_t G_field_elem[5];//number of elements in each filed 

using namespace boost;

//////////--------------------------read_ip_network_from filter---------------------------------------------->

int read_networks_from_term(node* net_node,uint8_t field_index)///source_ip_address { string1;sting2; ...)
	{//field index - number of field

	int i=0;
	for(node* net_n=net_node->child_node;((net_n!=NULL)&&(i<1000));net_n=net_n->next_node, i++)
		{
		Netw cur_net;
		if (s2Netw(net_n->name,cur_net)>0)
			{
			net_n->err_gen("network" + net_n->name + "is wrong");return 1;

		}

		G_field_value[field_index][i]=cur_net.ip;
		G_field_mask[field_index][i]=cur_net.mask;
	}
	G_field_elem[field_index]=i;
return 0;
}
//////////--------------------------read_port_from filter---------------------------------------------------->

int read_port_from_term(node* net_node,uint8_t field_index) //source_port string;
	{//field index - number of field

	int i=0;
	string s_port=net_node->value;


	boost::regex port_interval_reg("\\[\\d*\\,\\d*\\]");

	boost::sregex_token_iterator iter_port_interval(s_port.begin(), s_port.end(), port_interval_reg, 0);
 	boost::sregex_token_iterator end;

	for( i=0 ; (iter_port_interval != end)&&(i<1000); ++iter_port_interval, i++) 
		{
      	string s_port_interval=(*iter_port_interval).str();
		boost::regex min_port_reg("\\d*\\,");
		boost::regex max_port_reg("\\,\\d*");
		
		uint16_t p_min, p_max, p;

		boost::smatch m;
		string s;
		

		//---------------------------------------------------MIN port
		boost::regex_search(s_port_interval,m,min_port_reg);
		
		s=*m.begin();

		if (s=="")
			{
			p_min=0;
		}
		else
			{
			s=s.substr(0,s.size()-1); 

			p_min=stoi(s);//
		}

		//----------------------------------------------------MAX port
		boost::regex_search(s_port_interval,m,max_port_reg);
		
		s=*m.begin();

		if (s=="")
			{
			p_max=0xffff;
		}
		else
			{
			s=s.substr(1,s.size()); 

			p_max=stoi(s);//
		}

		G_field_value[field_index][i]=p_min;
		G_field_mask[field_index][i]=p_max;
	}


	string s_port_point=boost::regex_replace(s_port, port_interval_reg,string(""));
	
	boost::regex port_point("\\d+");

	boost::sregex_token_iterator iter_port_point(s_port_point.begin(), s_port_point.end(), port_point, 0);

	regex port_reg("\\d+");

	for(; (iter_port_point != end)&&(i<1000); ++iter_port_point, i++) 
		{
		string s_port_point=(*iter_port_point).str();
		int p=stoi(s_port_point);//
		G_field_value[field_index][i]=p;
		G_field_mask[field_index][i]=p;

		
	}

	G_field_elem[field_index]=i;


return 0;	
}

//////////--------------------------read_ip_protocol_from filter---------------------------------------------->

int read_ip_protocol_from_term(node* net_node,uint8_t field_index)///source_ip_address { string1;sting2; ...)
	{//field index - number of field

	boost::regex tcp_reg("tcp");
	boost::regex udp_reg("udp");

	string s_ip_prot=net_node->value;

	int i=0;



	if(boost::regex_search(s_ip_prot,tcp_reg))
		{
		G_field_value[field_index][i]=0x6;
		G_field_mask[field_index][i]=0xff;
		i++;
	}
	if(boost::regex_search(s_ip_prot,udp_reg))
		{
		G_field_value[field_index][i]=0x11;
		G_field_mask[field_index][i]=0xff;
		i++;
	}

	G_field_elem[field_index]=i++;
	
return 0;
}
//////////--------------------------read_action_from filter---------------------------------------------->

int read_action_from_term(node* then_n,uint32_t &act)
	{
	std::string s_act=then_n->value;

	if  (s_act=="reject")
		{
		act=0xff;
	        return 0;
	}
	if (s_act=="accept")
		{
		act=1;
		return 0;
	}
	uint32_t nh_ip;
	if (s2ip(s_act,nh_ip)!=0)
		{
		then_n->err_gen("action " + then_n->value + " is wrong. May be \"accept\" or \"reject\" ");
		return 1;
	}

	//try find index of nh_ip in Filter nh_ip array 
	int ind_nh=-1;

	for (int i=0;i<G_f_par->nh_c;i++)
		{
		if (G_f_par->nh_arr[i]==nh_ip) 
			{
			ind_nh=i;
			break;
		}
	}

	if (ind_nh<0)
		{
		if (G_f_par->nh_c==0xFE)
			{
			then_n->err_gen("To may next-hops ip addresses in the FILTER section. Maximum 255 different IP next-hops");
			return 1;
		}
	
		G_f_par->nh_arr[G_f_par->nh_c]=nh_ip;
		ind_nh=G_f_par->nh_c; // 0 means no new next-hop
		G_f_par->nh_c++;
	}


	act=1+((ind_nh+1)<<8);

	return 0;

}