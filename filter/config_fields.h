
#include "filter.h"
#include "read_fields.h"

#include "filter_const.h"

struct rte_acl_config cfg;

READ_FIELD_DATA r_field_data[5] = {
// ip protocol
{
.name="protocol",
.read_data=read_ip_protocol_from_term,
},
{
.name="source-address",
.read_data=read_networks_from_term,
},
{
.name="destination-address",
.read_data=read_networks_from_term,
},
{
.name="source-port",
.read_data=read_port_from_term,
},
{
.name="destination-port",
.read_data=read_port_from_term,
}};

//////---------FIELD CONFIG OF FILTER------------------------>>>

rte_acl_field_def ipv4_defs[5] = {
/* first input field - always one byte long. */ // tcp or udp
{
.type = RTE_ACL_FIELD_TYPE_BITMASK,
.size = sizeof (uint8_t),
.field_index = 0,
.input_index = 0,
.offset = 9,//9-th byte 
},
/* next input field (IPv4 source address) - 4 consecutive bytes. */
{
.type = RTE_ACL_FIELD_TYPE_MASK,
.size = sizeof (uint32_t),
.field_index = 1,
.input_index = 1,
.offset = 3*4,
},
/* next input field (IPv4 destination address) - 4 consecutive bytes. */
{
.type = RTE_ACL_FIELD_TYPE_MASK,
.size = sizeof (uint32_t),
.field_index = 2,
.input_index = 2,
.offset = 4*4,
},
/*
* Next 2 fields (src & dst ports) form 4 consecutive bytes.
* They share the same input index.
*/
{///source port
.type = RTE_ACL_FIELD_TYPE_RANGE,
.size = sizeof (uint16_t),
.field_index = 3,
.input_index = 3,
.offset = 5*4,
},
{//destination port
.type = RTE_ACL_FIELD_TYPE_RANGE,
.size = sizeof (uint16_t),
.field_index = 4,
.input_index = 3,
.offset = 5*4+2,
},
};




//////---------FIELD CONFIG OF FILTER------------------------|||
