#include "service.h"
	
#include <list>


		    bool SERV_USERS_LIST::empty()	{ return a.empty(); }
std::list<uint32_t>::iterator SERV_USERS_LIST::begin () {return a.begin();}
std::list<uint32_t>::iterator SERV_USERS_LIST::end ()	{ return a.end();}


std::list<uint32_t>::iterator SERV_USERS_LIST::erase (list<uint32_t>::iterator position)
	{
	std::lock_guard<std::mutex> lock(mut);
	return a.erase(position);
}

		    void SERV_USERS_LIST::push_front(uint32_t val)
	{
	std::lock_guard<std::mutex> lock(mut);
	return a.push_front(val);
}