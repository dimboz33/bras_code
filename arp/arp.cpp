#include "arp.h"
#include "../timers/timers_array.h"
#include "../routing/interfaces.h"
#include "../routing/forwarding_table.h"
#include <arpa/inet.h> // for change bit order

#include "../chassis/bras_chassis.h"
#include "../clients/clients.h"
#include "../radius/rad_client.h"

extern struct LogInt* G_log_ints; 
extern struct PhyInt* G_phy_ints;  
extern  timers_array* G_timers_arr; 




int ARP_not_received(int time,
		uint32_t d1[4],//
		void* p1[4] //pointer to ARPtable, pointer to arp entry
			)
	{


ARPe* arp=(ARPe*)p1[1];

if (arp->state==0)
	{
	arp->is_ok=0;// MAC is not actual;

	arp->timer_n=-1;		 // no timer set


return 0;
}

		arp->timer_n=G_timers_arr->new_timer(G_arp_table->getting_timer,
							ARP_not_received,			
						d1,
						p1);


//could not leave getting state if arp  is referend by static route

if (arp->state==1) //getting
	{
	if (arp->counter>0)
		{
		arp->counter--;
	}
	else
		{
		arp->is_ok=0;
		
		if (arp->is_static_route==1)
			{
			arp->counter= G_arp_table->max_count_of_getting_tries;
			G_for_table->Refresh_routes();//we need to recalculate forwarding table
		}
		else 
			{
			arp->is_ok=0;// MAC is not actual;
			arp->timer_n=-1;
			return 0;
		}

	}
	
}
if (arp->state==2) //valid
	{
	arp->state=1;//getting
	arp->counter= G_arp_table->max_count_of_getting_tries;
}
 
Generate_ARP_Request(arp);

return 0;
}
//-----------------------------------------------------------

int ARPe::get_arp()	
	{
	if (this->is_ok==1) return -2; //arp is valid
	if (!(this->timer_n<0)) return -1;//it tries to request address
	//don't use curr_arp->state field becouse mac address of static routes could have state=0;
	//but valid timer for getting arp

	//------------------------------------------should generate arp iniation----->>
	this->state=2; //that is trick to get state when state was 2 and we don't receive arp
			//for arp request variables
	uint32_t d1[4];//
	void* p1[4]; //pointer to ARPtable, pointer to arp entry
	p1[1]=(void*)this;
	ARP_not_received(0,d1,p1); // continus request of arp several times
	//-------------------------------------------should generate arp iniation-----||	

return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!! PROCESSING OF RECEIVED ARP PACKET

void ARP_packet_processing(uint8_t* eth_header,uint8_t* L2_payload,uint32_t log_int_index)
	{
	uint8_t Opcode; //wireshard  
	Opcode=L2_payload[7];

		//We should first update MAC table and then send replay
		
		struct LogInt* log_int=G_log_ints+log_int_index; // logical interface

		uint16_t arp_sector=log_int->arp_index;
		uint32_t Sender_IP=htonl(*((uint32_t*)(L2_payload+14)));
		if ((Sender_IP&log_int->mask)!=(log_int->net.ip&log_int->mask))
			{
			//sender address is out of our network
			return;	
		}
		//sender address good
		ARPe* curr_arp=&(G_arp_table->arp_arr[0])+(Sender_IP-(log_int->net.ip&log_int->mask)+arp_sector); // current arp
		for (int i=0;i<6;i++)
			{
			curr_arp->mac_addr[i]=L2_payload[8+i];
		}
		//vlan and physical_int_index should be initialized with mac table

		curr_arp->counter= G_arp_table->max_count_of_getting_tries;

		if ((curr_arp->is_static_route==1)&&(curr_arp->is_ok==0))
			{
			curr_arp->state=2;
			curr_arp->is_ok=1;//we need to set correct values before recalculating refreshing routes
			//mac of static route and state was not ok
			G_for_table->Refresh_routes();
		}
		curr_arp->state=2;//valid
		curr_arp->is_ok=1; // this is true mac address could be used by routing table


		uint32_t d1[4];//parametres for timer
		void* p1[4]; //pointer to ARPtable, pointer to arp entry

		p1[1]=(void*)curr_arp;


		//initialize arp timer assosiated with received arp entry
		
		if (curr_arp->timer_n<0)		 // no timer set
			{
			curr_arp->timer_n=G_timers_arr->new_timer(G_arp_table->valid_timer,
						ARP_not_received,			
						d1,
						p1);
	
		}
		else 
			{
			//reset timer
			curr_arp->timer_n=G_timers_arr->reset_timer(G_arp_table->valid_timer,curr_arp->timer_n);

			if (G_timers_arr->t_data[curr_arp->timer_n].timeslot<0)
				{
				return;

			}

			if (curr_arp->timer_n<0)
				{

				return;// may be a problem with timers//!!!
			}
			

		}

	if (curr_arp->timer_n<0)
		{

		return;// may be a problem with timers//!!!
	}

	
	uint32_t TPA=htonl(*(uint32_t*)(L2_payload+24));

	if ((Opcode==1)&&(TPA==log_int->net.ip)) // request and targer procolo addres is ours then we should responce
		{
		//we should generate arp responce
		uint8_t arp_packet[46];//28+18vlan_ethernet
 		for (int i=0;i<6;i++)
			{
			arp_packet[i+6]=G_phy_ints[curr_arp->physical_int_index].mac_addr[i];
			arp_packet[i]=curr_arp->mac_addr[i];
		}
		uint8_t  data=12;
		
		if (curr_arp->vlan!=0x10) // with vlan tag
			{
			arp_packet[12]=0x81; arp_packet[13]=0x00;
			*((uint16_t*)(arp_packet+14))=curr_arp->vlan;//htons(curr_arp->vlan);
			data=data+4;
		}

	
	      arp_packet[data++]=0x08; arp_packet[data++]=0x06; // arp ethertype
		arp_packet[data++]=0x00; arp_packet[data++]=0x01; // Hardware type
		arp_packet[data++]=0x08; arp_packet[data++]=0x00; // Protocol type IP
		arp_packet[data++]=0x06;	//harwware size 6
		arp_packet[data++]=0x04;	//protocol size 4
		arp_packet[data++]=0x00; arp_packet[data++]=0x02; // Opcode replay
		for (int i=0;i<6;i++)
			{ //sender Mac address
			arp_packet[data++]=arp_packet[i+6];	
		}
		//Sender IP address
			*((uint32_t*)(arp_packet+data))=htonl(log_int->net.ip);data=data+4;
		
		for (int i=0;i<6;i++)
			{ //target Mac address - BROADCAST
			arp_packet[data++]=arp_packet[i];		
		}	

		//Target IP address
			*((uint32_t*)(arp_packet+data))=htonl(Sender_IP);data=data+4;
		
		send_packet(arp_packet,data,curr_arp->physical_int_index);
	}


return;
}


///////////////////////////////////////////////////GENERATE ARP PACKET

uint8_t Generate_ARP_Request(ARPe* curr_arp)
{
uint8_t arp_packet[46];//28+18vlan_ethernet

for (int i=0;i<6;i++)
			{
			arp_packet[i+6]=G_phy_ints[curr_arp->physical_int_index].mac_addr[i];
			arp_packet[i]=0xFF;//broadcast
		}
		uint8_t  data=12;
		
		if (curr_arp->vlan!=0x10) // with vlan tag 0x10 - is reverted 0x1000
			{
			arp_packet[12]=0x81; arp_packet[13]=0x00;
			*((uint16_t*)(arp_packet+14))=curr_arp->vlan;//htons(curr_arp->vlan);
			data=data+4;
		}

	
	      arp_packet[data++]=0x08; arp_packet[data++]=0x06; // arp ethertype
		arp_packet[data++]=0x00; arp_packet[data++]=0x01; // Hardware type
		arp_packet[data++]=0x08; arp_packet[data++]=0x00; // Protocol type IP
		arp_packet[data++]=0x06;	//harwware size 6
		arp_packet[data++]=0x04;	//protocol size 4
		arp_packet[data++]=0x00; arp_packet[data++]=0x01; // Opcode request
		for (int i=0;i<6;i++)
			{ //sender Mac address
			arp_packet[data++]=arp_packet[i+6];	
		}

		LogInt* log_int=G_log_ints+G_phy_ints[curr_arp->physical_int_index].Vlan_log_int_table[htons(curr_arp->vlan)];
		
		//Sender IP address
			*((uint32_t*)(arp_packet+data))=htonl(log_int->net.ip);data=data+4;
		
		for (int i=0;i<6;i++)
			{ //target Mac address - BROADCAST
			arp_packet[data++]=arp_packet[i];		
		}	

		//Target IP address
		uint32_t Target_IP=(log_int->net.ip&log_int->mask)+(curr_arp-(&(G_arp_table->arp_arr[0])+log_int->arp_index));

					

			*((uint32_t*)(arp_packet+data))=htonl(Target_IP);data=data+4;
		
		send_packet(arp_packet,data,curr_arp->physical_int_index);




return 1;
}

uint8_t cl_ARP_packet_processing(uint8_t* eth_header,uint8_t* L2_payload,uint32_t phy_int_index)
	{
// is respond 1 - delete packet 
// is respond 0 - send packet back

//no matter Respond or request; we must prolongate abonent session deleting process
//check is that abonent in out routing


uint8_t *m=eth_header;

uint8_t L2_len=L2_payload-m;

uint8_t l2_key[11]={0};

memcpy(l2_key+0,m+6,6);//source address
memcpy(l2_key+6,m+14,2);//VLAN_OUT address
if (L2_len==22) memcpy(l2_key+8,m+18,2);//VLAN_IN address

l2_key[10]=phy_int_index;

	int32_t hash=rte_hash_lookup(cl_l2_ht, l2_key);

int ip_hash;
CL_IP_SESS *cl_sess;

	// �� ����� �������� �������� �� ������
	if (hash<0) return 1;  //goto AAAA;
	
	// �� ����� �������� �������� �� ������
	if (cl_l2_dt[hash].cl_ip_hash<0) return 1;  //goto AAAA;
			
	ip_hash=cl_l2_dt[hash].cl_ip_hash; 
	cl_sess=cl_ip_sess_dt+ip_hash;
	uint32_t target_prot_add = *((uint32_t*)(L2_payload+24)); // requested ip

	if (( target_prot_add & cl_sess->mask ) != (cl_sess->ip & cl_sess->mask )) 
		return 1;// ������ ������ ����� ��� ������� ����� �� ������� ����� � ���������� arp � ����

	cl_sess->arp_k_counter=cl_sess->arp_k_max_counter;//	G_rad_auth.conf.p0.arp_k_counter; 	

	
	AAAA:

	if (L2_payload[7]==1 ) //Request
		{
		for ( int i=0;i<6;i++)
			{
			eth_header[i]=eth_header[i+6];//eth_dst_new= eth_src_old
			L2_payload[i+18]=eth_header[i+6];//target hadware address

			eth_header[i+6]=G_phy_ints[phy_int_index].mac_addr[i]; // eth_src_new=our_mac_address
			L2_payload[i+8]=G_phy_ints[phy_int_index].mac_addr[i]; // sender hardware address
		}
		
		//we should change target and serder protocol address 

		uint32_t send_prot_add=*((uint32_t*)(L2_payload+14)); 

		*((uint32_t*)(L2_payload+14))=*((uint32_t*)(L2_payload+24)); 
		*((uint32_t*)(L2_payload+24))=send_prot_add; 

		L2_payload[7]=2; //REPLAY 

		return 0;	
	}


return 1;// no replay
}

//---------------------------------------------------------------------------------------------------------

uint8_t cl_Generate_ARP_Request(uint32_t ip_hash)
	{
uint8_t arp_packet[50];//28+22vlan_ethernet

//int ip_hash=d1[0];

if (!(ip_hash<size_cl_ip_ht)) return -1;

CL_IP_DAEL *cl_ip=cl_ip_dt+ip_hash;

if (cl_ip->phy_int==0xFF) return -1;// meance client is deleted

CL_INT_DAEL *cl_forw_in =  cl_forw_dt[0+2*cl_ip->phy_int].cl_arr+cl_ip->cl_index_in;

CL_IP_SESS *cl_sess=cl_ip_sess_dt+ip_hash;


if (cl_sess->state!=1) return -1;// meance client is deleted



for (int i=0;i<6;i++)
			{
			arp_packet[i+6]=G_phy_ints[cl_ip->phy_int].mac_addr[i];
			arp_packet[i]=cl_forw_in->mac[i];//mac of client		
		}
		uint8_t  data=12;

			arp_packet[data++]=0x81; arp_packet[data++]=0x00;
			*((uint16_t*)(arp_packet+data))=cl_forw_in->vlan_out;//htons(curr_arp->vlan);
			data+=2;
//-----------------

			if (cl_forw_in->vlan_in!=0x10)
				{
				arp_packet[data++]=0x81; arp_packet[data++]=0x00;
				*((uint16_t*)(arp_packet+data))=cl_forw_in->vlan_in;//htons(curr_arp->vlan);
				data+=2;
			}
	
	      arp_packet[data++]=0x08; arp_packet[data++]=0x06; // arp ethertype
		arp_packet[data++]=0x00; arp_packet[data++]=0x01; // Hardware type
		arp_packet[data++]=0x08; arp_packet[data++]=0x00; // Protocol type IP
		arp_packet[data++]=0x06;	//harwware size 6
		arp_packet[data++]=0x04;	//protocol size 4
		arp_packet[data++]=0x00; arp_packet[data++]=0x01; // Opcode request
		for (int i=0;i<6;i++)
			{ //sender Mac address
			arp_packet[data++]=arp_packet[i+6];	
		}

		//Sender IP address shoul set hear default gateway of client //!!!!!!!!!!!!?????????????????????????????
			*((uint32_t*)(arp_packet+data))=cl_sess->def_gw_ip;data=data+4;
		for (int i=0;i<6;i++)
			{ //target Mac address - BROADCAST
			arp_packet[data++]=arp_packet[i];		
		}	

		//Target IP address
		uint32_t Target_IP=cl_sess->ip;//ip of client

					

			*((uint32_t*)(arp_packet+data))=htonl(Target_IP);data=data+4;
		
		send_packet(arp_packet,data,cl_ip->phy_int);




return 1;
}
