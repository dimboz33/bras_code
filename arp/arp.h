//--------------------------------------------------structure of arp table
#include <inttypes.h> // for uint32_t
#include <vector>

#ifndef ARP_TYPE

#define ARP_TYPE

struct ARPe {

// data
	uint8_t	mac_addr[8];
	uint16_t	vlan; //0xffff for without vlan case
	
	uint16_t	log_int_index;
	uint8_t	physical_int_index; 

//states

	uint8_t state; // 0-empty ; 1 -getting ; 2- valid
	uint8_t is_ours;// is this mac address is ours or not 1 - this is our mac 0 not 
	uint8_t is_ok;//is this mak address is valid 
	uint8_t counter; 

	uint8_t is_static_route;
// timer 

	int32_t timer_n; //timer number is array of timers  -1 means no set 


int get_arp();
};

////////////// ARP table with parametres

class ARP_TABLE {
public:
std::vector<ARPe> arp_arr;
uint32_t arp_amount;


uint32_t empty_timer;// in seconds;
uint32_t getting_timer;// in seconds;
uint32_t valid_timer; //in seconds;
uint32_t max_count_of_getting_tries;


ARP_TABLE () 
	{
/////////////////////////////////////////////////////
this->empty_timer=60;//seconds
this->getting_timer=5;//seconds
this->valid_timer=60;//secons
this->max_count_of_getting_tries=5;////
//////////////////////////////////////////////////////
}

};



//!!!!!!!!!!! PROCESSING OF RECEIVED ARP PACKET

void ARP_packet_processing(uint8_t* eth_header,uint8_t* L2_payload,uint32_t log_int_index);

uint8_t cl_ARP_packet_processing(uint8_t* eth_header,uint8_t* L2_payload,uint32_t phy_int_index); //arp packet from client

////////////////////////////

int ARP_not_received(int time,
		uint32_t d1[4],//
		void* p1[4] //pointer to ARPtable, pointer to arp entry
);

uint8_t Generate_ARP_Request(ARPe* curr_arp);
uint8_t cl_Generate_ARP_Request(uint32_t ip_hash);

#ifndef MAIN_FILE
extern ARP_TABLE G_arp_table_arr[2]; //--------------------for PFE module
extern ARP_TABLE *G_arp_table; //--------------------for RE module 
#else

ARP_TABLE G_arp_table_arr[2]; //--------------------for PFE module




ARP_TABLE *G_arp_table=G_arp_table_arr+G_config_bit;	   
//----------------------for RE module (reference because there is a problem in assigment of vector data(arp_entries) )
//--we need to get the same element in  RE structer and PFE structure

#endif

#endif


