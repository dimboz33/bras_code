#include "dpdk_include.h"
#include "chas_def_struct_glob_var.h"
#include "extern_chass_var.h"
#include "display.h"

#include <unistd.h> // for usleep microsecond 

#include "../timers/time_slot_change.h"

//-----------------------------SENDING PACKET BY RE----------------------------------

uint8_t send_packet(uint8_t* data, uint16_t length, uint8_t d_port,uint8_t* ci)
                                                                   
{                                                                  
struct rte_mbuf *m;
uint8_t* m_data;


m=rte_pktmbuf_alloc(l2fwd_pktmbuf_pool);

if (m==NULL)
	{
	return 2;//no memory in m_buf poll
}
m_data= rte_pktmbuf_mtod(m,uint8_t*);
m->udata64=0;//means from RE

uint8_t add=1;
if ((d_port==0xFE)&&(ci==NULL))
	{
	rte_pktmbuf_free(m);
	return 1;
}


if (d_port==0xFE)
	{ 
	add=0;
	
	rte_memcpy((uint8_t*)(m->buf_addr)+64,ci,8);//for control information

}
	m_data[0]=0;
	rte_memcpy(m_data+add,data, length);//first byte should be 0 for outgoing core
	m_data[0]=0;                                   
	m->data_len=length+add;      ////??????????????????????                                     
	m->pkt_len=length+add;       ////??????????????????????                                     



if (rte_ring_enqueue(rings_of_ports[d_port] ,m)!=0)   
                /*we should delete packet not enough space in output ring */     
                {                                                  
                rte_pktmbuf_free(m);                               
                port_statistics[d_port*2].dropped++;//not sure     
return 1;//mean dropped packet;                                    
}                                                                  
                                                                   
          port_statistics[3*0xFE].tx++;                                                         
                                                                   
return 0;                                                          
}                                                                  
//----------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//Master LOOP

/* main processing loop  Master*/
 int RE_main_processing_loop(__attribute__((unused)) void *dummy)

	{
	struct rte_mbuf *pkts_burst[MAX_PKT_BURST];
	struct rte_mbuf *m;
        unsigned lcore_id;
        uint64_t prev_tsc, diff_tsc, cur_tsc, timer_tsc;
	uint8_t portid;
	uint8_t pk_st;
	struct rte_ring* our_ring;
	uint32_t nb_rx;
        prev_tsc = 0;
        timer_tsc = 0;

	lcore_id=rte_lcore_id();

        RTE_LOG(INFO, L2FWD, "Master core %u is started\n", lcore_id);
	

	//We should configure shadows of physicalk interfaces


	portid=0xFE;//LOOPBACK 
	our_ring=rings_of_ports[portid];

	int m_timer=0;
	//---------------------------initial config
	if (C_reload_config()!=0) return -1;//error in config 

	our_ring=rings_of_ports[0xFE];
                                
	while (1) 
		{
		cur_tsc = rte_rdtsc();
		diff_tsc = cur_tsc - prev_tsc;               
	
			int is_any_packet=0;

			
		
                        //---------------------------------------------------- Timers processing---->>			


         /* advance the timer */           
                                timer_tsc += diff_tsc;            
                                                                  
                                /* if timer has reached its timeout */
                                      if (unlikely(timer_tsc >= (uint64_t) timer_period))
						{
               	                                //print_stats();    
                                                /* reset the timer */
                                                timer_tsc = 0;  

						time_slot_change(); 

						m_timer++;

						//3*0x00->current //3*0x10->last second//3*0x20  last two second
					/*	
						memcpy(port_statistics+0x60,port_statistics+0x30,0x30*sizeof(struct l2fwd_port_statistics));
						memcpy(port_statistics+0x30,port_statistics+0x00,0x30*sizeof(struct l2fwd_port_statistics));

						for (int i=0;i<0x30;i++)
							{
							uint64_t diff=port_statistics[i+0x30].rx-port_statistics[i+0x60].rx;
							//uint64_t diff2=port_statistics[i+0x30].m[7]-port_statistics[i+0x60].m[7];
							port_statistics[i].m[7]=0;
							if (1)///diff!=0)
								{
								for (int p=0;p<6;p++)
									{//port_statistics[i+0x60].m[p]=(port_statistics[i+0x30].m[p]-port_statistics[i+0x60].m[p])/diff;								
									port_statistics[i].m[p]=0;
								}
							}
							 
						     // if (diff2!=0) port_statistics[i+0x60].m[7]=diff/diff2;								


	

						}
					*/

						if (m_timer>10)
							{
							C_reload_config();
							m_timer=1;
						} 
                                        }                         
                        //---------------------------------------------------- Timers processing----||           
                        //----------------------------------------------------
                        //----------------------------------------------------
                        //---------------------------------------------------- Packet processing--->>
                               
                        prev_tsc = cur_tsc;   


			nb_rx=rte_ring_dequeue_burst( our_ring,(void **)pkts_burst,MAX_PKT_BURST) ;

			if (nb_rx>0) is_any_packet=1;


			port_statistics[3*portid].rx += nb_rx;
			int j;

			uint8_t d_portid;
			uint8_t* pk_st;//packet star
			for (j = 0; j < nb_rx; j++) {
				int16_t packet_diff=0; // how longer became header
				m = pkts_burst[j];
				rte_prefetch0(rte_pktmbuf_mtod(m, void *));
				pk_st=rte_pktmbuf_mtod(m, uint8_t *);
				
				uint8_t* m_d=(uint8_t*)(pkts_burst[j]->buf_addr)+64;//for control information
				
				packet_diff=0;
				//Process packet	
				d_portid=RE_Receive_packet(pk_st,m_d,m->pkt_len,&packet_diff);
				
			
				if ((d_portid!=0xFE)&&(d_portid<Phy_Int_Count+1))
					{ //that is usuall routing port
					packet_diff++;
					pk_st[-packet_diff]=0;
				}

				if (packet_diff!=0)
					{
					if (packet_diff>0)	rte_pktmbuf_prepend(m,packet_diff);
					else			rte_pktmbuf_adj (m,-packet_diff);
				}
		


				if ((d_portid==0xff)||(d_portid==0x80+Phy_Int_Count) )
					{
					port_statistics[3*portid].dl++;
					rte_pktmbuf_free(m);
				}

				else
					{
					//case when destination port is RE is also included
					if (rte_ring_enqueue (rings_of_ports[d_portid],m)!=0) 
						/*we should delete packet not enough space in output ring */
						{
						rte_pktmbuf_free(m);
						 port_statistics[3*portid].dropped++;
					}
					else	
						{
						port_statistics[3*portid].tx++;
					}


				}

				
			}	
                        //---------------------------------------------------- Packet processing---||

			if (is_any_packet==0) usleep(10000);//mikrosecond
                                                                           
       }

return 0;
}



