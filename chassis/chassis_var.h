


//////--------------------------------CHASSIS VAR------------------------------>>

 


extern struct rte_ring * rings_of_ports[RTE_MAX_ETHPORTS];

extern uint8_t	Output_lcores[RTE_MAX_ETHPORTS];
extern uint8_t	Input_lcores[RTE_MAX_ETHPORTS];


extern struct lcore_queue_conf lcore_queue_conf[RTE_MAX_LCORE];

extern static const struct rte_eth_conf port_conf = {
	.rxmode = {
		.split_hdr_size = 0,
		.header_split   = 0, /**< Header Split disabled */
		.hw_ip_checksum = 0, /**< IP checksum offload disabled */
		.hw_vlan_filter = 0, /**< VLAN filtering disabled */
		.jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
		.hw_strip_crc   = 0, /**< CRC stripped by hardware */
	},
	.txmode = {
		.mq_mode = ETH_MQ_TX_NONE,
	},
};

extern struct rte_mempool * l2fwd_pktmbuf_pool = NULL;

extern struct l2fwd_port_statistics port_statistics[0x100];

extern static uint16_t nb_rxd = RTE_TEST_RX_DESC_DEFAULT;
extern static uint16_t nb_txd = RTE_TEST_TX_DESC_DEFAULT;

/* ethernet addresses of ports */
extern  struct ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];


/* mask of enabled ports */
extern static uint32_t l2fwd_enabled_port_mask =0x3;

/* list of enabled ports */
extern  uint32_t l2fwd_dst_ports[RTE_MAX_ETHPORTS];




extern uint8_t nb_ports_a=0; /*nubmer of port_active */
 


/* A tsc-based timer responsible for triggering statistics printout */

extern static int64_t timer_period = 1 * TIMER_MILLISECOND * 1000; /* default period is 1 seconds */

//////-----------------------CHASSIS VAR--------------------------------||

