

//////////////////DEFINED PARAMETRES--->>>>

#define RTE_LOGTYPE_L2FWD RTE_LOGTYPE_USER1

/*size of room for packet */
#define MBUF_SIZE (2048 + sizeof(struct rte_mbuf) + RTE_PKTMBUF_HEADROOM)



#define BURST_TX_DRAIN_US 100 /* TX drain every ~100us */
/*
 *  * Configurable number of RX/TX ring descriptors
 *   */
#define RTE_TEST_RX_DESC_DEFAULT 512//128  at 1024 L3fwd drops beginning
#define RTE_TEST_TX_DESC_DEFAULT 512 //512


#define N_rep 16 //64 very good
#define MAX_PKT_BURST 64 //32 //const for slow structures kni for example and RE
//#define INT_P_B_RECV 32 //packet burst
//#define INT_P_B_SEND 256
//#define RING_P_B_RECV 1024



#define MAX_RX_QUEUE_PER_LCORE 16
#define MAX_TX_QUEUE_PER_PORT 16

#define TIMER_MILLISECOND 2000000ULL /* around 1ms at 2 Ghz */
#define MAX_TIMER_PERIOD 86400 /* 1 day max */


///////======================================DEFINED PARAMETRES-----|



#define ci_off  128-32 //32 byte offset before packet starts 
#define FILT_C 64 //filters count


///////================================================STRUCTURES------------->>

struct mbuf_table {
	unsigned len;
	struct rte_mbuf *m_table[MAX_PKT_BURST];
};

struct lcore_queue_conf {
	uint8_t port ;
	struct mbuf_table tx_mbufs;


} __rte_cache_aligned;

/* Per-port statistics struct */
struct l2fwd_port_statistics {
	uint64_t tx; // 
	uint64_t dl; // deleted
	uint64_t rx;
	uint64_t dropped; // queue problem
	uint64_t m[10]; //marker
} __rte_cache_aligned;

////////////=======================================STRUCTURES-------------||





//////////////////------------------------------TOTALLY GLOBAL VAR---------------------->
#ifndef BRAS_CHASSIS

extern uint32_t NB_MBUF;
extern uint32_t MEMPOOL_CACHE_SIZE;
extern uint32_t RING_P_B_RECV;
extern uint32_t RING_SIZE;
extern uint32_t  prefetch_numb;

extern uint32_t RING_P_B_RECV; //256 64-optimized!!!
extern uint32_t INT_P_B_RECV; //packet burst
extern uint32_t INT_P_B_SEND;


#endif

////////-----------------INTERFACE TYPE TABLE--------->

extern volatile int force_quit;//volatile

extern  uint8_t  Phy_Int_Count;
extern uint8_t int_types[];//0-ethernet; 1 - kni; 0xFF - not used

extern uint8_t G_mac_ints[];

////////-----------------INTERFACE TYPE TABLE---------|


/////////////////////////////////HASH TABLES --->>
extern uint32_t size_cl_dhcp_ht;
extern uint32_t size_cl_l2_ht;
extern uint32_t size_cl_ip_ht;

extern struct  rte_hash *cl_dhcp_ht;
extern struct  rte_hash *cl_l2_ht;
extern struct  rte_hash *cl_ip_ht;



static struct rte_hash_parameters  cl_dhcp_ht_params;
static struct rte_hash_parameters  cl_l2_ht_params;
static struct rte_hash_parameters  cl_ip_ht_params;


/////////////////////////////////HASH TABLES ---||

extern uint8_t G_config_bit; //volatile
extern volatile uint8_t G_first_config; //volatile
