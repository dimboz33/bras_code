
extern struct rte_ring * rings_of_ports[0x100];
extern struct rte_ring * rings_of_FILTERS[];

extern unsigned ring_size;
extern uint8_t nb_ports;

extern uint8_t	Output_lcores[0x100];
extern uint8_t	Input_lcores[0x100];

extern struct lcore_queue_conf lcore_queue_conf[0x100];

extern struct rte_mempool *l2fwd_pktmbuf_pool;

extern struct l2fwd_port_statistics port_statistics[2*0x100];


/* ethernet addresses of ports */

extern  struct ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];

extern  struct rte_kni *KNI_PORTS[0x100];


/* A tsc-based timer responsible for triggering statistics printout */
extern  int64_t timer_period; /* default period is 1 seconds */



