

#include "dpdk_include.h"
#include "chas_def_struct_glob_var.h"
#include "extern_chass_var.h"
/* display usage */
static void
l2fwd_usage(const char *prgname)
{
	printf("%s [EAL options] -- -p PORTMASK [-q NQ]\n"
	       "  -p PORTMASK: hexadecimal bitmask of ports to configure\n"
	       "  -q NQ: number of queue (=ports) per lcore (default is 1)\n"
		   "  -T PERIOD: statistics will be refreshed each PERIOD seconds (0 to disable, 10 default, 86400 maximum)\n",
	       prgname);
}





/* Print out statistics on packets dropped */
 void print_stats(void)
{
	uint64_t total_packets_dropped, total_packets_tx, total_packets_rx;
	unsigned portid;

	total_packets_dropped = 0;
	total_packets_tx = 0;
	total_packets_rx = 0;

	const char clr[] = { 27, '[', '2', 'J', '\0' };
	const char topLeft[] = { 27, '[', '1', ';', '1', 'H','\0' };

		/* Clear screen and move to top left */
	printf("%s%s", clr, topLeft);

	printf("\nPort statistics ====================================");

	char eth_type[]={'E','t','h','\0'};

	char kni_type[]={'K','n','i','\0'};

	char *type_port;

	
	
	for (portid = 0; portid < Phy_Int_Count+1; portid++) {
	
		uint8_t port_number=portid;	
		if (int_types[portid]>1)
			{
			continue;//unknown type configured
		}
		
		type_port=eth_type;
		if (portid==Phy_Int_Count) 
			{
			type_port=kni_type;
			port_number=0;
		}
		
///-----------------------INPUT DIRECTION

		printf("\nStatistics for INPUT direction port %s %u ------------------------------"
			   "\nPackets sent to Chassis: %24"PRIu64
			   "\nPackets received from Media: %20"PRIu64
      			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64,
				type_port,
			   port_number,
			   port_statistics[3*portid].tx,
			   port_statistics[3*portid].rx,
		           port_statistics[3*portid].dl,
			   port_statistics[3*portid].dropped);

//		total_packets_dropped += port_statistics[portid*2].dropped;
//		total_packets_tx += port_statistics[portid*2].tx;
//		total_packets_rx += port_statistics[portid*2].rx;

//-----------------FILTERS STATISTICS

		if (int_types[portid]==1)
			{	

			printf("\nStatistics for INPUT FILTER port %s %u ------------------------------"
			   "\nPackets sent to Chassis: %24"PRIu64
			   "\nPackets received from Interface: %20"PRIu64
      			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64,
				type_port,
			   portid,
			   port_statistics[3*portid+1].tx,
			   port_statistics[3*portid+1].rx,
		           port_statistics[3*portid+1].dl,
			   port_statistics[3*portid+1].dropped);

		}

///----------------------OUTPUT DIRECTION

		printf("\nStatistics for OUTPUT direction port %s %u ------------------------------"
			   "\nPackets sent to Media: %24"PRIu64
			   "\nPackets received from Chassis: %20"PRIu64
			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64,
				type_port,
			   port_number,
			   port_statistics[3*portid+2].tx,
			   port_statistics[3*portid+2].rx,
			   port_statistics[3*portid+2].dl,
			   port_statistics[3*portid+2].dropped);


	}
/*
	printf("\nAggregate statistics ==============================="
		   "\nTotal packets sent: %18"PRIu64
		   "\nTotal packets received: %14"PRIu64
		   "\nTotal packets dropped: %15"PRIu64,
		   total_packets_tx,
		   total_packets_rx,
		   total_packets_dropped);
*/

	printf("\n====================================================\n");
}

//=================================================================================================================

 void sprint_stats(char* buff)
{

	int b_n=0;

	uint64_t total_packets_dropped, total_packets_tx, total_packets_rx;
	unsigned portid;

	total_packets_dropped = 0;
	total_packets_tx = 0;
	total_packets_rx = 0;

	const char clr[] = { 27, '[', '2', 'J', '\0' };
	const char topLeft[] = { 27, '[', '1', ';', '1', 'H','\0' };

		/* Clear screen and move to top left */
	b_n+=sprintf(buff+b_n,"%s%s", clr, topLeft);

	b_n+=sprintf(buff+b_n,"\nPort statistics ====================================");

	char eth_type[]={'x','e','-','\0'};

	char kni_type[]={'K','n','i','-','\0'};

	char *type_port;

	
	
	for (portid = 0; portid < Phy_Int_Count+1; portid++) {
	
		uint8_t port_number=portid;	
		if (int_types[portid]>1)
			{
			continue;//unknown type configured
		}
		
		type_port=eth_type;
		if (portid==Phy_Int_Count) 
			{
			type_port=kni_type;
			port_number=0;
		}
		
///-----------------------INPUT DIRECTION

		b_n+=sprintf(buff+b_n,"\nStatistics for INPUT direction port %s %u ------------------------------"
			   "\nPackets sent to Chassis: %24"PRIu64
			   "\nPackets received from Media: %20"PRIu64
      			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64
			   "\nmarker 1: %21"PRIu64
			   "\nmarker 2: %21"PRIu64
			   "\nmarker 3: %21"PRIu64
			   "\nmarker 4: %21"PRIu64
		           "\nmarker 7: %21"PRIu64,
				type_port,
			   port_number,
			   port_statistics[3*portid].tx,
			   port_statistics[3*portid].rx,
		           port_statistics[3*portid].dl,
			   port_statistics[3*portid].dropped,
			port_statistics[3*portid+0x00].m[1],//+0x60
			port_statistics[3*portid+0x00].m[2],
			port_statistics[3*portid+0x00].m[3],  
			port_statistics[3*portid+0x00].m[4],
			port_statistics[3*portid+0x00].m[7]
				);

//		total_packets_dropped += port_statistics[portid*2].dropped;
//		total_packets_tx += port_statistics[portid*2].tx;
//		total_packets_rx += port_statistics[portid*2].rx;

//-----------------FILTERS STATISTICS

		if (int_types[portid]==1)
			{	

		b_n+=sprintf(buff+b_n,"\nStatistics for INPUT FILTER port %s %u ------------------------------"
			   "\nPackets sent to Chassis: %24"PRIu64
			   "\nPackets received from Interface: %20"PRIu64
      			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64
			   "\nmarker 1: %21"PRIu64
			   "\nmarker 2: %21"PRIu64
			   "\nmarker 3: %21"PRIu64
			   "\nmarker 4: %21"PRIu64
			   "\nmarker 7: %21"PRIu64,
				type_port,
			   portid,
			   port_statistics[3*portid+1].tx,
			   port_statistics[3*portid+1].rx,
		           port_statistics[3*portid+1].dl,
			   port_statistics[3*portid+1].dropped,
			port_statistics[3*portid+1+0x60].m[1],
			port_statistics[3*portid+1+0x60].m[2],
			port_statistics[3*portid+1+0x60].m[3],  
			port_statistics[3*portid+1+0x60].m[4],
			port_statistics[3*portid+1+0x60].m[7]
				);

		}

///----------------------OUTPUT DIRECTION

		b_n+=sprintf(buff+b_n,"\nStatistics for OUTPUT direction port %s %u ------------------------------"
			   "\nPackets sent to Media: %24"PRIu64
			   "\nPackets received from Chassis: %20"PRIu64
			   "\nPackets deleted: %20"PRIu64
			   "\nPackets dropped: %21"PRIu64
       			"\nmarker 1: %21"PRIu64
			   "\nmarker 2: %21"PRIu64
			   "\nmarker 3: %21"PRIu64
			   "\nmarker 4: %21"PRIu64
			   "\nmarker 5: %21"PRIu64
			   "\nmarker 6: %21"PRIu64
			   "\nmarker 7: %21"PRIu64,
				type_port,
			   port_number,
			   port_statistics[3*portid+2].tx,
			   port_statistics[3*portid+2].rx,
			   port_statistics[3*portid+2].dl,
			   port_statistics[3*portid+2].dropped,

			port_statistics[3*portid+2+0x60].m[1],
			port_statistics[3*portid+2+0x60].m[2],
			port_statistics[3*portid+2+0x60].m[3],  
			port_statistics[3*portid+2+0x60].m[4],
			port_statistics[3*portid+2+0x60].m[5],
			port_statistics[3*portid+2+0x60].m[6],
			port_statistics[3*portid+2+0x60].m[7]
			);


	}


		b_n+=sprintf(buff+b_n,"\n====================================================\n");
}

