 

#include "dpdk_include.h"
#include "chas_def_struct_glob_var.h"
#include "extern_chass_var.h"





/*main processing loop INTERFACE OUTPUT CORE*/
int Interface_FILTER_CORE_main_processing_loop(__attribute__((unused)) void *dummy)
{	
	const uint8_t F_C=16; //FILTER COUNT
	
struct rte_mbuf **pkts_burst=		malloc(sizeof(struct rte_mbuf*)*RING_P_B_RECV);
	uint32_t *filter_result=	malloc(sizeof(uint32_t)*F_C*RING_P_B_RECV);

struct rte_mbuf **log_pkt=		malloc(sizeof(struct rte_mbuf*)*RING_P_B_RECV);//logiucal interface packets from
struct rte_mbuf **cl_pkt=		malloc(sizeof(struct rte_mbuf*)*RING_P_B_RECV);//client interface packets from
	
	uint8_t *cl_int=			malloc(sizeof(uint8_t)*RING_P_B_RECV);//next interface
	uint8_t *log_int=		malloc(sizeof(uint8_t)*RING_P_B_RECV);//next interface

	uint16_t log_pkt_c=0;
	uint16_t cl_pkt_c=0;//counters



uint8_t** L3_arr=			malloc(sizeof(uint8_t*)*RING_P_B_RECV);// arrya of reference to L3 headers of packets  

struct rte_mbuf **rb=malloc(sizeof(struct rte_mbuf*)*32*RING_P_B_RECV);
	uint16_t rb_c[32]={0};

struct rte_mbuf **re_p=		malloc(sizeof(struct rte_mbuf*)*RING_P_B_RECV);
struct rte_mbuf **del_p=	malloc(sizeof(struct rte_mbuf*)*RING_P_B_RECV);//packet needed to be deleted

	uint16_t del_p_c=0;
	uint16_t re_p_c=0;


	struct rte_mbuf *m;
	unsigned lcore_id;
	uint64_t prev_tsc, diff_tsc, cur_tsc, timer_tsc;
	unsigned  portid;

	uint32_t i,j,nb_rx;
	struct lcore_queue_conf *qconf;
	struct rte_ring* our_ring;
	const uint64_t Core_hz = rte_get_tsc_hz();
	
	prev_tsc = 0;
	timer_tsc = 0;

	lcore_id = rte_lcore_id();
	qconf = &lcore_queue_conf[lcore_id];



	portid = qconf->port;
	our_ring=rings_of_FILTERS[portid];


	RTE_LOG(INFO, L2FWD, "entering main loop on lcore %u\n", lcore_id);

	RTE_LOG(INFO, L2FWD, " -- lcoreid=%u portid=%u, Input_Filter_CORE\n", lcore_id,portid);
	

	const uint32_t drain_tsc = rte_get_tsc_hz()/1000;//1 ms
	uint32_t ms_counter=0;

	portid = qconf->port;


	while (G_first_config==1)
		{
		int a=1;
		a++;
		// wait for load configuration
	}


	while (force_quit != 1) {

					
			nb_rx=rte_ring_dequeue_burst( our_ring,(void **)pkts_burst,RING_P_B_RECV, 0 );//PKT_REC_BURST

			if (nb_rx==0) continue;

			port_statistics[3*portid+1].rx += nb_rx;

			if (port_statistics[3*portid+1].m[7]<nb_rx)
				{
				port_statistics[3*portid+1].m[7]=nb_rx;
			}
			

				
			cur_tsc = rte_rdtsc();

			port_statistics[3*portid+1].m[0]=cur_tsc;

			
			if (unlikely((cur_tsc-prev_tsc)>drain_tsc))
				{
				ms_counter++;
				prev_tsc =cur_tsc;
				
			}
		

			uint16_t fp_c;// filters packets count
			int j1;
			
			log_pkt_c=0;
			cl_pkt_c=0;

			uint8_t nb_pre=prefetch_numb;
			
				for (j=0;(j<nb_pre)&&(j<nb_rx);j++)	
			 		{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j],uint8_t*);//pkts_burst[j1]->buf_addr+ci_off;
				//	rte_prefetch0(m1-16);
				//	rte_prefetch0(m1+16);
			    	}
						
			

			//int k=0;
			for (j=0;j<nb_rx;j++)
				{
				//uint8_t* ci=(uint8_t*)(pkts_burst[j]->buf_addr)+64;
				uint8_t* ci=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);
				
				
				L3_arr[j]=ci+ci[8];//ci[8];rte_pktmbuf_mtod(pkts_burst[j],uint8_t*)
				

				if (j+nb_pre<nb_rx)
					{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j+nb_pre],uint8_t*);//@@
				//	rte_prefetch0(m1-16);
				//	rte_prefetch0(m1+16);
				
		    		}
							
			}
			
			//	nb_rx=k;
			
				port_statistics[3*portid+1].m[1]+=rte_rdtsc()-port_statistics[3*portid+1].m[0];
				//------------------------------------------PREFATCH PACKETS------------->>>			
			/*
				for (j=0;(j<nb_pre)&&(j<nb_rx);j++)	
			 		{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j],uint8_t*);//pkts_burst[j1]->buf_addr+ci_off;
					rte_prefetch0(m1);
			    	}
			*/
				//------------------------------------------PREFATCH PACKETS-------------|||
				
			
				//-------------------------------------for clients packets
				uint64_t marker;
				PP_FILTER(L3_arr,pkts_burst,filter_result,portid,nb_rx,0,ms_counter,0,prefetch_numb,G_config_bit,&marker);
				uint8_t d_port;   
				for (j=0;j<nb_rx;j++)
					{
					uint8_t* ci=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);
					d_port=ci[9];

					if (filter_result[j*F_C]==0xFF) d_port=0xFF;

					if (d_port>0xFC) del_p[del_p_c++]=pkts_burst[j]; //0xFF 0xFE
					else	rb[d_port*RING_P_B_RECV+(rb_c[d_port]++)]=pkts_burst[j];
					
				}

			port_statistics[3*portid+1].m[2]+=marker-port_statistics[3*portid+1].m[0];

			port_statistics[3*portid+1].m[3]+=rte_rdtsc()-port_statistics[3*portid+1].m[0];

			int ret=0;
			//------------------------------------------>>SEND PACKET TO INTERFACE RING
			for ( j=0;(j<Phy_Int_Count+1);j++) 				
				{

				if (rb_c[j]>0)//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{
					ret=rte_ring_enqueue_burst(rings_of_ports[j] ,(void*)(rb+j*RING_P_B_RECV),rb_c[j], 0);
					port_statistics[3*portid+1].tx+=ret;
					if (ret < rb_c[j]) {
						port_statistics[3*portid+1].dropped += (rb_c[j] - ret);
						do {
							rte_pktmbuf_free(rb[j*RING_P_B_RECV+ret]);
						} while (++ret < rb_c[j]);
	
					}
				rb_c[j]=0;

			}

				
			
			}
			port_statistics[3*portid+1].m[4]+=rte_rdtsc()-port_statistics[3*portid+1].m[0];
			//port_statistics[3*portid+1].m[7]++;

			
			///------------------> FOR RE ring

			j=0xFE;
				if (re_p_c>0)//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{
					ret=rte_ring_enqueue_burst(rings_of_ports[0xFE] ,(void*)re_p,re_p_c, 0);
					//port_statistics[3*portid+1].tx+=ret;
					if (unlikely(ret < rb_c[j])) {
						//port_statistics[3*portid+1].dropped += (re_p_c - ret);
						do {
							rte_pktmbuf_free(re_p[ret]);
						} while (++ret < re_p_c);
	
					}
				}

				re_p_c=0;
			///-------------------|| For RE ring

			///-------------------DELETE-PACKET------->
			j=0xFF;
				if (del_p_c>0)
					{
					port_statistics[3*portid+1].dl+=del_p_c;
						ret=0;
						do {
							rte_pktmbuf_free(del_p[ret]);
						} while (++ret < del_p_c);
	
				}
	
				del_p_c=0;


			//--------------------DELETE-PACKET-------||
			
			
								


			
	}

return 0;
}