#include "dpdk_include.h" 

#include "chas_def_struct_glob_var.h"

#define BRAS_CHASSIS

//#include "../PP/PP.h"
//#include "../timers/time_slot_change.h"
#include "CORES_LOOPS.h"


uint32_t  prefetch_numb=		4;
uint32_t NB_MBUF=		65535;//8192 optimized value
uint32_t MEMPOOL_CACHE_SIZE=	32;//????

uint32_t RING_P_B_RECV=	256; //256 64-optimized!!!
uint32_t INT_P_B_RECV=  64; //packet burst
uint32_t INT_P_B_SEND= 64;


uint32_t RING_SIZE=	2048;





//////--------------------------------CHASSIS VAR------------------------------>>

int kni_module_is_started=0;

uint8_t nb_ports;//number of valid port on Chassis

struct rte_ring * rings_of_ports[0x100]={NULL};//0xFE for RE
struct rte_ring * rings_of_FILTERS[0x100]={NULL};


uint8_t	Output_lcores[0x100];
uint8_t	Input_lcores[0x100];

uint8_t	FILTER_lcores[0x100]; //cores for input filtering

uint8_t 	kni_core_id;

struct rte_kni *KNI_PORTS[0x100]={NULL};

struct lcore_queue_conf lcore_queue_conf[0x100];// start with big latter



 const struct rte_eth_conf port_conf = {
	.rxmode = {
		.split_hdr_size = 0,
		.header_split   = 0, //< Header Split disabled
		.hw_ip_checksum = 0, //< IP checksum offload disabled 
		.hw_vlan_filter = 0, //< VLAN filtering disabled 
		.jumbo_frame    = 1, //< Jumbo Frame Support disabled 
		.max_rx_pkt_len = 1600,
		.hw_strip_crc   = 1, //< CRC stripped by hardware 
	},
	.txmode = {
		.mq_mode = ETH_MQ_TX_NONE,
	}
};



struct rte_mempool *l2fwd_pktmbuf_pool = NULL;

struct l2fwd_port_statistics port_statistics[2*0x100];

 uint16_t nb_rxd = RTE_TEST_RX_DESC_DEFAULT;
 uint16_t nb_txd = RTE_TEST_TX_DESC_DEFAULT;

/* ethernet addresses of ports */
 struct ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];

 uint8_t kni_mac[6]={0x00,0x19,0xDB,0xF3,0x94,0x30}; //mac addres of my nik
/* mask of enabled ports */
 uint32_t l2fwd_enabled_port_mask =0x3;

/* list of enabled ports */
 uint32_t l2fwd_dst_ports[RTE_MAX_ETHPORTS];


/* A tsc-based timer responsible for triggering statistics printout */

 int64_t timer_period = 1 * TIMER_MILLISECOND * 1000; /* default period is 1 seconds */



//////-----------------------CHASSIS VAR--------------------------------||






//--------------------------------------

/* Callback for request of changing MTU */
static int
kni_change_mtu(uint8_t port_id, unsigned new_mtu)
{
	int ret;
	struct rte_eth_conf conf;

	printf("Change MTU of port %d to %u\n", port_id, new_mtu);


	return 0;
}

/* Callback for request of configuring network interface up/down */
static int
kni_config_network_interface(uint8_t port_id, uint8_t if_up)
{
	int ret = 0;

	printf("Configure network interface of %d %s\n",port_id, if_up ? "up" : "down");

	return ret;
}




//------------------------------------INIT ETH PORT------------->
void init_eth_port(uint8_t portid)
	{
		/* init port */
		printf("Initializing port %u... ", (unsigned) portid);
		fflush(stdout);
		int ret = rte_eth_dev_configure(portid, 1, 1, &port_conf);
		if (ret < 0)
			rte_exit(EXIT_FAILURE, "Cannot configure device: err=%d, port=%u\n",
				  ret, (unsigned) portid);

		rte_eth_macaddr_get(portid,&l2fwd_ports_eth_addr[portid]);


		///external reference//write mac to physical interface data structure
		
		memcpy(G_mac_ints+portid*6,l2fwd_ports_eth_addr[portid].addr_bytes,6);
		

		/* init one RX queue */
		fflush(stdout);
		for (uint8_t q_numb=0;q_numb<1;q_numb++)
			{
			ret = rte_eth_rx_queue_setup(portid, q_numb, nb_rxd,
					     rte_eth_dev_socket_id(portid),
					     NULL,
					     l2fwd_pktmbuf_pool);
			if (ret < 0)
				rte_exit(EXIT_FAILURE, "rte_eth_rx_queue_setup:err=%d, port=%u\n",
					  ret, (unsigned) portid);
		
		}
		for (uint8_t q_numb=0;q_numb<1;q_numb++)
			{
			/* init one TX queue on each port */
			fflush(stdout);

			ret = rte_eth_tx_queue_setup(portid, q_numb, nb_txd,
				rte_eth_dev_socket_id(portid),
				NULL);
			if (ret < 0)
				rte_exit(EXIT_FAILURE, "rte_eth_tx_queue_setup:err=%d, port=%u\n",
					ret, (unsigned) portid);

		}

		/* Start device */
		ret = rte_eth_dev_start(portid);
		if (ret < 0)
			rte_exit(EXIT_FAILURE, "rte_eth_dev_start:err=%d, port=%u\n",
				  ret, (unsigned) portid);

		printf("done: \n");

		//rte_eth_promiscuous_enable(portid);

		printf("Port %u, MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n\n",
				(unsigned) portid,
				l2fwd_ports_eth_addr[portid].addr_bytes[0],
				l2fwd_ports_eth_addr[portid].addr_bytes[1],
				l2fwd_ports_eth_addr[portid].addr_bytes[2],
				l2fwd_ports_eth_addr[portid].addr_bytes[3],
				l2fwd_ports_eth_addr[portid].addr_bytes[4],
				l2fwd_ports_eth_addr[portid].addr_bytes[5]);

return;
}
//-------------------------INIT_ETH_PORT----------------------------||


//-------------------------INIT KNI PORT----------------------------->>

void init_kni_port(uint8_t port_id,uint8_t lcore_id)
	{
	struct rte_kni *kni;
	struct rte_kni_conf conf;

	
	
	memset(&conf, 0, sizeof(conf));
	
	if (port_id<Phy_Int_Count)
		{
		snprintf(conf.name, RTE_KNI_NAMESIZE,"br_xe-%u", port_id);
	}
	else
		{
		snprintf(conf.name, RTE_KNI_NAMESIZE,"br_kni-0");

	}

	conf.core_id=lcore_id;

	conf.force_bind = 0;
	
	conf.group_id=0;
	conf.mbuf_size = 2048;

	///external reference//write mac to physical interface data structure
		int j;

		if (port_id==Phy_Int_Count) // that is br_kni port
			{ 
		 	memcpy(G_mac_ints+port_id*6,kni_mac,6);
		}
	
			struct rte_kni_ops ops;
			struct rte_eth_dev_info dev_info;

			memset(&dev_info, 0, sizeof(dev_info));
			rte_eth_dev_info_get(0, &dev_info);//0-port-id
			conf.addr = dev_info.pci_dev->addr;
			conf.id = dev_info.pci_dev->id;

			memset(&ops, 0, sizeof(ops));
			ops.port_id = port_id;
			
			ops.change_mtu = kni_change_mtu;//very important!!!!!!!!!!!!!!!!!
			ops.config_network_if =kni_config_network_interface;			

			kni = rte_kni_alloc(l2fwd_pktmbuf_pool, &conf, &ops);

		if (kni==NULL)
			{
			rte_exit(EXIT_FAILURE, "Fail to create kni for port: %s", conf.name);

		}
	KNI_PORTS[port_id]=kni;

return;
}

//-------------------------INIT KNI PORT-----------------------------||

//==============================================================================
int
main_bras_chassis(		int argc, char **argv,uint8_t const PPhy_Int_Count)
{
	//nb_port_c - number of ports configured
	struct lcore_queue_conf *qconf;
	struct rte_eth_dev_info dev_info;
	int ret;
	
	uint8_t portid;
	unsigned lcore_id;

	printf("NB_MBUF=		%d\n",NB_MBUF);
	printf("MEMPOOL_CACHE_SIZE=	%d\n",MEMPOOL_CACHE_SIZE);
	printf("RING_SIZE=		%d\n",RING_SIZE);


	printf("RING_P_B_RECV=		%d\n",RING_P_B_RECV);
	printf("INT_P_B_RECV=		%d\n",INT_P_B_RECV);
	printf("INT_P_B_SEND=		%d\n",INT_P_B_SEND);
	


	/* init EAL */
	ret = rte_eal_init(argc, argv);
	if (ret < 0)
		{
		rte_exit(EXIT_FAILURE, "Invalid EAL arguments\n");
		return -1;
	}
	argc -= ret;
	argv += ret;

	//------------------------initialize ACL structures
	ret=ini_acl_structer();
	if (ret < 0) rte_exit(EXIT_FAILURE, "Cant initiate ACL structers \n");


	
	/////////////////////////////////////////////// set socket id of all hashes-->

	cl_dhcp_ht_params.socket_id =	rte_socket_id();
	  cl_l2_ht_params.socket_id = 	rte_socket_id();
	  cl_ip_ht_params.socket_id = 	rte_socket_id();


	cl_dhcp_ht_params.entries	= size_cl_dhcp_ht;
	  cl_l2_ht_params.entries	= size_cl_l2_ht;
	  cl_ip_ht_params.entries	= size_cl_ip_ht;

	cl_dhcp_ht_params.key_len 	=16; //mac(6byte)+Circuit_ID(4byte)+Agent_ID(6byte);
	cl_l2_ht_params.key_len 	=11;//mac(6byte)+vlan_out(2byte)+vlan_in(byte)+phy_int(1byte);
	cl_ip_ht_params.key_len		=4;


	cl_dhcp_ht_params.name ="cl_dhcp_ht";
	   cl_l2_ht_params.name="cl_l2_ht";
	   cl_ip_ht_params.name="cl_ip_ht";

	/////////////////////////////////////////////// set socket id of all hashes--|

	///////////////////////////////////////////////create HASH TABLES ----->

	cl_dhcp_ht=rte_hash_create(&cl_dhcp_ht_params);
	cl_l2_ht=rte_hash_create(&cl_l2_ht_params);
	cl_ip_ht=rte_hash_create(&cl_ip_ht_params);

	///////////////////////////////////////////////create HASH TABLES -----|
	
	/////////////////////////////////////////////// IF INITIALIZATION WAS WRONG------>

	if  (cl_dhcp_ht==NULL) 	rte_exit(EXIT_FAILURE, "Cannot build the DHCP_MAC hash\n");
	if  (cl_l2_ht==NULL)	rte_exit(EXIT_FAILURE, "Cannot build the CLIENT VLANs hash\n");
	if  (cl_ip_ht==NULL) 	rte_exit(EXIT_FAILURE, "Cannot build the CLIENT IP hash\n");

	///////////////////////////////////////////////IF INITIALIZATION WAS WRONG------|


	/* create the mbuf pool */

	int aaa = rte_socket_id();

	l2fwd_pktmbuf_pool =
		
			rte_pktmbuf_pool_create("mbuf_pool", NB_MBUF,
		MEMPOOL_CACHE_SIZE, 0, RTE_MBUF_DEFAULT_BUF_SIZE,
		rte_socket_id());

		
		/*
		rte_mempool_create("mbuf_pool", NB_MBUF,
				   MBUF_SIZE, 32,
				   sizeof(struct rte_pktmbuf_pool_private),
				   rte_pktmbuf_pool_init, NULL,
				   rte_pktmbuf_init, NULL,
				   rte_socket_id(), 0);
		*/		

	if (l2fwd_pktmbuf_pool == NULL)
		rte_exit(EXIT_FAILURE, "Cannot init mbuf pool\n");


//------------this is real number of eth port on the board
	nb_ports = rte_eth_dev_count();
	if (nb_ports == 0)
		rte_exit(EXIT_FAILURE, "No Ethernet ports - bye\n");

	if (nb_ports > RTE_MAX_ETHPORTS)
		nb_ports = RTE_MAX_ETHPORTS;

	

	
	for (portid = 0; portid < nb_ports; portid++) {
	
		rte_eth_dev_info_get(portid, &dev_info);
	}

	if (PPhy_Int_Count<nb_ports) 
		{
		printf("MAXIMUM SUPPORTED PORTS IS %n",Phy_Int_Count );
		nb_ports=PPhy_Int_Count	;
	}

	Phy_Int_Count=nb_ports;

	

	qconf = NULL;
	uint8_t master_lcore=rte_get_master_lcore();
	lcore_id=rte_get_next_lcore(master_lcore,1,0);


//--------------------Initialize the port/queue configuration of each logical core------------>>>

	for (portid = 0; portid < Phy_Int_Count+1; portid++) { //only for physical interfaces
 		
		if (int_types[portid]>1)
			{
			continue;//unknown type configured
		}

		char  name_ring_p[3]="p_ \n";//port
		char  name_ring_f[3]="f_ \n"; //filter
		char  name_ring_s[3]="s_ \n";//shadow

		name_ring_p[2]=portid+65;// latter A
		name_ring_f[2]=portid+65;
		name_ring_s[2]=portid+65;


		rings_of_ports[portid]= rte_ring_create(name_ring_p, RING_SIZE, rte_socket_id(), RING_F_SC_DEQ);

		if ((rings_of_ports[portid])==NULL) return -2;

		if (int_types[portid]==1) // this is client port
			{
			rings_of_FILTERS[portid]=rte_ring_create(name_ring_f, RING_SIZE, rte_socket_id(), RING_F_SC_DEQ);
		}

		if (portid!=Phy_Int_Count)
			{
			//for each physical interface should create shad_que
			rings_of_ports[portid+0x80]=rte_ring_create(name_ring_s, RING_SIZE, rte_socket_id(), RING_F_SC_DEQ);
		}

		
		//----------------------------------------------------
		if (portid==Phy_Int_Count) continue;// this is initialization only for physical ports
		//input_core

		Input_lcores[portid]=lcore_id;
		lcore_queue_conf[lcore_id].port=portid;

		lcore_id=rte_get_next_lcore(lcore_id,1,0);
		
	
		//----------------------------------------------------

		//output_core
		Output_lcores[portid]=lcore_id;
		lcore_queue_conf[lcore_id].port=portid;			

		lcore_id=rte_get_next_lcore(lcore_id,1,0);

		//----------------------------------------------------
		//----------------------------------------------------

		//filter_core
		//if ethernet port

		if (int_types[portid]==1) // if interface is clients 
		{
		FILTER_lcores[portid]=lcore_id;
		lcore_queue_conf[lcore_id].port=portid;

		lcore_id=rte_get_next_lcore(lcore_id,1,0);
		}

		//lcore_id=rte_get_next_lcore(lcore_id,1,0);

		//lcore_id=rte_get_next_lcore(lcore_id,1,0);


	//	lcore_id=rte_get_next_lcore(lcore_id,1,0);


	//	lcore_id=rte_get_next_lcore(lcore_id,1,0);


	}

	//--get number for kni core;
	kni_core_id=lcore_id;lcore_id=rte_get_next_lcore(lcore_id,1,0);


//--------------------Initialize the port/queue configuration of each logical core------------||||		



	/// initialize  input ring for RE 

	rings_of_ports[0xFE]=rte_ring_create("Loopback", RING_SIZE, rte_socket_id(), 0);

	
	//----------------KNI PRE INITIALIZATION--------->
	uint8_t num_of_kni_ports=Phy_Int_Count+1; //kni and shadows

	// Invoke rte KNI init to preallocate the ports 
	
	rte_kni_init(Phy_Int_Count+1);kni_module_is_started=1;//num_of_kni_ports);
	
	
	//----------------KNI PRE INITIALIZATION---------||


	//-----------------------------------------------------Initialise each port----------------->>>


	// initialize port stats
	memset(&port_statistics, 0, sizeof(port_statistics));


	//kni_core_id
	//for physical port
	for (portid = 0; portid < Phy_Int_Count; portid++)
		{

		init_eth_port(portid);
		//SHADOW PORT	
		init_kni_port(portid,lcore_id); //kni_core_id+1 - is next free core after occupied by dpdk

	}
		//br_kni port
		init_kni_port(Phy_Int_Count,0); //0 core is not so busy
	

	//-----------------------------------------------------Initialise each port-----------------|||


	//----------Launch INTERFACES ETH AND FILTERS CORES-->>
	for (portid=0;portid < Phy_Int_Count;portid++)

		{

		if (int_types[portid]>1)
			{
			continue;//unknown type configured
		}
	
		if (int_types[portid]==0) //ETH not client
			{
			//-----------------------------------------------------------
			rte_eal_wait_lcore (Output_lcores[portid]);
			rte_eal_remote_launch(Interface_OUTPUT_CORE_ETH_main_processing_loop,
					NULL,
					Output_lcores[portid]);
			//-----------------------------------------------------------	
	
			rte_eal_wait_lcore (Input_lcores[portid]);
      			rte_eal_remote_launch(Interface_INPUT_CORE_ETH_main_processing_loop,
                                        NULL,
                              Input_lcores[portid]);
			//-----------------------------------------------------------
	
		}
		
		if (int_types[portid]==1) //client core
			{
			//-----------------------------------------------------------

			rte_eal_wait_lcore (Output_lcores[portid]);
			rte_eal_remote_launch(Interface_OUTPUT_CORE_ETH_main_processing_loop,
					NULL,
					Output_lcores[portid]);
			//-----------------------------------------------------------	
	
		
			//----------------------------------------------------------

			rte_eal_wait_lcore (FILTER_lcores[portid]);
       			rte_eal_remote_launch(Interface_FILTER_CORE_main_processing_loop,
                                        NULL,
                              FILTER_lcores[portid]);

			//!!!!!!!!!!!!!!!
				rte_eal_wait_lcore (Input_lcores[portid]);
       			rte_eal_remote_launch(Interface_INPUT_CORE_ETH_main_processing_loop,
                                        NULL,
                              Input_lcores[portid]);


		}

	}
	//----------Launch INTERFACES ETH AND FILTERS CORES--||

	//----------Launch KNI_CORE---->>
	
	rte_eal_wait_lcore (kni_core_id);
        rte_eal_remote_launch( CORE_KNI_main_processing_loop,
                                        NULL,
                              kni_core_id);

	
	//----------Launch KNI_CORE----||

	

	// Launch RE core
	if (rte_lcore_id()==master_lcore)
		{
		RE_main_processing_loop(NULL);//RE_KNI_main_processing_loop
	}

	RTE_LCORE_FOREACH_SLAVE(lcore_id) {
		 rte_eal_wait_lcore(lcore_id);	
	}
		
	

	return 0;
}

//----------------------------------------------------

void free_dpdk_data (){


	force_quit=1; //stop non RE CORES;
	int lcore_id;
/*
	RTE_LCORE_FOREACH_SLAVE(lcore_id) {
		rte_eal_wait_lcore(lcore_id);			
	}
*/
	//Releasing ports---------------------------------------->>>
	

	// stop ports 
	for (int portid = 0; portid <Phy_Int_Count; portid++) {

		printf("Closing Eth port %d...", portid);
		rte_eth_dev_stop(portid);
		rte_eth_dev_close(portid);
		printf(" Done\n");
	}
	printf("Bye...\n");

	//free kni port

	for (int portid = 0; portid < Phy_Int_Count+1; portid++) 
		{
		if (KNI_PORTS[portid]==NULL) continue;
		printf("Closing Kni port %d...", portid);
		rte_kni_release(KNI_PORTS[portid]);
		printf(" Done\n");

	}
	
	//Releasing ports--------------------------------------------||

	//-----------------------------------RELEASING KNI MODULE--->>
	if (kni_module_is_started==1)
		{
		printf("Releasing Kni module\n");
		rte_kni_close ();
		printf("Done\n");
	}
	//-----------------------------------RELEASING KNI MODULE---||

	//if (l2fwd_pktmbuf_pool!=NULL) rte_mempool_free(l2fwd_pktmbuf_pool);

	//-------------deleting rings--------------->> 
	for (int i=0;i<0x100;i++)
		{
		if (rings_of_ports[i]!=NULL) rte_ring_free(rings_of_ports[i]);
		if (rings_of_FILTERS[i]!=NULL) rte_ring_free(rings_of_FILTERS[i]);
	}
	//-------------deleting rings---------------||

	if (cl_dhcp_ht!=NULL) rte_hash_free (cl_dhcp_ht);
	if (cl_l2_ht!=NULL) rte_hash_free (cl_l2_ht);
	if (cl_ip_ht!=NULL) rte_hash_free (cl_ip_ht);

	printf("DPDK data structures is released\n");



}