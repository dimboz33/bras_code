#include "dpdk_include.h"
#include "chas_def_struct_glob_var.h"
#include "extern_chass_var.h"
#include "display.h"

#include <unistd.h> // for usleep microsecond 

#include "../timers/time_slot_change.h"


//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//---------------------------------SENDING PACKET BY KNI -----------------------------------------


static int
Send_burst_2_KNI(struct rte_mbuf **m_table, unsigned n,uint8_t portid)
{
	unsigned ret;
	unsigned queueid =0;
		rte_kni_handle_request(KNI_PORTS[portid]);	

		ret=rte_kni_tx_burst(KNI_PORTS[portid], m_table, (uint16_t) n);

		//rte_kni_handle_request(KNI_PORTS[portid]);	


		
		port_statistics[3*portid+2].tx += ret;
	if (unlikely(ret < n)) {
		port_statistics[3*portid+2].dropped += (n - ret);
		do {
			rte_pktmbuf_free(m_table[ret]);
		} while (++ret < n);
	}

	return 0;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

//-----------------------------SENDING PACKET BY RE----------------------------------

uint8_t send_packet(uint8_t* data, uint16_t length, uint8_t d_port,uint8_t* ci)
                                                                   
{                                                                  
struct rte_mbuf *m;
uint8_t* m_data;


m=rte_pktmbuf_alloc(l2fwd_pktmbuf_pool);

if (m==NULL)
	{
	return 2;//no memory in m_buf poll
}
m_data= rte_pktmbuf_mtod(m,uint8_t*);
m->udata64=0;//means from RE

uint8_t add=1;
if ((d_port==0xFE)&&(ci==NULL))
	{
	rte_pktmbuf_free(m);
	return 1;
}


if (d_port==0xFE)
	{ 
	add=0;
	
	rte_memcpy((uint8_t*)(m->buf_addr)+64,ci,8);//for control information

}
	m_data[0]=0;
	rte_memcpy(m_data+add,data, length);//first byte should be 0 for outgoing core
	m_data[0]=0;                                   
	m->data_len=length+add;      ////??????????????????????                                     
	m->pkt_len=length+add;       ////??????????????????????                                     



if (rte_ring_enqueue(rings_of_ports[d_port] ,m)!=0)   
                /*we should delete packet not enough space in output ring */     
                {                                                  
                rte_pktmbuf_free(m);                               
                port_statistics[d_port*2].dropped++;//not sure     
return 1;//mean dropped packet;                                    
}                                                                  
                                                                   
          port_statistics[3*0xFE].tx++;                                                         
                                                                   
return 0;                                                          
}                                                                  
//----------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
//Master LOOP

/* main processing loop  Master*/
 int RE_KNI_main_processing_loop(__attribute__((unused)) void *dummy)

	{
	struct rte_mbuf *pkts_burst[MAX_PKT_BURST];
	struct rte_mbuf *m;
        unsigned lcore_id;
        uint64_t prev_tsc, diff_tsc, cur_tsc, timer_tsc;
	uint8_t portid;
	uint8_t pk_st;
	struct rte_ring* our_ring;
	uint32_t nb_rx;
        prev_tsc = 0;
        timer_tsc = 0;

	lcore_id=rte_lcore_id();

        RTE_LOG(INFO, L2FWD, "Master core %u is started\n", lcore_id);
	

	//We should configure shadows of physicalk interfaces


	uint8_t port_re=0xFE;		//LOOPBACK 

	uint8_t port_kni = Phy_Int_Count;

	//our_ring=rings_of_ports[portid];


	//----------------------------------------------------- KNI variables --->>

	int16_t diff_arr[MAX_PKT_BURST];

	struct rte_mbuf *re_p[MAX_PKT_BURST];
	struct rte_mbuf *del_p[MAX_PKT_BURST];//packet needed to be deleted
	uint16_t del_p_c=0;
	uint16_t re_p_c=0;

	struct rte_mbuf *ring_burst[32][MAX_PKT_BURST];
	uint16_t rb_c[32]={0}; // ring burst counter
	
	uint32_t src_ip_buf[MAX_PKT_BURST];
	uint32_t dst_ip_buf[MAX_PKT_BURST];

	uint16_t pack_ind_buf[MAX_PKT_BURST];

	uint16_t src_ind;
	uint16_t pack_ind;

	uint8_t L2_header_len[MAX_PKT_BURST];

	int32_t src_cl_buf[MAX_PKT_BURST];
	int32_t dst_cl_buf[MAX_PKT_BURST];
	uint32_t arp_ind_buf[MAX_PKT_BURST];

	void* src_ip_pointers[MAX_PKT_BURST];
	void* dst_ip_pointers[MAX_PKT_BURST];

	uint8_t ci_buf[MAX_PKT_BURST][16];


	uint16_t log_pkt_c=0;


	struct rte_mbuf *log_pkt[MAX_PKT_BURST];

	for (int i=0;i<MAX_PKT_BURST;i++)
		{
		src_ip_pointers[i]=(void*)(src_ip_buf+i);
		dst_ip_pointers[i]=(void*)(dst_ip_buf+i);

	}

	//-----------------------------------------------------  KNI variables ---||




	int m_timer=0;
	//---------------------------initial config
	if (C_reload_config()!=0) return -1;//error in config 



	                                
	while (1) 
		{
			cur_tsc = rte_rdtsc();
			diff_tsc = cur_tsc - prev_tsc;               
	
			int is_any_packet=0;

			
			//---------------------------------------------------------------------------------- RE PART ---->>
                        //---------------------------------------------------- Timers processing---->>			


      				           
                        timer_tsc += diff_tsc;            
                                                                  
                                
                                      if (unlikely(timer_tsc >= (uint64_t) timer_period))
						{
               	                                //print_stats();    
                                                /* reset the timer */
                                                timer_tsc = 0;  

						time_slot_change(); 

						m_timer++;

						//3*0x00->current //3*0x10->last second//3*0x20  last two second
						
						memcpy(port_statistics+0x60,port_statistics+0x30,0x30*sizeof(struct l2fwd_port_statistics));
						memcpy(port_statistics+0x30,port_statistics+0x00,0x30*sizeof(struct l2fwd_port_statistics));

						for (int i=0;i<0x30;i++)
							{
							uint64_t diff=port_statistics[i+0x30].rx-port_statistics[i+0x60].rx;
							uint64_t diff2=port_statistics[i+0x30].m[7]-port_statistics[i+0x60].m[7];
							if (diff!=0)
								{
								for (int p=0;p<6;p++)
								port_statistics[i+0x60].m[p]=(port_statistics[i+0x30].m[p]-port_statistics[i+0x60].m[p])/diff;								

							}
							 
						      if (diff2!=0) port_statistics[i+0x60].m[7]=diff/diff2;								


	

						}


						if (m_timer>10)
							{
							C_reload_config();
							m_timer=1;
						} 
                                        }                         
                        //---------------------------------------------------- Timers processing----||           
                        //----------------------------------------------------
                        //----------------------------------------------------
                        //---------------------------------------------------- Packet processing--->>
                               
                        prev_tsc = cur_tsc;   
 			

			nb_rx=rte_ring_dequeue_burst( rings_of_ports[0xFE],(void **)pkts_burst,MAX_PKT_BURST) ;

			if (nb_rx>0) is_any_packet=1;


			port_statistics[3*portid].rx += nb_rx;
			int j;

			uint8_t d_portid;
			uint8_t* pk_st;//packet star
			for (j = 0; j < nb_rx; j++) {
				int16_t packet_diff=0; // how longer became header
				m = pkts_burst[j];
				rte_prefetch0(rte_pktmbuf_mtod(m, void *));
				pk_st=rte_pktmbuf_mtod(m, uint8_t *);
				
				uint8_t* m_d=(uint8_t*)(pkts_burst[j]->buf_addr)+64;//for control information
				
				packet_diff=0;
				//Process packet	
				d_portid=RE_Receive_packet(pk_st,m_d,m->pkt_len,&packet_diff);
				
			
				if ((d_portid!=0xFE)&&(d_portid<Phy_Int_Count+1))
					{ //that is usuall routing port
					packet_diff++;
					pk_st[-packet_diff]=0;
				}

				if (packet_diff!=0)
					{
					if (packet_diff>0)	rte_pktmbuf_prepend(m,packet_diff);
					else			rte_pktmbuf_adj (m,-packet_diff);
				}
		


				if ((d_portid==0xff)||(d_portid==0x80+Phy_Int_Count) )
					{
					port_statistics[3*portid].dl++;
					rte_pktmbuf_free(m);
				}

				else
					{
					//case when destination port is RE is also included
					if (rte_ring_enqueue (rings_of_ports[d_portid],m)!=0) 
						/*we should delete packet not enough space in output ring */
						{
						rte_pktmbuf_free(m);
						 port_statistics[3*portid].dropped++;
					}
					else	
						{
						port_statistics[3*portid].tx++;
					}


				}

				
			}	
                        //---------------------------------------------------- Packet processing---||

			//---------------------------------------------------------------------------------- RE PART ----||
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------
			//---------------------------------------------------------------------------------- KNI PART ---->>



		portid=port_kni;

		//RECEIVE PACKETS BY KNI
		//-------------------------------------------------------------------------------------------				

						

			nb_rx= rte_kni_rx_burst(KNI_PORTS[portid], pkts_burst, MAX_PKT_BURST);
			
			if (nb_rx>0) is_any_packet=1;
			
			uint8_t L_config_bit=G_config_bit;
			
			port_statistics[3*portid].rx += nb_rx;

				int16_t packet_diff; //  L2headet_length_output-L2headet_length_input;
 			
			
			uint8_t nb_pre=8;//prefetch_numb;
			
			src_ind=0;	
			pack_ind=0;

			
		
			//----------------------------------PRE_PROCESSING---!!!!!!!!!!!!!!!------>>
			
			struct rte_mbuf *m_pkt;
				

			for (j = 0; j < nb_rx; j++) { //nb_rx
				uint8_t d_portid;
				packet_diff=0;
			
				m_pkt=pkts_burst[j];
				uint8_t* m=rte_pktmbuf_mtod(m_pkt, uint8_t*);

				uint8_t* m_d=ci_buf[j];

				uint8_t* m_dre=(uint8_t*)(pkts_burst[j]->buf_addr)+64;
				//-------------------------------------------

				//uint64_t kk=m_pkt->ol_flags;					
		
				d_portid=pack_pre_processing(m,m_d,portid,m_pkt->pkt_len,
								L2_header_len,
								src_ip_buf,pack_ind_buf,
								dst_ip_buf,
								pack_ind,
								&src_ind,
								L_config_bit);
			
				//if packet should be deleted or send to RE

				if (d_portid!=0xFC)
					{
					if (d_portid==0xFF) 	del_p[del_p_c++]=m_pkt;
					if (d_portid==0xFE) 
						{
						rte_memcpy(m_dre,m_d,12); // before packet
						re_p[re_p_c++]=m_pkt;
					}
					continue;
				}
				else
					{
					pkts_burst[pack_ind]=pkts_burst[j];
					pack_ind++;
				}
					
			}
			//----------------------------------PRE_PROCESSING---!!!!!!!!!!!!!!!------||

			nb_rx=pack_ind;
		

			//----------------------------------ROUTING-TABLE---!!!!!!!!!!!!!!!------>>

			routing_table(src_ip_pointers,dst_ip_pointers,
					nb_rx,0,
					src_cl_buf,
					dst_cl_buf,
					arp_ind_buf,
					L_config_bit);


			//----------------------------------ROUTING-TABLE---!!!!!!!!!!!!!!!------||
			
						

			//----------------------------------POST_PROCESSING---!!!!!!!!!!!!!!!------>>

			

			for (j = 0; j < nb_rx; j++) { //nb_rx
				uint8_t d_portid;
				packet_diff=0;

				m_pkt=pkts_burst[j];
				uint8_t* m=rte_pktmbuf_mtod(m_pkt, uint8_t*);

				uint8_t* m_d=ci_buf[j];//(uint8_t*)(pkts_burst[j]->buf_addr)+64;
	
				uint8_t* m_dre=(uint8_t*)(pkts_burst[j]->buf_addr)+64;
				//-------------------------------------------
				
		
				d_portid=pack_post_processing(m,m_d,portid,&packet_diff,
								L2_header_len,
								src_ip_buf,
								src_cl_buf,pack_ind_buf,
								dst_cl_buf,
								arp_ind_buf,
								j,
								L_config_bit);
				


				if (packet_diff!=0)
				{
					if (packet_diff>0)	rte_pktmbuf_prepend(m_pkt,packet_diff);
					else			rte_pktmbuf_adj (m_pkt,-packet_diff);
				}
					

				if (d_portid>0xFC)
					{
					if (d_portid==0xFF)	
						{
						del_p[del_p_c++]=m_pkt;
						continue;
					}
					if (d_portid==0xFE)
						{
						rte_memcpy(m_dre,m_d,12); // before packet
						re_p[re_p_c++]=m_pkt;continue;
					}
					continue;
				}

				rte_memcpy(m-packet_diff,m_d,12);



				ring_burst[d_portid][rb_c[d_portid]++]=m_pkt;
				
			}

			//----------------------------------POST_PROCESSING---------------------------||


					uint16_t ret=0; 

				
			//---------------------------------SEND TO OUTGOING_CORE_PORT------------->>
			for ( j=0;(j<Phy_Int_Count+1);j++) 				
				{

				if (rb_c[j]>0)//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{
					ret=rte_ring_enqueue_burst(rings_of_ports[j] ,(void*)ring_burst[j],rb_c[j]);
					port_statistics[3*portid].tx+=ret;
					if (ret < rb_c[j]) {
						port_statistics[3*portid].dropped += (rb_c[j] - ret);
						do {
							rte_pktmbuf_free(ring_burst[j][ret]);
						} while (++ret < rb_c[j]);
	
					}
				}
				rb_c[j]=0;

			}



			
			//---------------------------------SEND TO RE------------->>
			
		
				if (unlikely(re_p_c>0))//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{
					ret=rte_ring_enqueue_burst(rings_of_ports[0xFE] ,(void*)re_p,re_p_c);
					//port_statistics[3*portid].tx+=ret;
					if (unlikely(ret < re_p_c)) 
						{
						//port_statistics[3*portid].dropped += (re_p_c - ret);
						do {
							rte_pktmbuf_free(re_p[ret]);
						} while (++ret < re_p_c);
	
					}
				}

				re_p_c=0;
			///-------------------------------------------------------|| For RE ring

			///-------------------DELETE-PACKET----------------------->>
			j=0xFF;
				if (unlikely(del_p_c>0))
					{
					port_statistics[3*portid].dl+=del_p_c;
						ret=0;
						do {
							rte_pktmbuf_free(del_p[ret]);
						} while (++ret < del_p_c);
	
				}
	
				del_p_c=0;


			//--------------------DELETE-PACKET-----------------------||
			
//SEND PACKETS BY KNI
//-------------------------------------------------------------------------------------------			
//-------------------------------------------------------------------------------------------	
//-------------------------------------------------------------------------------------------	
//-------------------------------------------------------------------------------------------	
//-------------------------------------------------------------------------------------------		

			our_ring=rings_of_ports[portid];	
		
			nb_rx=rte_ring_dequeue_burst( our_ring,(void **)pkts_burst,MAX_PKT_BURST ) ;
			
			
			if (nb_rx>0) is_any_packet=1;

			port_statistics[3*portid+2].rx += nb_rx;

			for (j = 0; j < nb_rx; j++) 
				{
				uint8_t result;
				m = pkts_burst[j];

					
				uint8_t *pk_st=rte_pktmbuf_mtod(m, uint8_t*);
				uint8_t *ci=pk_st;
				uint8_t ret;
				uint8_t type_p=ci[0];

				if (type_p==1)
					{
					ret=Output_Core_transmit_packet(pk_st,ci,portid,m->pkt_len);
				}
				else 
					{
					rte_pktmbuf_adj(m,1); //delete first byte		
				}

				if (type_p>1)//unlikely(ret==0xFF)
					{
					port_statistics[3*portid+2].dl++;
					rte_pktmbuf_free(m);
				}
				log_pkt[ log_pkt_c++]=m;
				
					
			}	

			
			Send_burst_2_KNI((struct rte_mbuf **)log_pkt,log_pkt_c,portid);
			log_pkt_c = 0;
			
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//RECEIVE PACKETS FROM SHADOWS INTERFACES

		for (portid=0;portid<Phy_Int_Count;portid++)
			{

			//-------------------RECEIVE PACKET----------------------------->>
			nb_rx= rte_kni_rx_burst(KNI_PORTS[portid], pkts_burst, MAX_PKT_BURST);
	
			if (nb_rx>0) is_any_packet=1;

			//rte_kni_handle_request(KNI_PORTS[portid]); //VERY IMPORTANT MODULE PROB HANGING PREVENTION!!!!!!!
			for (j = 0; j < nb_rx; j++)
				{
				m_pkt=pkts_burst[j];
				uint8_t* m=rte_pktmbuf_mtod(m_pkt, uint8_t*);
				rte_pktmbuf_prepend(m_pkt,1);
				m[-1]=0;//as from RE
			}

			if (nb_rx>0)
				{
				ret=rte_ring_enqueue_burst(rings_of_ports[portid] ,(void*)pkts_burst,nb_rx);	
				if (ret < nb_rx) {
					do
						{
						rte_pktmbuf_free(pkts_burst[ret]);
					} while (++ret < nb_rx);
				}
			}
		
			//-------------------RECEIVE PACKET-------------------------------||
			//-----------------------SEND PACKET------------------------------>>
		
			nb_rx=rte_ring_dequeue_burst(rings_of_ports[portid+0x80],(void **)pkts_burst,MAX_PKT_BURST) ;

			if (nb_rx>0) is_any_packet=1;

			Send_burst_2_KNI((struct rte_mbuf **)pkts_burst,nb_rx,portid);

			//-----------------------SEND PACKET------------------------------||



			//---------------------------------------------------------------------------------- KNI PART ----||			

			//if (is_any_packet==0) usleep(1000);//mikrosecond
                                                                           
		}

	}
return 0;
}



