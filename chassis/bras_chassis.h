////////////////////////////////////---FOR USING--CHASSIS FUNCTION-BY-OTHER FUNCTIONS
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include <rte_debug.h>
#include <rte_ether.h>
#include <rte_ethdev.h>
#include <rte_ring.h>
#include <rte_mempool.h>
#include <rte_mbuf.h>


/* list of enabled ports */
extern uint32_t l2fwd_dst_ports[RTE_MAX_ETHPORTS];

/* ethernet addresses of ports */
extern  struct ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];

extern uint8_t Phy_Int_Count;

extern "C" {
int main_bras_chassis(	int argc, char **argv,uint8_t const );


uint8_t send_packet(uint8_t* data, uint16_t length, uint8_t d_port,uint8_t* ci=NULL);//ci -control information 

void free_dpdk_data(void);

}