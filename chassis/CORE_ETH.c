
#include "dpdk_include.h"
#include "chas_def_struct_glob_var.h"
#include "extern_chass_var.h"
#include "display.h"

#include "../timers/time_slot_change.h"


 //2byte index in queue of packet with that filter ///1 byte filter number

int Interface_INPUT_CORE_ETH_main_processing_loop(__attribute__((unused)) void *dummy)
{		
	
	

	struct rte_mbuf **pkts_burst1=malloc( sizeof(struct rte_mbuf*)*INT_P_B_RECV*N_rep); ///!!! size PKT_REC_BURST is very importante do not change!!!!!!
	struct rte_mbuf **pkts_burst;
	



	int16_t *diff_arr=malloc(sizeof(int16_t)*INT_P_B_RECV); 


	struct rte_mbuf ** ring_burst1=malloc(sizeof(struct rte_mbuf*)*32*INT_P_B_RECV*N_rep);
	struct rte_mbuf ** ring_burst;
	struct rte_mbuf ** ring_burst2;//current

	uint16_t rb_c1[32*N_rep]={0}; // ring burst counter
	uint16_t *rb_c;
	uint16_t *rb_c2;//current


	struct rte_mbuf **re_p=		malloc(sizeof(struct rte_mbuf*)*INT_P_B_RECV);
	struct rte_mbuf **del_p=	malloc(sizeof(struct rte_mbuf*)*INT_P_B_RECV);//packet needed to be deleted

	struct rte_mbuf **ring_2_F1=	malloc(sizeof(struct rte_mbuf*)*INT_P_B_RECV*N_rep);//ring to filters cores // 64 filter each ring for each filer
	struct rte_mbuf **ring_2_F;	//next
	struct rte_mbuf **ring_2_F2;//current

	uint16_t ring_2_F_c1[N_rep]={0}; // ring to FILTER counter
	uint16_t *ring_2_F_c;
	uint16_t *ring_2_F_c2;//current

	uint16_t del_p_c=0;
	uint16_t re_p_c=0;

	struct rte_mbuf *m;
	unsigned lcore_id;
	uint64_t prev_tsc, diff_tsc, cur_tsc, timer_tsc;
	unsigned portid;
	uint32_t i,j,nb_rx;
	uint8_t* pk_st;//start of the packet dat
	struct lcore_queue_conf *qconf;
	const uint64_t drain_tsc = (rte_get_tsc_hz() + US_PER_S - 1) / US_PER_S * BURST_TX_DRAIN_US;

	prev_tsc = 0;
	timer_tsc = 0;

	struct rte_eth_link link;//for checking link status
	memset(&link, 0, sizeof(link));

	lcore_id = rte_lcore_id();
	qconf = &lcore_queue_conf[lcore_id];


	RTE_LOG(INFO, L2FWD, "entering main loop on lcore %u\n", lcore_id);

	portid = qconf->port;
		RTE_LOG(INFO, L2FWD, " -- lcoreid=%u portid=%u, Interface to Chassis\n", lcore_id,portid);
	
	nb_rx=0;
	uint32_t *src_ip_buf=	malloc(sizeof(uint32_t)*INT_P_B_RECV);
	uint32_t *dst_ip_buf=	malloc(sizeof(uint32_t)*INT_P_B_RECV);

	int16_t *pack_ind_buf=	malloc(sizeof(int16_t)*INT_P_B_RECV);
	for (int i=0;i<INT_P_B_RECV;i++)
		{
		pack_ind_buf[i]=-1;
	}

	uint16_t src_ind;
	uint16_t pack_ind;

	uint8_t *L2_header_len=	malloc(sizeof(uint8_t)*INT_P_B_RECV);

	int32_t *src_cl_buf=	malloc(sizeof(int32_t)*INT_P_B_RECV);
	int32_t *dst_cl_buf=	malloc(sizeof(int32_t)*INT_P_B_RECV);

	int32_t *arp_ind_buf=	malloc(sizeof(uint32_t)*INT_P_B_RECV);

	void** src_ip_pointers=	malloc(sizeof(void*)*INT_P_B_RECV);
	void** dst_ip_pointers=	malloc(sizeof(void*)*INT_P_B_RECV);

	uint8_t *ci_buf=	malloc(sizeof(uint8_t)*INT_P_B_RECV*16);//control information buffer
	for (i=0;i<INT_P_B_RECV;i++)
		{
		src_ip_pointers[i]=(void*)(src_ip_buf+i);
		dst_ip_pointers[i]=(void*)(dst_ip_buf+i);

	}


	while (G_first_config==1)
		{
		// wait for load configuration
		int a=1;
		a++;
	}

	
	uint8_t L_config_bit=0;

	uint8_t q_numb=0;//queue number

	int INT_P_B_RECV_half=INT_P_B_RECV>>1;

	int nb_rx1=0;

	int burst_part=0;

	uint8_t int_types_this=int_types[portid];

	uint16_t  sq_c;//sequence current
	uint16_t  sq_n=0;// sequence next
	uint16_t  tot_seq=0;//total sequence

	nb_rx=0;


	//-----------------------------------------------control plane policers--------------------------------->>>
	//int cl_vlan_c[4096];//clients vlan counter

	if (int_types_this==1)
		{
	


	}

	//-----------------------------------------------control plane policers---------------------------------|||

	uint64_t count_64;
	while (force_quit != 1) { //force_quit!=1
			
	
		sq_c=sq_n;
		sq_n++;
		if (sq_n==N_rep) sq_n=0;
		
		if (tot_seq<N_rep) tot_seq++;
	

pkts_burst=pkts_burst1+	INT_P_B_RECV*sq_c;

//------------------------------------------------------
		ring_2_F= ring_2_F1+sq_n*INT_P_B_RECV;
		ring_2_F_c=ring_2_F_c1+sq_n;

		ring_burst=ring_burst1+sq_n*INT_P_B_RECV*32;
		rb_c=rb_c1+32*sq_n;
//------------------------------------------------------
		ring_2_F2= ring_2_F1+sq_c*INT_P_B_RECV;
		ring_2_F_c2=ring_2_F_c1+sq_c;

		ring_burst2=ring_burst1+sq_c*INT_P_B_RECV*32;
		rb_c2=rb_c1+32*sq_c;
//-------------------------------------------------

		
		nb_rx=0;
		
		
		for (int k=1;(nb_rx+31<INT_P_B_RECV)&&(k>0);)	
			{
			k=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);

			nb_rx+=k;
		
		}

				
		//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
		//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
		//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
		//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
			

			uint8_t w_param=0;
			if (nb_rx==INT_P_B_RECV) w_param=1;

			if (port_statistics[3*portid].m[7]<nb_rx)
				{
				port_statistics[3*portid].m[7]=nb_rx;
			}
			

			if (unlikely(nb_rx==0))
				{

				//continue;
				/*
				//check link status
				rte_eth_link_get_nowait(portid, &link);

				if (int_states[portid]!=link.link_status)
					{
					int_states[portid]=link.link_status;
					//generate procedure


				}
				*/
			}

			uint8_t L_config_bit=G_config_bit;

			port_statistics[3*portid].rx += nb_rx;

			int16_t packet_diff; //  L2headet_length_output-L2headet_length_input;
 			
			
			uint8_t nb_pre=prefetch_numb;//8  prefetch_numb
			
			src_ind=0;	
			pack_ind=0;

				
			port_statistics[3*portid].m[0]=rte_rdtsc();
			
		
			//----------------------------------PRE_PROCESSING---!!!!!!!!!!!!!!!------>>
	
			for (j=0;(j<nb_pre)&&(j<nb_rx);j++)	
				{
				uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);//@@
				
				
				_mm_prefetch(m1,_MM_HINT_NTA);
				//rte_prefetch0(m1);//!!!!!!!!!!!!!!!!
				//rte_prefetch0(m1-64);
		    	}
	
			struct rte_mbuf *m_pkt;		

			for (j = 0; j < nb_rx; j++) { //nb_rx
				uint8_t d_portid;
				packet_diff=0;
				
				
				if (j+nb_pre<nb_rx)
					{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j+nb_pre],uint8_t*);//@@
					_mm_prefetch(m1,_MM_HINT_NTA);

				//	rte_prefetch0(m1);//!!!!!!
				//	rte_prefetch0(m1-64);				
		    		}
				

				
				//pkts_burst[j]=pkts_burst1[j];//@@@@@@@@@@@@@@@@@@

				m_pkt=pkts_burst[j];
				uint8_t* m=rte_pktmbuf_mtod(m_pkt, uint8_t*);

				

				uint8_t* m_dre=(uint8_t*)(pkts_burst[j]->buf_addr)+64;
				//-------------------------------------------
				uint8_t* m_d=ci_buf+pack_ind*16;

				//uint64_t kk=m_pkt->ol_flags;					
		
				d_portid=pack_pre_processing(m,m_d,portid,m_pkt->pkt_len, //ci_buf not m_d
								L2_header_len,	src_ip_buf,pack_ind_buf,
								dst_ip_buf,
								pack_ind,
								&src_ind,
								L_config_bit);
				
			
				//if packet should be deleted or send to RE

				if (d_portid>0xFC)
					{
					if (d_portid==0xFF) 
						{
						
						del_p[del_p_c++]=m_pkt;
						

					}
					if (d_portid==0xFE) {
						rte_memcpy(m_dre,m_d,12); 

						re_p[re_p_c++]=m_pkt;
						
					}
					
				}
				else
					{
					pkts_burst[pack_ind]=pkts_burst[j];
					pack_ind++;
					
				}
					
			}
			nb_rx=pack_ind;

			//----------------------------------PRE_PROCESSING---!!!!!!!!!!!!!!!------||


			count_64=rte_rdtsc()-port_statistics[3*portid].m[0];

			//if (count_64>port_statistics[3*portid].m[1]) port_statistics[3*portid].m[1]=count_64;
			
			if (w_param==1) port_statistics[3*portid].m[1]=count_64;

			

			

			// if that is client's port
			//rte_prefetch0(src_ip_buf); // that string is very important 140 byte traffic
			//if (int_types_this==1) rte_prefetch0(src_ip_buf);//(int_types[portid]==1)//this is speeds up application!!!!!
			//else	src_ind=0;
			
			//rte_prefetch0(dst_ip_buf);
			//rte_prefetch0(src_ip_pointers);// that string is very important 140 byte traffic

			//----------------------------------ROUTING-TABLE---!!!!!!!!!!!!!!!------>>
			
			//--------hash calc--->>

			for (j = 0; j < nb_rx; j++) 
				{
				uint8_t* b_xor=(uint8_t*)(dst_ip_buf+j);
				arp_ind_buf[j]=b_xor[0]^b_xor[1]^b_xor[2]^b_xor[3];
			}

			//--------hash calc---// 
		
			routing_table(src_ip_pointers,dst_ip_pointers,
					nb_rx,src_ind, //src_ind=0
					src_cl_buf,
					dst_cl_buf,
					arp_ind_buf,
					L_config_bit);


			//----------------------------------ROUTING-TABLE---!!!!!!!!!!!!!!!------||

			
			//port_statistics[3*portid].m[2]+=rte_rdtsc()-port_statistics[3*portid].m[0];


			count_64=rte_rdtsc()-port_statistics[3*portid].m[0];

			//if (count_64>port_statistics[3*portid].m[2]) port_statistics[3*portid].m[2]=count_64;

			if (w_param==1) port_statistics[3*portid].m[2]=count_64;

			
			//----------------------------------POST_PROCESSING---!!!!!!!!!!!!!!!------>>

			for (j=0;(j<nb_pre)&&(j<nb_rx);j++)	
				{
				uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);//@@
				//rte_prefetch0(m1-8);//!!!!!!!!!!!!!!!!
				_mm_prefetch(m1-8,_MM_HINT_NTA);


		    	}		

			
			for (j = 0; j < nb_rx; j++) { //nb_rx
			//	port_statistics[3*portid].m[0]=rte_rdtsc();
				uint8_t d_portid;
				packet_diff=0;

				m_pkt=pkts_burst[j];
				uint8_t* m=rte_pktmbuf_mtod(m_pkt, uint8_t*);

				if (j+nb_pre<nb_rx)
					{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j+nb_pre],uint8_t*);//@@
				//	rte_prefetch0(m1-8);//!!!!!!
					_mm_prefetch(m1-8,_MM_HINT_NTA);
				
		    		}

				uint8_t* m_d=ci_buf+j*16;//(uint8_t*)(pkts_burst[j]->buf_addr)+64;
	
				//if (unlikely(m_d[4]>2)) continue;// that traffic have benn sorted yet to RE or deletions

				uint8_t* m_dre=(uint8_t*)(pkts_burst[j]->buf_addr)+64;
				//-------------------------------------------
					
			//	port_statistics[3*portid].m[1]+=rte_rdtsc()-port_statistics[3*portid].m[0];


				d_portid=pack_post_processing(m,m_d,portid,&packet_diff,
								L2_header_len,
								src_ip_buf,
								src_cl_buf,pack_ind_buf,
								dst_cl_buf,
								arp_ind_buf,
								j,
								L_config_bit);
				
			//	port_statistics[3*portid].m[2]+=rte_rdtsc()-port_statistics[3*portid].m[0];



				if (packet_diff!=0)
					{
					if (packet_diff>0)	rte_pktmbuf_prepend(m_pkt,packet_diff);
					else			rte_pktmbuf_adj (m_pkt,-packet_diff);
				}				
		

				if (d_portid>0xFC)
					{



					if  (d_portid==0xFF)
						{	
						del_p[del_p_c++]=m_pkt;continue;
					}
					if (d_portid==0xFE)
						{
						rte_memcpy(m_dre,m_d,12); // before packet

						re_p[re_p_c++]=m_pkt;continue;
					}
				}
				
				if (packet_diff<=0)	memcpy(m-8,m_d,12);//for decreasing influense between cores!!!!!

				memcpy(m-packet_diff,m_d,12);
				
							
				
				if (m_d[4]==1)// log int speed's up from 21 drop to 17 drop!!!! FANTASTIC!!!
					{
					
					ring_burst2[d_portid*INT_P_B_RECV+rb_c2[d_portid]++]=m_pkt;
					continue;

				}
				

			if (int_types_this==1) ring_2_F2[ring_2_F_c2[0]++]=m_pkt;


			//_mm_clflush((m-16));
			//_mm_clflush(m);
				
			}


			//port_statistics[3*portid].m[3]+=rte_rdtsc()-port_statistics[3*portid].m[0];




			count_64=rte_rdtsc()-port_statistics[3*portid].m[0];

			//if (count_64>port_statistics[3*portid].m[3]) port_statistics[3*portid].m[3]=count_64;

			if (w_param==1) port_statistics[3*portid].m[2]=count_64;


			//port_statistics[3*portid].m[7]++;

			//nb_rx=0;
			//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
			//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);



			//----------------------------------POST_PROCESSING---------------------------||
			//nb_rx=0;
			//nb_rx+=rte_eth_rx_burst((uint8_t) portid, 0,pkts_burst+nb_rx,32);
		
			uint16_t ret=0;

			//---------------------------------SEND TO RE------------->>
			
		
				if (unlikely(re_p_c>0))//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{
					ret=rte_ring_enqueue_burst(rings_of_ports[0xFE] ,(void*)re_p,re_p_c, 0);
					port_statistics[3*portid].tx+=ret;
					if (unlikely(ret < re_p_c)) 
						{
						port_statistics[3*portid].dropped += (re_p_c - ret);
						do {
							rte_pktmbuf_free(re_p[ret]);
						} while (++ret < re_p_c);
	
					}
				}

				re_p_c=0;
			///-------------------|| For RE ring

			///-------------------DELETE-PACKET------->
			j=0xFF;
				if (unlikely(del_p_c>0))
					{
					port_statistics[3*portid].dl+=del_p_c;
						ret=0;
						do {
							rte_pktmbuf_free(del_p[ret]);
						} while (++ret < del_p_c);
	
				}
	
				del_p_c=0;


			//--------------------DELETE-PACKET-------||

			//!!!!!!!!!!!!!!!!!!!!!!!!!################!!!!!!!!!!!!
			 if (unlikely(tot_seq<N_rep)) continue;//for queueing of activity

				//---------------------------------SEND TO FILTER-------------

				if ((ring_2_F_c[0]>0)&&(int_types_this==1))  //it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{//full queue or no one element.//rings_of_ports[0xFE];rings_of_FILTERS[portid]
					ret=rte_ring_enqueue_burst (rings_of_FILTERS[portid] ,(void*)ring_2_F,ring_2_F_c[0],0 );
				
					port_statistics[3*portid].tx+=ret;

					if (unlikely(ret < ring_2_F_c[0])){
						port_statistics[3*portid].dropped += (ring_2_F_c[0] - ret);
						do {
							rte_pktmbuf_free(ring_2_F[ret]);
						} while (++ret < ring_2_F_c[0]);
	
					}
						
					
					
				ring_2_F_c[0]=0;

				}
				
				//---------------------------------SEND TO OUTGOING_CORE_PORT------------->>
				for ( j=0;(j<Phy_Int_Count+1);j++) 				
				{
				if (rb_c[j]>0)//it is very important rte_ring_enquene_bust with 0 as nb_rx -> givers error
					{

					ret=rte_ring_enqueue_burst(rings_of_ports[j] ,(void*)(ring_burst+j*INT_P_B_RECV),rb_c[j],0);						
					
					port_statistics[3*portid].tx+=ret;
					if (ret < rb_c[j]) {
						port_statistics[3*portid].dropped += (rb_c[j] - ret);
						do {
							rte_pktmbuf_free(ring_burst[j*INT_P_B_RECV+ret]);
						} while (++ret < rb_c[j]);
	
					}
				}
				rb_c[j]=0;

			}

			

		}
return 0;
}

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

/* Send the burst of packets from Interface to the medium*/
static int
Send_burst_2_ETH(struct rte_mbuf **m_table, unsigned n,uint8_t portid,uint8_t queue_numb)
{
	unsigned ret=0;
	
	int ret_count=0;

	queue_numb=0;
	
	int r=1;

	while (ret<n)
		{ 
		r=rte_eth_tx_burst(portid, queue_numb, m_table+ret, (uint16_t) n-ret);
		//if (r==0) break;
		//if (ret_count==10000) break;
		ret+= r;
	}


	port_statistics[3*portid+2].tx += ret;	

	if (unlikely(ret < n)) {
		port_statistics[3*portid+2].dropped += (n - ret);
		do {
			rte_pktmbuf_free(m_table[ret]);
		} while (++ret < n);
	}

	return 0;
}

/*main processing loop INTERFACE OUTPUT CORE*/
int Interface_OUTPUT_CORE_ETH_main_processing_loop(__attribute__((unused)) void *dummy)
{	


	

uint8_t** L3_arr=	malloc(sizeof(uint8_t*)*INT_P_B_SEND);


const uint8_t F_C=16; //FILTER COUNT

struct rte_mbuf **del_p=	malloc(sizeof(struct rte_mbuf *)*INT_P_B_SEND);
uint16_t del_p_c=0;

uint16_t log_pkt_c=0;
uint16_t cl_pkt_c=0;//counters

struct rte_mbuf **log_pkt=	malloc(sizeof(struct rte_mbuf *)*INT_P_B_SEND);//logiucal interface packets from
struct rte_mbuf **cl_pkt=	malloc(sizeof(struct rte_mbuf *)*INT_P_B_SEND);//client interface packets from

uint32_t *filter_result=		malloc(sizeof(uint32_t)*F_C*INT_P_B_SEND);

int ret;

	
struct rte_mbuf **pkts_burst=	malloc(sizeof(struct rte_mbuf *)*INT_P_B_SEND);
	


	struct rte_mbuf *m;
	unsigned lcore_id;
	uint64_t prev_tsc, diff_tsc, cur_tsc, timer_tsc;
	unsigned  portid;

	uint32_t i,j,nb_rx;
	struct lcore_queue_conf *qconf;
	struct rte_ring* our_ring;

	
	prev_tsc = 0;
	timer_tsc = 0;

	lcore_id = rte_lcore_id();
	qconf = &lcore_queue_conf[lcore_id];



	portid = qconf->port;
	our_ring=rings_of_ports[portid];


	RTE_LOG(INFO, L2FWD, "entering main loop on lcore %u\n", lcore_id);

	RTE_LOG(INFO, L2FWD, " -- lcoreid=%u portid=%u, Chassis to Interface\n", lcore_id,portid);
	
	const uint32_t drain_tsc = rte_get_tsc_hz()/1000;//1 ms
	uint32_t ms_counter=0;

	portid = qconf->port;

	while (G_first_config==1)
		{
		// wait for load configuration
		int a=1;
		a++;
	}

	///rte_delay_ms(5000);
	uint8_t q_numb=0;//queue number

	uint8_t int_types_this=int_types[portid];

	while (force_quit != 1) { //force_quit!=1


		//	q_numb=(q_numb+1)&0x3;
					
			nb_rx=rte_ring_dequeue_burst( our_ring,(void **)pkts_burst,INT_P_B_SEND, 0);//INT_P_B_SEND
			
			q_numb=1-q_numb;

			if (nb_rx==0) continue;
			
			cur_tsc = rte_rdtsc();

			if (unlikely((cur_tsc-prev_tsc)>drain_tsc))
				{
				ms_counter++;
				prev_tsc =cur_tsc;
				
			}


			port_statistics[3*portid+2].rx += nb_rx;

			if (port_statistics[3*portid+2].m[7]<nb_rx)
				{
				port_statistics[3*portid+2].m[7]=nb_rx;
			}


			cl_pkt_c=0;
			
			log_pkt_c=0;

			del_p_c=0;
			//------------------we need to collect all packets from clients

			uint8_t nb_pre=prefetch_numb;

			port_statistics[3*portid+2].m[0]=rte_rdtsc();

			
			
			for (j=0;(j<nb_pre)&&(j<nb_rx);j++)	
				{
				uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);//@@
				rte_prefetch0(m1);
				rte_prefetch0(m1+8);

				//_mm_prefetch(m1,_MM_HINT_NTA);
				//_mm_prefetch(m1+8,_MM_HINT_NTA);
		    	}
			

			for (j=0;j<nb_rx;j++)
				{
				uint8_t* ci=rte_pktmbuf_mtod(pkts_burst[j], uint8_t*);

				if (j+nb_pre<nb_rx)
					{
					uint8_t* m1=rte_pktmbuf_mtod(pkts_burst[j+nb_pre],uint8_t*);//@@
					rte_prefetch0(m1);
					rte_prefetch0(m1+8);
					//_mm_prefetch(m1,_MM_HINT_NTA);
					//_mm_prefetch(m1+8,_MM_HINT_NTA);	    		
		    		}
								
				if (ci[0]==2) //&&(int_types_this==1))
					{
					cl_pkt[cl_pkt_c]=pkts_burst[j];
					L3_arr[cl_pkt_c]=ci+ci[8];
					cl_pkt_c++;
				}
				else 
					{
					log_pkt[log_pkt_c]=pkts_burst[j];
					log_pkt_c++;
				}

			}

		//port_statistics[3*portid+2].m[1]+=rte_rdtsc()-port_statistics[3*portid+2].m[0];



	if (int_types_this==1) //that is client port
		{
			uint64_t marker;

			//-------------------run filter
				PP_FILTER(L3_arr,cl_pkt,filter_result,portid,cl_pkt_c,0,ms_counter,1,prefetch_numb,G_config_bit,&marker);
			//alreay generate good L2header
			
			//------------------delete filtered

			uint8_t J1=0;
			for (j=0;j<cl_pkt_c;j++)
				{
				if (filter_result[j*F_C]==0xFF) 
					{
					del_p[del_p_c++]=cl_pkt[j];

				}
				else
					{
					cl_pkt[J1]=cl_pkt[j];
					J1++;
						
				}
				
			}
			cl_pkt_c=J1;

			//port_statistics[3*portid+2].m[2]+=marker-port_statistics[3*portid+2].m[0];

			//port_statistics[3*portid+2].m[3]+=rte_rdtsc()-port_statistics[3*portid+2].m[0];

			//---------------------SEND PACKETS TO CLIENTS------------------------
			
			if (cl_pkt_c>0) Send_burst_2_ETH((struct rte_mbuf **)cl_pkt,cl_pkt_c,portid,q_numb);				
			cl_pkt_c=0;	
			
			
			//port_statistics[3*portid+2].m[4]+=rte_rdtsc()-port_statistics[3*portid+2].m[0];
			


	}

			
			//---------------------------SEND_PACKETS_TO_LOG_INT---------------------			
			
			for (j = 0; j < log_pkt_c; j++) {
				
				m = log_pkt[j];
				uint8_t *pk_st=rte_pktmbuf_mtod(m, uint8_t*);
				uint8_t *ci=pk_st;

				uint8_t type_p=ci[0];

				if (type_p==1)
					{
					ret=Output_Core_transmit_packet(pk_st,ci,portid,m->pkt_len);
				}
				else //type==0
					{ 
					rte_pktmbuf_adj(m,1); //delete first byte

				}

			}	

			//port_statistics[3*portid+2].m[5]+=rte_rdtsc()-port_statistics[3*portid+2].m[0];
			
			if (log_pkt_c>0)
				{
				Send_burst_2_ETH((struct rte_mbuf **)(log_pkt),log_pkt_c,portid,q_numb);				
				log_pkt_c=0;
			}
			//delete packets

			for (j=0;j<del_p_c;j++)
				{
				port_statistics[3*portid+2].dl++;
				rte_pktmbuf_free(del_p[j]);
			}
			
			//port_statistics[3*portid+2].m[6]+=rte_rdtsc()-port_statistics[3*portid+2].m[0];

			//port_statistics[3*portid+2].m[7]++;
		


	}

return 0;

}

  



