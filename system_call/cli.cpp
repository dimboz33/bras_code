#include "cli.h"

int G_qid=0;//global queue id

//--------------------------------------------
_MSG::_MSG ()
	{
	type=0;
	memset(text,0,MSGSZ);
}

_MSG::_MSG (int type1,std::string& s)//cut the sending string
	{
std::string cur_s;
memset(text,0,MSGSZ);

if (s.size()+1>MSGSZ) //1 - end of string simbol
	{
	cur_s=s.substr(1,MSGSZ-1);
	s=s.substr(MSGSZ,s.size());
}
else 	{
	cur_s=s;
	s="";
}

type=type1;

length=cur_s.size()+1;
memcpy(text,cur_s.c_str(),length-1);
}


//---------------------------------------------------------------

void* main_cli_function(void*)
	{
int key=0x11041988;//getpid();

int G_qid=msgget(key,IPC_CREAT| 0666);

if (G_qid==-1)	return NULL;	


_MSG msg;

while (1)
	{
	msgrcv(G_qid,&msg,MSGSZ, -5,0);// message type from 1 to 4

	if (msg.type==2)	system(msg.text);

}

return NULL;
}



//---------------------------------------------------------------

void delete_cli_queue()
	{
	if (G_qid!=0) msgctl(G_qid, IPC_RMID, NULL);

}

//---------------------------------------------------------------

void run_system_call(std::string s)
	{
_MSG msg=_MSG(2,s);
msg.type=2;//system call

int ret= msgsnd (G_qid,&msg, msg.length, IPC_NOWAIT);
	
return;

}





 