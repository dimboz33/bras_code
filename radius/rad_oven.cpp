#include <arpa/inet.h> // for change bit order
#include "rad_oven.h"
#include "../timers/timers_array.h"
#include <list>
#include "../clients/clients.h"
#include "../filter/service.h"
#include "../filter/policer.h"
#include "rad_client.h"



#include <arpa/inet.h> // for change bit order


extern struct timers_array* G_timers_arr; 


std::string byte2str(const uint8_t* data,uint8_t length)
	{

	std::string s="";

	char byte_s[2+1];

	for (int i=0;i<length;i++)
		{
		sprintf(byte_s,"%02x",data[length-1-i]);
		s+=std::string(byte_s);
	}

return s;

}





RAD_req::RAD_req(uint32_t cl_numb1,uint8_t type1,uint8_t service1) // if service1==C_serv_count -> session accounting update
		{
		//type of request 0-auth;1-account Start; 2-account STOP; 3 account-UPdate
		if (service1>C_serv_count) return;


		CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_numb1;
		CL_IP_DAEL *cl_ip=cl_ip_dt+cl_numb1;

		cl_numb=cl_numb1;

		type=type1;

		service=service1;

		cl_m_session=cl_sess->M_sess;
	
		cl_m_session_str= byte2str((uint8_t*)&cl_m_session,8);

		cl_session =(service1<C_serv_count)?(cl_m_session_str+"_"+std::to_string(cl_sess->sess[service1])):cl_m_session_str;
	
		terminate_cause=(type1==2)?cl_sess->terminate_cause[service]:0;


		CL_INT_DAEL *cl_forw_in =  cl_forw_dt[cl_ip->phy_int*2].cl_arr+cl_ip->cl_index_in;
		CL_INT_DAEL *cl_forw_out=  cl_forw_dt[cl_ip->phy_int*2+1].cl_arr+cl_ip->cl_index_out;	

		username=cl_sess->username;				

		if (type1>0) //not authentication
			{
			serv_count=0;

				for (int i=0;i<C_serv_count;i++)
					{

					if (service1<C_serv_count) i=service1;//####

					if ((cl_forw_in->service_bits&(1<<i))!=0)
						{
						serv_name[serv_count]=G_serv_agr->arr[i].name;

						uint16_t filter_in = cl_forw_in->tr_co[i].policer;
						uint16_t filter_out=cl_forw_out->tr_co[i].policer;

						pol_name[serv_count*2+0]=G_pol_agr->arr[filter_in].name;
						pol_name[serv_count*2+1]=G_pol_agr->arr[filter_out].name;
						serv_count++;				
					}

					if (service1<C_serv_count) break;//####	
				}

			

			byte_counter[0]= cl_forw_in->tr_co[service1].byte_counter;
			byte_counter[1]=cl_forw_out->tr_co[service1].byte_counter;
	
			pack_counter[0]=cl_forw_in->tr_co[service].pack_counter;
			pack_counter[1]=cl_forw_out->tr_co[service].pack_counter;

		}

		else
			{ //authentication case
			
			password=cl_sess->password;
		}
		
	
		this->cl_port=cl_ip->phy_int;
		this->cl_ip=cl_sess->ip;

	}

int RAD_OVEN_new_trial(int time,
		uint32_t d1[4],	//d1 - cells_number
		void* p1[4]   	//p1 - pointer to RAD_OVEN)
		);


uint8_t RAD_QUE::generate_request(uint32_t cl_num,uint8_t type, uint8_t service) //hash_vlan
	{

	if (rad_oven->rad_cl_conf->rad_servers.size()==0) return -1;

	//check is any free space in oven
	if (rad_oven->free_cells.begin()==rad_oven->free_cells.end()) 
		{
		//IF NO any free cells , we need to set new clients in que
	
	this->que.push_back(RAD_req(cl_num,type,service));
	} 
	else
		{
		uint8_t cells_numb=rad_oven->free_cells.front();
		rad_oven->free_cells.pop_front();
		rad_oven->generate_request(cells_numb,RAD_req(cl_num,type,service));
	}



return 0;
}


//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------


uint8_t RAD_OVEN::generate_request(uint8_t cells_numb, RAD_req data1)
	{
	RAD_OVEN_EL* curr_cell=this->oven+cells_numb;
	if (curr_cell->state!=0) 
		{
		return -1;
	}
	curr_cell->state=1;
	curr_cell->serv_num=0;
	curr_cell->trial=0;
	curr_cell->data=data1;

	this->w_s_request(cells_numb);//write and send 

	uint32_t d1[4];
	void* p1[4];
	//p1[0]=(void*)this;
	d1[0]=cells_numb;
	d1[1]=this->rad_cl_conf->prot_type;
	curr_cell->timer_n=G_timers_arr->new_timer(this->rad_cl_conf->timeout,RAD_OVEN_new_trial,d1,p1);

return 0;
}
int RAD_OVEN_new_trial(int time,
		uint32_t d1[4],	//d1 - cells_number d1[1] -0 - auth ; 1 - acc
		void* p1[4]   	//p1 - pointer to RAD_OVEN) //o problem in config change
		)
	{
	
	RAD_OVEN* rad_oven=(d1[1]==0)?(&(G_rad_auth.oven)):(&(G_rad_acc.oven));
	uint8_t cells_numb=d1[0];
//---------------------------------------------------
	RAD_OVEN_EL* curr_cell=rad_oven->oven+cells_numb;
	if (curr_cell->state!=1)
		{
		int b=0;
		b++;
	 	return -1;
	}
	curr_cell->trial++;
	if (!(curr_cell->trial<rad_oven->rad_cl_conf->num_trials))
		{
		curr_cell->serv_num++;
		curr_cell->trial=0;
	}
	if (!(curr_cell->serv_num<rad_oven->rad_cl_conf->rad_servers.size()))
		{

		if (curr_cell->data.type==0) //only for authentication set unreach service
			{

			//we needed to set default filter for client; May be default filter should be 
			//applied after initialization of client and we needed do nothing 
			//we need to free oven
			
			RAD_req* d=&(rad_oven->oven[cells_numb].data);//data
	
			uint32_t cl_numb=d->cl_numb;
			
			CL_IP_SESS * cl_sess=cl_ip_sess_dt+cl_numb;
			
			//applay service unreach_serv

			if ((cl_sess->state==1)&&(cl_sess->M_sess==d->cl_m_session))
				{
				uint8_t unr_serv=rad_oven->rad_cl_conf->p0.unreach_serv;

				add_service_and_policer_to_user(G_serv_agr->arr[unr_serv].name,"","",cl_numb);
			}
		}


	
		rad_oven->add_free_cell(cells_numb);




	return 1;
	}
	rad_oven->w_s_request(cells_numb);

	curr_cell->timer_n=G_timers_arr->new_timer(rad_oven->rad_cl_conf->timeout,RAD_OVEN_new_trial,d1,p1);

	

	//We need to set new request in timer

return 0;
}

///-----------------------------------------------------------------

uint8_t RAD_OVEN::add_free_cell(uint8_t cells_numb)
	{

	RAD_OVEN_EL* curr_cell=this->oven+cells_numb;

	if (curr_cell->state==0)
		{
		 return 1;
	}
	//element should be used
	curr_cell->state=0;
	curr_cell->timer_n=-1;

	if (this->rad_que->que.begin()==this->rad_que->que.end())
		{
		this->free_cells.push_back(cells_numb);
		return 0;	
	}

		
	//we need to use new free cells to generate request
	RAD_req n_req; //new request

	n_req=this->rad_que->que.front();

	this->rad_que->que.pop_front();
	this->generate_request(cells_numb,n_req);
	
return 0;
}


