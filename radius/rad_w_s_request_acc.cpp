

#include "rad_conf.h"
#include "rad_oven.h"
#include "rad_global.h"
#include "../timers/timers_array.h"

//#include "/usr/src/kernels/3.18.7-200.fc21.x86_64/include/config/crypto/md5.h"

#include <openssl/md5.h>//for md5

#include <arpa/inet.h> // for change bit order

#include "../PP/PP.h" //prepare L2 header 

#include "../chassis/bras_chassis.h"//send packet

#include "../clients/clients.h"

#include "../filter/service.h"

#include "../filter/policer.h"

using namespace std;

extern int RANDOM_DATA_FILE;



extern struct timers_array* G_timers_arr; 

extern RAD_SERVERS G_rad_servers;



//----------------------------------------------------------------------------------------
uint8_t RAD_OVEN::w_s_request_acc(uint8_t cells_numb)//write and send request
	{
u_char  data[512]={0}; // data is where we will form packet radius
uint16_t dt_p; //data pointer in radius packet
int i;
uint8_t rad_p=46;
dt_p=rad_p;//18[ethernet]+20[ip]+8[udp]

RAD_OVEN_EL* curr_el=this->oven+cells_numb;



if (curr_el->data.type==0) return -3;//
//authentication request should be processing only by rad_authentication client

 data[dt_p++]=4; //responce for accounting

//set identifier
data[dt_p++]=cells_numb;

//length field we will fill at the end
dt_p+=2;

//-------------------------------------------------
//request authenticator write will be done after all attribute filling for accounting case
dt_p+=16;

uint32_t cl_numb=this->oven[cells_numb].data.cl_numb;



RAD_server *curr_serv= &this->rad_cl_conf->rad_servers[0]+curr_el->serv_num;
//--------------------------------------------------------------------write attributes--------------------->>
RAD_req* d=&(curr_el->data);//data

//----------------------------------------------Acct-Status-Type
data[dt_p++]=40;               //type
data[dt_p++]=6;	             //length
dt_p+=3;                       //3 bytes 0
data[dt_p++]=curr_el->data.type;

//-----------------------------------------------Acct-Session-Id--------->>>
data[dt_p++]=44; //type

data[dt_p]=2+sprintf((char*)data+dt_p+1,"%s",d->cl_session.c_str());

dt_p+=data[dt_p]-1;
//-----------------------------------------------Acct-Session-Id----------|||
//----------------------------------USERNAME------------------------------>>>
data[dt_p++]=1;//type username

data[dt_p]=2+sprintf((char*)data+dt_p+1,"%s",d->username.c_str());

dt_p+=data[dt_p]-1;

//-----------------------------------USERNAME------------------------------|||


for (int i=0;i<d->serv_count;i++)
	{
//-------------------------------------------------Name of SErvices and policers--->>
data[dt_p++]=26;// Vendor spesific
uint8_t  l_poz=dt_p++;

*((uint32_t*)(data+dt_p))=ntohl(RAD::vendor_id);dt_p+=4;//vendor_id
data[dt_p++]=RAD::vendor_type;


data[dt_p]=2+sprintf((char*)data+dt_p+1,"Service=%s;input_policer=%s;output_policer=%s;",\
d->serv_name[i].c_str(),\
d->pol_name[0+2*i].c_str(),\
d->pol_name[1+2*i].c_str());//value

data[l_poz]=data[dt_p]+6;dt_p+=data[dt_p]-1;
//-------------------------------------------------Name of SErvices and policers---||

}

//------------------------------------------------Acct-Multi-Session-Id
data[dt_p++]=50; //type;
data[dt_p]=2+sprintf((char*)data+dt_p+1,"%s",d->cl_m_session_str.c_str());

dt_p+=data[dt_p]-1;

///----------------------------------------------Acc_Multi_Session-Id


::uint64_t value;
//------------------------------------------------Acct-Input-Octets
data[dt_p++]=42; //type
data[dt_p++]=6; //only fore lower byte
*((uint32_t*)(data+dt_p))=ntohl(d->byte_counter[0]);dt_p+=4;
//------------------------------------------------Acct-Output-Octets
data[dt_p++]=43; //type
data[dt_p++]=6; //only fore lower byte
*((uint32_t*)(data+dt_p))=ntohl(d->byte_counter[1]);dt_p+=4;
//------------------------------------------------Acct-Input-Packets
data[dt_p++]=47; //type
data[dt_p++]=6; //only fore lower byte
*((uint32_t*)(data+dt_p))=ntohl(d->pack_counter[0]);dt_p+=4;
//------------------------------------------------Acct-Output-Packets
data[dt_p++]=48; //type
data[dt_p++]=6; //only fore lower byte
*((uint32_t*)(data+dt_p))=ntohl(d->pack_counter[1]);dt_p+=4;


if (d->type==2)//accounting stop
	{
	data[dt_p++]=49; //type
	data[dt_p++]=6; // length
	*((uint32_t*)(data+dt_p))=htonl(d->terminate_cause);dt_p+=4;
}

this->add_common_attribute(data,dt_p,cells_numb);

//--------------------------------------------------------------------write attributes---------------------||




//---------------------------------------------------------write length of the packet
*(uint16_t*)(data+rad_p+2)=ntohs((uint16_t)(dt_p-rad_p));

///-------------set authenticator---------------------------->>

MD5_CTX c;
MD5_Init(&c);

MD5_Update(&c,(void *)(data+rad_p),dt_p-rad_p);
MD5_Update(&c,(void *)(curr_serv->shar_secr),curr_serv->shar_secr_l);
MD5_Final(data+rad_p+4,&c);


///-------------set authenticator----------------------------||

//----------------------------------UDP header------------------------------------->>>

uint8_t udp_p=18+20;//udp pointer; ethernet-18 + ip - 20
uint16_t length=dt_p-rad_p;

*(uint16_t*)(data+udp_p)=this->rad_cl_conf->client_port; //source port
*(uint16_t*)(data+udp_p+2)=this->rad_cl_conf->serv_port; //dest port
*(uint16_t*)(data+udp_p+4)=ntohs(length+8);

//----------------------------------UDP header-------------------------------------|||

// ip header


//----------------------------------IP header------------------------------------->>>

uint8_t ip_p=18;// ip pointer

data[ip_p]=0x45;
///ip length
*(uint16_t*)(data+ip_p+2)=ntohs(length+8+20);//8 udp header
// TTL
data[ip_p+8]=0xFF;//
// PROTOCOL UDP 
data[ip_p+9]=0x11;// 

//----------------------------------IP header------------------------------------||||

//get output interfave and write L2 header
int16_t pk_diff=0;
uint8_t ci[8];
uint8_t out_phy_int =Forwarding_table_act(curr_serv->ip,data+ip_p,(uint8_t*)&ci,&pk_diff);


if (out_phy_int!=0xFF)
	{
 	send_packet(data+ip_p-pk_diff,dt_p+ip_p-pk_diff,out_phy_int,ci);

}
//if only exist mac address


///We need to generate time for action when responce is not reseived


return 0;
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
int RAD_OVEN::add_common_attribute(uint8_t* data,uint16_t &dt_p,uint8_t cells_numb)
	{

RAD_req* d=&(this->oven[cells_numb].data);//data

	//----------------------------------------------NAS-IP-ADDRESS
	data[dt_p++]=4;               //type
	data[dt_p++]=6;	             //length
	*((uint32_t*)(data+dt_p))=htonl(RAD::nas_ip_address);
	dt_p+=4;
	//----------------------------------------------NAS-Identifier
	data[dt_p++]=32; 									//type
	uint8_t len_value=sprintf((char*)data+dt_p+1,"%s", RAD::nas_identifier.c_str());//value
	data[dt_p++]=2+len_value;							//length
	dt_p+=len_value;
	//----------------------------------------------NAS-Port
	data[dt_p++]=5;               //type
	data[dt_p++]=6;	             //length
	*((uint32_t*)(data+dt_p))=htonl(d->cl_port);
	dt_p+=4;
	//----------------------------------------------Framed-IP-Address
	data[dt_p++]=8;               //type
	data[dt_p++]=6;	             //length
	*((uint32_t*)(data+dt_p))=htonl(d->cl_ip);
	dt_p+=4;
	//----------------------------------------------Framed-IP-Netmask
	data[dt_p++]=9;               //type
	data[dt_p++]=6;	             //length
	*((uint32_t*)(data+dt_p))=0xFFFFFFFF;
	dt_p+=4;
	//----------------------------------------------
	
	return 0 ;


}