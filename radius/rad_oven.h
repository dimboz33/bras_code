

#ifndef RAD_OVEN_HEADER

#define RAD_OVEN_HEADER

#include "rad_conf.h"
#include <list>

using namespace std;



class RAD_OVEN;



struct RAD_req 
	{
	uint32_t cl_numb;//key of client in ip hash table
	uint8_t type;//type of request 0-auth;1-account Start; 2-account STOP; 3 account-UPdate
	uint8_t service;	

	::uint64_t cl_m_session;		//client multisssion
	
	string cl_m_session_str;
	string cl_session;		// clietn session

	uint32_t terminate_cause;	// in case of account stop

	string username;
	string password;

	int serv_count;
	string serv_name[16];
	string  pol_name[16*2];		//0-input; 1-output;

	::uint64_t byte_counter[2];	//0-input; 1-output;
	::uint64_t pack_counter[2];	//0-input; 1-output;
	
	uint32_t cl_port;
	uint32_t cl_ip;

//------------------------------------------------------------
	
	RAD_req()
		{
		this->cl_numb=0;
		this->type=0;
		this->service=0;
	}


	RAD_req(uint32_t cl_numb1,uint8_t type1,uint8_t service1);
};

//--------------------------------------RAD_QUE-->>

class RAD_QUE
	{
public:

list<RAD_req> que;

uint8_t generate_request(uint32_t cl_numb,uint8_t type, uint8_t service); //ip_hash //
//type of request 0-auth;1-account Start; 2-account STOP; 3 account-UPdate

RAD_OVEN* rad_oven;

};



//----------------------------------------RAD_QUE--||


//----------------------------------------RAD_OVEN-->>

struct RAD_OVEN_EL
	{
uint8_t serv_num;//number of server
char auth_str[16];
RAD_req data;//client number
uint8_t state; //0-free 1 - enable
uint8_t trial;
int32_t timer_n; 
};


class RAD_OVEN
	{
public:

RAD_OVEN_EL oven[0x100];//0x100

list<uint8_t> free_cells;//init from 0 to 255

RAD_CL_conf* rad_cl_conf;

RAD_QUE* rad_que;

RAD_OVEN()
	{

	for (int i=0;i<0x100;i++)  //0x100
		{
		this->free_cells.push_back(i);
	}
return;
}

RAD_OVEN & operator= (const RAD_OVEN & other)
	{

	free_cells =other.free_cells;
	rad_cl_conf=other.rad_cl_conf;
	memcpy(this->oven,other.oven,0x100*sizeof(RAD_OVEN_EL));
}	


//---------------------------------------------------------------------------

uint8_t generate_request(uint8_t cells_numb, RAD_req data1);
uint8_t add_free_cell(uint8_t cells_numb);
uint8_t read_responce(uint8_t* m,uint16_t pk_len); 
uint8_t w_s_request_auth(uint8_t cells_numb);//write and send requst authentication
uint8_t w_s_request_acc(uint8_t cells_numb);//write and send requst  accounting
uint8_t w_s_request(uint8_t cells_numb);//write ans send request
int add_common_attribute(uint8_t* data,uint16_t &dt_p,uint8_t cells_numb);
};


//----------------------------------------RAD_OVEN--||




int RAD_OVEN_new_trial(int time,
		uint32_t d1[4],	//d1 - cells_number
		void* p1[4]   	//p1 - pointer to RAD_OVEN)
		);



std::string byte2str(const uint8_t* data,uint8_t length);

#endif
