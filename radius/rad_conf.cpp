#include <arpa/inet.h> // for change bit order
#include "../config/node.h"


#include "../filter/service.h"
#include "rad_conf.h"
#include "../routing/route.h" // for s2ip
#include "rad_global.h"

#include "../clients/clients.h"


using namespace boost;
using namespace std;

extern RAD_SERVERS G_rad_servers;


RAD_CL_conf::RAD_CL_conf(uint8_t type)// 0 -authen 1- accaunt
	{
	// we needed to load config from node tree

	
	this->dubl_serv_count=0;

	this->serv_port=htons(1812+type);//1812;
	this->client_port=htons(4444+type);//4444;

	this->prot_type=type;


	if (type==0)
		{

		this->p0.idle_timeout=0;

		this->p0.reject_serv=-1;

		this->p0.error_serv=-1;//when receive error AVP or wrong service

		this->p0.unreach_serv=-1;// when radius is unreachable

	}

}

//==============================================================================================================

int get_serv_id_from_conf_var(node* service_n,string name_value)
	{

	node* curr_n=find(name_value,service_n);
	int i;

			if (curr_n==NULL) return -2;
						

			try 	{ 
				i=G_serv_agr->h_t.at(curr_n->value);
			 }
			catch(...){
				curr_n->err_gen("That servis should be configured in section Service");
				i=-1;
			}

			
	return i;

}

//==================================================================================================
int  RAD_CL_conf::load_config(node* config_tree1)
	{


	node* prot_serv_n;
	node* cl_conf_n;
	rad_servers.clear();
//--------------------------------------------------------------------------------------------AUTHENTICATION_CLIENT_CASE-->>
	if (this->prot_type==0)
		{
			cl_conf_n=find("access",config_tree1);

			if (cl_conf_n==NULL)  return 0; //empty config
			//------------------------------------------------------------username template---->>

			node* username_n=find("username",cl_conf_n);
			if (username_n!=NULL)
				{
				if (username_n->value=="") 
					{
					username_n->err_gen("username should be non empty string!!!");
					return -1;
				}
				CL_IP_SESS::username_template=username_n->value;

			}
			node* password_n=find("password",cl_conf_n);
			if (password_n!=NULL) CL_IP_SESS::password_template=password_n->value;

			//------------------------------------------------------------username template----||
			node* idle_time_n=find("idle-timeout",cl_conf_n);

			if (idle_time_n!=NULL)
				{

				int kk=s2numb(idle_time_n->value);

				if (kk<0)
					{
					idle_time_n->err_gen("should be from 0 to 65535");
					return -1;
				}

				else
					{
					this->p0.idle_timeout=kk;
				}
			}

			node* service_n=find("service",cl_conf_n);

			if (service_n==NULL) 
				{
				cl_conf_n->err_gen("At least service default should be configured");

				return -1;
			}
			//--------------------------------------------------------

			
			int def_serv=get_serv_id_from_conf_var(service_n,"default");
		
			if (def_serv==-1) return -1; //error in value in config

			if (def_serv!=-2)//value is configured
				{
			//	this->p0.reject_serv=def_serv; reject service should be configured specially
				this->p0.error_serv=def_serv;
				this->p0.unreach_serv=def_serv;
			}
			//--------------------------------------------------------

			int curr_ind=get_serv_id_from_conf_var(service_n,"reject-receive");

			if (curr_ind==-1) return -1; //error in value in config

			if (curr_ind!=-2)//value is configured
				{
				this->p0.reject_serv=curr_ind;
			}
			//--------------------------------------------------------

			 curr_ind=get_serv_id_from_conf_var(service_n,"error-receive");

			if (curr_ind==-1) return -1; //error in value in config

			if (curr_ind!=-2)//value is configured
				{
				this->p0.error_serv=curr_ind;
			}
		
			//--------------------------------------------------------

			 curr_ind=get_serv_id_from_conf_var(service_n,"radius-unreachable");

			if (curr_ind==-1) return -1; //error in value in config

			if (curr_ind!=-2)//value is configured
				{
				this->p0.unreach_serv=curr_ind;
			}

			//-------------------------------------------------------------------------------
			node* arp_k_time_n=find("arp-keepalive-timer",cl_conf_n);
			this->p0.arp_k_time=60*5;

			if (arp_k_time_n!=NULL)
				{
				int kk=s2numb(arp_k_time_n->value);
				if (kk<0)
					{
					arp_k_time_n->err_gen("should be from 0 to 65535");
					return -1;
				}
				this->p0.arp_k_time=kk;			}

			//-------------------------------------------------------------------------------
			node* arp_k_counter_n=find("arp-keepalive-counter",cl_conf_n);
			this->p0.arp_k_counter=3;

			if (arp_k_counter_n!=NULL)
				{
				int kk=s2numb(arp_k_counter_n->value);
				if (kk<0)
					{
					arp_k_counter_n->err_gen("should be from 0 to 65535");
					return -1;
				}
				this->p0.arp_k_counter=kk;
			}
			//-------------------------------------------------------------------------------

	}
//-----------------------------------------------------------------------------------AUTHENTICATION_CLIENT_CASE---||
	if (this->prot_type==1)
		{
		cl_conf_n=find("accounting",config_tree1);
		if (cl_conf_n==NULL) return 0; //empty config
	}





//if configured idlw timeout and tiels number
//----------------------------------------------------TIMEOUT_AND_NUMB_OF_TRIELS-->>

this->num_trials=G_rad_servers.num_trials;
	this->timeout=G_rad_servers.timeout;//in seconds

//--------------------------------------------------timeout---------->>>
	node* timeout_n=find("timeout",cl_conf_n);
	if (timeout_n!=NULL)
		{
		this->timeout=s2numb(timeout_n->value);		
	}
//--------------------------------------------------timeout----------|||


//--------------------------------------------------tries_count-------->>>
node* trials_n=find("trials_count",cl_conf_n);
	if (trials_n!=NULL)
		{
		this->num_trials=s2numb(trials_n->value);		
	}
//--------------------------------------------------tries_count--------|||


 //----------------------------------------------------TIMEOUT_AND_NUMB_OF_TRIELS--||
	node* rad_servers_n=find("radius-servers",cl_conf_n);

	if (rad_servers_n==NULL) return 0;

	node* rad_g_n=find("radius",config_tree1); 

	if (rad_g_n==NULL) return 0;

//----------------------------------------------------NAS_IDENTIFIERS_AND_IP---->>
	
	node* nas_ident_n=find("nas-identifier",rad_g_n);
	if (nas_ident_n!=NULL) RAD::nas_identifier=nas_ident_n->value;


	node* nas_ip_n=find("nas-ip-address",rad_g_n);
	if (nas_ip_n!=NULL)
		{
		if (s2ip(nas_ip_n->value,RAD::nas_ip_address)>0)
			{
			nas_ip_n->err_gen("IP ADDRESS "+nas_ip_n->value+" is not valid");	
			return -1;
		}

		
		
	}
	

//----------------------------------------------------NAS_IDENTIFIERS_AND_IP----||



	//----------------------------------------------------VENDOR_ID and VENDOR_TYPE---->>

	node* vend_id_n=find("vendor-id",rad_g_n);
	if (vend_id_n!=NULL) 
		{
		int k=s2numb(vend_id_n->value);
		if (k<0) 
			{
			vend_id_n->err_gen("vendor-id should be value from 1 to 65535");
			return -1; 
		}
		RAD::vendor_id=k;
	}

	node* vend_t_n=find("vendor-type",rad_g_n);
	if (vend_t_n!=NULL) 
		{
		int k=s2numb(vend_t_n->value);
		if (k<0) 
			{
			vend_t_n->err_gen("vendor-type value should be value from 1 to 65535");
			return -1; 
		}
		RAD::vendor_type=k;
	}

	//----------------------------------------------------VENDOR_ID and VENDOR_TYPE----||

	//-----------------------port config--------------------->>
	node* port_n=find("port",cl_conf_n);
	if (port_n!=NULL)
		{
		int port1=s2numb(port_n->value);
		if ((port1>0)&&(port1<0x10000))
			{
			this->serv_port=htons(port1);

		}	

	}
	//-----------------------port config---------------------||



	node* loc_rad_servers_n=find("radius-servers",cl_conf_n);

	if (loc_rad_servers_n==NULL)
		{
		return -2;
	}

	string rad_servers_s=loc_rad_servers_n->value;

	smatch m;

	std::string ip_reg="((\\d+)(\\.\\d+){3})";
	
	if (!regex_match(rad_servers_s,m,regex("^((\\["+ip_reg+"(\\, ("+ip_reg+"))*\\])|"+ip_reg+")$")))
		{
		loc_rad_servers_n->err_gen("The chaine of ip addresses of radius servers should be in following format:[ip1, ip2, .. ,ipn] or ip1");	
		return -1;
	}
	

	rad_servers.clear();

	sregex_token_iterator iter_ip_server (rad_servers_s.begin(), rad_servers_s.end(),regex(ip_reg), 0);
	sregex_token_iterator end;
	for(int i=0; (iter_ip_server != end)&&(i<50); ++iter_ip_server,i++) 
		{
		string s_ip_server=(*iter_ip_server).str();
		uint32_t curr_rad_ip;

		if (s2ip(s_ip_server,curr_rad_ip)>0)
			{
			loc_rad_servers_n->err_gen("IP ADDRESS "+s_ip_server+" is not valid");	
			return -1;
		}

		//try to find that server in config of radius servers
	
		int rad_index=-1;
		for (int k=0;k<G_rad_servers.arr.size();k++)
			{
			if (G_rad_servers.arr[k].ip==curr_rad_ip)
				{
				rad_index=k;break;
			}
		}

		if (rad_index<0)
			{
			loc_rad_servers_n->err_gen("I can't find configuration for radius server with ip="+ip2s(curr_rad_ip) + " in radius section");	
			return -1;
		}
		rad_servers.push_back(G_rad_servers.arr[rad_index]);
	}


//============================================================================




if (this->prot_type==1)
	{
	//----------------------------------------------load dublication string---->>>

	node* dubl_n=find("duplicate-servers-count",cl_conf_n);

	if (dubl_n!=NULL)
		{	

		this->dubl_serv_count=s2numb(dubl_n->value);

		if ((this->dubl_serv_count<0)||(this->dubl_serv_count>rad_servers.size()))

			{
			dubl_n->err_gen("duplicate-servers-count should be a value from interval [0;Number of Radius servers]");
			return -1;
		}

	}
	//----------------------------------------------load dublication string----|||


	//load update timer; default is 5min 300secons
	this->p1.update_timer=300;

	node* update_timer_n=find("update-timer",cl_conf_n);

	if (update_timer_n==NULL) return 0;	

	this->p1.update_timer=s2numb(update_timer_n->value);

	if ((this->p1.update_timer<0)||(this->p1.update_timer>65535))

		{
		update_timer_n->err_gen("update time should be in interval [60;65535]");
		return -1;
	}

	
}

return 0;

}

