//#include <functional>
#include <unordered_map>
#include "../config/node.h"

#include "rad_servers.h"
#ifndef RAD_COA_HEADER

#define RAD_COA_HEADER


class RAD_COA;

class RAD_TLV
	{
public:
uint8_t attr_n;		//--attribute number
uint8_t data_type;	// 0 -str;1-2b; 2-4b; 3-string from vendor spesific

string str;
int16_t _2b;
int32_t _4b;

int state;		// 0-inial;//1- data is read correctly; -1 error in TLV; -2 data structes not found; 2 dublication TLV found
			//3 conflicting data//4 nas identifier mismatched//5-unsupported service //6 missind attribute
uint32_t fun_bit;	//function bit 0x1 ->user identifier; 0x10->session identifier; 0x1000 nas identifier

int32_t ip_hash;	// -1 meancne not found
int8_t  serv_id;	// -1 meance not found

static int32_t ip_hash_g;//globla for that request
static int8_t  serv_id_g;//globla for that request

int16_t pol_in;
int16_t pol_out;



int (RAD_TLV::* p_d)(); //process data //RAD_TLV::

int read_data(uint8_t* data);//return state;

//-----------------------------------------------------
RAD_TLV();
RAD_TLV(uint8_t attr_n1,
	uint8_t data_type1,
	uint32_t fun_bit); //data_type 0=str;1-2b;2-4b;

//----------------------------------------------------
//process_data function collection

void clear_state();


int p_d_1();	//username 
int p_d_44();	//sess_id
int p_d_50();	//M_sess_id
int p_d_8();	//framed ip address
int p_d_26(); //vendor spesific

int p_d_32(); //nas id
int p_d_4();  //nas ip

int compare(uint8_t message_type);


};

//--------------------------------------------------------------------

class RAD_COA
	{
public:

vector<RAD_server> rad_servers;//radius servers

uint16_t serv_port;//3799; we are server


uint8_t process_packet(uint8_t* m,uint16_t pk_len); //return phy_int of responce

int load_config(node* config_tree);

int state;//0-initial;1 ack; -1 error in TLV ; -2 couldn't make request; 2 dublication of TLV; -3 unknow TLV;3 -conflicting parameters
		//5 ausupported service //6 missing attribute
int tlv_n;//tlv number for problem TLV

int ip_hash;
int16_t serv_id;


////////////////////////////////////////////////////////////////

std::unordered_map<uint8_t,RAD_TLV> tlv_set;
void add_tlv(const RAD_TLV &a);
void clear();

RAD_COA();

};

#ifndef MAIN_FILE

extern RAD_COA	   G_rad_coa;

#else

RAD_COA	   G_rad_coa;			// radius CoA

#endif


#endif
