#include "rad_conf.h"
#include "rad_oven.h"
#include "rad_global.h"
#include "../timers/timers_array.h"

//#include "/usr/src/kernels/3.18.7-200.fc21.x86_64/include/config/crypto/md5.h"

#include <openssl/md5.h>//for md5

#include <arpa/inet.h> // for change bit order

#include "../PP/PP.h" //prepare L2 header 

#include "../chassis/bras_chassis.h"//send packet

#include "../clients/clients.h"

using namespace std;

extern int RANDOM_DATA_FILE;

extern struct timers_array* G_timers_arr; 

extern RAD_SERVERS G_rad_servers;


//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
string get_value_from_service_string (string S,string param) //parameter - Service/input_policer/output_policer
	{
	string value="";

	boost::smatch m;
	
	if (boost::regex_search(S,m,boost::regex(param+"=[^;]*"))) 
		{
		string ms=*m.begin();
		value=ms.substr(param.size()+1,ms.size()-param.size()-1);
	}

return value;
}
	
//---------------------------------------------------------------------------------------------------------

uint8_t* find_attribute (uint8_t* start, uint8_t* end, uint8_t att_type) //end is next element after packet data
	{

	for (int i=0;i<end-start;i+=start[i+1])
		{
		if (!(i+start[i]<end-start)) return NULL;
		
		if (start[i]==att_type) return start+i;

	}

return NULL; //if we find nothing

}

//---------------------------------------------------------------------------------------------------------

uint8_t RAD_OVEN::read_responce(uint8_t* m,uint16_t pk_len) //packet begin with L3 header for check IP address 
//of server
	{
uint8_t *m_d = m+20+8;//IP+ udp


if (pk_len<20+8+20) return -3 ;// ip 20 udp 8 radius min 20 // packet to short

//-----------------------------Check UPD ports----------->>>
uint8_t *udp_h=m+20;// udp header;
if ((*(uint16_t*)udp_h!=this->rad_cl_conf->serv_port)||(*(uint16_t*)(udp_h+2)!=this->rad_cl_conf->client_port))
	{
return -1 ;// udp ports is wrong
}
//-----------------------------Check UPD ports-----------||||
uint8_t cells_numb=m_d[1];//identifier

RAD_OVEN_EL* curr_el=this->oven+cells_numb;


RAD_req* d=&(curr_el->data);//data

uint32_t cl_numb=d->cl_numb;

CL_IP_DAEL * cl_ip=cl_ip_dt+cl_numb;

CL_IP_SESS * cl_sess=cl_ip_sess_dt+cl_numb;

if (curr_el->state!=1) return -2;//cells is not active

if ((cl_sess->state!=1)||(cl_sess->M_sess!=d->cl_m_session))
	{
	G_timers_arr->delete_timer(curr_el->timer_n);
	
	this->add_free_cell(cells_numb);

	return -3;//client is not valid
}




uint16_t length_rad_p=(m_d[2]<<8)+m_d[3];
if (length_rad_p<20) return -2;// packet is to short

//-----------check ip of server----------------------------->>>

uint8_t *p_end=m_d+length_rad_p; //packet end

RAD_server *curr_serv= &this->rad_cl_conf->rad_servers[0]+curr_el->serv_num;

if (curr_serv->ip!=htonl(*((uint32_t*)m+3))) return -4;// source ip of packet is differ from ip of cur_server for that client

//-----------check ip of server-----------------------------|||

//cheack authenticator
char auth_comp[16];

MD5_CTX c;
  MD5_Init(&c);
  MD5_Update(&c,(void *)m_d,4); 
  MD5_Update(&c,(void *)curr_el->auth_str,16);
  MD5_Update(&c,(void *)(m_d+20),length_rad_p-20);
  MD5_Update(&c,(void *)(curr_serv->shar_secr),curr_serv->shar_secr_l);
  MD5_Final((unsigned char *)auth_comp,&c);

//for auth_responce
if ((!(memcmp(auth_comp,m_d+4,16)==0))&&(this->rad_cl_conf->prot_type==0)) return -5 ;// wrong authenticator

//for accounting don't check responce authenticator



uint8_t answer_code=m_d[0];

//we need to get information about users servises; Write appropriate servise and free oven_cells
//and delete timer

//find vendor attribute




if ((this->rad_cl_conf->prot_type==0)&&(answer_code==2)) //authentication accept
	{
do
	{
	
	uint8_t* vend_spec_att_addr=find_attribute(m_d+20,p_end,26);///26- vendor spesific attribute this->rad_conf->Vendor_Specific
	if (vend_spec_att_addr==NULL)  break;
	m_d=m_d+vend_spec_att_addr[1];//very importante;
	//check vendor-Id rfc 2865 page 47
	if (*(uint32_t*)(vend_spec_att_addr+2)!=htonl(RAD::vendor_id)) continue;////?????

	uint8_t vend_spec_att_len=vend_spec_att_addr[1]-6;
	
	
		string serv_pol_name="";// service and policer name;
		string service_name="";

		string input_policer_name="";
		string output_policer_name="";

		uint8_t *service_att_addr=find_attribute(vend_spec_att_addr+6,vend_spec_att_addr+vend_spec_att_addr[1],RAD::vendor_type);///???

		if (service_att_addr==NULL) break; //no any service attribute
	
		serv_pol_name=std::string((char*)(service_att_addr+2),service_att_addr[1]-2);
//format is: Service="aaa";input_policer="bbb";output_policer="cccc";
//first shouldfind Service="xxx"; 

service_name       =get_value_from_service_string(serv_pol_name,"Service"); 
input_policer_name =get_value_from_service_string(serv_pol_name,"input_policer"); 
output_policer_name=get_value_from_service_string(serv_pol_name,"output_policer"); 

add_service_and_policer_to_user(service_name,input_policer_name,output_policer_name,cl_numb);

} while (false);
	}//end of if

if ((this->rad_cl_conf->prot_type==0)&&(answer_code==3)) //authentication reject
	{
	//
	if (!(rad_cl_conf->p0.reject_serv<0) )
		{
		//reject sevice is configured
		
		add_service_and_policer_to_user(rad_cl_conf->p0.reject_serv,-1,-1,cl_numb);//-1 means default poliser

	}
	else
		{
	//should delete client

	del_cl(cl_numb,3);//Lost Service;
	}
}



G_timers_arr->delete_timer(curr_el->timer_n);

//if accounting server answer will be refused then our behaviour will be the same as accounting srver answer accept

  //only for acounting servrs in case when dublication is configured

/*
if ((this->rad_cl_conf->prot_type==1)&&(curr_el->serv_num<this->rad_cl_conf->dubl_serv_count-1)) //dubl_serv_count>=2 only should be for success work that feature
	{
	curr_el->serv_num++;
	curr_el->trial=0;
	//should generate another request
	this->w_s_request(cells_numb);//write and send 

	uint32_t d1[4];
	void* p1[4];
	p1[0]=(void*)this;
	d1[0]=cells_numb;

	curr_el->timer_n=G_timers_arr->new_timer(this->rad_cl_conf->timeout,RAD_OVEN_new_trial,d1,p1);
	return 0;
}

*/

if ((this->rad_cl_conf->prot_type==0)&&(answer_code==3)) //authentication


this->add_free_cell(cells_numb);

return 0;
}


