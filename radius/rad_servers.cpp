#include "rad_servers.h"
#include "../routing/route.h" //for functions converting from string to net or ip

int RAD_SERVERS::load_config(node* config_tree1)
	{

	this->arr.clear();

	node* radius_n=find("radius",config_tree1);

	if (radius_n==NULL) return 0;
		
	//--------------------------radius servers----------------------------------------->>>>
	uint8_t i=0;
	node* servers_n=find("server",radius_n);

	if (servers_n==NULL) return 0;



	for (node* ser_n=servers_n->child_node;(ser_n!=NULL)&(i<0xFF);ser_n=ser_n->next_node)
		{
		RAD_server curr_rad;

		node* shar_secr_n=find("shared-secret",ser_n);

		curr_rad.shar_secr_l=0;

		if (shar_secr_n!=NULL)
			{ 
			curr_rad.shar_secr_l=shar_secr_n->value.size();
			shar_secr_n->value.copy(curr_rad.shar_secr,shar_secr_n->value.size());
		}

		if (s2ip(ser_n->name,curr_rad.ip)>0)
			{
			ser_n->err_gen("IP address"+ser_n->name+"is not valid!!!");return -1;
		}

		this->arr.push_back(curr_rad);
		i++;
	}
//--------------------------radius servers---------------------------------------------||||


	
//--------------------------------------------------timeout---------->>>
	node* timeout_n=find("timeout",radius_n);
	this->timeout=-1;
	if (timeout_n!=NULL)
		{
		this->timeout=s2numb(timeout_n->value);		
	}
	if (this->timeout<1) this->timeout=10;//seconds
//--------------------------------------------------timeout----------|||


//--------------------------------------------------tries_count-------->>>
node* trials_n=find("trials_count",radius_n);
	this->num_trials=-1;
	if (trials_n!=NULL)
		{
		this->num_trials=s2numb(trials_n->value);		
	}
	if (this->num_trials<1) this->num_trials=3;//
//--------------------------------------------------tries_count--------|||




return 0;

}
