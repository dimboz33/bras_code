
#include <arpa/inet.h> // for change bit order
#include <openssl/md5.h>//for md5
#include "rad_coa.h"
#include "rad_global.h"

#include "../clients/clients.h"
#include "../filter/service.h"
#include "../filter/policer.h"

using namespace boost;

string get_value_from_service_string (string S,string param); //parameter - Service/input_policer/output_policer

RAD_TLV::RAD_TLV();
RAD_TLV::RAD_TLV(uint8_t attr_n1,
		uint8_t data_type1,
		uint32_t fun_bit1);  //data_type 0=str;1-2b;2-4b;
//-----------------------------------------------

void RAD_TLV::clear_state();

//-----------------------------------------------

int RAD_TLV::read_data(uint8_t* data);

//--------------------------------------------

//-------------------------------------------------comparea state---->>>

int RAD_TLV::compare(uint8_t message_type);

//-------------------------------------------------comparea state----||| 

//---------------------------process_data for different TLV------------------->>

//all functions in that section return state of RAD_TLV class

int RAD_TLV::p_d_1();//username 

//-------------------------------------------------------------------

int RAD_TLV::p_d_44();//sess_id

//------------------------------------------------------------------------

int RAD_TLV::p_d_50();//M_sess_id

//----------------------------------------------------------------------------

int RAD_TLV::p_d_8();//framed ip address

//----------------------------------------------------------------------------

int RAD_TLV::p_d_26();//vendor spesific

//-----------------------------------------------------------------------------

int RAD_TLV::p_d_32();//nas id string

//-----------------------------------------------------------------------------

int RAD_TLV::p_d_4();//nas ip



