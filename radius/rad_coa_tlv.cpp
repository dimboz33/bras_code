
#include <arpa/inet.h> // for change bit order
#include <openssl/md5.h>//for md5
#include "rad_coa.h"
#include "rad_global.h"

#include "../clients/clients.h"
#include "../filter/service.h"
#include "../filter/policer.h"
using namespace boost;



string get_value_from_service_string (string S,string param); //parameter - Service/input_policer/output_policer


RAD_TLV::RAD_TLV()
	{
}

RAD_TLV::RAD_TLV(uint8_t attr_n1,
		uint8_t data_type1,
		uint32_t fun_bit1)  //data_type 0=str;1-2b;2-4b;
	{
	this->attr_n=attr_n1;
	this->data_type=data_type1;
	this->fun_bit=fun_bit1;
	clear_state();
}	
//-----------------------------------------------
void RAD_TLV::clear_state()
	{
	
	ip_hash=-2;//initial state
	serv_id=-2;//initial state
	pol_in=-2;
	pol_out=-2;
	state=0;
	this->ip_hash_g=-1;
	this->serv_id_g=-1;
	
}

//-----------------------------------------------

int RAD_TLV::read_data(uint8_t* data)
	{
if (state==1) return 2;//Dublicate
if (state!=0) return state;
switch (data_type)	{
case 0: 
	if (data[1]<2)  return -1;
	str=string((char*)data+2,data[1]-2);
		return 1;
case 1:
	if (data[1]!=2+2) return -1;

	_2b=htons(*(uint16_t*)(data+2));
		return 1;
case 2:
	if (data[1]!=2+4) return -1;

	_4b=htonl(*(uint32_t*)(data+2));
	
	return 1;
case 3:
	if (data[1]<7) return -1;
	//check vendor_id
	if (RAD::vendor_id!=htonl(*(uint32_t*)(data+2)) ) return -1;//no our vendor_id
	data+=2+4;

	if (data[0]!=RAD::vendor_type) return -1; //not our vendor_type

	if (data[1]<3) return -1; //too small length

	str=string((char*)data+2,data[1]-2);//-2 ???
	return 1;
}
}

//--------------------------------------------

//-------------------------------------------------comparea state---->>>
int RAD_TLV::compare(uint8_t message_type)
	{

	if ((this->fun_bit&0x1)!=0)
		{
		//----check ip_hash
		if (this->ip_hash_g<0)
			{
			this->ip_hash_g=this->ip_hash;
		}
		else
			{
			if (this->ip_hash_g!=this->ip_hash) return 3;
		}

	}
	if ((this->fun_bit&0x10)!=0)
		{

		if ((message_type==43)&&(this->attr_n!=26)) return 1;//
		//in coa-request olny vendor-spesific can set new serv_id

		if (this->serv_id<0) return 1;

		//----check serv_id
		if (this->serv_id_g<0)
			{
			this->serv_id_g=this->serv_id;
		}
		else
			{
			if (this->serv_id_g!=this->serv_id) return 3;
		}

	}
	return 1;
}

//-------------------------------------------------comparea state----||| 

//---------------------------process_data for different TLV------------------->>

//all functions in that section return state of RAD_TLV class

int RAD_TLV::p_d_1()//username 
	{
	//main thread don't need to lock mutex
	auto find_value=cl_ht.username.find(str);
	
	if (find_value==cl_ht.username.end())
		{
		return -2;
	}

	this->ip_hash=find_value->second;
	return 1;
	
}

//-------------------------------------------------------------------

int RAD_TLV::p_d_44()//sess_id
	{
	//let's convert sees_id from string to ::uint64_t value
	::uint64_t M_sess_id_64;
	::int16_t serv_numb;


	if (sscanf(this->str.c_str(),"%016llX_%x",&M_sess_id_64,&serv_numb)!=2)
		{
		if (sscanf(this->str.c_str(),"%016llX",&M_sess_id_64)!=1)
			{
			return -2;/// couln't find such M_session_id
		}
		serv_numb=-1;
	}

	auto find_value=cl_ht.M_sess.find(M_sess_id_64);

	if (find_value==cl_ht.M_sess.end())
		{
		return -2; //couldn't find such M_session_id
	}

	this->ip_hash=find_value->second;
	
	//should try to find service number
	
	for (int i=0;i<C_serv_count;i++)
		{
		if (serv_numb==cl_ip_sess_dt[this->ip_hash].sess[i]) this->serv_id=i;
	}
	if (this->serv_id<0)	this->_2b=-1;
	else			this->_2b=serv_numb;
	
	return 1;
}

//------------------------------------------------------------------------
/*
int RAD_TLV::p_d_50()//M_sess_id
	{
	//let's convert sees_id from string to ::uint64_t value
	::uint64_t M_sess_id_64;

	if (sscanf(this->str.c_str(),"%016llX",&M_sess_id_64)!=1)
		{
		return -2;/// couln't find such M_session_id
	}
	auto find_value=cl_ht.M_sess.find(M_sess_id_64);

	if (find_value==cl_ht.M_sess.end())
		{
		return -2; //couldn't find such M_session_id
	}

	this->ip_hash=find_value->second;
	
	return 1;
}
*/

//----------------------------------------------------------------------------

int RAD_TLV::p_d_8()//framed ip address
	{
	this->ip_hash=rte_hash_lookup(cl_ip_ht,&this->_4b);
	if (this->ip_hash<0) return -2;//could not find 

	return 1;

}

//----------------------------------------------------------------------------
int RAD_TLV::p_d_26()//vendor spesific
	{

	string service_name       =get_value_from_service_string(this->str,"Service"); 
	string input_policer_name =get_value_from_service_string(this->str,"input_policer"); 
	string output_policer_name=get_value_from_service_string(this->str,"output_policer"); 


	auto serv_id_f=G_serv_agr->h_t.find(service_name); // _f -> found
	if (serv_id_f==G_serv_agr->h_t.end()) return 5;//couldn't find service
	this->serv_id=serv_id_f->second;

	if (input_policer_name!="")
		{
		auto pol_in_f=G_pol_agr->h_t.find(input_policer_name); 
		if (pol_in_f!=G_pol_agr->h_t.end()) this->pol_in=pol_in_f->second;
		else return 5;//couldn't find policer
	}

	if (output_policer_name!="")
		{
		auto pol_out_f=G_pol_agr->h_t.find(output_policer_name); 
		if (pol_out_f!=G_pol_agr->h_t.end()) this->pol_out=pol_out_f->second;
		else return 5;//couldn't find policer
	}


	return 1;

}

//-----------------------------------------------------------------------------


int RAD_TLV::p_d_32()//nas id string
	{

	if (this->str!=RAD::nas_identifier) return 4; //nas missmatched

	return 1;

}

//-----------------------------------------------------------------------------

int RAD_TLV::p_d_4()//nas ip
	{

	if (this->_4b!=RAD::nas_ip_address) return 4; //nas missmatched

	return 1;

}


