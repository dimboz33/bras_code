#include <arpa/inet.h> // for change bit order
#include <openssl/md5.h>//for md5
#include "rad_coa.h"
#include "rad_global.h"


#include "../clients/clients.h"
#include "../filter/service.h"
#include "../filter/policer.h"
using namespace boost;

#include "../routing/route.h" // for s2ip


extern RAD_SERVERS G_rad_servers;

int RAD_COA::load_config(node* config_tree1)
	{

	rad_servers.clear();

	node* rad_g_n=find("radius",config_tree1); 

	if (rad_g_n==NULL) return 0;



	node* CoA_n=find("CoA",config_tree1);

	if (CoA_n==NULL) return 0;

	//-----------------------port config--------------------->>
	node* port_n=find("port",CoA_n);
	if (port_n!=NULL)
		{
		int port1=s2numb(port_n->value);
		if ((port1>0)&&(port1<0x10000))
			{
			this->serv_port=port1;

		}	

	}
	//-----------------------port config---------------------||

	node* loc_rad_servers_n=find("radius-servers",CoA_n);

	if (loc_rad_servers_n==NULL) return 0;

	string rad_servers_s=loc_rad_servers_n->value;

	smatch m;

	std::string ip_reg="((\\d+)(\\.\\d+){3})";
	
	if (!regex_match(rad_servers_s,m,regex("^((\\["+ip_reg+"(\\, ("+ip_reg+"))*\\])|"+ip_reg+")$")))
		{
		loc_rad_servers_n->err_gen("The chaine of ip addresses of radius servers should be in following format:[ip1, ip2, .. ,ipn] or ip");	
		return -1;
	}

	rad_servers.clear();	

	sregex_token_iterator iter_ip_server (rad_servers_s.begin(), rad_servers_s.end(),regex(ip_reg), 0);
	sregex_token_iterator end;

	for(int i=0; (iter_ip_server != end)&&(i<50); ++iter_ip_server,i++) 
		{
		string s_ip_server=(*iter_ip_server).str();
		uint32_t curr_rad_ip;

		if (s2ip(s_ip_server,curr_rad_ip)>0)
			{
			loc_rad_servers_n->err_gen("IP ADDRESS "+s_ip_server+" is not valid");	
			return -1;
		}

		//try to find that server in config of radius servers
	
		int rad_index=-1;
		for (int k=0;k<G_rad_servers.arr.size();k++)
			{
			if (G_rad_servers.arr[k].ip==curr_rad_ip)
				{
				rad_index=k;break;
			}
		}

		if (rad_index<0)
			{
			loc_rad_servers_n->err_gen("I can't find configuration for radius server with ip="+ip2s(curr_rad_ip) + " in radius section");	
			return -1;
		}
		rad_servers.push_back(G_rad_servers.arr[rad_index]);
	}

	

return 0;
}
