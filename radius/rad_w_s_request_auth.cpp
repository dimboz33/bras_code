
//r_w_responce -> read and write responce packet


#include "rad_conf.h"
#include "rad_oven.h"
#include "../timers/timers_array.h"

//#include "/usr/src/kernels/3.18.7-200.fc21.x86_64/include/config/crypto/md5.h"

#include <openssl/md5.h>//for md5

#include <arpa/inet.h> // for change bit order

#include "../PP/PP.h" //prepare L2 header 

#include "../chassis/bras_chassis.h"//send packet

#include "../clients/clients.h"

using namespace std;

extern int RANDOM_DATA_FILE;



extern struct timers_array* G_timers_arr; 


extern RAD_SERVERS G_rad_servers;




//-------------------------------------------------------------------------------------
uint8_t RAD_OVEN::w_s_request(uint8_t cells_numb)//write ans send request
	{

	RAD_OVEN_EL* curr_el=this->oven+cells_numb;

	if (curr_el->data.type==0)
		{
		return this->w_s_request_auth(cells_numb); //authentication
	}
	else
		{
		return this->w_s_request_acc(cells_numb); //accounting
	}

return 0;
}

//----------------------------------------------------------------------------------------
uint8_t RAD_OVEN::w_s_request_auth(uint8_t cells_numb)//write and send request
	{

u_char  data[512]; // data is where we will form packet radius
uint16_t dt_p; //data pointer in radius packet
int i;
uint8_t rad_p=46;
dt_p=rad_p;//18[ethernet]+20[ip]+8[udp]

RAD_OVEN_EL* curr_el=this->oven+cells_numb;


for (i=0;i<512;i++) //???
	{
	data[i]=0;
}


//--------

if (curr_el->data.type>0) return -3;//
//accounting request should be processing only by rad_accounting client

//-----------
//set code 

 data[dt_p++]=1; //responce authentication


//set identifier
data[dt_p++]=cells_numb;

//length field we will fill at the end
dt_p+=2;
//message authenticator

read(RANDOM_DATA_FILE,curr_el->auth_str,16);   
memcpy(data+dt_p,curr_el->auth_str,16);
//request authenticator is 16 byte

dt_p+=16;


//now we can write TLV data

///cl_numb is 
uint32_t cl_numb=this->oven[cells_numb].data.cl_numb;
RAD_req* d=&(curr_el->data);//data

CL_IP_SESS *cl_sess=cl_ip_sess_dt+cl_numb;

if (cl_sess->state!=1) return -1;// meance client is not valid

if (d->cl_m_session!=cl_sess->M_sess) return -1;// this is another new client

RAD_server *curr_serv= &this->rad_cl_conf->rad_servers[0]+curr_el->serv_num;

//-----------------------------------USERNAME------------------------------>>>

data[dt_p++]=1;//type username

data[dt_p]=2+sprintf((char*)data+dt_p+1,"%s",d->username.c_str());

dt_p+=data[dt_p]-1;


//-----------------------------------USERNAME------------------------------|||


//-----------------------------------PASSWORD------------------------------>>>
data[dt_p++]=2;// type password

uint16_t l_h_pass=(d->password.length()+0xF)&0xFFF0; //round up by module 16

data[dt_p++]=2+l_h_pass;//length with code and length

memcpy(data+dt_p,d->password.c_str(),d->password.length());//write password at plase where hashed password will be

const char* c_pass= d->password.c_str();

char b[16];
										//b1 rfc2865 page 27
MD5_CTX h;
MD5_Init(&h);

for (int i=0;i<l_h_pass;i=i+16)
	{
	MD5_Update(&h,(void*)(curr_serv->shar_secr),curr_serv->shar_secr_l);	//write S rfc2865 page 27
	void* c=(i==0)?(void*)curr_el->auth_str:(void*)(data+dt_p+i-16);
	MD5_Update(&h,c,16);						//write RA  rfc2865 page 27
	MD5_Final((unsigned char *)b,&h);							// b1=MD5(S+RA);

	for (int i1=0;i1<16;i1++)
		{
		data[dt_p+i1+i]=b[i]^data[dt_p+i1+i];
	}
}
dt_p+=l_h_pass;

//-----------------------------------PASSWORD------------------------------|||



this->add_common_attribute(data,dt_p,cl_numb);

//---------------------------------------------------------write length of the packet
*(uint16_t*)(data+rad_p+2)=ntohs((uint16_t)(dt_p-rad_p));






//----------------------------------UDP header------------------------------------->>>

uint8_t udp_p=18+20;//udp pointer; ethernet-18 + ip - 20
uint16_t length=dt_p-rad_p;

*(uint16_t*)(data+udp_p)=this->rad_cl_conf->client_port; //source port
*(uint16_t*)(data+udp_p+2)=this->rad_cl_conf->serv_port; //dest port
*(uint16_t*)(data+udp_p+4)=ntohs(length+8);

//----------------------------------UDP header-------------------------------------|||

// ip header


//----------------------------------IP header------------------------------------->>>

uint8_t ip_p=18;// ip pointer

data[ip_p]=0x45;
///ip length
*(uint16_t*)(data+ip_p+2)=ntohs(length+8+20);//8 udp header
// TTL
data[ip_p+8]=0xFF;//
// PROTOCOL UDP 
data[ip_p+9]=0x11;// 

//----------------------------------IP header------------------------------------||||

//get output interfave and write L2 header
int16_t pk_diff=0;
uint8_t ci[8];
uint8_t out_phy_int =Forwarding_table_act(curr_serv->ip,data+ip_p,(uint8_t*)&ci,&pk_diff);


if (out_phy_int!=0xFF)
	{
 	send_packet(data+ip_p-pk_diff,dt_p+ip_p-pk_diff,out_phy_int,ci);

}
//if only exist mac address


///We need to generate time for action when responce is not reseived


return 0;
}
