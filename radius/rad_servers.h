#ifndef RAD_SERVERS_HEADER

#define RAD_SERVERS_HEADER
#include <inttypes.h> // for uint32_t
#include <vector>
#include "../config/node.h"


//RADIUS CONFIG SHOULD BE LOADED AFTER SERVICES CONFIG

struct RAD_server
	{
uint32_t ip;
char shar_secr[64];
uint8_t shar_secr_l; // length of shar_secr

};

class RAD_SERVERS
	{
public:

std::vector<RAD_server> arr;

int num_trials;
int timeout;//in seconds

int load_config(node* config_tree);

};

#ifndef MAIN_FILE

extern RAD_SERVERS G_rad_servers;

#else

RAD_SERVERS G_rad_servers;


#endif
#endif
