#include <arpa/inet.h> // for change bit order
#include <openssl/md5.h>//for md5
#include "rad_coa.h"
#include "rad_global.h"


#include "../clients/clients.h"
#include "../filter/service.h"
#include "../filter/policer.h"
using namespace boost;



RAD_COA::RAD_COA()
	{
	this->serv_port=3799;//default value

//---------------Initialize TLV set------------------>>
//attr_n//data_type//fun_bit

//data type //0-str;1-2byte;2-4byte;
//fun_bit 0x1 user id; 0x10 sess_id; 0x100 nas_id
add_tlv(	RAD_TLV (1,	0,	0x1));	//avp_username
add_tlv(	RAD_TLV	(44,	0,	0x11));	//avp_sess_id
//add_tlv(	RAD_TLV	(50,	0,	0x1));	//avp_M_sess_id
add_tlv(	RAD_TLV (8,	2,	0x1));	//avp_fram_ip_Add	
add_tlv(	RAD_TLV (26,	3,	0x10));	//avp_vendor_sp	

add_tlv(	RAD_TLV	(32,	0,	0x100)); //avp_nas_id
add_tlv(	RAD_TLV	(4,	2,	0x100)); //avp_nas_ip

//---------------Initialize TLV set------------------||

//------------------------------------Initialize---process_data_function----->>>
tlv_set[1].p_d=	&RAD_TLV::p_d_1;
tlv_set[44].p_d=&RAD_TLV::p_d_44;
//tlv_set[50].p_d=&RAD_TLV::p_d_50;
tlv_set[8].p_d=	&RAD_TLV::p_d_8;
tlv_set[26].p_d=&RAD_TLV::p_d_26;

tlv_set[32].p_d=&RAD_TLV::p_d_32;
tlv_set[4].p_d=	&RAD_TLV::p_d_4;


//------------------------------------Initialize---process_data_function-----|||
}

//-------------------------------------------------------------------
//-------------------------------------------------------------------
//-------------------------------------------------------------------


//------------------------------------------------------------------------------------
void RAD_COA::clear()
	{
	this->state=0;
	this->ip_hash=-1;
	this->serv_id=-1;

return;
}
//------------------------------------------------------------------------------------

void RAD_COA::add_tlv(const RAD_TLV &a)
	{
	this->tlv_set[a.attr_n]=a;
}
//------------------------------------------------------------------------------------

uint8_t RAD_COA::process_packet(uint8_t* m,uint16_t pk_len) //return phy_int of responce

	{

if (pk_len<20+8+20) return 0xFF ;// ip 20 udp 8 radius min 20 // packet to short


uint8_t *m_d = m+20+8;//IP+ udp

uint32_t src_ip=htonl(*(uint32_t*)(m+12));

//--------------------check source ip---->>
int rad_index=-1;
for (int k=0;k<this->rad_servers.size();k++)
	{
	if (this->rad_servers[k].ip==src_ip)
		{rad_index=k;
		break;
	}	
}
if (rad_index<0) return 0xFF;// source ip is differ from radius CoA server
RAD_server *curr_serv=&G_rad_servers.arr[0]+rad_index; 

//--------------------check source ip----||
//-----------------------------Check UPD ports----------->>>
uint8_t *udp_h=m+20;// udp header;


if (*(uint16_t*)(udp_h+2)!=htons(this->serv_port)) return 0xFF ;// udp ports is wrong

//swap udp ports
memcpy(udp_h+2,udp_h,2);
*(uint16_t*)(udp_h)=htons(this->serv_port);
//-----------------------------Check UPD ports-----------||||


uint16_t length_rad_p=(m_d[2]<<8)+m_d[3];
if (length_rad_p<20) return -2;// packet is to short

unsigned char auth_check[16]={0};

//----------------------cheack authenticator------------>>>

MD5_CTX c;
MD5_Init(&c);
MD5_Update(&c,(void*)m_d,4); 
MD5_Update(&c,(void*)auth_check,16);
MD5_Update(&c,(void *)(m_d+20),length_rad_p-20);
MD5_Update(&c,(void *)(curr_serv->shar_secr),curr_serv->shar_secr_l);
MD5_Final(auth_check,&c);

if (memcmp(auth_check,m_d+4,16)!=0) return 0xFF;// authenticator is wrong

//----------------------cheack authenticator------------|||

uint8_t message_type=m_d[0];

if ((message_type!=40)&&(message_type!=43)) return 0xFF;//we are interesting only in diconnect-request and coa-request

//----------------------------set states to inial values-->>>
this->state=0;
this->ip_hash=-1;
this->serv_id=-1;

for (auto curr_tlv=this->tlv_set.begin();curr_tlv!=this->tlv_set.end();curr_tlv++)
	{
	curr_tlv->second.clear_state();
}

//----------------------------set states to inial values---|||


//------------------------------------------read_data ------------->>>

for (uint8_t* data=m_d+20;(data<m_d+length_rad_p)&&(!(data+data[1]>m_d+length_rad_p));data+=data[1])
	{
	
	auto curr_tlv=this->tlv_set.find(data[0]);
		
	if (curr_tlv==this->tlv_set.end())
		{
		this->state=-3;//unknown TLV
		goto WRITE_ANSWER;
	}	
	this->tlv_n=data[1];
	
	int ret=curr_tlv->second.read_data(data);

	curr_tlv->second.state=ret;
	
	if (ret!=1)
		{
		this->state=ret;
		goto WRITE_ANSWER;
	}
}

//------------------------------------------read_data-------------|||

//-----------------------------------process_data------------------->>>
//should process all data

for (auto curr_tlv=this->tlv_set.begin();curr_tlv!=this->tlv_set.end();curr_tlv++)
	{
	if  (curr_tlv->second.state!=1) continue; 
	RAD_TLV* a=&(curr_tlv->second);
	this->state=(a->*(a->p_d))();//(a.*(a.p_d))();
	if (this->state!=1) goto WRITE_ANSWER;
	int res=curr_tlv->second.compare(message_type);
	this->ip_hash=a->ip_hash_g;
	this->serv_id=a->serv_id_g;


	if (res!=1)
		{
		this->state=res;
		goto WRITE_ANSWER;
	}

}
//-----------------------------------process_data-------------------|||

if ((this->ip_hash<0)||((this->serv_id<0)&&(message_type==43))) this->state =6;// missing attributes

WRITE_ANSWER:

uint8_t* data=m_d+20;
int dt_p=0;



//---------------------------------------------------------ACK---------->>>
if (this->state==1)
	{	//not exectly sure that all data is good 
		//at that section there is a pobability that request is bad  

	if (message_type==40)//disconnect message
		{
		if (this->serv_id<0) del_cl(this->ip_hash,10);//delete hole client; 
		else	if (del_service_from_user(this->ip_hash,this->serv_id)<0) this->state=-2;//session context not found;
	}
	if (message_type==43) //coa message
		{
		//if sess_id TLV was not null we should delete service with sess_id index
		RAD_TLV tlv_44=this->tlv_set[44];//session_id
		RAD_TLV tlv_26=this->tlv_set[26];//vendor spesific


		int16_t old_serv_num=-1;

		if (this->serv_id!=tlv_44.serv_id)	
			{

			//new service is written at tlv_26, hence tlv_44 meance old session that should be replaced
			// is old session_id is not written -> we should only add new service 

			if (!(tlv_44.serv_id<0)) 
				{
				old_serv_num=tlv_44._2b;//number in session id attribute after (M_session_id)_
				del_service_from_user(this->ip_hash,tlv_44.serv_id);
			}
			add_service_and_policer_to_user(this->serv_id, //new service id 
						tlv_26.pol_in, //-1 meance default value
						tlv_26.pol_out,//-1 meance default value	
						this->ip_hash);		
		}
		else
			{
			//should only change policers if they are setted
			if (!(tlv_26.pol_in<0))	G_pol_agr->set_pol_to_user(this->ip_hash,0,this->serv_id,tlv_26.pol_in);	
			if (!(tlv_26.pol_out<0))	G_pol_agr->set_pol_to_user(this->ip_hash,1,this->serv_id,tlv_26.pol_out);
		}


	}
}
//-----------------------------------------------AKC----|||


//-----------------------------------------------NAK---->>>

if (this->state!=1)
	{	//NAK 

//--------------------Error-Cause Attribute
	data[dt_p++]=101;
	data[dt_p++]=6;
	uint32_t err_code;
	if (this->state==3) err_code=503;//conflicting data 	->Invalid Request
	if (this->state==-2) err_code=503;//session context not found
	if (this->state==-1) err_code=404;//error in TLV 	->Invalid Request
	if (this->state==2)  err_code=404;//dublication TLV 	->Invalid Request
	if (this->state==-3) err_code=401;//unknown TLV
	if (this->state==4) err_code=403;//NAS Identification Mismatch 
	if (this->state==5) err_code=405;//Unsupported service
	if (this->state==6) err_code=402;//missing attributes
	*(uint32_t*)(data+dt_p)=htonl(err_code);
	dt_p+=4;
}

//-----------------------------------------------NAK----|||

if (this->state!=1) m_d[0]=message_type+2; //NAC
else		    m_d[0]=message_type+1; //ACK

//------------------------------------------write length
*(uint16_t*)(m_d+2)=ntohs((uint16_t)(dt_p+20));

//--------------------------------------new authenticator---->>>

MD5_Init(&c);
MD5_Update(&c,(void*)m_d,4); 
MD5_Update(&c,(void*)auth_check,16);
if (dt_p>0) MD5_Update(&c,(void *)(m_d+20),dt_p);
MD5_Update(&c,(void *)(curr_serv->shar_secr),curr_serv->shar_secr_l);
MD5_Final(m_d+4,&c);

//--------------------------------------new authenticator----|||

SEND_PACKET:

return 0;
//in case return 0 at RE function should be procedure thath will be change IP address and L2 headers

}

//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
//----------------------------------------------------------------//----------------------------------------------------------------
 int32_t RAD_TLV::ip_hash_g=-2;//global for that request
 int8_t  RAD_TLV::serv_id_g=-1;//globla for that request



//---------------------------process_data for different TLV-------------------||