#ifndef RAD_CL_HEADER

#define RAD_CL_HEADER

#include "rad_conf.h"
#include "rad_oven.h"
#include <mutex>



class RAD_CLIENT
	{

public:
	RAD_CL_conf conf;//config

	RAD_QUE que;//queue
	
	RAD_OVEN oven;// oven

	std::mutex* mut;//mutex of all hash tables



	
	RAD_CLIENT(uint8_t type)
		{
		this->conf=RAD_CL_conf(type);// type 0 -auth; 1-account
		this->que.rad_oven=&(this->oven);
		this->oven.rad_cl_conf=&(this->conf);
		this->oven.rad_que=&(this->que);
		mut=new std::mutex;
	}
	~RAD_CLIENT()
		{
		delete mut;
	}


//-----------------------------------------

uint8_t request(uint32_t cl_num,uint8_t type, uint8_t service)//type 
	{

	std::lock_guard<std::mutex> lock(*mut);

	return this->que.generate_request(cl_num,type,service);
}

//-----------------------------------------

int load_config(node* config_tree)
	{
	
	int b= this->conf.load_config(config_tree);
	
	return b;
}

int read_packet(uint8_t *L2_payload,uint16_t pk_len)
	{
	return this->oven.read_responce(L2_payload,pk_len);

}

};

#ifndef MAIN_FILE

//----------------------extern 

extern RAD_CLIENT G_rad_acc;
extern RAD_CLIENT G_rad_auth;

#else

RAD_CLIENT G_rad_auth=RAD_CLIENT(0);	//radius authentication client

RAD_CLIENT G_rad_acc=RAD_CLIENT(1);	// radius accounting client

#endif
#endif
