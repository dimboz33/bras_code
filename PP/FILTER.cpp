#include <rte_acl.h>

#include <rte_prefetch.h>

//---------------------------

#include <stdint.h>
#include <rte_common.h>
#include <rte_mempool.h>
#include <rte_memory.h>
#include <rte_atomic.h>
#include <rte_prefetch.h>
#include <rte_branch_prediction.h>

#include <rte_mbuf.h>

//for rte_mbuf.h
//-------------------------------

#include "../clients/clients.h"

#include "../routing/interfaces.h"
#include "../filter/service.h"
#include "../filter/policer.h"
#include "../filter/filter.h"

////////-------------------------filter

#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"


extern rte_acl_ctx* acl_arr[];
//extern struct LogInt* G_log_ints;
//extern struct PhyInt * G_phy_ints;


extern "C" uint8_t PP_FILTER (const uint8_t* p_arr[], // packet L3 data 
			
			struct rte_mbuf *pkt[], // mbuf headers for packets
			uint32_t result[],
			uint8_t phy_int_i, 
			uint32_t num_packets,
			uint8_t	filter_num,
			uint32_t ts_new, //time_slot of milisecond
		

			uint8_t is_output, // 1 if output packet, 0 if input packet
			uint8_t pre_n, //prefatch number
			uint8_t L_config_bit,
			::uint64_t *marker
			 ) 	// port that will call that function
						//pack_arr -paket array 


{

	uint8_t ci_offset=4-4*is_output; //input filter 4; output filter 0

//prefatch
/*
		for (int i=0;(i<pre_n)&(i<num_packets);i++)//num_packets//(i<pre_n)&
			{
			uint8_t* ci=rte_pktmbuf_mtod(pkt[i], uint8_t*);
			uint32_t  cl_num=(*((uint32_t*)(ci+ci_offset)))>>8;//4 for input filter; 0 for output
			CL_INT_DAEL* curr_cl=cl_forw_dt[2*phy_int_i+is_output].cl_arr+cl_num;

			rte_prefetch1(curr_cl);
			//rte_prefetch1(ci);
			rte_prefetch1(curr_cl+64);

		}

*/
		//if (acl_arr!=NULL) // if filter is configured
		//	{

			if (acl_arr[filter_num]!=NULL) 	rte_acl_classify(acl_arr[is_output+2*L_config_bit],p_arr, result, num_packets, 16);//16
			
			

		//} 


		*marker=rte_rdtsc();

const uint8_t F_C=16;//FILTER COUNT in the chain
//uint16_t CL_F_MASK=15; // 0000 0000 0000 0111











//-------------------------------------------INCREMENT COUTERS----------------------------->>

CL_INT_DAEL* curr_cl,pre_cl;//prefatched client



POLICER *curr_policer;

int serv_count1=(G_serv_agr_arr+L_config_bit)->serv_count;

	for (int i=0;i<num_packets;i++)
		{


		uint8_t* m=rte_pktmbuf_mtod(pkt[i], uint8_t*);
		uint8_t* ci=rte_pktmbuf_mtod(pkt[i], uint8_t*);

		uint32_t i_F_C=i*F_C;

		uint32_t  cl_num=(*((uint32_t*)(ci+ci_offset)))>>8;//4 for input filter; 0 for output

		if (cl_num==0xFFFFFF)
			{
			result[i_F_C]=0xff;
			continue;
		}
		
		if (result[i_F_C]==0xff) continue;
	
			
		curr_cl=cl_forw_dt[2*phy_int_i+is_output].cl_arr+cl_num;
/*			

		//----prefatch
		if (i+pre_n<num_packets)
			{
			uint8_t* ci_1=rte_pktmbuf_mtod(pkt[i+pre_n], uint8_t*);
			uint32_t  cl_num_1=(*((uint32_t*)(ci_1+ci_offset)))>>8;//4 for input filter; 0 for output
			CL_INT_DAEL* curr_cl_1=cl_forw_dt[2*phy_int_i+is_output].cl_arr+cl_num_1;

			rte_prefetch1(curr_cl);
			//rte_prefetch0(ci_1);


			rte_prefetch1(curr_cl+64);
		}
*/
		//----prefatch


		uint16_t CL_SERVICE_BITS=curr_cl->service_bits;

		uint8_t serv_num=0xff;

		for (int j=0;j<serv_count1;j++)//serv_count1
			{
			//priorities of services could be chanded dynamicly
			//result values goes from most priority service to less priority
			//result=0xab; a -index of next hop ip;/ b-service_id_number+1; 
			//service_id like it is written at servics bits in clients forwarding data structers
			int cur_serv_numb=(G_serv_agr_arr+L_config_bit)->p_que[j];

			if ((((1<<cur_serv_numb)&CL_SERVICE_BITS)>0)&(result[i_F_C+j]>0)) //if match in filter and appropriate service present
				{
				result[i_F_C+0]=result[i_F_C+j];
				serv_num=cur_serv_numb;
				break;
			}
			
		}

		if ((result[i_F_C]==0)||(serv_num==0xff)) 
			{
			result[i_F_C]=0xff;
			continue;		
		}
	

		//---------------------------------

		if (is_output)
			{
			//should set up mac and vlans
			//set mac address
			
			rte_memcpy(m,curr_cl->mac,6);
			rte_memcpy(m+6,G_phy_ints[phy_int_i].mac_addr,6);
			
			//*((uint32_t*)m)=*((uint32_t*)curr_cl->mac);
			//*((uint16_t*)(m+4))=*((uint16_t*)(curr_cl->mac+4));
		
			//*((uint32_t*)m+6)=*((uint32_t*)curr_cl->mac);
			//*((uint16_t*)(m+4+6))=*((uint16_t*)(G_phy_ints[phy_int_i].mac_addr+4));


			
			//outer VLAN TAG 0x8100	
			m[12]=0x81; m[13]=0x00;
			*((uint16_t*)(m+14))=curr_cl->vlan_out;//htons(curr_cl->vlan_out);


			if (curr_cl->vlan_in!=0x10)
				{
				//inner VLAN TAG 0x88a8
				m[16]=0x81; m[17]=0x00;
				*((uint16_t*)(m+18))=curr_cl->vlan_in;//htons(curr_cl->vlan_in);
			}
			
		}
		else
			{
			//should check vlans
			/* l2 header is alreay changed
			uint16_t vlan_ex=(*(uint16_t*)(m+14))&0xFF0F;
			uint16_t vlan_in=(*(uint16_t*)(m+18))&0xFF0F;

			if (unlikely((vlan_in!=curr_cl->vlan_in)||(vlan_ex!=curr_cl->vlan_out))) 
				{
				result[i_F_C]=0xFF;//delete packet
				continue;//to another packet

			}
			*/
			
					
		}
		//------------------------------------------------



		//-----------------------------Policer action------>>
		//uint8_t serv_num=result[i_F_C]-1; // servis number

		curr_policer=G_pol_agr->arr+(curr_cl->tr_co[serv_num].policer);

		uint32_t del_t=ts_new-curr_cl->tr_co[serv_num].last_time_slot;//in miliseconds
			
		//continue;


		if (del_t>0)
			{
			curr_cl->tr_co[serv_num].last_time_slot=ts_new;

			if (!(del_t<curr_policer->bucket_size_in_ms))
				{
				curr_cl->tr_co[serv_num].bucket_level=0;
			}
			else
				{
				uint32_t decr_l=curr_policer->speed*del_t;// Kbit/s * ms= bit; decrease of level in buckets
				if (decr_l>curr_cl->tr_co[serv_num].bucket_level)
					{
					curr_cl->tr_co[serv_num].bucket_level=0;
				}
				else
					{
					curr_cl->tr_co[serv_num].bucket_level-=decr_l;
				}
			}
		}
		uint16_t pkt_len1=(pkt[i]->pkt_len<60) ? 60 : pkt[i]->pkt_len;

		uint32_t new_l=curr_cl->tr_co[serv_num].bucket_level+((pkt_len1+4)<<3);//new level; *8 bits in byte (4 byte is fcs frame check sequence)

		if (new_l>curr_policer->bucket_size)
			{
			result[i_F_C]=0xFF;//delete packet
		}
		else
			{
		
			curr_cl->tr_co[serv_num].bucket_level=new_l;
			
			curr_cl->tr_co[serv_num].pack_counter++; 
			
			curr_cl->tr_co[serv_num].byte_counter+=pkt_len1; 

			curr_cl->tr_co[C_serv_count].pack_counter++; //total session counters
			
			curr_cl->tr_co[C_serv_count].byte_counter+=pkt_len1; //total session counters

			if (is_output)
				{
				G_log_int_counters_out[phy_int_i][4097].pack++;
				G_log_int_counters_out[phy_int_i][4097].byte+=pkt_len1;
			}
				
		}



		//---------------CHECK NEXT_HOP ACTION----------------->>
		uint8_t nh_ind= ((uint8_t*)(result+i_F_C))[1];// 0x0000FF00
		
		if (unlikely(nh_ind>0))
			{

			int16_t pk_diff=0;

			nh_ind=nh_ind-1;
			uint32_t nh_ip=G_f_par_arr[L_config_bit].nh_arr[nh_ind];
			uint32_t arp_index=G_for_table_arr[L_config_bit]->get_arp_index_for_ip(nh_ip,0);						 

			if (unlikely(arp_index==0xFFFFFFFF)) 
				{
				result[i_F_C]=0xFF; // if no next-hop delete packet
				continue;
			}
			ARPe* curr_arp=&(G_arp_table_arr[L_config_bit].arp_arr[arp_index]);

			if (unlikely(curr_arp->is_ours==1)) //FROM CLIENT NO TRAFFIC TO RE
				{
				result[i_F_C]=0xFF;
			}	 

			*((uint32_t*)(ci))=1+(((curr_arp->log_int_index)&(0xFF))<<8)+(arp_index<<16);//set next-hop logical interface

			ci[9]=curr_arp->physical_int_index;

			uint16_t len_L2header_in=(*((uint16_t*)(m+16))==0x0081)?18:14;
			
			if  (unlikely(curr_arp->is_ok==0)) // arp is requested and don't valid
				{
				if (curr_arp->state==0)//arp was not initiated
					{
					ci[0]=5; // Means MAC wanted RE should initiate ARP

					pk_diff=0-len_L2header_in;//?????

					ci[9]=0xFE; //send to RE
					//should change dst_ip of packet for ARP RE process
					*(uint32_t*)(m+len_L2header_in+16)=rte_bswap32(nh_ip);

					goto 	PACKET_DIFF_ACTION;
				}
				else
					{
					result[i_F_C]=0xFF;
					continue;
				}
			}


			
			/////// /////////////////////// SET VLAN IF IT SOULD BE -->
			if (curr_arp->vlan==0x10)
				{
				pk_diff=14-len_L2header_in;
			}
			else 
				{
				pk_diff=18-len_L2header_in;
				m[12-pk_diff]=0x81; m[13-pk_diff]=0x00;
				*((uint16_t*)(m+14-pk_diff))=curr_arp->vlan;//vlan in written in arp in wrong order
			}

		PACKET_DIFF_ACTION:

			rte_memcpy(m-pk_diff,m,12);
			m[9-pk_diff]=curr_arp->physical_int_index;

			if (pk_diff!=0)
				{
				if (pk_diff>0)	rte_pktmbuf_prepend(pkt[i],pk_diff);
				else		rte_pktmbuf_adj (pkt[i],-pk_diff);
			}

/////////////////////////////// SET VLAN IF IT SHOULD BE --|


		}

		//---------------CHECK NEXT_HOP ACTION-----------------||					
		
	}

return 0;
}