#include "../routing/interfaces.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"

#include  <rte_memcpy.h> // for memcpy

#include "../clients/clients.h"

#include <rte_cycles.h>//for unlikely()

#include <rte_acl.h>

#include <rte_prefetch.h>

#include <rte_byteorder.h>

#include <rte_memcpy.h>
#include <rte_mbuf.h>


extern struct LogInt* G_log_ints;
extern struct PhyInt * G_phy_ints;
extern uint8_t Phy_Int_Count;
extern ForTabTre* G_for_table;


//////---------HASH TABLES------>


extern rte_hash *cl_ip_ht;


 /// --- DATA TABLES --->


extern CL_IP_DAEL	*cl_ip_dt;

extern CL_FORW_DATA cl_forw_dt[];

/// --- DATA TABLES ---|
 
////////-------------------------filter

extern rte_acl_ctx*  acl_arr;

//function return number of port 0xFF means drop packet
extern "C" uint8_t Input_Core_Receive_packet (uint8_t *m,uint8_t *ci,uint8_t phy_int_i,int16_t* pk_diff)


//rte_mbuf* p_mbuf,uint8_t phy_int_i,int16_t* pk_diff,uint8_t* filter_num_add) // port that will call that function
{//ci -control information 0 byte- interface number; 1 byte - type of packet ; 2+3 - log int number
//pk_diff - L2headet_length_output-L2headet_length_input;
//filter_num_add- address with filter number cell. That we need to write filter number
//determine logical port


uint32_t log_int_i; 

uint8_t* L2_payload;

uint16_t vlan;

int32_t	hash_d;
uint32_t IP_dst;
uint32_t arp_index;

ARPe* curr_arp;




uint16_t curr_ethertype;

if ((*(uint16_t*)(m+12)==0x0081)) // 0x8100
	{  
	if  ((*((uint16_t*)(m+16))==0x0081)) // 0x8100//another vlan tag from user packet
		{
	//some code for processing users packet

///////////////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM CLIENT INTERFACE -->


		L2_payload=m+22;//two vlan tags
	
		//ethertype in wrong order
		
	
		curr_ethertype=*(((uint16_t*)(L2_payload)-1));//for readabilyte of code

		
		// if arp packet send to BRAS
		if (unlikely(curr_ethertype==0x0608)) // this is ARP 
			{
			if (cl_ARP_packet_processing(m,L2_payload,phy_int_i)==1)
				{
				//delete packet;
				return 0xFF;
			}
	
			return phy_int_i; // send packet back
		}

		

		if (unlikely(curr_ethertype!=0x0008)) // this is no ip packet
			{
			//delete packet
			return 0xFF;
		}
		// ip packet
		
		//if dhcp packet
			

		////////////////////////////////////////////DHCP filter--->
		if (unlikely(L2_payload[9]==17)) // udp port
			{
			if ((*(uint32_t*)(L2_payload+20))==0x43004400) 
				{
				//this is dhcp packet

				// check if BOOTREQUEST
				if (L2_payload[20+8]==1) //not sure!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					{//send packet to RE
						ci[0]=3;//3- DHCP packet from client
						ci[1]=phy_int_i;
					//send packet with L2 headers packet_diff==0
					return 0xFE; //send to RE
				}
				else
					{
					return 0xFF;
				}
			}

		}

		////////////////////////////////////////////DHCP filter---|
		// this is ussual IP packet

		//determine external and internal vlan tag.

		uint16_t vlan_ex ,vlan_in; //vlan external & vlan internal
	



		//vlans in wrong order
		vlan_ex=(*(uint16_t*)(m+14))&0xFF0F;//((m[14]<<8)+m[15])&0x0FFF;//	htons(*(uint16_t*)(m+14))&0x0FFF;	
		vlan_in=(*(uint16_t*)(m+18))&0xFF0F;//htons(*(uint16_t*)(m+18))&0x0FFF;




		
		IP_dst=rte_bswap32(*(uint32_t*)(L2_payload+16));

		uint32_t src_ip=rte_bswap32(*((uint32_t*)(L2_payload+12)));
		
		arp_index=G_for_table->get_arp_index_for_ip(IP_dst);





		curr_arp=&G_arp_table.arp_arr[arp_index];

		uint8_t cl_port;
		uint32_t cl_num; 
		
		int32_t hash;
			

		hash_d=rte_hash_lookup(cl_ip_ht, &IP_dst);

		hash=rte_hash_lookup(cl_ip_ht, &src_ip);

	

	
					


		
		if (unlikely(hash<0))
			// not found;
			{

////for test for lurge count of clietns
			
			uint32_t cl_ip_add=src_ip;
			int32_t ip_hash=rte_hash_add_key(cl_ip_ht,&cl_ip_add);
			cl_ip_dt[ip_hash].phy_int=phy_int_i;
	
			
			//--------------generate data for input core
		int32_t cl_forw_index_input=cl_forw_dt[0+2*phy_int_i].new_cl();
		cl_ip_dt[ip_hash].cl_index_in=cl_forw_index_input;

		//--------------generate data for output core
		int32_t cl_forw_index_output=cl_forw_dt[1+2*phy_int_i].new_cl();
		cl_ip_dt[ip_hash].cl_index_out=cl_forw_index_output;
					

		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].state=1;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].state=1;

		for (int i=0;i<6;i++)
			{
			cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].mac[i]=m[i];
			cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].mac[i]=m[i];
		}	
		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].vlan_in=vlan_in;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].vlan_in=vlan_in;

		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].vlan_out=vlan_ex;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].vlan_out=vlan_ex;




			return 0xFF;

		}
		

		cl_port=cl_ip_dt[hash].phy_int;
		cl_num=cl_ip_dt[hash].cl_index_in;//src_ip&0xFFFFFF;
	
		if (unlikely(cl_port!=phy_int_i)) // that client not from our port
			{
			return 0xFF; // packet should be deleted
		}
		//process of input packet of current client

/*

		CL_INT_DAEL* curr_cl=cl_forw_dt[2*phy_int_i+1].cl_arr+cl_num;

		//check mac vlan tags 

			
		if (unlikely((vlan_in!=curr_cl->vlan_in)||(vlan_ex!=curr_cl->vlan_out)))
			{
			return 0xFF;
		}
	*/
		//filter_num_add=0;//for clients

		// i mean no need to check mac address

		/// Input_filter
		uint32_t result=0;

		*((uint32_t*)(ci+4))=2+(((cl_num)&(0xFFFFFF))<<8); // type of source interface[client] or logical


///////////////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM CLIENT INTERFACE ---|
	
	goto ROUTING_TABLE;	
	}

	else 
		{
	

		vlan=((m[14]<<8)+m[15])&0x0FFF;

	
	
		L2_payload=m+18;
	}
}
else 
	{ // this is ussual ethernet without vlan tag

	vlan=4096;
	////log_int_int is determened
	L2_payload=m+14;
}


///////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM LOGICAL INTERFACE -->


log_int_i=G_phy_ints[phy_int_i].Vlan_log_int_table[vlan];//vlan

if  (unlikely(log_int_i==0xFFFF)) // no active vlan subinterface 
	{
	return 0xFF;
}


//in wrong order
curr_ethertype=*((uint16_t*)(L2_payload-2));




//*filter_num_add=1; //for interfaces 


IP_dst=rte_bswap32(*(uint32_t*)(L2_payload+16));

arp_index=G_for_table->get_arp_index_for_ip(IP_dst);

curr_arp=&G_arp_table.arp_arr[arp_index];



hash_d=rte_hash_lookup(cl_ip_ht, &IP_dst);


*((uint32_t*)(ci+4))=1+(((log_int_i)&(0xFFFFFF))<<8); /// 1 - source interface type - log interface 


// that information needed for ARP and DHCP protocols. If packet ussual that information will be overrided to destination log_int

///////////////////////////////ARP filter-->

if (unlikely(curr_ethertype==0x0608)) // this is ARP 
	{
	ci[0]=4;// 4 ARP packet
	//use all packet packetdiff=0
	return 0xFE; //send to RE
}

///////////////////////////////ARP filter---|





/////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM LOGICAL INTERFACE ---|

//////////////////////////////////////////////////////////////////////////////// 
ROUTING_TABLE:


uint16_t len_L2header_in=L2_payload-m;// length of L2 header of input packet



// Get IP and find next-interface





uint16_t len_L2header_out;

///////////////////////////////////////////////////////////////////////////////// ip_32_table tables of clients networks /32 

uint8_t cl_num;

uint16_t cl_port;




	if (!(hash_d<0)) // find out client interface for that dst_address and client number for that destination address 
			//1 not found; 0 - address is founded
	{
	cl_port=cl_ip_dt[hash_d].phy_int;
	cl_num=cl_ip_dt[hash_d].cl_index_out;

	*((uint32_t*)(ci))=2+(((cl_num)&(0xFFFFFF))<<8);// set destination client number

	len_L2header_out=22;// two vlans tag
	*pk_diff=len_L2header_out-len_L2header_in;

	ci[8]=len_L2header_out;//offset of L3 header;

	ci[9]=cl_port;
	
	return cl_port; //send to apropriate core, that process output physical interface 

}


///////////////////////////////////////////////////////////////////////////////// usual IP TABLE with not /32 prexises 

else 
	{
	*((uint32_t*)(ci))=1+(((curr_arp->log_int_index)&(0xFFFFFF))<<8);//set next-hop logical interface


if (unlikely(arp_index==0xFFFFFFFF))
	{
	return 0xFF; // if no next-hop delete packet
}



if (unlikely(curr_arp->is_ours==1)) // packet is designet to RE
	{ 
	ci[0]=6;// packet to interface of our device
	return 0xFE; //send to RE
}

uint8_t* data;

	
////////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP-->

if  (unlikely(curr_arp->is_ok==0)) // arp is requested and don't valid
	{
		ci[0]=5; // Means MAC wanted RE should initiate ARP
		*pk_diff=0-len_L2header_in;

		//*((uint32_t*)(m))=arp_index; // set arp index at the end of the packet

		return 0xFE; //send to RE

	}


////////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP--|


/////// /////////////////////// SET VLAN IF IT SOULD BE -->
if (curr_arp->vlan==0xFFFF)
	{
	data=L2_payload-14;
}
else 
	{
	data=L2_payload-18;
	data[12]=0x81; data[13]=0x00;
	*((uint16_t*)(data+14))=curr_arp->vlan;//vlan in written in arp in wrong order
}


	len_L2header_out=L2_payload-data;


/////////////////////////////// SET VLAN IF IT SHOULD BE --|

/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS-->
 



rte_memcpy(data,curr_arp->mac_addr,6);
rte_memcpy(data+6,G_phy_ints[curr_arp->physical_int_index].mac_addr,6);


/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS--|

*pk_diff=len_L2header_out-len_L2header_in;


ci[8]=len_L2header_out;//offset of L3 header;

ci[9]=curr_arp->physical_int_index;

return curr_arp->physical_int_index;
}
}





