#include "../routing/interfaces.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"

#include  <rte_memcpy.h> // for memcpy

#include "../clients/clients.h"

#include <rte_cycles.h>//for unlikely()

#include <rte_acl.h>

#include <rte_prefetch.h>

#include <rte_byteorder.h>

#include <rte_memcpy.h>
#include <rte_mbuf.h>


extern uint8_t Phy_Int_Count;



//////---------HASH TABLES------>


extern rte_hash *cl_ip_ht;


 /// --- DATA TABLES --->


extern CL_IP_DAEL	*cl_ip_dt;

//extern CL_FORW_DATA cl_forw_dt[];

/// --- DATA TABLES ---|
 
////////-------------------------filter

extern rte_acl_ctx*  acl_arr;



//function return number of port 0xFF means drop packet
extern "C" uint8_t pack_pre_processing (uint8_t *m,uint8_t *ci,uint8_t phy_int_i,uint16_t pk_len,
					uint8_t* L2_header_len,
					uint32_t* src_ip_buf,int16_t*  pack_ind_buf,
					uint32_t* dst_ip_buf,

					uint16_t pack_ind,
					uint16_t* src_ind, // in burst of packets
					uint8_t L_config_bit)


//rte_mbuf* p_mbuf,uint8_t phy_int_i,int16_t* pk_diff,uint8_t* filter_num_add) // port that will call that function



//ci : RE case: 0-byte - type
//ci : PFE case: 0-byte- type of int_destination (1-log_int),2-client);(1-3)byte -logical interface index /or client number

//0-3 byte destination data/// 4-7 byte source data ///
//8   byte 0ffset of L3 header /// 9 byte destination interface  /// 10-byte config bit

{


uint32_t log_int_i; 

uint8_t* L2_payload;

uint16_t vlan;


uint16_t curr_ethertype;
uint8_t this_is_client=1;
uint16_t vlan_ex ,vlan_in; //vlan external & vlan internal
//clients with one vlan is client without internal vlan.


ci[9]=phy_int_i;




G_log_int_counters_in[phy_int_i][4097].pack++;
G_log_int_counters_in[phy_int_i][4097].byte+=pk_len;


pack_ind_buf[pack_ind]=-1;



if ((*(uint16_t*)(m+12)==0x0081)) // 0x8100
	{  

	vlan_ex=(*(uint16_t*)(m+14))&0xFF0F;

	if  (*((uint16_t*)(m+16))==0x0081)// 0x8100//another vlan tag from user packet
		{
	//some code for processing users packet

///////////////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM CLIENT INTERFACE -->

		if (int_types[phy_int_i]==0) return 0xFF; // that is not client interface 

		L2_payload=m+22;//two vlan tags
	
		//ethertype in wrong order
		
	
		curr_ethertype=*(((uint16_t*)(L2_payload)-1));//for readabilyte of code

		
		
		// ip packet
		
		//if dhcp packet
			

	
		// this is ussual IP packet

		//determine external and internal vlan tag.

		
	

		//vlans in wrong order
		//vlan_ex=(*(uint16_t*)(m+14))&0xFF0F;//((m[14]<<8)+m[15])&0x0FFF;//	htons(*(uint16_t*)(m+14))&0x0FFF;	
		vlan_in=(*(uint16_t*)(m+18))&0xFF0F;//htons(*(uint16_t*)(m+18))&0x0FFF;


		
		this_is_client=1;
	
	goto DHCP_CHECK;	

	}

	else 
		{


		vlan=rte_bswap16(vlan_ex);
		
		vlan_in=0x10;
	
	
		L2_payload=m+18;
	}
}
else 
	{ // this is ussual ethernet without vlan tag

	vlan=4096;
	////log_int_int is determened
	L2_payload=m+14;
	
	this_is_client=0;
}


///////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM LOGICAL INTERFACE -->



log_int_i=G_phy_ints_arr[L_config_bit][phy_int_i].Vlan_log_int_table[vlan];//vlan

curr_ethertype=*((uint16_t*)(L2_payload-2));

if  (log_int_i==0xFFFF) // no active vlan subinterface 
	{
	this_is_client=1;
	goto DHCP_CHECK; //!!!!!!!!!!!!!!!!!!!#####################!
	return 0xFF;
}
else this_is_client=0;


//in wrong order





//*filter_num_add=1; //for interfaces 


*((uint32_t*)(ci+4))=1+(((log_int_i)&(0xFFFFFF))<<8); /// 1 - source interface type - log interface 

//count incoming packet

//-for test//


// that information needed for ARP and DHCP protocols. If packet ussual that information will be overrided to destination log_int

G_log_int_counters_in[phy_int_i][vlan].pack++;
G_log_int_counters_in[phy_int_i][vlan].byte+=pk_len;

///////////////////////////////ARP filter-->

if (unlikely(curr_ethertype==0x0608)) // this is ARP 
	{
	ci[0]=4;// 4 ARP packet
	//use all packet packetdiff=0
	return 0xFE; //send to RE
}

///////////////////////////////ARP filter---|

if (unlikely(curr_ethertype!=0x0008)) // this is no ip packet
	{
	//delete packet
	return 0xFF;
}

goto ROUTING_TABLE;

/////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM LOGICAL INTERFACE ---||


//------------------------------------------------------------------------------------DHCP CHECK-------------------->>
DHCP_CHECK:


		//ci[8]=L2_payload-m;

		// if arp packet send to BRAS !!!! arp policy!!!
		if (unlikely(curr_ethertype==0x0608)) // this is ARP 
			{
			ci[0]=7;//ARP from client
			ci[8]=L2_payload-m;

			return 0xFE; //sen to RE;
		}

		

		if (unlikely(curr_ethertype!=0x0008)) // this is no ip packet
			{
			//delete packet
			return 0xFF;
		}


	if (unlikely(L2_payload[9]==17)) // udp port
			{
			if ((*(uint32_t*)(L2_payload+20))==0x43004400) 
				{
				//this is dhcp packet

				// check if BOOTREQUEST
				if (L2_payload[20+8]==1) //not sure!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					{
					//if //check allowed combination of vlan-s

					//send packet to RE
						ci[0]=3;//5- DHCP packet from client

						ci[8]=L2_payload-m;
					//send packet with L2 headers packet_diff==0

					//if ((rte_rdtsc()&0x1)==1) return 0xFF;//for random client access//@@@@
					return 0xFE; //send to RE/
				}
				else
					{
					return 0xFF;
				}
			}

		
	}
//------------------------------------------------------------------------------------DHCP CHECK--------------------||

	*((uint32_t*)(ci+4))=2; /// 2 - source interface type - may be client


	//MAGIC OCCURES next string is not executed in proper sequence !!!!!!!!!!!!!!!!!#####@!!!!!!!!!!!!!
	// i think so because 	at last stage i don't see wright vlan tags at proper place in very very very few
	//cases 


	*((uint16_t*)(ci+0))=vlan_ex; // 4 first bytes of ci is not userd yet. because destination is not determened
	*((uint16_t*)(ci+2))=vlan_in; 

	//needed to check


	src_ip_buf[*src_ind]=rte_bswap32(*((uint32_t*)(L2_payload+12))); //--------write client ip address for check
	pack_ind_buf[pack_ind]=(*src_ind)++;
	goto ROUTING_TABLE;

//////////////////////////////////////////////////////////////////////////////// 
ROUTING_TABLE:

// Get IP and find next-interface
dst_ip_buf[pack_ind]=rte_bswap32(*(uint32_t*)(L2_payload+16));

L2_header_len[pack_ind]=L2_payload-m;


return 0xFC;//that means ok


}





