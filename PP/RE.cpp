#include "../routing/interfaces.h"

#include <arpa/inet.h> // for change bit order


#include "../clients/dhcp.h"

#include "../radius/rad_client.h"

#include "../radius/rad_coa.h"

#include "../routing/forwarding_table.h" // forwarding table

//m - adress od packet // return number of interface
extern "C" uint8_t RE_Receive_packet(uint8_t *m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff);


extern ForTabTre* G_for_table;

void swap_L2_L3_addresses(uint8_t *L3_h,uint8_t *L2_h);//defined at that file


uint8_t RE_Receive_packet(uint8_t *m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff)
{
	uint8_t phy_int_i; //Physical interfase index


	



	uint8_t type=ci[0];
// 1- logical interface ; 2 - client ; 3-DHCP from client;4 - ARP packet;5- ARP wanted ; 6-packet to interface;
// (4)ARP packet should (5)ARP wanted and be log_int before 2 (byte); (6) should be log_int before (2 byte)

	uint16_t log_int_i=ci[5];//for 4,5,6 information should be wright

	uint8_t * L2_payload;

	uint16_t vlan;

	if ((type==4)||(type==6))//this is packet from ussual logical interface
		{ 

	
		
		if (G_log_ints[log_int_i].vlan==4096) // means no vlan
			{
			L2_payload=m+14;
		}
		else
			{
			L2_payload=m+18;
		}
	}



//////////////////////////////////////////DIFFERENT TYPES OF PACKETS
//-------------------------------------

	if (type==3) // this is DHCP from client 
		{
		uint8_t phy_int_out;
		phy_int_out=DHCP_packet_from_client(m,ci,pk_len,pk_diff);	///should write this function
		return phy_int_out;
	}

/////---------------------	 

	if (type==4) // this is ARP 
		{
		ARP_packet_processing(m,L2_payload,log_int_i);	//don't work during changing of config
		return 0x80+ci[9];//input_interface+128; copy to linux core
	}
//----------------------------------------------------------------------------------------
	if (type==5) // wanted mac 
		{ //don't work during changing of config 
		  //arp_index may be different on RE and PFE
			//in that case packet starts with L3!!! payload
		uint32_t IP_dst=htonl(*(uint32_t*)(m+16));

	
		uint32_t arp_index=G_for_table->get_arp_index_for_ip(IP_dst,0);

		if (arp_index==0xFFFFFFFF) return 0xff; //delete packet

		
		ARPe* curr_arp=&(G_arp_table->arp_arr[arp_index]);

		if ((curr_arp->state==0)&&(curr_arp->is_ok==0)) // arp request is not initiated and  // arp is requested and don't valid
			{
			curr_arp->get_arp();
		}
		return 0xFF;
	}
//-----------------------------------------------------------------------------------------

	if (type==7) // that is arp from client
		{
		phy_int_i=ci[9];
		if (cl_ARP_packet_processing(m,m+ci[8],phy_int_i)==0) //that we shoul send 
		{//change packet as replay

		//ci[9]- phy interface estination or source of arp request;

		//pk_diff++; m[-1]=0; //becouse we dont generate that packet from zero. Shoul set 0 byte before packet.
		//all needed action will be done at chassis RE core part.
		return phy_int_i;
		}
		return 0xFF;//no answer; should delete packet
	}

	if (type==6) // this is stripped IP packet to our device 
		{
			

		//determine protocol
		//case dhcp

		if (L2_payload[9]==17) // udp protocol
			{
			uint16_t s_port=htons(*(uint16_t*)(L2_payload+20));
			uint16_t d_port=htons(*(uint16_t*)(L2_payload+20+2));


			///--------DHCP-------------------------------------------->>>>>>
			if ((s_port==0x43)&&(d_port=0x43)) //ok
				{
				//this is dhcp packet

				// check if BOOTRESPOND
				if (L2_payload[20+8]==2) //ok
					{
					uint8_t phy_int_out;

					*pk_diff+=m-L2_payload;

					phy_int_out=DHCP_packet_from_server(L2_payload,ci,pk_len,pk_diff);


					return phy_int_out; //send to appropriate output port
					// in case dhcp we should create valid l2 header from DHCP function
					// becouse table ip_32_table may not knows abount this client ( it not created)
				}
		
					{
					return 0xFF;
				}
			}
			///--------DHCP--------------------------------------------|||||||

			//-------------------RADIUS-------------->>

			if ((s_port==G_rad_auth.conf.serv_port)&&(d_port==G_rad_auth.conf.client_port))
				{

				G_rad_auth.read_packet(L2_payload,pk_len);
				return 0xFF;

			}
			if ((s_port==G_rad_acc.conf.serv_port)&&(d_port==G_rad_acc.conf.client_port))
				{

				G_rad_acc.read_packet(L2_payload,pk_len);
				return 0xFF;
			}

			if (d_port==G_rad_coa.serv_port)
				{
				if (G_rad_coa.process_packet(L2_payload,pk_len)!=0) return 0xFF; //delete
				swap_L2_L3_addresses(L2_payload,m);//should change!!!!!! if dest mac will be changed
				return ci[1];//return back
			}

			//-------------------RADIUS--------------||

		

		}
			//should change!!!!!!
			//---------------------------------------ICHP_ECHO_REQUEST----------->
			if ((L2_payload[9]==1)&&(L2_payload[20]==8)) // icmp //request
				{
				L2_payload[20]=0;
				
				uint32_t checksum=*((uint16_t*)(L2_payload+20+2));
				checksum+=8;
				*((uint16_t*)(L2_payload+20+2))=checksum+(checksum>>16);//change icmp checksum;
			
				swap_L2_L3_addresses(L2_payload,m);
				return ci[1];
			}
			//---------------------------------------ICHP_ECHO_REQUEST-----------||		



		return 0x80+ci[9];//send to linux Core
	}


	return 0xFF;
}

void swap_L2_L3_addresses(uint8_t *L3_h,uint8_t *L2_h)
	{

	//change source and destination ip
				uint8_t src_ip[4];
				uint8_t dst_ip[4];
			
				memcpy(src_ip,L3_h+12,4);
				memcpy(dst_ip,L3_h+16,4);

				memcpy(L3_h+16,src_ip,4);
				memcpy(L3_h+12,dst_ip,4);
		
	//change src and destination mac

				uint8_t src_mac[6];
				uint8_t dst_mac[6];
			
				memcpy(src_mac,L2_h+6,6);
				memcpy(dst_mac,L2_h,6);

				memcpy(L2_h,src_mac,6);
				memcpy(L2_h+6,dst_mac,6);
return;

}



