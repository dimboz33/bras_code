#include "../routing/interfaces.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"

#include  <rte_memcpy.h> // for memcpy

#include "../clients/clients.h"

#include <rte_cycles.h>//for unlikely()

#include <rte_acl.h>

#include <rte_prefetch.h>

#include <rte_byteorder.h>

#include <rte_memcpy.h>
#include <rte_mbuf.h>


extern struct LogInt* G_log_ints;
extern struct PhyInt * G_phy_ints;
extern uint8_t Phy_Int_Count;
extern ForTabTre* G_for_table;


//////---------HASH TABLES------>

extern rte_hash *cl_vlan_ht;
extern rte_hash *cl_ip_ht;
extern rte_hash *cl_dhcp_mac_ht;

 /// --- DATA TABLES --->


extern CL_IP_DAEL	*cl_ip_dt;

//extern CL_FORW_DATA cl_forw_dt[];

/// --- DATA TABLES ---|

extern uint32_t  prefetch_numb;
 
////////-------------------------filter

extern rte_acl_ctx*  acl_arr;

//function return number of port 0xFF means drop packet
extern "C" void routing_table (

	const void **src_ip_pointers,
	const void **dst_ip_pointers,

	uint16_t pack_num, uint16_t src_ind, 

	int32_t *src_cl_buf,
	int32_t *dst_cl_buf,
	uint32_t *arp_ind_buf,
	uint8_t L_config_bit
	)

{


//ci : RE case: 0-byte - type
//ci : PFE case: 0-byte- type of int_destination (1-log_int),2-client);(1-3)byte -logical interface index /or client number

//0-3 byte for input direction /// 4-7 byte for output direction ///
//8   byte 0ffset of L3 header /// 9 byte destination interface  /// 10-byte config bit

	//pack_num=32;
	
	//src_ind=32;
	
	
	if (pack_num>0 )
		{
		int tr_c=0;//transmit count
		for (;tr_c+64<pack_num;tr_c+=64)
			{
			rte_hash_lookup_bulk(cl_ip_ht,dst_ip_pointers+tr_c,64,dst_cl_buf+tr_c); 
		}
			rte_hash_lookup_bulk(cl_ip_ht,dst_ip_pointers+tr_c,pack_num-tr_c,dst_cl_buf+tr_c); 

	}
	if (src_ind>0) 
		{
		int tr_c=0;//transmit count
		for (;tr_c+64<src_ind;tr_c+=64)
			{
			rte_hash_lookup_bulk(cl_ip_ht,src_ip_pointers+tr_c,64,src_cl_buf+tr_c); //pack_num//src_ind
		}
			rte_hash_lookup_bulk(cl_ip_ht,src_ip_pointers+tr_c,src_ind-tr_c,src_cl_buf+tr_c); 


	}

	//rte_hash_lookup_bulk(cl_ip_ht,dst_ip_pointers,pack_num,dst_cl_buf); 

	for (int i=0;(i<src_ind)&(i<prefetch_numb);i++)//&(i<prefetch_numb)
		{

		if (!(src_cl_buf[i]<0)) rte_prefetch0(cl_ip_dt+src_cl_buf[i]);

	}


	for (int i=0;i<pack_num;i++)
		{

		if (dst_cl_buf[i]<0)
			{
			arp_ind_buf[i]=G_for_table_arr[L_config_bit]->get_arp_index_for_ip(*((uint32_t*)(dst_ip_pointers[i])),arp_ind_buf[i]);						 
			
			
		}
		else
			{
			if (i<prefetch_numb) rte_prefetch0(cl_ip_dt+dst_cl_buf[i]);
			
		}
	}



return;

}





