#include "../routing/interfaces.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"

#include  <rte_memcpy.h> // for memcpy

#include "../clients/clients.h"

#include <rte_cycles.h>//for unlikely()

#include <rte_acl.h>

#include <rte_prefetch.h>

#include <rte_byteorder.h>

#include <rte_memcpy.h>
#include <rte_mbuf.h>


extern uint8_t Phy_Int_Count;

//////---------HASH TABLES------>


extern rte_hash *cl_ip_ht;


/// --- DATA TABLES --->

extern CL_IP_DAEL	*cl_ip_dt;

//extern CL_FORW_DATA cl_forw_dt[];

/// --- DATA TABLES ---|
 
////////-------------------------filter

extern rte_acl_ctx*  acl_arr;

extern uint32_t  prefetch_numb;

extern int32_t ADD_CLIENT_BY_PACKET;


void swap_L2_L3_addresses(uint8_t *L3_h,uint8_t *L2_h);

//function return number of port 0xFF means drop packet
extern "C" uint8_t pack_post_processing(uint8_t *m,uint8_t *ci,uint8_t phy_int_i,int16_t* pk_diff,
					uint8_t* L2_header_len,
					uint32_t* src_ip_buf,
					int32_t* src_cl_buf,int16_t*  pack_ind_buf,
					int32_t* dst_cl_buf,
					int32_t* arp_ind_buf,
					uint16_t pack_ind,
					uint8_t L_config_bit)





//rte_mbuf* p_mbuf,uint8_t phy_int_i,int16_t* pk_diff,uint8_t* filter_num_add) // port that will call that function


//ci : RE case: 0-byte - type
//ci : PFE case: 0-byte- type of int_destination (1-log_int),2-client);(1-3)byte -logical interface index /or client number

//0-3 byte for input direction /// 4-7 byte for output direction ///
//8   byte 0ffset of L3 header /// 9 byte destination interface  /// 10-byte config bit

{


uint32_t log_int_i; 

uint8_t L2h_len=L2_header_len[pack_ind];
uint8_t* L2_payload=m+L2h_len;

uint16_t vlan;

int32_t	hash;
uint16_t vlan_ex;
uint16_t vlan_in;




uint16_t curr_ethertype;
uint8_t pref_n=prefetch_numb; //prefetch number


if (pack_ind+pref_n<64)//!!!!!!
	{
	if (pack_ind_buf[pack_ind+pref_n]>=0) 	rte_prefetch2(cl_ip_dt+src_cl_buf[pack_ind_buf[pack_ind+pref_n]]);
	if (dst_cl_buf[pack_ind+pref_n]>=0) 	rte_prefetch2(cl_ip_dt+dst_cl_buf[pack_ind+pref_n]);
}


hash=src_cl_buf[pack_ind_buf[pack_ind]];
if (ci[4]==2) //not logical interface may be client
			//if (L2_header_len[pack_ind]==22)///from client
		{
		hash=src_cl_buf[pack_ind_buf[pack_ind]];

		uint32_t vlan_comb=*((uint32_t*)(ci+5));

		vlan_ex=*(uint16_t*)(ci+0);//(*(uint16_t*)(m+14))&0xFF0F;//
		vlan_in=*(uint16_t*)(ci+2);
		//if (L2h_len==22 ) 	vlan_in=(*(uint16_t*)(m+18))&0xFF0F;
		//else			vlan_in=0x10;
		
		if (unlikely(hash<0))	// not found;
			{

		if (ADD_CLIENT_BY_PACKET==0) return 0xFF;

		//for speed up testing process
		
		
		uint32_t src_ip_cl=htonl(*(uint32_t*)(m+L2h_len+12));
		int ip_hash=rte_hash_add_key(cl_ip_ht,&src_ip_cl);

		CL_IP_SESS *cl_sess=cl_ip_sess_dt+ip_hash;
		if (cl_sess->state==0) cl_sess->state=1;
		else return 0xFF;
	
		cl_ip_dt[ip_hash].vlan_in=vlan_in;
		cl_ip_dt[ip_hash].vlan_out=vlan_ex;

		cl_ip_dt[ip_hash].phy_int= phy_int_i;
		//--------------generate data for input core
		int32_t cl_forw_index_input=cl_forw_dt[0+2*phy_int_i].new_cl();
		cl_ip_dt[ip_hash].cl_index_in=cl_forw_index_input;

		//--------------generate data for output core
		int32_t cl_forw_index_output=cl_forw_dt[1+2*phy_int_i].new_cl();
		cl_ip_dt[ip_hash].cl_index_out=cl_forw_index_output;


		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].state=1;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].state=1;

		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].clear();
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].clear();

		//cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].service_bits=1;
		//cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].service_bits=1;

		for (int i=0;i<6;i++)
			{
			cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].mac[i]=m[i+6];
			cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].mac[i]=m[i+6];
		}	

		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].vlan_in=vlan_in;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].vlan_in=vlan_in;

		cl_forw_dt[0+2*phy_int_i].cl_arr[cl_forw_index_input].vlan_out=vlan_ex;
		cl_forw_dt[1+2*phy_int_i].cl_arr[cl_forw_index_output].vlan_out=vlan_ex;

		cl_sess->ip=src_ip_cl;
		cl_sess->state=1;

		add_service_and_policer_to_user("A","","",ip_hash);
	
		return 0xFF;

		}

		
		if (unlikely((vlan_in!=cl_ip_dt[hash].vlan_in)||
			     (vlan_ex!=cl_ip_dt[hash].vlan_out)))
			{
			return 0xFF;
		}
		uint8_t cl_port=cl_ip_dt[hash].phy_int;
		uint32_t cl_num=cl_ip_dt[hash].cl_index_in;
		
		if (unlikely(cl_port!=phy_int_i)) // that client not from our port
			{
			return 0xFF; // packet should be deleted
		}

		CL_INT_DAEL* curr_cl=cl_forw_dt[2*phy_int_i+0].cl_arr+cl_num;

		//rte_prefetch2(curr_cl);
		//rte_prefetch2(curr_cl+64);
		//rte_prefetch2(curr_cl+128);

		//process of input packet of current client

		uint32_t result=0;

		uint8_t* L3= m+ci[8];

		*((uint32_t*)(ci+4))=2+(cl_num<<8); // type of source interface[client] or logical


		//---------------------------------------icmp from client to gateway---------->>>
	/*
		
		if (unlikely((L3[9]==1)&(L3[20]==8))) //icmp request
			{

			CL_IP_SESS *cl_sess=cl_ip_sess_dt+hash;


			//policer to this traffic 
			
			if (unlikely(*((uint32_t*)(L3+16))==cl_sess->def_gw_ip))
				{
				L3[20]=0;
				
				uint32_t checksum=*((uint16_t*)(L3+20+2));
				checksum+=8;
				*((uint16_t*)(L3+20+2))=checksum+(checksum>>16);//change icmp checksum;
				uint32_t ip_cl=*((uint32_t*)(L3+12));
				*((uint32_t*)(L3+12))=*((uint32_t*)(L3+16));
				*((uint32_t*)(L3+16))=ip_cl;
				cl_num=cl_ip_dt[hash].cl_index_out;
				*((uint32_t*)(ci))=2+(cl_num<<8); // set destination client number

				*pk_diff=0;

				ci[9]=phy_int_i;

				return phy_int_i;	
			}
			

		}
	*/
		//---------------------------------------icmp from client to gateway----------|||

///////////////////////////////////////////////////////////////////////////////////////// PROCESSING OF PACKET FROM CLIENT INTERFACE ---|
	
	goto ROUTING_TABLE;	
	}

//if (ci[4]!=1) return 0xFF;//that is controll traffic from previous process iteration


ROUTING_TABLE:

uint16_t len_L2header_in=L2_header_len[pack_ind];// length of L2 header of input packet



// Get IP and find next-interface



uint16_t len_L2header_out;

///////////////////////////////////////////////////////////////////////////////// ip_32_table tables of clients networks /32 

uint32_t cl_num;//MOther FUCKET ONE DAY LOST BECOUSe OF THAT was UINT8_T TYPE!!!!!

uint16_t cl_port;



	hash=dst_cl_buf[pack_ind];


	


	if (!(hash<0)) // find out client interface for that dst_address and client number for that destination address 
			//1 not found; 0 - address is founded
	{
	
	cl_port=cl_ip_dt[hash].phy_int;
	cl_num=cl_ip_dt[hash].cl_index_out;

	CL_INT_DAEL* curr_cl=cl_forw_dt[2*phy_int_i+1].cl_arr+cl_num;

	//rte_prefetch2(curr_cl);
	//rte_prefetch2(curr_cl+64);
	//rte_prefetch2(curr_cl+128);

	*((uint32_t*)(ci))=2+(cl_num<<8); // set destination client number

	len_L2header_out=(cl_ip_dt[hash].vlan_in!=0x10)?22:18;// two vlans tag if vlan_in!= 4096; other 1 tag
	
	*pk_diff=len_L2header_out-len_L2header_in;
	
	ci[8]=len_L2header_out;//offset of L3 header;

	ci[9]=cl_port;

	return cl_port; //send to apropriate core, that process output physical interface 

}


///////////////////////////////////////////////////////////////////////////////// usual IP TABLE with not /32 prexises 

else 
	{
	 	
uint32_t arp_index;

arp_index=arp_ind_buf[pack_ind];

if (unlikely(arp_index==0xFFFFFFFF))
	{

	return 0xFF; // if no next-hop delete packet
}

ARPe* curr_arp;



curr_arp=&(G_arp_table_arr[L_config_bit].arp_arr[arp_index]);

if (unlikely(curr_arp->is_ours==1)) // packet is designet to RE
	{ 
	ci[0]=6;// packet to interface of our device

	ci[1]=phy_int_i;

	if (ci[4]==2) return 0xFF;//client shouldn't send packet directly to bras RE exept control protocols

	return 0xFE; //send to RE
}



uint8_t* data;

	*((uint32_t*)(ci))=1+(arp_index<<8); //set next-hop logical interface
	//*((uint32_t*)(ci))=1+(((curr_arp->log_int_index)&(0xFF))<<8)+(arp_index<<16); //set next-hop logical interface



//////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP-->

if  (unlikely(curr_arp->is_ok==0)) // arp is requested and don't valid
	{

	if (curr_arp->state==0)//arp was not initiated
		{
		ci[0]=5; // Means MAC wanted RE should initiate ARP

		*pk_diff=0-len_L2header_in;

		return 0xFE; //send to RE
		
	}
	else
		{
		return 0xFF;
	}
}


////////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP--|
//set arp index for next hop



/////// /////////////////////// SET VLAN IF IT SOULD BE -->
if (curr_arp->vlan==0x10)
	{
	data=L2_payload-14;
}
else 
	{
	data=L2_payload-18;
	data[12]=0x81; data[13]=0x00;
	*((uint16_t*)(data+14))=curr_arp->vlan;//vlan in written in arp in wrong order
}


	len_L2header_out=L2_payload-data;


/////////////////////////////// SET VLAN IF IT SHOULD BE --|

/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS-->
 


/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS--|

*pk_diff=len_L2header_out-len_L2header_in;

ci[8]=len_L2header_out;//offset of L3 header;

ci[9]=curr_arp->physical_int_index;

ci[10]=L_config_bit;

return curr_arp->physical_int_index;
}
}





