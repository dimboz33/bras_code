
//#include <rte_mbuf.h>
#ifndef PP

#define PP


	//uint8_t Input_Core_Receive_packet (rte_mbuf* p_mbuf, uint8_t phy_int_i,int16_t* pk_diff,uint8_t* filter_num_add); // port that will call that function ci - control information
	uint8_t Input_Core_Receive_packet (uint8_t *m,int8_t *ci, uint8_t phy_int_i,uint16_t pk_len,int16_t* pk_diff); // port that will call that function ci - control information


	uint8_t Output_Core_transmit_packet (uint8_t *m,uint8_t *ci,uint8_t phy_int_i,uint16_t pk_len); 

	uint8_t RE_Receive_packet(uint8_t *m,uint8_t *ci,uint16_t pk_len,int16_t* pk_diff);
	uint8_t Forwarding_table_act(uint32_t IP_dst,uint8_t *m,uint8_t *ci,int16_t* pk_diff);
	
	void ip_check_sum( uint16_t* ip_head_addr);



#endif
