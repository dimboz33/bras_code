#include "../routing/interfaces.h"
#include <arpa/inet.h> // for change bit order
#include "../routing/forwarding_table.h" // forwarding table
#include "../arp/arp.h"

#include <rte_byteorder.h>
 
//#include "" // ip_32_table_find (uint32_t ip,uint8_t& port, uint16_t& client_number);  

#include "../clients/clients.h"

#include <rte_cycles.h> // for likely

#include  <rte_memcpy.h> // for memcpy

extern uint8_t Phy_Int_Count;



//////---------HASH TABLES------>


extern rte_hash *cl_ip_ht;


 /// --- DATA TABLES --->


extern CL_IP_DAEL	*cl_ip_dt;

//extern CL_FORW_DATA cl_forw_dt[];

//types of packet

//function return  0xFF means drop packet; 0x0 means forward


extern "C" uint8_t Output_Core_transmit_packet (uint8_t *m,uint8_t *ci,uint8_t phy_int_i,uint16_t pk_len) // port that will call that function



//ci : RE case: 0-byte - type
//ci : PFE case: 0-byte- type of int_destination (1-log_int),2-client);(1-3)byte -logical interface index /or client number

//0-3 byte for input direction /// 4-7 byte for output direction ///
//8   byte 0ffset of L3 header /// 9 byte destination interface  /// 10-byte config bit


{
	
	//only for packet to log_int
	
	 // packet to Log interface
	uint8_t L_config_bit=ci[10];
	
	//G_log_ints_arr[L_config_bit][log_int_i].counters.out_pack++;
	//G_log_ints_arr[L_config_bit][log_int_i].counters.out_byte+=pk_len;

	uint16_t arp_index=*((uint16_t*)(ci+1));
	
	ARPe* curr_arp=&(G_arp_table_arr[L_config_bit].arp_arr[arp_index]);
	
	uint16_t vlan=rte_bswap16(curr_arp->vlan);

	G_log_int_counters_out[phy_int_i][vlan].pack++;
	G_log_int_counters_out[phy_int_i][vlan].byte+=pk_len;

	G_log_int_counters_out[phy_int_i][4097].pack++;
	G_log_int_counters_out[phy_int_i][4097].byte+=pk_len;	


	rte_memcpy(m,curr_arp->mac_addr,6);
	rte_memcpy(m+6,G_phy_ints[phy_int_i].mac_addr,6);
	
	return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////


//-----------------------CHECKSUM CALCULATION OF IP HEADER------->

 void ip_check_sum( uint16_t* ip_head_addr)
{

//find checksum
uint32_t CRS;
//uint16_t* CRS1;
uint8_t i;
CRS=0;

//zero plase where will be checksum
ip_head_addr[5]=0;


for (i=0;i<10;i++)
 {	
  CRS+=ip_head_addr[i];
 } 

CRS=0xFFFF-(CRS+(CRS>>16));



ip_head_addr[5]=*((uint16_t*)&CRS);

return;
}

//-----------------------CHECKSUM CALCULATION OF IP HEADER-------|

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////




//-------------------------return number of output interface //L3 header is ready-------------->

uint8_t Forwarding_table_act(uint32_t IP_dst,uint8_t *m,uint8_t *ci,int16_t* pk_diff)
{
	//Forwarding table action 
///m- pointer that started with L3 header.
	
uint32_t arp_index;

//set destination ip address 

((uint32_t*)m)[4]=htonl(IP_dst);


arp_index=G_for_table->get_arp_index_for_ip(IP_dst,0);

if (arp_index==0xFFFFFFFF)
	{
	return 0xFF; // if no next-hop delete packet
}

ARPe* curr_arp;



curr_arp=&(G_arp_table->arp_arr[arp_index]);

if (curr_arp->is_ours==1) // packet is designet to RE
	{ // should cut off L2 header
 	return 0xFF; //it shouldn't because that function is used by RE
}

uint8_t* data;

	*((uint32_t*)(ci))=1+(((curr_arp->log_int_index)&(0xFFFFFF))<<8); //set next-hop logical interface



////////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP-->
if  (curr_arp->is_ok==0) // arp is requested and don't valid
	{
		//set ethertype

		ci[0]=5; // Means MAC wanted RE should initiate ARP

		
		return 0xFE; //send to RE

	}


////////////////////////////////////////////////////////////////////////CASE WHEN THIS IS NO MAC ADRRESS FOR THAT DESTINATION IP--|

///set source ip address as address  of outgoing interface

((uint32_t*)m)[3]=htonl(G_log_ints[curr_arp->log_int_index].net.ip);



//--------------calculate ipheader checksum-->>

ip_check_sum((uint16_t*)m);

//--------------calculate ipheader checksum--||

/////////////////////////////// SET VLAN IF IT SOULD BE -->
if (curr_arp->vlan==0x10) //reversed 4096 in hex
	{
	data=m-14;
}
else 
	{
	data=m-18;
	data[12]=0x81; data[13]=0x00;
	*((uint16_t*)(data+14))=curr_arp->vlan;//htons(curr_arp->vlan);
}

	*pk_diff+=m-data;



/////////////////////////////// SET VLAN IF IT SHOULD BE --|

/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS-->
for (int i=0;i<6;i++)
	{
	data[i]=curr_arp->mac_addr[i];
	data[i+6]=G_phy_ints[curr_arp->physical_int_index].mac_addr[i];
}

/////////////////////////////// SET DESTINATION AND SOURCE MAC ADDRESS--|

//set ethertype

m[-2]=0x08; m[-1]=0x00;


return curr_arp->physical_int_index;

}
